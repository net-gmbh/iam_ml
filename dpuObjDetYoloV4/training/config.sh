#!/bin/bash

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


# PROJECT_CONFIG:
#   directory which contains config.py model specification

# DATASET_DIR:
#   directory location of the dataset
#   /datasets is the docker mapped path for DATASET_DIR (see config.bat)

# WORK_DIR:
#   directory location for any generated data during processing
#   /work is the docker mapped path for WORK_DIR (see config.bat)

# TARGET_ARCH:
#   target iam hardware version: iam_zu2 or iam_zu5

# FINAL_FILE_NAME:
#   filename for the final compiled model file

# QUANTIZATION_NUM_ITER:
#   numer of cycles in vai_q_caffe quantize
#   value must be smaler than number of available samples in dataset 


# --- set active project ---

# 1: Wsn, 2: netLogo
index=1


if [[ $index -eq 1 ]] 
then
    PROJECT_CONFIG=projectWsn
    DATASET_DIR="/datasets/bb_Wsn"
    WORK_DIR="/work/yolov4_Wsn"
    TARGET_ARCH="iam_zu5"
    FINAL_FILE_NAME="dpu_yolov4_wsn"
	QUANTIZATION_NUM_ITER=276
else	
	PROJECT_CONFIG=projectNetLogo
    DATASET_DIR="/datasets/bb_NetLogo"
    WORK_DIR="/work/yolov4_NetLogo"
    TARGET_ARCH="iam_zu5"
    FINAL_FILE_NAME="dpu_yolov4_netLogo"
	QUANTIZATION_NUM_ITER=739
fi

# Wsn dataset: 276 samples
# netLogo dataset: 739 samples
