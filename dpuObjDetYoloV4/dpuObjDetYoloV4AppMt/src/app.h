/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */



#ifndef AppClass_H
#define AppClass_H

#include <filesystem>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include "camDevice.h"


class appClass
{
public:
    appClass();
    ~appClass();
    
    int  init(int autorunMode);     
    void close();         
    void setSensorPixFormat();	
    void calculateOutputImgSize();
    void processPendingTasks(); 	
    int  loadModel(std::string path);
    int  loadGraph(const std::string filename);
    int  readClassNames(const std::string filename, std::vector<std::string>& classNames);	
    int  readAnchorFile(const std::string filename, std::vector<float>& biases);	
    int  xmlFileBooster(const std::string filenameIn, const std::string filenameOut);
    int  readCfgFile(const std::string filename);
    int  getDpuVersion();     
    int  getImageVersion(int *mainVersion, int *subVersion);

    int  readDefaultModelFile(const std::string filename, std::string &name);

    int dpuVersion;            
    std::string trainLabel;    
    
};
#endif //AppClass_H
