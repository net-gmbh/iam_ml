#!/bin/bash

# -------------------------------------------------
#      start bash on iam in application folder
# -------------------------------------------------


# --- read iam target settings ---
source iam_settings.mk


# --- prepare ssh access ---
if [[ $strict_host_key_checking -eq 0 ]] 
then
	echo no
	ssh-copy-id -o "StrictHostKeyChecking no" -i $key $user@$ip
else
	echo yes
	ssh-copy-id -i $key $user@$ip
fi


# --- create target folders ---
ssh -i $key $user@$ip "mkdir -p $folder"
ssh -i $key $user@$ip "mkdir -p $prog_folder"


# --- open bash via ssh ---
# -t option allows to execute the cd ... code
# in order to go to application folder
ssh -t -i $key $user@$ip "cd $prog_folder ; bash --login"

