# YoloV4 Training #


## Workflow ##
---

The model training is done with the darknet framework and the trained model is then converted to a caffe model. After that Vitis-AI tools quantize the model and compile the final DPU code for model execution on the **iam ML ready** camera.   
This training workflow is based on the Xilinx Vitis-AI tutorial [Xilinx-yolov4-tutorial](https://github.com/Xilinx/Vitis-AI-Tutorials/tree/master/Design_Tutorials/07-yolov4-tutorial) .  
The following steps have to be executed for training yolov4 on a custom dataset.

### Preparation ###
Darknet can be compiled for Linux or Windows, see [AlexeyAB/darknet](https://github.com/AlexeyAB/darknet) for details. This tutorial uses a Linux based docker image for training. The training process requires GPU support via NVIDIA Cuda, so a suitable GPU has to be available. Using a windows based system, "Docker Desktop" with WSL backend and the corresponding Cuda Driver should be installed (see "Docker Desktop" and Nvidia Cuda documentation). On a Linux system the docker image can be used accordingly, assuming a Linux environment with Cuda support and docker installed.    
 
** 1. Build Docker Image for Training **  
Build docker image...  
`win_docker_build_image.bat`  
Starting from the Nvidia/cuda image, [AlexeyAB/darknet](https://github.com/AlexeyAB/darknet) is pulled and required libraries are installed. Darknet is build and python is installed (see Dockerfile).    

** 2. Download/prepare dataset **  
The dataset for this tutorial contains 274 labeled images, containing srews, washers and nuts. It can be downloaded from [WsnBoxes.zip](https://nc.net-gmbh.com/s/xa9qSFiTwim7Fsq).
The zip content can be extracted to any available location.    
Training a Yolo detector requires an image dataset with objects marked with bounding boxes.The darknet training framework expects a txt file for each image file, placed in the same directory and with the same name, but with .txt extension.   
For each object instance in the image the txt file contains a line:  
>object_class_id  x_center  y_center  width  height   
>>with object_class_id in the range 0,1,2....numClasses-1   
and  x_center, y_center, width, height in the range 0.0...1.0

>	eg: image1.txt:   
>>	1 0.716797 0.395833 0.216406 0.147222   
	0 0.687109 0.379167 0.255469 0.158333   
	1 0.420312 0.395833 0.140625 0.166667   

This labeling data format is not very useful for managing datasets. Therefore labeling images can also be done using the Pascal VOC data format (xml file) and converting the labels to darknet format afterward. Labeling images can e.g. be done with the tool [labelImg](https://github.com/tzutalin/labelImg) using Pascal VOC format. Many other tools are available for labeling images with object boxes.    
The dataset for this example contains 274 Pascal VOC labeled images, containing srews, washers and nuts.

** 3. Download pretrained weights **
The training process starts with pretrained weights. Download the weight file [yolov4.conv.137](https://github.com/AlexeyAB/darknet/releases/download/yolov4/yolov4.conv.137) and copy the file to subfolder `/yoloFiles`.   

**4. Edit configWin.bat **  
Set `DATASET_DIR_MAPPING` to dataset directory location, which will be mapped to docker images as `datasets`.
Set `WORK_DIR_MAPPING` to working directory location, which will be mapped to docker images as `work`.   

**5. Edit config.sh **  
Set `PROJECT_CONFIG` to directory location that contains the network configuration config.py.
Set `DATASET_DIR` to dataset directory location within docker container (according to `datasets` on `DATASET_DIR_MAPPING`).
Set `WORK_DIR` to working directory location within docker container (according to `work` on `WORK_DIR_MAPPING`).
Set `TARGET_ARCH` to target iam version (zu2/zu5).
Set `QUANTIZATION_NUM_ITER` to number of vai_q_caffe iterations (number of training samples or less).
Define the final DPU instruction file name `FINAL_FILE_NAME`.



### Step 1: Dataset Preprocessing ###
Execute...    
`win_s1_prepareData.bat`   
This script resizes the training images and converts the labels from Pascal VOC to darknet format.   
The training data will be copied to `WORK_DIR/data`.

### Step 2: Prepare Lists ###
Darknet requires lists with samples used for training and evaluation. The anchor generator needs a list of samples to analyse and the Vitis-AI quantization tool needs a list of image files. 
Execute...   
`win_s2_prepareLists.bat`   
The generated list files will be in `WORK_DIR/lists`.

### Step 3: Generate Anchors ###
The yolov4 model requires seed values for the bounding box prediction. These 9 anchor values for the yoloV4 model should fit the shape of the objects in the training data. Darknet provides a script, which calculates these anchor values by clustering.   
Execute...   
`win_s3_generateAnchors.bat`   
This generates the anchor file `WORK_DIR/lists/anchors9.txt`.

### Step 4: Prepare Model ###
The original yoloV4 model configuration has to be slightly modified, in order to execute the model with the DPU. See [Xilinx-yolov4-tutorial](https://github.com/Xilinx/Vitis-AI-Tutorials/tree/master/Design_Tutorials/07-yolov4-tutorial) for details. The model configuration also has to be updated with the calculated anchor values and the number of classes in the dataset.   
Execute...   
`win_s4_prepareModel.bat`   
The script generates the model configuration file `WORK_DIR/modelCfg/yolov4_dpu.cfg`.   
This file can also be adapted according to training requirements. See [wiki-CFG-Parameters](https://github.com/AlexeyAB/darknet/wiki/CFG-Parameters-in-the-%5Bnet%5D-section) for details.   
The parameter `subdivisions` can be adjusted to the GPU memory resources. subdivisions=64 will work on almost any graphics card. With more memory available subdivisions can be set to 32 or 16 for speed improvement.   
With `steps` and `scales` the learning rate during training can be optimised. `max_batches` defines the end of the training procedure.

### Step 5: Training ###
Start training with...   
`win_s5_training.bat`   
Training starts with the pretraind weights from file 
Weight files are written to `WORK_DIR/outputTraining/yolov4_dpu_last`.   
During training the log file `WORK_DIR/train.log` and the chart file `WORK_DIR/chart.png` are updated each 100 epochs and can be tracked, in order to observe the training progress. 
Training can be stopped with Cntr+C, the `yolov4_dpu.cfg` file can be edited an training can be restarted by executing...    
`win_s5a_trainingRestart.bat`   
Restarted training starts with the weights in `WORK_DIR/outputTraining/yolov4_dpu_last.weights`.

### Step 6: Darknet to Caffe Conversion ###
The following steps (6,7,8,9) are using the Vitis-AI docker image. 
1. Start the Vitis-AI docker with...   
`win_vitisAi_start.bat`   
2. Activate the conda environment for caffe...   
`conda activate vitis-ai-caffe`    
3. Execute Model Conversion...   
`s6_vai_darknet2caffe.sh`   
The resulting caffe model `yolov4_dpu.prototxt` and the weight file `yolov4_dpu.caffemodel` are written to `WORK_DIR/vaiModels`.


### Step 7: Prepare Quantization ###
The caffe model file has to be modified slightly for the Vitis-AI quantization tool. See [Xilinx-yolov4-tutorial](https://github.com/Xilinx/Vitis-AI-Tutorials/tree/master/Design_Tutorials/07-yolov4-tutorial) for details.   
In the Vitis-AI docker container, with conda environment `vitis-ai-caffe` activated, execute...   
`s7_vai_prepareQuant.sh`   
The script generates the quantized model file `WORK_DIR/vaiModels/yolov4_dpu_quant.prototxt`.   


### Step 8: Quantization ###
In the Vitis-AI docker container, with conda environment `vitis-ai-caffe` activated, execute..  
`s8_vai_quant.sh`   
The script executes the Vitis-AI tool `vai_q_caffe`, which generates a new model with quantized weights.   
The quantized model file `deploy.prototxt` and the new weight file `deploy.caffemodel`  will be located in the folder `WORK_DIR/vaiModels`.


### Step 9: Compiling ###
In the Vitis-AI docker container, with conda environment `vitis-ai-caffe` activated, execute..
`s9_vai_compile.sh`   
The script executes the Vitis-AI tool `vai_c_caffe`, which generates the DPU instruction code file `WORK_DIR/vaiModels/FINAL_FILE_NAME.xmodel`. This file has to be copied to the iam camera.


---
### Step 10: Running Application on iam ###
---
The folder [dpuObjDetYoloV4AppMt](./../dpuObjDetYoloV4AppMt) contains the application source files for YoloV4 object detection and the scripts for building the application and transfering it to the **iam ML**. .   
The trained model and the configuration has to be copied to the `/model` subfolder of the application. Each model has to placed in its own subfolder in `dpuObjDetYoloV4AppMt/model`, which contains the compiled instruction file, the corresponding `anchors9.txt` and the `labelList.txt` files, as well as the `config.txt` file with yolov4 threshold settings.
At startup the application loads the model from the folders /model/default_zu2 or /model/default_zu5 (depending on the detected camera version). Models from other folders can be loaded during runtime using the camera GUI features.     

After starting the application, the iam camera can be connected from host with Synview Explorer.   
Models can be activated with camera GUI feature `Model->ModelToLoad` and `Model->ModelLoadCommand`   

![YoloAppWsn](/dpuObjDetYoloV4/misc/images/screenshotYoloV4.png)

With camera GUI feature `TestImages->TestPicList` test images from the training dataset can be displayed.
    

