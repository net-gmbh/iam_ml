
rem -------------------------------------------
rem ---  copy application files to iam      ---
rem -------------------------------------------


docker run^
  -it^
  --rm^
  -v %cd%:/workspace^
  -w /workspace^
  iam_xcomp2021^
  bash -c "./scripts/sh_copy_app_to_iam.sh"


rem pause