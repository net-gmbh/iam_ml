#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


import os
import sys
import cv2
import numpy as np

import importlib
import argparse

import tensorflow as tf
keras=tf.keras

print("Tensorflow version is ", tf.__version__)
print('Keras version      : ',keras.__version__)

from tensorflow_model_optimization.quantization.keras import vitis_quantize

import segTools as segTools




def main(argv):

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )      
                        
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )    
                        
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    #parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' )  

    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)
    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import    
        
    cfg = importlib.import_module(cf)

    workDir=args.workDir

    calib_image_dir  = os.path.join(workDir, 'data','calib_img')
    

    calib_batch_size = cfg.VAI_CALIB_BATCH_SIZE

    

    # --- read sample image list ---
    print('Read samples from dir: ', calib_image_dir)

    fileNameList = os.listdir(calib_image_dir)
    print('Number of samples found: ',len(fileNameList))
    print('')    






    numberOfSamples = len(fileNameList)

    X = np.zeros((numberOfSamples, cfg.HEIGHT, cfg.WIDTH, 3), dtype=np.float32)
    for i in range(numberOfSamples):        
        image = segTools.loadImageArrNormalized(os.path.join(calib_image_dir,fileNameList[i]), cfg.WIDTH, cfg.HEIGHT)
        X[i] = image

    print('Loading model ...')
    
    model = tf.keras.models.load_model(os.path.join(workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME))

    quantizer = vitis_quantize.VitisQuantizer(model)
    #quantized_model = quantizer.quantize_model(calib_dataset = X)
    #quantized_model = quantizer.quantize_model(calib_dataset = X, calib_batch_size=3, include_fast_ft=True, fast_ft_epochs=10)
    
    if cfg.VAI_QUANT_MODE=='normal':
        print('Quantize network (normal mode)...')
        quantized_model = quantizer.quantize_model(calib_dataset = X, include_fast_ft=False, fast_ft_epochs=10)
    elif cfg.VAI_QUANT_MODE=='fine':
        print('Quantize network (fine mode)...')
        quantized_model = quantizer.quantize_model(calib_dataset = X, calib_batch_size=None, include_fast_ft=True, fast_ft_epochs=6)
    else:
        print('ERROR: Unknown VAI_QUANT_MODE')
        exit(1) # error

    ## Save the model
    quantized_model.save(os.path.join(workDir, 'modelVai', 'model_quantized.h5'))


if __name__=="__main__":
    main(sys.argv)
    
    