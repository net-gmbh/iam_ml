#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


import os
import sys
import cv2
import numpy as np
import importlib
import argparse
import random
from pathlib import Path

import pre_proc as pp


import classifierModelBuilder as cmb


import tensorflow as tf
keras=tf.keras

print("Tensorflow version is ", tf.__version__)
print('Keras version      : ',keras.__version__)

from tensorflow_model_optimization.quantization.keras import vitis_quantize

# recursive directory create
def createDir(dirName):
    if not os.path.exists(dirName):  
        #Path(dirName).mkdir( 0o777, parents=True, exist_ok=True) # recursive
        Path(dirName).mkdir( parents=True, exist_ok=True) # recursive        
        if not os.path.exists(dirName):  
            print('ERROR: Could not create directory: ',dirName)
            return False
    return True


def central_resize(img, imgSize):    
    return cv2.resize(img,(imgSize,imgSize), interpolation = cv2.INTER_LINEAR)
  
  


def main(argv):

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )      
                        
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )    
                        
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' )  

    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)
    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import            
    cfg = importlib.import_module(cf)


    
    calib_image_dir = args.datasetDir

    calib_batch_size = cfg.VAI_CALIB_BATCH_SIZE

    
    # --- read sample image list ---
    print('Read samples from dir: ', calib_image_dir)

    random.seed(42) # keep results reproducible
    

    # --- get sample file names ---
    readSubdirs = True
    if readSubdirs:
        fileNameList=[]
        classNames = os.listdir(calib_image_dir)
        numClasses = len(classNames)
        samplesPerClassFrac=cfg.VAI_CALIB_NUMBER_OF_SAMPLES/numClasses
        samplesPerClass=int(samplesPerClassFrac+0.999)

        for id in range(numClasses):
            path = os.path.join(calib_image_dir,classNames[id])
            fileNames = os.listdir(path)   
            random.shuffle(fileNames)
        
            for n in range(min(samplesPerClass,len(fileNames))):
                fileNameList.append(os.path.join(classNames[id],fileNames[n]))

            print('Class',id,':',classNames[id],'--> Samples:',len(fileNames))
    else:
        fileNameList = os.listdir(calib_image_dir)
    
    print('Number of calib samples: ',len(fileNameList))
    
    

    # --- get samples ---
    valid_extensions = ['jpg','jpeg', 'bmp', 'png']
    validFileNameList = [fn for fn in fileNameList if any(fn.endswith(ext) for ext in valid_extensions)]

    numberOfSamples = len(validFileNameList)
#   numberOfSamples = int(len(validFileNameList)/8)   # test only

    calibData = np.zeros((numberOfSamples, cfg.IMAGE_HEIGHT, cfg.IMAGE_WIDTH, 3), dtype=np.float32)
    for i in range(numberOfSamples):
        currentFile = validFileNameList[i]
        
        image = cv2.imread(os.path.join(calib_image_dir,currentFile))
    
        image = central_resize(image, cfg.IMAGE_WIDTH)
        image=cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image=image.astype('float32')
        image = pp.preprocess(image, cmb.getInputScalingType(cfg))
        calibData[i] = image

    print("Number of calibration samples: ",numberOfSamples)

    print('Loding model ...')
    
    model = tf.keras.models.load_model(os.path.join(args.workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME))

    quantizer = vitis_quantize.VitisQuantizer(model)    
    
    if cfg.VAI_QUANT_MODE=='normal':
        quantized_model = quantizer.quantize_model(calib_dataset = calibData, include_fast_ft=False, fast_ft_epochs=10)
    elif cfg.VAI_QUANT_MODE=='fine':
        quantized_model = quantizer.quantize_model(calib_dataset = calibData,  calib_batch_size=cfg.VAI_CALIB_BATCH_SIZE, include_fast_ft=True, fast_ft_epochs=6)
    else:
        print('ERROR: Unknown VAI_QUANT_MODE')
        exit(1) # error

    pathName=os.path.join(args.workDir, 'modelVai')
    createDir(pathName)

    ## Save the model
    quantized_model.save(os.path.join(pathName, 'model_quantized.h5'))


if __name__=="__main__":
    main(sys.argv)

