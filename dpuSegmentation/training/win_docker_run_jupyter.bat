

rem stop with 'c' key
rem host browser: http://localhost:8888 

echo off
call configWin.bat
echo on

docker run^
  --gpus all^
  -it^
  --rm^
  -v %WORK_DIR_MAPPING%:/work^
  -v %DATASET_DIR_MAPPING%:/dataset^
  -v %cd%:/workspace^
  -w /workspace^
  -p 8888:8888^
  %TRAIN_IMAGE_NAME%^
  jupyter notebook --ip 0.0.0.0 --no-browser --allow-root



rem pause


