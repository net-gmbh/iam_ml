rem --------------------------------------------------
rem             Classifier Training Config
rem --------------------------------------------------


set INDEX=1

IF %INDEX%==1 (
    rem --- config file name in /projectConfig directory ---
    set PROJECT_CONFIG=config_Vgg16
    rem --- dataset and work dir mapping for docker containers ---
    set DATASET_DIR_MAPPING=D:/ml2_datasets/fruit26/images
    set WORK_DIR_MAPPING=D:/ml2_work/class_fruit26_vgg16
	rem --- dataset and work in case of venv environment ---
    rem (don't care for docker training)
    set DATASET_DIR_IN_VENV=D:/ml2_datasets/fruit26/images
    set WORK_DIR_IN_VENV=D:/ml2_work/class_fruit26_vgg16
    )

IF %INDEX%==2 (
    rem --- config file name in /projectConfig directory ---
    set PROJECT_CONFIG=config_Resnet50V2
    rem --- dataset and work dir mapping for docker containers ---
    set DATASET_DIR_MAPPING=D:/ml2_datasets/fruit26/images
    set WORK_DIR_MAPPING=D:/ml2_work/class_fruit26_res50
	rem --- dataset and work in case of venv environment ---
    rem (don't care for docker training)
    set DATASET_DIR_IN_VENV=D:/ml2_datasets/fruit26/images
    set WORK_DIR_IN_VENV=D:/ml2_work/class_fruit26_res50
    )
	
IF %INDEX%==3 (
    rem --- config file name in /projectConfig directory ---
    set PROJECT_CONFIG=config_Dense121
    rem --- dataset and work dir mapping for docker containers ---
    set DATASET_DIR_MAPPING=D:/ml2_datasets/fruit26/images
    set WORK_DIR_MAPPING=D:/ml2_work/class_fruit26_dense121
	rem --- dataset and work in case of venv environment ---
    rem (don't care for docker training)
    set DATASET_DIR_IN_VENV=D:/ml2_datasets/fruit26/images
    set WORK_DIR_IN_VENV=D:/ml2_work/class_fruit26_dense121
    )
	
IF %INDEX%==4 (
    rem --- config file name in /projectConfig directory ---
    set PROJECT_CONFIG=config_IncV3
    rem --- dataset and work dir mapping for docker containers ---
    set DATASET_DIR_MAPPING=D:/ml2_datasets/fruit26/images
    set WORK_DIR_MAPPING=D:/ml2_work/class_fruit26_incv3
	rem --- dataset and work in case of venv environment ---
    rem (don't care for docker training)
    set DATASET_DIR_IN_VENV=D:/ml2_datasets/fruit26/images
    set WORK_DIR_IN_VENV=D:/ml2_work/class_fruit26_incv3
    )








rem --- docker images used for training and vitisAi ---

set TRAIN_IMAGE_NAME=vai_141_train 

rem set VAI_IMAGE_NAME=xilinx/vitis-ai-gpu:1.4.1.978
set VAI_IMAGE_NAME=xilinx/vitis-ai-cpu:1.4.1.978



