#!/bin/bash

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


source config.sh

echo "Config: $PROJECT_CONFIG"

python pycode/generateDefs.py \
                --configPath $(pwd)/projectConfig --configFile $PROJECT_CONFIG \
				--workDir $WORK_DIR_IN_DOCKER

source $WORK_DIR_IN_DOCKER/modelVai/vai_config.sh

echo ""
echo "MODEL_TYPE               : " $MODEL_TYPE
echo "NUM_CLASSES              : " $NUMBER_OF_CLASSES
echo "WIDTH                    : " $IMAGE_WIDTH
echo "HEIGHT                   : " $IMAGE_HEIGHT
echo "VAI_DIR                  : " $VAI_DIR
echo "VAI_TARGET_DEVICE        : " $VAI_TARGET_DEVICE
echo "VAI_FINAL_MODEL_FILE_NAME: " $VAI_FINAL_MODEL_FILE_NAME
echo ""


	 
vai_c_tensorflow2 \
	 -m $VAI_DIR/model_quantized.h5 \
	 -a arch/iam_$VAI_TARGET_DEVICE.json \
	 -o $VAI_DIR \
	 -n $VAI_FINAL_MODEL_FILE_NAME 