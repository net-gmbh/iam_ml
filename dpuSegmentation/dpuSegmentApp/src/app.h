/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#ifndef AppClass_H
#define AppClass_H

#include "OsDep.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <filesystem>
#include "iamDevice.h"
#include "common.h"
#include "imgSegmentDpu.h"


// --- GigE Server Event Processing ---
#define GEVSrvEv_Exit                   0x0000
#define GEVSrvEv_Connect                0x0001
#define GEVSrvEv_Disconnect             0x0002
#define GEVSrvEv_StreamStart            0x0003
#define GEVSrvEv_StreamStop             0x0004
#define GEVSrvEv_SmartXMLEvent          0x0010

#define GEVSrvEv_ReadFlag               0x0000
#define GEVSrvEv_WriteFlag              0x0001

#define GEVSrvEv_ImgFmt_DeviceWidth     0x0005
#define GEVSrvEv_ImgFmt_DeviceHeight    0x0006
#define GEVSrvEv_ImgFmt_PixFmt          0x0007

// --- XML Register Adresses ---
#define AdrSmartWidth             0x0100
#define AdrSmartHeight            0x0104
#define AdrSmartPixFmt            0x0200
#define AdrSmartPayloadSize       0x0300

#define AdrOutputImageMode        0x0400
#define AdrSensorDecimation       0x0404
#define AdrExposureTime           0x0408
#define AdrAutoExposureMode       0x040C

#define AdrAwb                    0x0418

#define AdrEnableClassifier       0x0420

#define AdrEnableOsd              0x042C

#define AdrEnableTestPic          0x0430
#define AdrTestPicIdx             0x0434
#define AdrTestPicNumber          0x0438

#define AdrModelDirString         0x043C
#define AdrLoadModelCommand       0x0440


#define AdrSensorWidth            0x0510
#define AdrSensorHeight           0x0514

#define AdrOutputImage            0x0520

#define AdrSaveFrontendSettings   0x0540

#define AdrEnablePrintfTime       0x0544


#define COLOR_RED   "\x1B[31m"
#define COLOR_GRN   "\x1B[32m"
#define COLOR_YEL   "\x1B[33m"
#define COLOR_BLU   "\x1B[34m"
#define COLOR_MAG   "\x1B[35m"
#define COLOR_CYN   "\x1B[36m"
#define COLOR_WHT   "\x1B[37m"
#define COLOR_RESET "\x1B[0m"


class appClass 
{
public:
    appClass();
    ~appClass();

    iamDeviceClass iamDevice;

    int init( int autorunMode );
    LvStatus InitServer         ( void );
    bool ErrorOccurred          ( LvStatus ErrorStatus);
    
    LvStatus SendImage          ( LvipImgInfo* pImgInfo );
    void     GigeEventCallback  ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam );    
    void     NewBufferCallback  ( LvBuffer* pBuffer );

    void calculateOutputImgSize();
    void setSensorPixFmt();
	
    void loop1ms();
    int loadModel(std::string path);
    int getImageVersion(int *mainVersion, int *subVersion);
    	
    void readClassListFile(const std::string filename);
	int  getDpuVersion();
    int  stringCompare(char a[], char b[]);

    imgSegmentClass imgSegment;    
	
    cv::Mat testPicRgb;

    LvStatus SynviewStatus;
        
    bool    bIsConnected;
    bool    exitAppFlag;    
    bool    autorun;
    bool    modelOkFlag;
    bool    doModelLoadFlag;
    bool    enableStreaming;
    bool    enableSegmentationFlag;
    bool    enableOsdFlag;
    bool    enableTestPicFlag;
    bool    enablePrintfTimeFlag;
    int     outputImagePattern;
    bool    saveFrontendSettingsRequestFlag;

    int32_t sensorWidth;
    int32_t sensorHeight;

    int   imgWidthOut;
    int   imgHeightOut;
    int   sensorPixelFormat;   

    int   sensorDecimation;
    int   outputImageMode; 
    int   outputPixelFormat;
    int   autoExposureMode;
    float exposureTime;

    int   dpuVersion;
    char  modelDirString[256];
       
    double tickCountClass1;
    double tickCountClass2;

    

    std::vector<int> topIdxVector;
    std::vector<float> topPropVector;
    
    std::vector<std::string> classNames;
    std::vector<int> redMarker;
    std::vector<int> greenMarker;
    std::vector<int> blueMarker;
   
    
    std::vector<std::string> testImageFileNames;
    

    int  loadTestImage;   
    int  loadTestImageFlag;
    
};
#endif //AppClass_H
