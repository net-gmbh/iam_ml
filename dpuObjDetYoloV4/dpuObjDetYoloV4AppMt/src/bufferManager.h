/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


//                                                                 +-------------------+
//                                                       +-------> | Processing Task 1 | --+
//                                                       |         +-------------------+   |
//   +----------+                +-------------+         |                  ...            |
//   | Synview  | ------------>  |   Buffer1   |         |         +-------------------+   | 
//   | Callback |                | (sv.buffer) | --------+-------> | Processing Task N | --+  
//   +----------+                +-------------+                   +-------------------+   |  
//                 pushBuffer1()                   pullBuffer1()                           |  
//                                                 pullDoneBuffer1()                       | 
//                                                                                         |
//                                                                          pushBuffer2()  |
//                                                                      pushDoneBuffer2()  |
//                                                                                         |
//                                                                                         v
//                                                                             +-------------------+
//                                                                             |    Buffer2        |
//                                                                             | (vector<cv::Mat>) |
//                                                                             +-------------------+
//                                                                                         |
//                                                                          pullBuffer2()  |
//                                                                      pullDoneBuffer2()  |  
//                                                                                         |  
//                                                        +-------------+                  |
//                                                        | Transmiter/ | <----------------+
//                                                        |    Sink     |
//                                                        |    Task     |
//                                                        +-------------+
//
// buffers allow syncronization between different tasks
// buffer 1 manages synview buffer handles
// buffer 2 manages cv:Mat elements
//
// pull requests ALLWAYS get the NEWEST image available, so lowest latency is achived
// buffer 2 rearanges the frame sequence, because processing tasks may overtake each other






#ifndef bufferManager_H
#define bufferManager_H

#include <vector>
#include <mutex>
#include <chrono>
#include "sv.synview.class.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

class bufferManagerClass
{
public:
    bufferManagerClass();
    ~bufferManagerClass();

    // --- buffer 1 ---
    void initBuffer1(int numBuffers);
    int  pushBuffer1(uint32_t hndlBuffer, int *idx, long long unsigned  *frameCount);
    int  pullBuffer1(int *idx, long long unsigned  *fcnt);
    void pullDoneBuffer1(int idx);
    
    // --- buffer 2 ---
    void initBuffer2(int numBuffers, int numSources);
    int  pushBuffer2(long long unsigned  frameCount, int* idx);    
    void pushDoneBuffer2(int idx, long long unsigned  frameCount);
    int  pullBuffer2(int *idx, long long unsigned  *frameCount);    
    void pullDoneBuffer2(int idx);    
    
    std::vector<cv::Mat> imagesBuffer2;

    double  fpsBuffer1In;
    double  fpsBuffer1Out;
    double  fpsBuffer2In;
    double  fpsBuffer2Out;
    bool    enableAnalyser;
    int     numFramesAnalyser;

private:
    std::mutex mtx;    

    std::vector<long long unsigned > fcntInWork;

    // --- buffer 1 ---
    std::vector<uint32_t> hndlVectorBuffer1;  
    std::vector<bool> lockedVectorBuffer1;      
    std::vector<bool> filledVectorBuffer1;    
    std::vector<long long unsigned >  fcntVectorBuffer1;
    int latestBufferIdxBuffer1;
    long long unsigned int fcnt;
    int numElementsBuffer1;

    // --- buffer 2 ---
    std::vector<bool> lockedVectorBuffer2;     
    std::vector<bool> filledVectorBuffer2;      
    std::vector<long long unsigned >  fcntVectorBuffer2;
    int numElementsBuffer2;
    int numSourcesBuffer2;
    long long unsigned  prevFcntBuffer2;

    void print_fcntInWork();

    int analyserBuffer1InFrameCounter;
    std::chrono::high_resolution_clock::time_point  analyserBuffer1InStartTime;

    int analyserBuffer1OutFrameCounter;
    std::chrono::high_resolution_clock::time_point  analyserBuffer1OutStartTime;

    int analyserBuffer2InFrameCounter;
    std::chrono::high_resolution_clock::time_point  analyserBuffer2InStartTime;

    int analyserBuffer2OutFrameCounter;
    std::chrono::high_resolution_clock::time_point  analyserBuffer2OutStartTime;
    

};



#endif //bufferManager_H
