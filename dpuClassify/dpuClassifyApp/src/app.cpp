/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

 
#include "app.h"
#include "main.h"
#include "picProc.cpp"

#define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); }

typedef struct _EventData
{
    void*  pCBEvent;
    void*  pCBContext;
} EventData;


//-----------------------------------------------------------------------------
// Callback function for frontend images

void LV_STDC GEVSrvCallbackNewBufferFunction ( LvHBuffer hBuffer, void* pUserPointer, void* pUserParam)
{
    appClass* pGevServer = (appClass*) pUserParam;
    // in UserPointer we hold pointer to buffer
    pGevServer->NewBufferCallback ( (LvBuffer*) pUserPointer );
}


//-----------------------------------------------------------------------------
// Callback function for gige server events

U32BIT StaticEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT* Param, void* CBParam )
{
    appClass *pGevServer = (appClass *)CBParam;
    pGevServer->GigeEventCallback ( Event, NrParam, Param, CBParam );    
    return 0;
}

void  putCharPtr   ( U32BIT *Param , char *p )   { Param[2] = (U32BIT)((U64BIT)p)&0xffffffff; Param[3] = (U32BIT)(((U64BIT)p)>>32); }
char* getCharPtr   ( U32BIT *Param )             { return (char  *)( Param[2] + (((U64BIT)Param[3])<<32) ); }


// --------------------------------------

appClass::appClass ( )
{    
    autorun = false;
}

appClass::~appClass()
{
    imgClassifier.cleanup();
}

// --------------------------------------

int appClass::init( int autorunMode )
{		
	printf (( "Init appClass...\n" ));

    bIsConnected           = false;
    exitAppFlag            = false;
    modelOkFlag            = false;
    autorun                = autorunMode;
    enableClassifierFlag   = true;
    enableOsdFlag          = true;
    enablePrintfDpuOutFlag = false;   
    enablePrintfPropFlag   = false;   
    enablePrintfTimeFlag   = false;   
    enableTestPicFlag      = false;
    loadTestImageFlag      = 1;
    enableStreaming        = false;
    saveFrontendSettingsRequestFlag=0;
    doModelLoadFlag        = false;
    strcpy ( modelDirString, "empty");
    trainLabel             = "none";
    sensorPixelFormat      = LvPixelFormat_BayerGR8;
    outputImageMode        = 0;    // 0=RGB, 1=Bayer
    outputPixelFormat      = LvPixelFormat_RGB8;

    // --- check iam platform version ---
    int imageVersionMain;
    int imageVersionSub;
    getImageVersion(&imageVersionMain, &imageVersionSub);
    if (imageVersionMain==3) {
        printf(COLOR_GRN "Image version: 3 --> ok\n" COLOR_RESET);
    } else {
        printf(COLOR_RED "False image version --> error\n" COLOR_RESET);
        return 0;
    }
    

    // --- init frontend ---
    SynviewStatus = iamDevice.InitCamera ();
    if (ErrorOccurred(SynviewStatus)) return 0;	
		
	setSensorPixFormat();    

    // startup image size ==> full sensor size
    SynviewStatus=iamDevice.GetInt32CameraParam( LvDevice_SensorWidth,  &sensorWidth );
    ErrorOccurred(SynviewStatus);
    SynviewStatus=iamDevice.GetInt32CameraParam( LvDevice_SensorHeight, &sensorHeight);
    ErrorOccurred(SynviewStatus);

    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_Width, sensorWidth);
    ErrorOccurred(SynviewStatus);
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_Height, sensorHeight);
    ErrorOccurred(SynviewStatus);
                    
    // setup default input image decimation factor
    if (sensorHeight>4*480)       sensorDecimation=4;
    else if  (sensorHeight>2*480) sensorDecimation=2;
    else                          sensorDecimation=1;

    calculateOutputImgSize();

    // use frontend auto exposure
    autoExposureMode=1;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Continuous);
    ErrorOccurred(SynviewStatus);

    // get the current exposure value from frontend
    SynviewStatus = iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &exposureTime);
    ErrorOccurred(SynviewStatus);

    SynviewStatus = iamDevice.SetFloatCameraParam ( LvDevice_Gamma, 1.0);
    ErrorOccurred(SynviewStatus);

    
     // redirect frontend image callback
    iamDevice.SetCallback ( GEVSrvCallbackNewBufferFunction, this );

    // --- init  GigE Server ---
    // enable Smart functions     
    // GlobalSmartEnable + GlobalFeatureLock + GlobalXMLEnable
    iamDevice.SetInt32CameraParam  ( LvDevice_LvSmartGeneralInq,     7 );      
    
    iamDevice.m_pDevice->SetString ( LvDevice_LvSmartAppXMLPath, XML_FILE);
    std::string path;
    iamDevice.m_pDevice->GetString ( LvDevice_LvSmartAppXMLPath, path);
    printf("Applied XML File: %s\n", path.c_str());

    // synview access to enable gige vision server
    int val;
    iamDevice.GetInt32CameraParam( LvDevice_LvGigEServerCapability, &val );
    if ( (val & 0x01) != 0 )
    {
        // enable gige server
        iamDevice.m_pDevice->SetInt ( LvDevice_LvGigEServerEnable, 0x01 );

        // check
        iamDevice.GetInt32CameraParam( LvDevice_LvGigEServerEnable, &val );
        PRINTF (( "appClass::InitServer: Enable GEV server. Result: %s", (val & 0x01) == 1?"OK":"NOT OK!" ));
        if ( (val & 0x01) == 1  )
        {
            // register the event callback
            EventData Buffer;
            Buffer.pCBEvent   = (void *)StaticEventCallback;
            Buffer.pCBContext = (void *)this;

            PRINTF (( "appClass::InitServer: Register GigE Vision Server Event %p with context: %p", Buffer.pCBEvent, Buffer.pCBContext ));
            SynviewStatus = iamDevice.m_pDevice->SetBuffer ( LvDevice_LvGigEServerEvent, (void *)&Buffer, sizeof(Buffer) );
            if (ErrorOccurred(SynviewStatus)) return 0;	
        }
    } 

    // --- check iam version ---
    dpuVersion = getDpuVersion();
    if (dpuVersion==2) {
        printf(COLOR_GRN "DPU zu2 found in FPGA image\n" COLOR_RESET);
    } else if (dpuVersion==5) {
        printf(COLOR_GRN "DPU zu5 found in FPGA image\n" COLOR_RESET);
    } else {
        printf(COLOR_RED "ERROR: No usable DPU found in FPGA image\n" COLOR_RESET);
        return 0;
    }

    // --- load dpu ---
      
    std::string default_model_path="model/default_";
        
    if (dpuVersion==2) {
        default_model_path.append("zu2");
    }

    if (dpuVersion==5) {
        default_model_path.append("zu5");
    }

    modelOkFlag = loadModel(default_model_path.c_str());
 

    kTopOsdNumber=4;
    if (kTopOsdNumber>(int)classNames.size()) kTopOsdNumber=(int)classNames.size();

    // --- read test img dir ---
    testImageFileNames.clear();
    std::string test_img_path="imgTest";
        
    for (auto &p : std::filesystem::recursive_directory_iterator(test_img_path)) {
        if ((p.path().extension() == ".png")||
            (p.path().extension() == ".jpg")||
            (p.path().extension() == ".JPG")||
            (p.path().extension() == ".bmp")) {
            testImageFileNames.push_back(p.path().string());
        }
    }
        
    if (testImageFileNames.size()>0) {
        printf("--- test images ---\n");
        for (int i=0; i<(int)testImageFileNames.size(); i++) {
            printf("test img %d: %s\n",i,testImageFileNames[i].c_str());
        }

        cv::Mat testPic=cv::imread(testImageFileNames[0]);
        cvtColor( testPic, testPicRgb, cv::COLOR_BGR2RGB );
        loadTestImage=1;
    }
         
    // start frontend in case of autostart mode ---
    if (autorun) {
        printf("Executing frontend autostart...\n");
        int error = iamDevice.OpenBuffers ();
        if ( error ) {
            printf("ERROR: OpenBuffers error\n");
        }
        
        error = iamDevice.StartAcquisition ();
        if ( error ) {
            printf("ERROR: StartAcquisition error\n");
        }
    }

    return 1;
}



 void appClass::loop1ms() {
    if (doModelLoadFlag) {
        // --- read config file ---
        printf( "---- Read config file ----\n");    
        std::string fn1="model/";
        fn1.append(modelDirString);

        modelOkFlag=loadModel(fn1.c_str());
        doModelLoadFlag=0;
    }

    // --- load test image ---
    if (loadTestImageFlag>0) {        
        if ( (int)testImageFileNames.size()>0 ) {           
            cv::Mat testPic=cv::imread(testImageFileNames[loadTestImage-1]);
            cvtColor( testPic, testPicRgb, cv::COLOR_BGR2RGB );
        }
        loadTestImageFlag=0; 
    }

    if (saveFrontendSettingsRequestFlag) {
        LvFeature ftr;     

        //iamDevice.m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "WhiteBalanceAuto", &ftr);
        //iamDevice.m_pDevice->SetEnumStr(ftr,"Once");
        //ErrorOccurred(SynviewStatus);    
		
		int restartAcquisition=0;
        int error=0;
		
		if ( iamDevice.IsAcquiring () ) {
            restartAcquisition=1;
            error = iamDevice.StopAcquisition ();
            if ( error) {
                printf(("ERROR: StopAcquisition error\n"));
            }
        }
        error = iamDevice.StartAcquisition ();
        if ( error ) {
            printf(("ERROR: StopAcquisition error\n"));
        }
        error = iamDevice.StopAcquisition ();
        if ( error) {
            printf(("ERROR: StopAcquisition error\n"));
        }     
                       
        iamDevice.m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "UserSetSelector", &ftr);
        ErrorOccurred(SynviewStatus);  
        iamDevice.m_pDevice->SetEnumStr(ftr,"UserSet1");
        ErrorOccurred(SynviewStatus); 
        iamDevice.m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "UserSetSave", &ftr);
        ErrorOccurred(SynviewStatus);  
        iamDevice.m_pDevice->CmdExecute(ftr); 
        ErrorOccurred(SynviewStatus);                     
        iamDevice.m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "UserSetDefault", &ftr);
        ErrorOccurred(SynviewStatus);  
        iamDevice.m_pDevice->SetEnumStr(ftr, "UserSet1");
        ErrorOccurred(SynviewStatus);  
		
		 if (restartAcquisition) {
             error = iamDevice.StartAcquisition ();
            if ( error ) {
                printf(("ERROR: StopAcquisition error\n"));
            }
        }

        saveFrontendSettingsRequestFlag=0;
    }
 }




bool appClass::ErrorOccurred(LvStatus ErrorStatus)
{
    if (ErrorStatus == LVSTATUS_OK) return false;
    LvLibrary::Logf ( "Error:%s\n", LvLibrary::GetLastErrorMessage().c_str() );
    PRINTF (( "iamApp::synview error: \"%s\"", LvLibrary::GetLastErrorMessage().c_str() ));
    return true;
}


void appClass::calculateOutputImgSize() {
        
    if (sensorDecimation==1) {
        imgWidthOut =int( sensorWidth /1);
        imgHeightOut=int( sensorHeight/1);
    } 
    if (sensorDecimation==2) {
        imgWidthOut =int( sensorWidth /2);
        imgHeightOut=int( sensorHeight/2);
    }
    if (sensorDecimation==4) {
        imgWidthOut =int( sensorWidth /4);
        imgHeightOut=int( sensorHeight/4);
    }    
}


void appClass::setSensorPixFormat() {
        
    // color sensor --> choose any available bayer format
    sensorPixelFormat = LvPixelFormat_BayerGR8;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);    
    if (SynviewStatus==LVSTATUS_OK) {
        printf("Sensor pixel format: BayerGR8\n");
        return;
    }  
    
    sensorPixelFormat = LvPixelFormat_BayerRG8;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("Sensor pixel format: BayerRG8\n");
        return;
    }
       
    sensorPixelFormat = LvPixelFormat_BayerGB8;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("Sensor pixel format: BayerGB8\n");
        return;
    }

    sensorPixelFormat = LvPixelFormat_BayerBG8;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("Sensor pixel format: BayerBG8\n");
        return;
    }

    sensorPixelFormat = LvPixelFormat_Mono8;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("Sensor pixel format: Mono8\n");
        return;
    }

    printf( "ERROR: No suitable pixel format found\n");                            
}





LvStatus appClass::SendImage ( LvipImgInfo* ImgInfo )
{
    LvStatus RetVal = -1;

    //PRINTF (( "iAMServerClass::SendImage: LvDevice_LvGigEServerImage: pImgInfo:%p pImg:%p PixFmt:%x Width:%d Height:%d LinePitch:%d", (int64_t)ImgInfo, (int64_t)ImgInfo->pData, ImgInfo->PixelFormat, ImgInfo->Width, ImgInfo->Height, ImgInfo->LinePitch ));
    RetVal =iamDevice. m_pDevice->SetInt ( LvDevice_LvGigEServerImage, (int64_t)ImgInfo );

    return RetVal;
}


// --------------------------------------------
//    GigE Server Event Callback Processing
// --------------------------------------------
void appClass::GigeEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam )
{
    int error=0;
    int Write = int(Param[0]);
    
    // --------------------------------------
    //    GigE Server Event: Connect
    // --------------------------------------
    if ( Event == GEVSrvEv_Connect )
    {
        PRINTF(("GigE Server Event: Connect"));
        if ( bIsConnected ) {
            PRINTF(("ERROR: Already connected!"));
        }
        bIsConnected = true;
        enableStreaming=false;
    }

    // --------------------------------------
    //    GigE Server Event: Disconnect
    // --------------------------------------
    else if ( Event == GEVSrvEv_Disconnect )
    {
        printf(("GigE Server Event: Start Disconnect ...\n"));
        if ( !bIsConnected ) {
            printf(("ERROR: Not connected!\n"));
        }

        if (!autorun) {
            if ( iamDevice.IsAcquiring () ) {
                printf(("Stopping acquisition\n"));
                iamDevice.StopAcquisition ();
                iamDevice.CloseBuffers ();
            }
        }

        bIsConnected = false;
        printf(("Disconnect done\n"));
    }

    // --------------------------------------
    //    GigE Server Event: Exit
    // --------------------------------------
    else if ( Event == GEVSrvEv_Exit ) {
        PRINTF(("GigE Server Event: Exit..."));
        int         *data  = (int        *) &Param[2];

        if ( *data == 0 ) {     // just exit
            exitAppFlag = true;
            bIsConnected = false;
        }
    }

    // -----------------------------------------------------------------
    //    GigE Server Event: Stream Start (=GigE acquisition start)
    // -----------------------------------------------------------------
    else if ( Event == GEVSrvEv_StreamStart )
    {
        int *data = (int *) &Param[2];

        if ( Write )
        {
             if (!autorun) {
                PRINTF(("GigE Server Event: Start Stream Start..."));
                if ( iamDevice.IsAcquiring () ) {
                    PRINTF(("Stopping camera"));
                    iamDevice.StopAcquisition ();

                    PRINTF (( "Closing buffers" ));
                    iamDevice.CloseBuffers();
                }

                error = iamDevice.OpenBuffers ();
                if ( error ) {
                    PRINTF(("ERROR: OpenBuffers error"));
                }

                // start frontend acquisition                
                error = iamDevice.StartAcquisition ();
                if ( error ) {
                    PRINTF(("ERROR: StopAcquisition error"));
                }

                PRINTF(("Start stream done"));
             }
             enableStreaming=true;
        } else {
            if (autorun) {
                *data = 0; // return command is done immediately 
            } else {
                *data = iamDevice.IsAcquiring () ? 0 : 1;     // reading the register returns the command value (1) until the acquisition is started (0) 
            }
            PRINTF(("GigE Server Event: GEVSrvEv_StreamStart read value:%d", *data ));
        }
    }

    // ----------------------------------------------------------------
    //    GigE Server Event: Stream Stop (=GigE acquisition stop)
    // ----------------------------------------------------------------
    else if ( Event == GEVSrvEv_StreamStop )
    {
        int *data = (int *) &Param[2];

        if ( Write )
        {
            if (!autorun) {
                PRINTF(("GigE Server Event: Start Stream Stop..."));
                error = iamDevice.StopAcquisition ();
                if ( error) {
                    PRINTF(("ERROR: StopAcquisition error"));
                }

                //PRINTF(("Closing buffers"));
                iamDevice.CloseBuffers ();                
            }
            enableStreaming=false;
        } else {
            if (autorun) {
                *data = 0;   // return command is done immediately 
            } else {
                *data = iamDevice.IsAcquiring () ? 1 : 0;    // reading the register returns the command value (1) until the acquisition is stopped (0)
            }
            //PRINTF(("GigE Server Event: GEVSrvEv_StreamStop read value:%d", *data ));
        }
    }

    
    // --------------------------------------------------
    //    GigE Server Event: XML Parameter Read/Write
    // --------------------------------------------------
    else if (Event == GEVSrvEv_SmartXMLEvent )
    {
        unsigned int adr   = (unsigned int)  Param[1];
        int         *data  = (int        *) &Param[2];
        float       *fdata = (float*)       &Param[2];

        if ( adr == AdrSmartWidth ) {  // width of GigE-Server output image
            if ( Write ) {
                // read only in XML
            }
            else {
                *data=imgWidthOut;
                PRINTF (( "XmlEvent: Read(AdrSmartWidth) -->: %d", *data ));                
            }
        }

        if ( adr == AdrSmartHeight ) // height of GigE-Server output image
        {
            if ( Write ) {
                // read only in XML
            }
            else {                
                *data=imgHeightOut;
                PRINTF (( "XmlEvent: Read(AdrSmartHeight) --> %d", *data ));
            }
        }

        if ( adr == AdrSmartPixFmt ) // pixel format of GigE-Server output image
        {
            if ( Write ) {                            
                // read only in XML       
            }
            else {                            
                *data=outputPixelFormat; 
                PRINTF (( "XmlEvent: Read(AdrSmartPixFmt) --> 0x%8.8x", *data ));
            }
        }

        if ( adr == AdrSmartPayloadSize ) {
            if ( Write ) {
                // read only in XML                
            }
            else {
                
                int SmartPayloadSize = imgWidthOut * imgHeightOut;
                if (outputPixelFormat==LvPixelFormat_RGB8) SmartPayloadSize *=3;                
                // XML invalitated from width, height and pixel format
                *data = SmartPayloadSize;
                PRINTF (( "XmlEvent: Read(AdrSmartPayloadSize) --> PayloadSize: %d Byte", *data ));
            }
        }

        if ( adr == AdrOutputImageMode ) {
            if ( Write ) {
                outputImageMode = *data;
                PRINTF (( "XmlEvent: Write(AdrOutputFormat) --> %d", *data ));
                // 0=RGB 1=Bayer
                if (outputImageMode==0 )outputPixelFormat=LvPixelFormat_RGB8;
                if (outputImageMode==1 )outputPixelFormat=LvPixelFormat_BayerGR8;
                // --> xml invalidates pixel format
            }
            else {
                *data=outputImageMode;
                PRINTF (( "XmlEvent: Read(AdrOutputFormat) -->: %d", *data ));
            }
        }

        if ( adr == AdrSensorDecimation ) {
            if ( Write ) {
                PRINTF (( "XmlEvent: Write(AdrSensorDecimation) --> %d", *data ));
                // xml enum: 1=div1, 2=div2, 4=div4
                sensorDecimation=*data;
                calculateOutputImgSize();   
                // --> xml invalidates width and height     
            }
            else {
                *data=sensorDecimation;
                PRINTF (( "XmlEvent: Read(AdrSensorDecimation) -->: %d", *data ));
            }
        }

        if ( adr == AdrExposureTime )  {
            if ( Write ) {
                exposureTime = *fdata;
                iamDevice.SetFloatCameraParam(LvDevice_ExposureTime,  exposureTime);
                iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &exposureTime);
                PRINTF (( "XmlEvent: Write(AdrExposureTime) --> %f (%f)", *fdata,exposureTime ));                
            }
            else {
                *fdata = exposureTime;
                PRINTF (( "XmlEvent: Read(AdrExposureTime) -->: %f", exposureTime ));
            }
        }        

        if ( adr == AdrAutoExposureMode ) {
            if ( Write ) {
                autoExposureMode = *data;
                PRINTF (( "XmlEvent: Write(AdrAutoExposureMode) --> %d", *data ));   
                if (autoExposureMode==0) {  //Off
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(SynviewStatus);  
                }           
                if (autoExposureMode==1) {  // Fronten AE
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Continuous);
                    ErrorOccurred(SynviewStatus);  
                }    
                if (autoExposureMode==2) { // App AE
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(SynviewStatus);  
                }      
                // get current exposure value from frontend
                SynviewStatus = iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &exposureTime);
                ErrorOccurred(SynviewStatus);     
            }
            else {
                *data = autoExposureMode;
                PRINTF (( "XmlEvent: Read(AdrAutoExposureMode) -->: %d", *data ));
            }
        }

        if ( adr == AdrAwb ) {  //WhiteBalanceAuto
            if ( Write ) {
                if (*data==1) {                    
                    LvFeature ftr;                    
                    iamDevice.m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "WhiteBalanceAuto", &ftr);
                    ErrorOccurred(SynviewStatus);  
                    iamDevice.m_pDevice->SetEnumStr(ftr,"Once");
                    ErrorOccurred(SynviewStatus);  
                    //iamDevice.m_pDevice->SetEnumStr(ftr,"Off");
                    //ErrorOccurred(SynviewStatus);  
                }               
            }
            else {
                *data=0; // done
            }
        }
        
        if ( adr == AdrEnableClassifier ) {
            if ( Write ) {
                enableClassifierFlag = *data;
                PRINTF (( "XmlEvent: Write(AdrEnableClassifier) --> %d", *data ));                  
            }
            else {
                *data=enableClassifierFlag;
                PRINTF (( "XmlEvent: Read(AdrEnableClassifier) -->: %d", *data ));
            }
        }

        if ( adr == AdrEnableOsd ) {
            if ( Write ) {
                enableOsdFlag = *data;                                
            }
            else {
                *data=enableOsdFlag;                
            }
        }

        if ( adr == AdrEnableTestPic ) {
            if ( Write ) {
                enableTestPicFlag = *data;                                
            }
            else {
                *data=enableTestPicFlag;                
            }
        }
        
        if ( adr == AdrTestPicIdx ) {
            if ( Write ) {
                loadTestImage = *data;   
                if (loadTestImage<1) loadTestImage=1;
                if (loadTestImage>(int)testImageFileNames.size()) {
                    loadTestImage = (int)testImageFileNames.size();
                }
                loadTestImageFlag=1;                             
            }
            else {
                *data=loadTestImage;                
            }
        }
        
        if ( adr == AdrTestPicNumber ) {
            if ( Write ) {
                // read only!                          
            }
            else {
                *data=(int)testImageFileNames.size();                
            }
        }
        
        if ( adr == AdrEnablePrintfDpuOut ) {
            if ( Write ) {
                enablePrintfDpuOutFlag = *data;                                
            }
            else {
                *data=enablePrintfDpuOutFlag;                
            }
        }

        if ( adr == AdrEnablePrintfPropOut ) {
            if ( Write ) {
                enablePrintfPropFlag = *data;                                
            }
            else {
                *data=enablePrintfPropFlag;                
            }
        }

        if ( adr == AdrEnablePrintfTime ) {
            if ( Write ) {
                enablePrintfTimeFlag = *data;                                
            }
            else {
                *data=enablePrintfTimeFlag;                
            }
        }  

        if ( adr == AdrSensorWidth ) // width of GigE-Server output image
        {
            if ( Write ) {                
                // read only in XML
            }
            else {
                *data=sensorWidth;
                PRINTF (( "XmlEvent: Read(AdrSensorWidth) -->: %d", *data ));                
            }
        }

        if ( adr == AdrSensorHeight ) // width of GigE-Server output image
        {
            if ( Write ) {
                // read only in XML
            }
            else {
                *data=sensorHeight;
                PRINTF (( "XmlEvent: Read(AdrSensorHeight) -->: %d", *data ));                
            }
        }

        if ( adr == AdrModelDirString )
        {
            if ( Write ) {
                // get the char pointer
                char* p = getCharPtr ( Param );

                // copy the string to local storage
                strncpy ( modelDirString, p, sizeof(modelDirString)-1 ); modelDirString[sizeof(modelDirString)-1] = 0;
                //PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Write AdrSmartString: \"%s\"", szString ));
            }
            else {
                // return the char pointer to local storage
                putCharPtr ( Param, modelDirString );
                //PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Read AdrSmartString: \"%s\"", szString ));
            }
        }

        if ( adr == AdrLoadModelCommand ) {  //load model command
            if ( Write ) {
                if (*data==1) {                    
                    doModelLoadFlag = true;
                }               
            }
            else {
                *data=0;//doModelLoadFlag; // done
            }
        }

        if ( adr == AdrSaveFrontendSettings ) {
            if ( Write ) {
                if (*data==1) {
                    printf ( "XmlEvent:Save frontend settings\n");                      
                    saveFrontendSettingsRequestFlag=1;                    
                }               
            }
            else {
                *data=0;//saveFrontendSettingsRequestFlag; // done
            }
        }

    }

    return;
}


int appClass::loadModel(std::string path) {

    printf( "---- Load model ----\n");

    if (!std::filesystem::is_directory(path.c_str())) {
        printf(COLOR_RED "Model directory not found!\n" COLOR_RESET);     
        return 0;       
    }
        
    // --- read model config file ---
    std::string conf_file_name=path;
    conf_file_name.append("/modelCfg.py");
    int ok = readCfgFile(conf_file_name);
    if (!ok) {
        return 0;   
    }
        
    // --- init DPU ---  
    // pick first *.xmodel file in default dir
    std::string ext(".xmodel");
    std::string elfFileName;
    for (auto &p : std::filesystem::recursive_directory_iterator(path)) {
        if (p.path().extension() == ext)
        elfFileName=p.path().stem().string();
    }
    std::string pathFnElf=path;
    pathFnElf.append("/");
    pathFnElf.append(elfFileName);
    pathFnElf.append(".xmodel");
                 
    if (FILE *file = fopen(pathFnElf.c_str(), "r")) {
        fclose(file);
        imgClassifier.configDpu(pathFnElf);
    } else {
        printf(COLOR_RED "DPU config file not found!\n" COLOR_RESET);    
        return 0;            
    }        
    
    return 1;
}




void appClass::readClassNames(const std::string filename, std::vector<std::string>& classNames)
{	
    classNames.clear();

	std::ifstream fp(filename);
	if (!fp.is_open())
	{		
		printf ("ERROR: Class names file not found\n");		
	}

	std::string name;
	while (!fp.eof())
	{
		std::getline(fp, name);
        if (name.back()=='\r') name.erase(name.length()-1,1); // remove cr
		if (name.length())
			classNames.push_back(name);
	}

	fp.close();	
}



int appClass::readCfgFile(const std::string filename) {
    FILE* fp;    
    char line[200];
    int  classIdxCounter=0;
    classNames.clear();

    printf("---- Parsing config file ----\n");
    fp = fopen(filename.c_str(), "r");
	if (fp == NULL) {
        printf("ERROR: Can not open config file\n");
        return 0;
    }

    while (!feof(fp)) {

		if (fgets(line, 100, fp) != NULL) {
            std::string lineStr=std::string(line);
			
            if (lineStr.compare(0,16,"inputScalingType")==0) {
                std::size_t found1 = lineStr.find("\'");            // search string start
                std::size_t found2 = lineStr.find("\'",found1+1,1); // search string end
                std::string labelTxt=lineStr.substr (found1+1,found2-found1-1);

                if (labelTxt.compare("none") == 0) {  
                    printf("inputScalingType: none\n");
                    imgClassifier.inputValueScalingMode=0;
                }  
                else if (labelTxt.compare("None") == 0) {  
                    printf("inputScalingType: none\n");
                    imgClassifier.inputValueScalingMode=0;
                }
                else if (labelTxt.compare("rgb1s") == 0) {  
                    printf("inputScalingType: rgb1s\n");
                    imgClassifier.inputValueScalingMode=1;
                }
                else if (labelTxt.compare("rgb01") == 0) {  
                    printf("inputScalingType: rgb01\n");
                    imgClassifier.inputValueScalingMode=2;
                }
                else if (labelTxt.compare("tf") == 0) {  
                    printf("inputScalingType: tf\n");
                    imgClassifier.inputValueScalingMode=3;
                }
                else if (labelTxt.compare("torch") == 0) {  
                    printf("inputScalingType: torch\n");
                    imgClassifier.inputValueScalingMode=4;
                }
                else if (labelTxt.compare("caffe") == 0) {  
                    printf("inputScalingType: caffe\n");
                    imgClassifier.inputValueScalingMode=5;
                } 
                else {
                    printf("inputScalingType: unknown\n");
                    imgClassifier.inputValueScalingMode=1;
                }
				continue;
			}
            
            if (lineStr.compare(0,10,"trainLabel")==0) {                
                std::size_t found1 = lineStr.find("\'");            // search string start
                std::size_t found2 = lineStr.find("\'",found1+1,1); // search string end
                trainLabel=lineStr.substr (found1+1,found2-found1-1);
                printf("trainLabel: %s\n",trainLabel.c_str());
            }
            
            if (lineStr.compare(0,10,"classLabel") == 0) {                
                std::size_t found1 = lineStr.find("\'");            // search string start
                std::size_t found2 = lineStr.find("\'",found1+1,1); // search string end
                std::string labelTxt=lineStr.substr (found1+1,found2-found1-1);
                printf("classLabel %d: %s\n",classIdxCounter, labelTxt.c_str());
                classNames.push_back(labelTxt);
                classIdxCounter++;
            }

	    }
	}

	printf("end of config file\n");
	fclose(fp);
    return 1;
}


int appClass::getDpuVersion() {
    FILE* fp;
    char line[200];
    int res=0;

    printf("---- Reading Dpu Config ----\n");
    fp = fopen("/etc/petalinux/product", "r");
	if (fp == NULL) {
        printf("ERROR: Can not open /etc/petalinux/product\n");
        fclose(fp);
        return 0;
    }

    if (fgets(line, 100, fp) != NULL) {           
        std::string lineStr=std::string(line);
        
        if (lineStr.compare(0,16,"iam_zu5_mipi_dpu")==0) {
            res=5;
            printf("zu5 id found\n");
        } else if (lineStr.compare(0,16,"iam_zu5_lvds_dpu")==0) {
            res=5;
            printf("zu5 id found\n");
        } else if (lineStr.compare(0,16,"iam_zu2_mipi_dpu")==0) {
            res=2;
            printf("zu2 id found\n");
        } else if (lineStr.compare(0,16,"iam_zu2_lvds_dpu")==0) {
            res=2;
            printf("zu2 id found\n");
		} else if (lineStr.compare(0,12,"iam_lvds_dpu")==0) {
            res=2;
            printf("zu2 id found\n");
        } else {
            printf("ERROR: unknown dpu id\n");
        }        
    } else {
        printf("gets error\n");        
    }

    fclose(fp);
    return res;
}

int appClass::getImageVersion(int *mainVersion, int *subVersion) {
    FILE* fp;
    char f1[200];
    char line[200];
    int res=0;

    printf("---- Reading  Image Version ----\n");
    fp = fopen("/etc/petalinux/version", "r");
	if (fp == NULL) {
        printf("ERROR: Can not open /etc/petalinux/version\n");
        fclose(fp);
        return 0;
    }

    if (fgets(line, 100, fp) != NULL) {
        int sres = sscanf(line, "%s", f1);        
        if (sres==1) {
            printf("Image version: %s\n",f1);
            
            int vres = sscanf(f1, "%i.%i",mainVersion, subVersion );     
            if (vres==2) {
                //printf("Main version: %i\n",*mainVersion);
                //printf("Sub  version: %i\n",*subVersion);
            } else {
                printf("Version format error\n");   
                *mainVersion = 0;
                *subVersion  = 0;  
            }
            
        } else {
            printf("sscanf error\n");            
        }
    } else {
        printf("gets error\n");        
    }

    fclose(fp);
    return res;
}


// -----------------------------------------------------------------------------
//                Callback function for each sensor frame
// -----------------------------------------------------------------------------

//   Sensor --> Debayer --> Decimate -----> 224x224 --> NN  
//   (Bayer)    (BGR)       (/1/2/4)   |
//   ---> OSD Overlay -->ReBayer(optional) --> send GigE image

void appClass::NewBufferCallback(LvBuffer* pBuffer)
{
    //PRINTF (( "appClass::CallbackNewBuffer [begin] (Buffer:%p)", pBuffer ));
    int AwaitDelivery;
    int NumUnderrunAct;

    if (autorun) printf("process image...\n");

    // buffer undefined
    if ( pBuffer == 0 )
    {
        PRINTF (( "CallbackNewBuffer: buffer pointer undefined [end]" ));
        return;
    }
    // event no longer defined
    if ( iamDevice.m_pEvent == NULL )
    {
        PRINTF (( "CallbackNewBuffer: event not defined! [end]" ));
        pBuffer->Queue();
        return;
    }

    // close event requested?
    if ( iamDevice.m_pEvent->CallbackMustExit () )
    {
        PRINTF (( "CallbackNewBuffer: exit requested! [end]" ));
        pBuffer->Queue();
        return;
    }

    // get buffer pool info
    if (iamDevice.m_pStream->GetInt32 ( LvStream_LvNumAwaitDelivery, &AwaitDelivery) != LVSTATUS_OK ) {
        PRINTF (( "appClass::ivQueryFrameGenTL: error get AwaitDelivery" ));
    }
    if (iamDevice.m_pStream->GetInt32 ( LvStream_LvNumUnderrun, &NumUnderrunAct) != LVSTATUS_OK ) {
        PRINTF (( "appClass::ivQueryFrameGenTL: error get NumUnderrun" ));
    }
    //PRINTF (( "appClass::ivQueryFrameGenTL: buffers in aquisition:%d underrun frames:%d aquiring flag:%d", AwaitDelivery, NumUnderrunAct-NumUnderrunLast, IsAcquiring() ));
    iamDevice.NumUnderrunLast = NumUnderrunAct;
    iamDevice.maxAquiredBuffers = std::max ( iamDevice.maxAquiredBuffers, AwaitDelivery );

    
    // -----------------------------
    // ---   Get image info      ---
    // -----------------------------

    LvipImgInfo ImgInfo;
    int err = pBuffer->GetImgInfo ( ImgInfo );       // depends on Uni Processing mode: if "Off" or "HwOnly", this returns the unprocessed buffer info
    if (err) {
        PRINTF (( "ERROR: CallbackNewBuffer::ivQueryFrameGenTL: Cannot retrieve image info" ));
        pBuffer->Queue();
        return;
    }
    
    uint8_t* pImageData = (uint8_t*)ImgInfo.pData; // get image data pointer
    int ImgWidth         = ImgInfo.Width;
    int ImgHeight        = ImgInfo.Height;
    //int ImgLinePitch     = ImgInfo.LinePitch;
    //int ImgBytesPerPixel = ImgInfo.BytesPerPixel;
    int ImgPixelFormat   = ImgInfo.PixelFormat;
    
    //PRINTF (( "CallbackNewBuffer:  Input: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgWidth, ImgHeight, ImgBytesPerPixel, ImgPixelFormat, pImageData ));
	
    // frontend is providing bayer data
    cv::Mat inPicMat = cv::Mat(cv::Size(ImgWidth, ImgHeight), CV_8U,(unsigned char*) pImageData); 
    
    // -----------------------------
    // --- Preprocessing         ---
    // -----------------------------
    cv::Mat inPicConv;  // debayered sensor image
    cv::Mat inPicRGB;	   // debayered and shrinked sensor image
	
    if (ImgPixelFormat==LvPixelFormat_Mono8) { // mono sensor       
        cvtColor( inPicMat, inPicConv, cv::COLOR_GRAY2RGB );   
    } else if (sensorPixelFormat == LvPixelFormat_BayerGR8) {
        cvtColor( inPicMat, inPicConv, cv::COLOR_BayerGR2BGR );             
    } else if (sensorPixelFormat == LvPixelFormat_BayerRG8) {
        cvtColor( inPicMat, inPicConv, cv::COLOR_BayerRG2BGR ); 
    } else if (sensorPixelFormat == LvPixelFormat_BayerGB8) {
        cvtColor( inPicMat, inPicConv, cv::COLOR_BayerGB2BGR ); 
    } else {
        cvtColor( inPicMat, inPicConv, cv::COLOR_BayerBG2BGR );         
    }
    // this produces RGB image,  opencv/synview bayer naming mismatch

    if (sensorDecimation==1) {         
        inPicRGB=inPicConv;
    }
    if (sensorDecimation==2) {         
        cv::resize(inPicConv,inPicRGB,cv::Size (ImgWidth/2,ImgHeight/2),cv::INTER_NEAREST );
    }
    if (sensorDecimation==4) {         
        cv::resize(inPicConv,inPicRGB,cv::Size (ImgWidth/4,ImgHeight/4),cv::INTER_NEAREST );
    }
    
       	
    // -----------------------------
    // --- Auto Exposure Control ---
    // -----------------------------
	if (autoExposureMode==2) {
        double ev;
        float newExpValue;

        cv::Scalar mv = cv::mean(inPicRGB);        
        ev=0.114*mv.val[0]+0.587*mv.val[1]+0.299*mv.val[2];  

        iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &newExpValue);                                  
        if ( ev>128) { // too bright
            if      ( ev>220) newExpValue/=1.3;
            else if ( ev>180) newExpValue/=1.15;
            else if ( ev>150) newExpValue/=1.06;            
        } else {        // too dark
            if      ( ev< 30) newExpValue*=1.3;
            else if ( ev< 50) newExpValue*=1.15;
            else if ( ev<100) newExpValue*=1.06;
        }
        if (newExpValue<   100) newExpValue=100;    // 100 us
        if (newExpValue>250000) newExpValue=250000; // 0.25 sec
        
        exposureTime=newExpValue;
        iamDevice.SetFloatCameraParam(LvDevice_ExposureTime,  newExpValue);
        PRINTF (( "AE: EV: %f --> Exposure: %f",ev,newExpValue ));     
	}

    //test pic ..
    if ((enableTestPicFlag)&&(testImageFileNames.size()>0)) {
        int xs=inPicRGB.cols;
        int ys=inPicRGB.rows;
        cv::resize(testPicRgb, inPicRGB, cv::Size(xs, ys), cv::INTER_NEAREST);
    }

    // -----------------------------
    // ---       Classify        ---
    // -----------------------------    
	int ok=0;
    if (enableClassifierFlag && modelOkFlag) {

        double tickCountClass1;
        double tickCountClass2;

        tickCountClass1 = (double) cv::getTickCount();
        ok=imgClassifier.procImg(inPicRGB);

		if (ok) {      
			if (enablePrintfDpuOutFlag) {
                printf("DPU output: ");
                if ((int)classNames.size()==2) { // binary classifier
                    printf("%f\n",imgClassifier.dpuResultBuffer[0]);
                } else {
                    for (int i=0; i<(int)classNames.size(); i++) {
                        printf("%f, ",imgClassifier.dpuResultBuffer[i]);
                    }
                    printf("...\n");
                }
            }

            if (enablePrintfPropFlag) {
                printf("prop vector: ");
                if ((int)classNames.size()==2) { // binary classifier
                    printf("%f\n",imgClassifier.activationsOutput[0]);
                } else {
                    for (int i=0; i<(int)classNames.size(); i++) {
                        printf("%f, ",imgClassifier.activationsOutput[i]);
                    }
                    printf("\n");   
                }
            }     

            if ((int)classNames.size()!=2) {               
                imgClassifier.topKSearch(kTopOsdNumber);      
            }     
        } else {
            printf("dpu fail on image %d x %d\n",inPicRGB.cols,inPicRGB.rows);            
        }

        tickCountClass2 = (double) cv::getTickCount();

        if (enablePrintfTimeFlag) {
            printf("dpu time %.2f ms (classifier %.2f ms) \n",1000.0*(imgClassifier.tickCountStop-imgClassifier.tickCountStart)/cv::getTickFrequency(),1000.0*(tickCountClass2-tickCountClass1)/cv::getTickFrequency());
        }
       
    }
    
    // -----------------------------
    // --- Generate Overlay  ---
    // -----------------------------
    if (enableClassifierFlag &&  enableOsdFlag) {
        int   fontFace = cv::FONT_HERSHEY_PLAIN;
        float fontScale = 4.0;  
        float fontScaleDyn;      
        cv::Size textSize;	
        int textThickness;        
        std::string ct;

        fontFace = cv::FONT_HERSHEY_DUPLEX;
        fontScale = 2.0;
        textThickness = 3;

        if (inPicRGB.rows<800) {
            fontScale = 1.2;
            textThickness = 1;
        }
        
        char charBuffer [10];
        
        cv::Size rectText = cv::getTextSize("XXX", fontFace, fontScale, textThickness, 0);
        int ypos=(3*rectText.height)/2;

	    if (ok) { 	
            if ((int)classNames.size()==2) { // binary classifier

                sprintf (charBuffer, "  (%3.2f)", imgClassifier.activationsOutput[0]);     

                if (imgClassifier.activationsOutput[0]>double(0.5)) {
                    ct=classNames.at(1);  
                } else {
                    ct=classNames.at(0);  
                }
                            
                ct.append(std::string(charBuffer));

                cv::String cto=cv::String(ct);
                int xpos=rectText.height;    
                
                if (xpos<inPicRGB.rows) {
                    cv::putText(inPicRGB, cto, cv::Point(xpos, ypos), fontFace, fontScale, cv::Scalar(255, 0, 0), textThickness, cv::LINE_AA, false);
                }  

            } else {
                for (int i=0; i<kTopOsdNumber; i++) {    

                    if (i==0) fontScaleDyn=fontScale;
                    else fontScaleDyn=(3*fontScale)/4;
                                    
                    sprintf (charBuffer, "  (%3.2f)", imgClassifier.topPropVector.at(i));                
                    ct=classNames.at(imgClassifier.topIdxVector.at(i));                 
                    ct.append(std::string(charBuffer));

                    cv::String cto=cv::String(ct);
                    int xpos=rectText.height;    
                    
                    if (xpos<inPicRGB.rows) {
                        cv::putText(inPicRGB, cto, cv::Point(xpos, ypos), fontFace, fontScaleDyn, cv::Scalar(255, 0, 0), textThickness, cv::LINE_AA, false);
                    }                

                    if (i==0) ypos += rectText.height*7/4;
                    else ypos += rectText.height*5/4;
                }
            }
	    } else {
            if (!modelOkFlag) {
                ct=std::string("no model loaded");
            } else {
                ct=std::string("classifier error");
            }
            cv::String cto=cv::String(ct);
            int xpos=rectText.height;    
                
            if (xpos<inPicRGB.rows) {
                cv::putText(inPicRGB, cto, cv::Point(xpos, ypos), fontFace, fontScale, cv::Scalar(255, 0, 0), textThickness, cv::LINE_AA, false);
            }                
        }       
    }
    
    // ------------------------------
    // --- Generate Output Image  ---
    // ------------------------------
    if (enableStreaming) {
        if (outputImageMode==0) {   // RGB image output mode             
            ImgInfo.Width=inPicRGB.cols;
            ImgInfo.Height=inPicRGB.rows;
            ImgInfo.LinePitch=inPicRGB.cols*3;
            ImgInfo.PixelFormat=outputPixelFormat;
            ImgInfo.BytesPerPixel=3;
            ImgInfo.pData=inPicRGB.ptr();    
            //PRINTF (( "Sending GigE img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgInfo.Width, ImgInfo.Height, ImgInfo.BytesPerPixel, ImgInfo.PixelFormat, ImgInfo.pData ));	    
            SendImage ( &ImgInfo ); // send image via gige    
        } else {                    // Bayer image output mode        
            cv::Mat bayerImg=ConvertRGB2BayerGR(inPicRGB);
            ImgInfo.Width=inPicRGB.cols;
            ImgInfo.Height=inPicRGB.rows;
            ImgInfo.LinePitch=inPicRGB.cols;
            ImgInfo.PixelFormat=outputPixelFormat;
            ImgInfo.BytesPerPixel=1;
            ImgInfo.pData=bayerImg.ptr();   
            //PRINTF (( "Sending GigE img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgInfo.Width, ImgInfo.Height, ImgInfo.BytesPerPixel, ImgInfo.PixelFormat, ImgInfo.pData ));	    
            SendImage ( &ImgInfo ); // send image via gige               
        }    
    }
    pBuffer->Queue();       // put buffer back into pool

    return;
}



