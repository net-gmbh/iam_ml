
/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

 #define GLOBAL_ENABLE_NEW_IOU 1


#include "dpuObjDetYoloV4.h"
#include "main.h"



static inline std::array<int64_t, 2> letterbox_dim(int img_h, int img_w, int h, int w ) {
    
    auto s = std::min(1.0f * w / img_w, 1.0f * h / img_h);
    return std::array{int64_t(img_h * s), int64_t(img_w * s)};
}

static inline cv::Mat cv_letterbox_img(const cv::Mat &img, int h, int w ) { // h,w: model input dims
    auto[new_h, new_w] = letterbox_dim(img.rows, img.cols, h, w);

    cv::Mat out = cv::Mat(h, w, CV_8UC3,cv::Scalar(128, 128, 128)); 

    cv::resize(img,
               out({int((h - new_h) / 2), int((h - new_h) / 2 + new_h)},
                   {int((w - new_w) / 2), int((w - new_w) / 2 + new_w)}),
               {int(new_w), int(new_h)},
               0, 0, cv::INTER_LINEAR);
    return out;
}





static float sigmoid(float p) { return 1.0 / (1 + exp(-p * 1.0)); }

static float overlap(float x1, float w1, float x2, float w2) {
  float left = max(x1 - w1 / 2.0, x2 - w2 / 2.0);
  float right = min(x1 + w1 / 2.0, x2 + w2 / 2.0);
  return right - left;
}

static float cal_iou(vector<float> box, vector<float> truth) {
  float w = overlap(box[0], box[2], truth[0], truth[2]);
  float h = overlap(box[1], box[3], truth[1], truth[3]);
  if (w < 0 || h < 0) return 0;

  float inter_area = w * h;
  float union_area = box[2] * box[3] + truth[2] * truth[3] - inter_area;
  return inter_area * 1.0 / union_area;
}


static float box_c(vector<float> a, vector<float> b) {
  float top = min(a[1] - a[3] / 2.0, b[1] - b[3] / 2.0);
  float bot = max(a[1] + a[3] / 2.0, b[1] + b[3] / 2.0);
  float left = min(a[0] - a[2] / 2.0, b[0] - b[2] / 2.0);
  float right = max(a[0] + a[2] / 2.0, b[0] + b[2] / 2.0);

  float res = (bot - top) * (bot - top) + (right - left) * (right - left);
  return res;
}



static float cal_new_iou(vector<float> box, vector<float> truth) {
  float c = box_c(box, truth);

  float w = overlap(box[0], box[2], truth[0], truth[2]);
  float h = overlap(box[1], box[3], truth[1], truth[3]);
  if (w < 0 || h < 0) return 0;
  float inter_area = w * h;
  float union_area = box[2] * box[3] + truth[2] * truth[3] - inter_area;
  float iou = inter_area * 1.0 / union_area;
  if (c == 0) return iou;

  float d = (box[0] - truth[0]) * (box[0] - truth[0]) +
            (box[1] - truth[1]) * (box[1] - truth[1]);
  float u = pow(d / c, 0.6);
  return iou - u;
}






void dpuObjDetYoloV4Class::correct_region_boxes(vector<vector<float>>& boxes, int n, int w, int h,  // w,h orig image size
                          int netw, int neth, int relative = 0) {                                    
  int new_w = 0;
  int new_h = 0;

  if (((float)netw / w) < ((float)neth / h)) {
    new_w = netw;
    new_h = (h * netw) / w;
  } else {
    new_h = neth;
    new_w = (w * neth) / h;
  }
  for (int i = 0; i < n; ++i) {
    boxes[i][0] = (boxes[i][0] - (netw - new_w) / 2. / netw) /
                  ((float)new_w / (float)netw);
    boxes[i][1] = (boxes[i][1] - (neth - new_h) / 2. / neth) /
                  ((float)new_h / (float)neth);
    boxes[i][2] *= (float)netw / new_w;
    boxes[i][3] *= (float)neth / new_h;
  }
}





void dpuObjDetYoloV4Class::detect(vector<vector<float>>& boxes, int channel,
            int gridHeight, int gridWidth, int gridIdx, float scale) { 
    
  int elementsPerBox = 5 + classificationCnt; // x,y,w,h,obj,p(0)....p(numClass-1)
  int numAnchors = channel/elementsPerBox;

  int sizeOut = channel * gridWidth * gridHeight;
  vector<float> result(sizeOut);    

  // Store every output node results 
  
  int idx=0;
  for (int c = 0; c < channel; ++c) {
    for (int h = 0; h < gridHeight; ++h) {
      for (int w = 0; w < gridWidth; ++w) {
        int offset = channel * (h *  gridWidth + w ) + c;        
        result[idx++] = results[gridIdx][offset];
      }
    }
  }



  float swap[gridHeight * gridWidth][numAnchors][elementsPerBox];
  
  float confLim = confObjMin;
  if (confLim<float(0.01)) confLim=float(0.01);

  auto conf_desigmoid = -logf(1.0f / confLim - 1.0f);

  for (int h = 0; h < gridHeight; ++h) {
    for (int w = 0; w < gridWidth; ++w) {
      for (int c = 0; c < channel; ++c) {
        int temp = c * gridHeight * gridWidth + h * gridWidth + w;
        swap[h * gridWidth + w][c / elementsPerBox][c % elementsPerBox] = result[temp];        
      }
    }
  }

  




  for (int h = 0; h < gridHeight; ++h) {
    for (int w = 0; w < gridWidth; ++w) {
      for (int c = 0; c < numAnchors; ++c) {
        
        if (int(boxes.size())<numBoxesLimit) {  // dc safety feature

          
          float obj_score_raw = swap[h * gridWidth + w][c][4] * scale;;
          if (obj_score_raw  < conf_desigmoid) continue;
          float obj_score = sigmoid( obj_score_raw );
          

          vector<float> box;

          box.push_back((w + sigmoid(swap[h * gridWidth + w][c][0] * scale)) / gridWidth);  // dc x
          box.push_back((h + sigmoid(swap[h * gridWidth + w][c][1] * scale)) / gridHeight); // dc y

          
          box.push_back(exp(swap[h * gridWidth + w][c][2] * scale) *
                          anchorBiases[2 * c + 2*numAnchors * gridIdx] / float(widthNet));         // dc w
          box.push_back(exp(swap[h * gridWidth + w][c][3] * scale) *
                          anchorBiases[2 * c + 2*numAnchors * gridIdx + 1] / float(heightNet));    // dc h
          

          box.push_back(-1); // space holder for detected class
          box.push_back(obj_score);
          for (int p = 0; p < classificationCnt; p++) {
            box.push_back(obj_score * sigmoid(swap[h * gridWidth + w][c][5 + p] * scale));
          }
         
          boxes.push_back(box);
          
        }
       
      }
    }
  }
}


vector<vector<float>> dpuObjDetYoloV4Class::applyNMS(vector<vector<float>>& boxes, int classes,
                               const float thres) {
  vector<pair<int, float>> order(boxes.size());
  vector<vector<float>> result;

  for (int k = 0; k < classes; k++) {
    for (size_t i = 0; i < boxes.size(); ++i) {
      order[i].first = i;
      boxes[i][4] = k; 
      order[i].second = boxes[i][6 + k];
    }
    sort(order.begin(), order.end(),
         [](const pair<int, float>& ls, const pair<int, float>& rs) {
           return ls.second > rs.second;
         });

    vector<bool> exist_box(boxes.size(), true);

    for (size_t _i = 0; _i < boxes.size(); ++_i) {
      size_t i = order[_i].first;
      if (!exist_box[i]) continue;
      if (boxes[i][6 + k] < confClassMin) {
        exist_box[i] = false;
        continue;
      }
      
      result.push_back(boxes[i]);

      for (size_t _j = _i + 1; _j < boxes.size(); ++_j) {
        size_t j = order[_j].first;
        if (!exist_box[j]) continue;
		
		
        float ovr = cal_iou(boxes[j], boxes[i]);
		/*float ovr = 0.0;
		if (GLOBAL_ENABLE_NEW_IOU == 0) {
			ovr = cal_iou(boxes[j], boxes[i]);
		} else {
			ovr = cal_new_iou(boxes[j], boxes[i]);
		}*/
	  	  
        if (ovr >= thres) exist_box[j] = false;
      }
    }
  }

  return result;
}


void dpuObjDetYoloV4Class::applyNMS_vialib(const vector<vector<float>>& boxes, const vector<float>& scores,
              const float nms, vector<size_t>& res,
              bool stable) {
  const size_t count = boxes.size();
  vector<pair<float, size_t>> order;
  for (size_t i = 0; i < count; ++i) {
    order.push_back({scores[i], i});
  }
  if (stable) {
    stable_sort(
        order.begin(), order.end(),
        [](const pair<float, size_t>& ls, const pair<float, size_t>& rs) {
          return ls.first > rs.first;
        });
  } else {
    sort(order.begin(), order.end(),
         [](const pair<float, size_t>& ls, const pair<float, size_t>& rs) {
           return ls.first > rs.first;
         });
  }
  vector<size_t> ordered;
  transform(order.begin(), order.end(), back_inserter(ordered),
            [](auto& km) { return km.second; });
  vector<bool> exist_box(count, true);

  for (size_t _i = 0; _i < count; ++_i) {
    size_t i = ordered[_i];
    if (!exist_box[i]) continue;
    if (scores[i] < confClassMin) {
      exist_box[i] = false;
      continue;
    }
    /* add a box as result */
    res.push_back(i);
    // cout << "nms push "<< i<<endl;
    for (size_t _j = _i + 1; _j < count; ++_j) {
      size_t j = ordered[_j];
      if (!exist_box[j]) continue;
      float ovr = 0.0;
      if (GLOBAL_ENABLE_NEW_IOU == 0) {
        ovr = cal_iou(boxes[j], boxes[i]);
      } else {
        ovr = cal_new_iou(boxes[j], boxes[i]);
      }
      if (ovr >= nms) exist_box[j] = false;
    }
  }
}



void dpuObjDetYoloV4Class::get_output(int8_t* dpuOut, int sizeOut, int oc, int oh, int ow,
                float output_scale, vector<float>& result) {
  // vector<int8_t> nums(sizeOut);
  // memcpy(nums.data(), dpuOut, sizeOut);
  for (int a = 0; a < oc; ++a) {
    for (int b = 0; b < oh; ++b) {
      for (int c = 0; c < ow; ++c) {
        int offset = b * oc * ow + c * oc + a;
        result[a * oh * ow + b * ow + c] = dpuOut[offset] * output_scale;
      }
    }
  }
}



void dpuObjDetYoloV4Class::postProcess(cv::Mat& frame, vector<int8_t*> results,
                  const float* output_scale) {           
  
  vector<vector<float>> boxes;
  boxes.reserve(numBoxesLimit);
  
  for (int ii = 0; ii < outputCnt; ii++) {
    int gridWidth = shapes.outTensorList[ii].width;
    int gridHeight = shapes.outTensorList[ii].height;
    int channel = shapes.outTensorList[ii].channel;
    //int sizeOut = channel * gridWidth * gridHeight;
    
    // Store the object detection frames as coordinate information  
    detect(boxes, channel, gridHeight, gridWidth, ii, output_scale[ii]);

    if (enablePrintfDpuFlag) {
      printf("grid %d:\n",ii);
      printf("  width:    %d\n", gridWidth); 
      printf("  height:   %d\n", gridHeight);
      printf("  channels: %d\n", channel);
      printf("  detected: %d\n", int(boxes.size()));
    }
  }

  // --- Restore the correct coordinate frame of the original image --- 
  correct_region_boxes(boxes, boxes.size(), frame.cols, frame.rows, widthNet, heightNet);

  // --- Apply NMS --- 

  vector<vector<float>> res;
  if (!nmsVialib) {  
    // vart demo style
    res = applyNMS(boxes, classificationCnt, NMS_THRESHOLD);
  } else {  
    // vitis ai library style    
    vector<float> scores(boxes.size());
    for (int k = 0; k < classificationCnt; k++) {
        transform(boxes.begin(), boxes.end(), scores.begin(), [k](auto& box) {
          box[4] = k;
          return box[6 + k];
        });
        vector<size_t> result_k;
        applyNMS_vialib(boxes, scores, NMS_THRESHOLD,  result_k,false);
        transform(result_k.begin(), result_k.end(), back_inserter(res),
                  [&boxes](auto& k) { return boxes[k]; });
    }
  }
  

  float fontScale=0.5;
  if (frame.cols>1000) fontScale=1.0;

  float h = frame.rows;
  float w = frame.cols;
  for (size_t i = 0; i < res.size(); ++i) {
    float xmin = (res[i][0] - res[i][2] / 2.0) * w + 1.0; // dc: +1??
    float ymin = (res[i][1] - res[i][3] / 2.0) * h + 1.0;
    float xmax = (res[i][0] + res[i][2] / 2.0) * w + 1.0;
    float ymax = (res[i][1] + res[i][3] / 2.0) * h + 1.0;

    int classId = res[i][4]; 

    if (res[i][6 + classId] > confClassMin) { 
      
      //string classname = classes[type];
      //int colId=res[i][6 + classificationCnt];
      int colId=classId;

      cv::Scalar col; //testmode grid id-->color
      if      (colId==0) col=cv::Scalar(255,    0,   0); // R
      else if (colId==1) col=cv::Scalar(  0,  255,   0); // G
      else if (colId==2) col=cv::Scalar(  0,    0, 255); // B
      else if (colId==3) col=cv::Scalar(255,    0, 255); // M
      else if (colId==4) col=cv::Scalar(  0,  255, 255); // C
      else               col=cv::Scalar(255,  255,   0); // Y

      
      rectangle(frame, cv::Point(xmin, ymin), cv::Point(xmax, ymax),
                  col, 1, 1, 0);

      if (classId<int(classNames.size())) {
        cv::putText(frame, classNames[classId], cv::Point(xmin, ymax), cv::FONT_HERSHEY_DUPLEX, fontScale, col, 1, cv::LINE_AA, false);
      }
      
    }
  }
}






dpuObjDetYoloV4Class::dpuObjDetYoloV4Class()  {
	configOkFlag = 0;  
  NMS_THRESHOLD = 0.3;
  confObjMin=0.6;
  confClassMin=0.6;
}


dpuObjDetYoloV4Class::~dpuObjDetYoloV4Class()
{    
}



int dpuObjDetYoloV4Class::configDpu() {    

  runner = vart::Runner::create_runner(subgraph[0], "run");

  auto myInputTensors = runner->get_input_tensors();
  auto myOutputTensors = runner->get_output_tensors();

  inputCnt = myInputTensors.size();
  outputCnt = myOutputTensors.size();

  inputsPtr.clear(); 
  outputsPtr.clear();
  inputs.clear();
  outputs.clear();
  inputTensors.clear();
  outputTensors.clear();
  results.clear();
  if (   data!=0) delete[]    data;
  if (result0!=0) delete[] result0;
  if (result1!=0) delete[] result1;
  if (result2!=0) delete[] result2;

  inputs.reserve(inputCnt);
  inputsPtr.reserve(inputCnt);
  outputs.reserve(outputCnt);
  outputsPtr.reserve(outputCnt);
   
  shapes.inTensorList = inshapes;
  shapes.outTensorList = outshapes;
  

  auto out_tensors = runner->get_output_tensors();
  printf("   model output tensors:\n");
  for (auto i = 0u; i < out_tensors.size(); ++i) {
    printf("      %s\n",out_tensors[i]->get_name().c_str());      
  }
  printf("   outputCnt: %d\n",outputCnt);

  auto in_tensors = runner->get_input_tensors();
  printf("   model input tensors:\n");
  for (auto i = 0u; i < in_tensors.size(); ++i) {
    printf("      %s\n",in_tensors[i]->get_name().c_str());      
  }
  printf("   inputCnt: %d\n",inputCnt);

  
    
    //set layer names directly...
    //getTensorShape(runner.get(), &shapes, inputCnt, 
    //    {"layer133-conv_fixed", "layer144-conv_fixed", "layer155-conv_fixed"});
    
  getTensorShape(runner.get(), &shapes, inputCnt, 
        {out_tensors[0]->get_name().c_str(), out_tensors[1]->get_name().c_str(), out_tensors[2]->get_name().c_str()});


    
  classificationCnt = int(classNames.size());
   
    
  nmsVialib = false;
  numBoxesLimit = 4000;
    

    //  dk_yolov3_voc_416_416_65.42G_2.0
    /*getTensorShape(runner.get(), &shapes, inputCnt, 
        {"layer81-conv_fixed", "layer93-conv_fixed", "layer105-conv_fixed"});
    classificationCnt = 20; // num classes
    anchorCnt = 3;
      // conf=0.3
      // nms=0.45
     
    anchorBiases = {  10, 13,  16,  30,  33,  23,
                30, 61,  62,  45,  59, 119,
               116, 90, 156, 198, 373, 326};    
    
    nmsVialib = true;
    CONF=0.3;
    NMS_THRESHOLD = 0.45;
*/
     
  inputTensors = cloneTensorBuffer(runner->get_input_tensors());
  widthNet  = shapes.inTensorList[0].width;
  heightNet = shapes.inTensorList[0].height;
  outputTensors = cloneTensorBuffer(runner->get_output_tensors());

  input_scale = get_input_scale(runner->get_input_tensors()[0]);
  printf("   input_scale: %f\n",input_scale);


  output_scale.clear();
  for (int i=0; i < outputCnt; i++) { 
      output_scale.push_back(get_output_scale(
      runner->get_output_tensors()[shapes.output_mapping[i]]));
  }

  printf("   output_scale[0]: %f\n", output_scale[0]);
  printf("   output_scale[1]: %f\n", output_scale[1]);
  printf("   output_scale[2]: %f\n", output_scale[2]);    

  printf("   outTensorList[0].size: %d\n", shapes.outTensorList[0].size);
  printf("   outTensorList[1].size: %d\n", shapes.outTensorList[1].size);
  printf("   outTensorList[2].size: %d\n", shapes.outTensorList[2].size);
   
  // input/output data define
  data = new int8_t[shapes.inTensorList[0].size *
                              inputTensors[0]->get_shape().at(0)];

  result0 = new int8_t[shapes.outTensorList[0].size *
                     outputTensors[shapes.output_mapping[0]]->get_shape().at(0)];
  result1 = new int8_t[shapes.outTensorList[1].size *
                     outputTensors[shapes.output_mapping[1]]->get_shape().at(0)];
  result2 = new int8_t[shapes.outTensorList[2].size *
                     outputTensors[shapes.output_mapping[2]]->get_shape().at(0)];
    
  results.push_back(result0);
  results.push_back(result1);
  results.push_back(result2);
    

	configOkFlag = 1;

	return 1;
}

int dpuObjDetYoloV4Class::cleanup() {
  delete[] data;
  delete[] result0;
  delete[] result1;
  delete[] result2;   
	return 1;
}




// -------------------------------------------
//               Process Image
// -------------------------------------------
// img RGB image 0...255

int dpuObjDetYoloV4Class::procImg(cv::Mat img) {

    // store local parameter copies for this frame
    confClassMin  = confClassThres;
    confObjMin    = confObjThres;
    NMS_THRESHOLD = nmsThres;

    cv::Mat imgLb = cv_letterbox_img(img, heightNet, widthNet );

    float sf = input_scale / float(256.);
         
    int idx=0;
    for (int h = 0; h < heightNet; ++h) {
      for (int w = 0; w < widthNet; ++w) {
        for (int c = 0; c < 3; ++c) {
          data[idx++] = float(imgLb.at<cv::Vec3b>(h, w)[c]) * sf; 
        }
      }
    }

    
    inputsPtr.clear();
    outputsPtr.clear();
    inputs.clear();
    outputs.clear();


    // input/output tensorbuffer prepare
    inputs.push_back(
            std::make_unique<CpuFlatTensorBuffer>(data, inputTensors[0].get()));

    outputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
            result0, outputTensors[shapes.output_mapping[0]].get()));
    outputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
            result1, outputTensors[shapes.output_mapping[1]].get()));
    outputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
            result2, outputTensors[shapes.output_mapping[2]].get()));
   
    inputsPtr.push_back(inputs[0].get());

    outputsPtr.resize( outputCnt); 
    outputsPtr[shapes.output_mapping[0]] = outputs[0].get();
    outputsPtr[shapes.output_mapping[1]] = outputs[1].get();
    outputsPtr[shapes.output_mapping[2]] = outputs[2].get();

    // execute model on DPU
    auto job_id = runner->execute_async(inputsPtr, outputsPtr);
    runner->wait(job_id.first, -1);


    postProcess(img, results, output_scale.data());

	return 1;
}



