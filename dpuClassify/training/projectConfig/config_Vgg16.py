#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------
#           Configuration file for classifier training
# ----------------------------------------------------------------

import os


# -------------------------------------------
#            Model Configuration
# -------------------------------------------

MODEL_TYPE   = 'vgg16'   # vgg16, incV3, dense121, res50V2
IMAGE_WIDTH  = 224
IMAGE_HEIGHT = 224
DENSE1_NUM = 1024
INIT_WEIGHTS = 'imagenet'  # 'imagenet', None or h5 weight file name
NUMBER_OF_CLASSES = 26




# -------------------------------------------
#           Training Configuration
# -------------------------------------------

# --- train/validation sample split ---
SPLIT_SEED = 1204
SPLIT_VALIDATION_RATIO = 0.2

# --- keras augmentation settings ---
USE_AUGMENTATION       = True
RANDOM_FLIP_HORIZONTAL = True
RANDOM_FLIP_VERTICAL   = False
RANDOM_CONTRAST        = (0.5, 0.0)
RANDOM_ZOOM            = (-0.1, 0.1)
RANDOM_ROTATION        = (-0.5, 0.5)     

# --- keras generator config ---
BATCH_SIZE = 20
CACHE_DATASET = True

# --- saving of weight files ---
SAVE_BEST_MODEL_ONLY = True
MODEL_TRAINED_FILE_NAME  = 'model_trained.h5'

# --- diagram generation during training ---
DIAGRAM_UPDATE_RATE = 5
DIAGRAM_FORMAT  = 'png'
DIAGRAM_ZOOM_EPOCHS = 10


# --- sequence of training steps ---
EPOCHS              = [   16,     20,     40,      40,      10,       36] 
TRAINABLE_LAYERS    = [    3,      3,      3,       8,       8,       23]
LEARNING_RATE       = [0.001, 0.0005, 0.0003, 0.00002, 0.00001, 0.000005]
DENSE1_DROPOUT_RATE = [  0.5,    0.5,    0.5,     0.5,     0.5,      0.5]




# -----------------------------------------
#           VitisAi Tool Settings
# -----------------------------------------

# --- quantizer ---
VAI_CALIB_NUMBER_OF_SAMPLES = 100 # number of samples evaluated by quantizer
VAI_CALIB_BATCH_SIZE = 1 
VAI_QUANT_MODE = 'normal' # 'normal' or 'fine'

# --- verify quantization ---
VAI_VERIFY_FLOAT_MODEL = True
VAI_VERIFY_QUANT_MODEL = True
VAI_VERIFY_BATCH_SIZE = 32
VAI_VERIFY_DATASET_FRACTION = 1.0

# --- compiler ---
VAI_TARGET_DEVICE = 'zu5'
VAI_FINAL_MODEL_FILE_NAME = 'model_class_fruit26_vgg16_zu5'





