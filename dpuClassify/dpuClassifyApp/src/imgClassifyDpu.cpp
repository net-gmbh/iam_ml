
/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#include "imgClassifyDpu.h"


imgClassifyClass::imgClassifyClass()  {		
	configOkFlag = 0;
    dpuInputBufferBytesAllocated = 0;
	dpuResultBufferBytesAllocated = 0;
    inputValueScalingMode = 1;
}


imgClassifyClass::~imgClassifyClass()
{    
}


int imgClassifyClass::configDpu(const std::string filename) {
    printf("Loading DPU model file %s\n",filename.c_str());
    //printf("Filename: %s\n",filename.c_str());

    if (FILE *file = fopen(filename.c_str(), "r")) {
        fclose(file);        
    } else {
        printf("ERROR: DPU model file not found\n");
        configOkFlag = 0;
        return 0;
    }
    
    graph = xir::Graph::deserialize(filename);

    subgraph = get_dpu_subgraph(graph.get());
    runner = vart::Runner::create_runner(subgraph[0], "run");

    inputTensors = runner->get_input_tensors();
    outputTensors = runner->get_output_tensors(); 

    //out_dims = outputTensors[0]->get_dims();
    //in_dims = inputTensors[0]->get_dims();
    out_dims = outputTensors[0]->get_shape();  // VitisAi1.4
    in_dims = inputTensors[0]->get_shape();    // VitisAi1.4

    printf("input dims: ");
    for (int i=0; i<(int)in_dims.size(); i++)
        printf("%d ",in_dims[i]);
    printf("\n");

    printf("output dims: ");
    for (int i=0; i<(int)out_dims.size(); i++)
        printf("%d ",out_dims[i]);
    printf("\n");


    netOutDim=out_dims[3];
    dpuResSize=netOutDim;

    if (dpuResultBufferBytesAllocated!=dpuResSize) {
        if (dpuResultBufferBytesAllocated>0) {
            delete(dpuResultBuffer);
            delete(activationsOutput);
        }
        dpuResultBuffer = new float[dpuResSize];
        activationsOutput = new float[dpuResSize];
        dpuResultBufferBytesAllocated = dpuResSize;
    }

    int inSize = in_dims[1]*in_dims[2]*in_dims[3]; // x,y,c
    if (dpuInputBufferBytesAllocated!=inSize) {
        if (dpuInputBufferBytesAllocated>0) {
            delete(dpuInputBuffer);
        }
        dpuInputBuffer = new float[inSize];
        dpuInputBufferBytesAllocated = inSize;
    }

    image2 = cv::Mat(in_dims[1], in_dims[2], CV_8SC3);

    in_dims[0] = 1;
    out_dims[0] = 1;

    
	
	configOkFlag = 1;

	return 1;
}

int imgClassifyClass::cleanup() {
	if (dpuResultBufferBytesAllocated>0) {
        delete(dpuResultBuffer);
        delete(activationsOutput);
    }

    if (dpuInputBufferBytesAllocated>0) {
        delete(dpuInputBuffer);
    }

	return 1;
}


void imgClassifyClass::calcSoftmax() {
  
  // substract dmax from dpu values, in order to avoid nummeric exp function problems
  double dmax= double(dpuResultBuffer[0]);
  for (int i = 1; i < netOutDim; i++) {
      if (dpuResultBuffer[i]>dmax) dmax=double(dpuResultBuffer[i]);
  }
  
  double sum = 0.0f;
  for (int i = 0; i < netOutDim; i++) {
    activationsOutput[i] = exp(double(dpuResultBuffer[i])-dmax);
    sum += activationsOutput[i];
  }
  for (int i = 0; i < netOutDim; i++) {
    activationsOutput[i] /= sum;
  }
  
}


void imgClassifyClass::TopK(const float* d, int size, int k) {
  assert(d && size > 0 && k > 0);
  priority_queue<pair<float, int>> q;

  for (auto i = 0; i < size; ++i) {
    q.push(pair<float, int>(d[i], i));
  }

  for (auto i = 0; i < k; ++i) {
    pair<float, int> ki = q.top();
    printf("top[%d] class %d prob = %-8f\n", i, ki.second, d[ki.second]);
    q.pop();
  }
}

// -------------------------------------------
//               Classify Image
// -------------------------------------------
// img RGB image 0...255

int imgClassifyClass::procImg(cv::Mat img) {	

    // --- get in/out tensor shape ---
    int inputCnt = inputTensors.size();
	int outputCnt = outputTensors.size();
	TensorShape inshapes[inputCnt];
	TensorShape outshapes[outputCnt];
	shapes.inTensorList = inshapes;
	shapes.outTensorList = outshapes;
	getTensorShape(runner.get(), &shapes, inputCnt, outputCnt);  	
  	int inSize = shapes.inTensorList[0].size;
  	int inHeight = shapes.inTensorList[0].height;
  	int inWidth = shapes.inTensorList[0].width;

    if ((dpuResultBufferBytesAllocated<dpuResSize)||(dpuInputBufferBytesAllocated<inSize)) {
        printf("alloc error\n");
        return 0;    
    } 
        
    // --- resize image ---
    cv::resize(img, image2, cv::Size(inHeight, inWidth), 0, 0, cv::INTER_NEAREST);
       
    // --- value scaling ---
    if (inputValueScalingMode==0) {   // 'none'
        for (int h = 0; h < inHeight; h++) {
            for (int w = 0; w < inWidth; w++) {
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 0] = float(image2.at<cv::Vec3b>(h, w)[0]);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 1] = float(image2.at<cv::Vec3b>(h, w)[1]);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 2] = float(image2.at<cv::Vec3b>(h, w)[2]);
            }
        }
    }

    if (inputValueScalingMode==1) {   // 'rgb1s'
        for (int h = 0; h < inHeight; h++) {
            for (int w = 0; w < inWidth; w++) {
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 0] = float(image2.at<cv::Vec3b>(h, w)[0])/float(127.5)  - float(1.0);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 1] = float(image2.at<cv::Vec3b>(h, w)[1])/float(127.5)  - float(1.0);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 2] = float(image2.at<cv::Vec3b>(h, w)[2])/float(127.5)  - float(1.0);
            }
        }
    }

    if (inputValueScalingMode==2) {   // 'rgb01'
        for (int h = 0; h < inHeight; h++) {
            for (int w = 0; w < inWidth; w++) {
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 0] = float(image2.at<cv::Vec3b>(h, w)[0])/float(255.0);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 1] = float(image2.at<cv::Vec3b>(h, w)[1])/float(255.0);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 2] = float(image2.at<cv::Vec3b>(h, w)[2])/float(255.0);
            }
        }
    }

    if (inputValueScalingMode==3) {   // 'tf'
        for (int h = 0; h < inHeight; h++) {
            for (int w = 0; w < inWidth; w++) {
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 0] = float(image2.at<cv::Vec3b>(h, w)[0])/float(127.5)  - float(1.0);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 1] = float(image2.at<cv::Vec3b>(h, w)[1])/float(127.5)  - float(1.0);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 2] = float(image2.at<cv::Vec3b>(h, w)[2])/float(127.5)  - float(1.0);
            }
        }
    }
    
    if (inputValueScalingMode==4) {   // 'torch'
        for (int h = 0; h < inHeight; h++) {
            for (int w = 0; w < inWidth; w++) {
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 0] = (float(image2.at<cv::Vec3b>(h, w)[0])/float(255.0)  - float(0.485))/float(0.229);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 1] = (float(image2.at<cv::Vec3b>(h, w)[1])/float(255.0)  - float(0.456))/float(0.224);
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 2] = (float(image2.at<cv::Vec3b>(h, w)[2])/float(255.0)  - float(0.406))/float(0.225);
            }
        }
    }
   
    if (inputValueScalingMode==5) { // 'caffe'
        for (int h = 0; h < inHeight; h++) {
            for (int w = 0; w < inWidth; w++) {
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 0] = (float(image2.at<cv::Vec3b>(h, w)[2])  - float(103.939));
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 1] = (float(image2.at<cv::Vec3b>(h, w)[1])  - float(116.779));
                    dpuInputBuffer[ h * inWidth * 3 + w * 3 + 2] = (float(image2.at<cv::Vec3b>(h, w)[0])  - float(123.68 ));
            }
        }
    }
    
    // --- prepare DPU data ---
    batchTensors.push_back(std::shared_ptr<xir::Tensor>(
        xir::Tensor::create(inputTensors[0]->get_name(), in_dims,
                            //xir::DataType::FLOAT, sizeof(float) * 8u)));
                            xir::DataType{xir::DataType::FLOAT, 8u})));  // VitisAi1.4



    inputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
        dpuInputBuffer, batchTensors.back().get()));


    batchTensors.push_back(std::shared_ptr<xir::Tensor>(
        xir::Tensor::create(outputTensors[0]->get_name(), out_dims,
                            //xir::DataType::FLOAT, sizeof(float) * 8u)));
                            xir::DataType{xir::DataType::FLOAT, 8u})));  // VitisAi1.4


    outputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
        dpuResultBuffer, batchTensors.back().get()));
    
    inputsPtr.clear();
    outputsPtr.clear();
    inputsPtr.push_back(inputs[0].get());
    outputsPtr.push_back(outputs[0].get());
    
    // --- execute DPU calculation ---
    tickCountStart = (double) cv::getTickCount();
    auto job_id = runner->execute_async(inputsPtr, outputsPtr);
    runner->wait(job_id.first, -1);
    tickCountStop = (double) cv::getTickCount();

    if (netOutDim ==1 ) { // binary classifier
        calcSigmoid();
    } else {
        calcSoftmax(); // not realy needed for classification
    }
    
    
    inputs.clear();    
    outputs.clear();    

	return 1;
}


void imgClassifyClass::calcSigmoid() {
    double xx  = double(dpuResultBuffer[0]);
    double den = double(1.0) + exp(-xx);
    activationsOutput[0]=double(1.0)/den;
}



int imgClassifyClass::topKSearch(int k) {

    if ((int)topIdxVector.size()!=k) {
        topIdxVector.clear();
        topIdxVector.assign(k, 0);        
    }

    if ((int)topPropVector.size()!=k) {
        topPropVector.clear();
        topPropVector.assign(k, 0.0);        
    }
    
    priority_queue<pair<float, int>> q;

    for (auto i = 0; i < netOutDim; ++i) {
        //q.push(pair<float, int>(activationsOutput[i], i)); // sort on activations
        q.push(pair<float, int>(dpuResultBuffer[i], i)); // sort on addBias without activations
    }

    for (auto i = 0; i < k; ++i) {
        pair<float, int> ki = q.top();
        //printf("top[%d] class %d prob = %-8f\n", i, ki.second, d[ki.second]);
        topIdxVector.at(i)  = ki.second;
        topPropVector.at(i) = activationsOutput[ki.second];

        q.pop();
    }

    return 1;
}

