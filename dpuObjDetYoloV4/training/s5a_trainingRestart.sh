#!/bin/bash

source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"	

#cwd=$(pwd)
# go to $WORK_DIR, so any darknet output will be written $WORK_DIR
cd $WORK_DIR

/darknet/darknet detector train \
   lists/trainConfig.data \
   modelCfg/yolov4_dpu.cfg \
   outputTraining/yolov4_dpu_last.weights \
   -map -dont_show -gpus 0 \
   2>&1 | tee train.log
   
   
   
