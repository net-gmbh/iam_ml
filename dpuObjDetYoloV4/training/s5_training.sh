#!/bin/bash

source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"	

# go to $WORK_DIR, so any darknet output will be written $WORK_DIR
cwd=$(pwd)
cd $WORK_DIR

/darknet/darknet detector train \
   lists/trainConfig.data \
   modelCfg/yolov4_dpu.cfg \
   $cwd/yoloFiles/yolov4.conv.137 \
   -map -dont_show -gpus 0 \
   2>&1 | tee train.log