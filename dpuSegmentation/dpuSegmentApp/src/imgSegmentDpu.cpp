/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


// see: https://github.com/Xilinx/Vitis-AI/blob/master/demo/VART/resnet50/src/main.cc

#include "imgSegmentDpu.h"


imgSegmentClass::imgSegmentClass()  {
	
	paramType          = 1;
    paramEnableOverlay = 1;
    paramEnableIdxMap  = 0;
    paramEnableMapWithDpuResolution = 0;
	configOkFlag       = 0;

    dpuInputBufferBytesAllocated  = 0;
	dpuResultBufferBytesAllocated = 0;    
}


imgSegmentClass::~imgSegmentClass()
{
    
}


int imgSegmentClass::configDpu(const std::string filename) {

    //printf("Loading DPU config ...\n");
    //printf("Filename: %s\n",filename.c_str());

    //if (FILE *file = fopen(filename.c_str(), "r")) {
    //    fclose(file);        
    //} else {
        //printf("ERROR: DPU Config file not found\n");
    //    configOkFlag = 0;
    //    return 0;
    //}
    
    //graph = xir::Graph::deserialize("model/dpu_zu5_fcn8n3_netLogo.elf");
    graph = xir::Graph::deserialize(filename);
    subgraph = get_dpu_subgraph(graph.get());
    runner = vart::Runner::create_runner(subgraph[0], "run");

    inputTensors = runner->get_input_tensors();
    outputTensors = runner->get_output_tensors(); 

    //out_dims = outputTensors[0]->get_dims();
    //in_dims = inputTensors[0]->get_dims();
    out_dims = outputTensors[0]->get_shape();  // VitisAi1.4
    in_dims = inputTensors[0]->get_shape();    // VitisAi1.4

    printf("input dims: ");
    for (int i=0; i<(int)in_dims.size(); i++)
        printf("%d ",in_dims[i]);
    printf("\n");

    printf("output dims: ");
    for (int i=0; i<(int)out_dims.size(); i++)
        printf("%d ",out_dims[i]);
    printf("\n");
   
    dpuResSize   = out_dims[1] *out_dims[2] *out_dims[3];
    dpuResChannelNum = out_dims[3];
    dpuInputSize =  in_dims[1] * in_dims[2] * in_dims[3]; // x,y,c    

    if (dpuResultBufferBytesAllocated!=dpuResSize) {
        if (dpuResultBufferBytesAllocated>0) {
            delete(dpuResultBuffer);
            delete(softmax);
        }
        dpuResultBuffer = new float[dpuResSize];
        softmax = new float[dpuResSize];
        dpuResultBufferBytesAllocated = dpuResSize;
    }
    
    if (dpuInputBufferBytesAllocated!=dpuInputSize) {
        if (dpuInputBufferBytesAllocated>0) {
            delete(dpuInputBuffer);
        }
        dpuInputBuffer = new float[dpuInputSize];
        dpuInputBufferBytesAllocated = dpuInputSize;
    }

    image2 = cv::Mat(in_dims[1], in_dims[2], CV_8SC3);

    in_dims[0] = 1;
    out_dims[0] = 1;
	
	configOkFlag = 1;   

	return 1;
}

int imgSegmentClass::cleanup() {
	if (dpuResultBufferBytesAllocated>0) {
        delete(dpuResultBuffer);
        delete(softmax);
    }

    if (dpuInputBufferBytesAllocated>0) {
        delete(dpuInputBuffer);
    }

	return 1;
}


int imgSegmentClass::procImage(cv::Mat img) { // img=RGB image
    
	if (dpuResultBufferBytesAllocated==0) return 0;     

     //get in/out tensor shape
    int inputCnt = inputTensors.size();
	int outputCnt = outputTensors.size();
	TensorShape inshapes[inputCnt];
	TensorShape outshapes[outputCnt];
	shapes.inTensorList = inshapes;
	shapes.outTensorList = outshapes;
	getTensorShape(runner.get(), &shapes, inputCnt, outputCnt);
	
  	//get shape info
  	//int outSize = shapes.outTensorList[0].size;
  	int inSize    = shapes.inTensorList[0].size;
  	int inHeight  = shapes.inTensorList[0].height;
  	int inWidth   = shapes.inTensorList[0].width;
    int outHeight = shapes.outTensorList[0].height;
    int outWidth  = shapes.outTensorList[0].width;    

    float mean[3] = {128, 128, 128};
    
    cv::resize(img, image2, cv::Size(inWidth, inHeight), 0,0, cv::INTER_LINEAR);
    
    // --- write data to dpu input buffer ---
    int i=0; // single image 
    for (int h = 0; h < inHeight; h++) {
        for (int w = 0; w < inWidth; w++) {           
            int iBase = i * inSize + h * inWidth * 3 + w * 3;
            dpuInputBuffer[iBase + 2] = (float(image2.at<cv::Vec3b>(h, w)[0])  - mean[0])/float(128); // RGB->BGR, so its trained
            dpuInputBuffer[iBase + 1] = (float(image2.at<cv::Vec3b>(h, w)[1])  - mean[1])/float(128);
            dpuInputBuffer[iBase + 0] = (float(image2.at<cv::Vec3b>(h, w)[2])  - mean[2])/float(128);
        }
    }
    
    // in/out tensor refactory for batch inout/output 
    batchTensors.push_back(std::shared_ptr<xir::Tensor>(
        xir::Tensor::create(inputTensors[0]->get_name(), in_dims,
                            //xir::DataType::FLOAT, sizeof(float) * 8u)));
                            xir::DataType{xir::DataType::FLOAT, 8u})));  // VitisAi1.4

    inputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
        dpuInputBuffer, batchTensors.back().get()));

    batchTensors.push_back(std::shared_ptr<xir::Tensor>(
        xir::Tensor::create(outputTensors[0]->get_name(), out_dims,
                            //xir::DataType::FLOAT, sizeof(float) * 8u)));
                            xir::DataType{xir::DataType::FLOAT, 8u})));  // VitisAi1.4

    outputs.push_back(std::make_unique<CpuFlatTensorBuffer>(
        dpuResultBuffer, batchTensors.back().get()));
    
    inputsPtr.clear();
    outputsPtr.clear();
    inputsPtr.push_back(inputs[0].get());
    outputsPtr.push_back(outputs[0].get());
    
    // --- execute dpu job --
    auto job_id = runner->execute_async(inputsPtr, outputsPtr);
    runner->wait(job_id.first, -1);
     
    // --- generate  semi transparent segmentation overlay ---
    if (paramEnableOverlay) {
        cv::Mat segMat(outHeight, outWidth, CV_8UC3);
        cv::Mat showMat(img.rows, img.cols, CV_8UC3);
        
        uint8_t colorB[] = {0,  244,  35,  35, 153, 153,  30,  30,  35, 152, 180,  60,  30, 142,  70,  100, 100, 230,  32};
        uint8_t colorG[] = {0,   35, 244,  35, 153, 153, 170, 220, 142, 251, 130,  20,  30,  30,  30,   60,  80,  30,  11};
        uint8_t colorR[] = {0,   35,  35, 244, 190, 153, 250, 220, 107, 152,  70, 220, 255,  30,  30,   30,  30,  30, 119};

        for (int row = 0; row < outHeight; row++) { // read dpu output buffer
            for (int col = 0; col < outWidth; col++) {
                int i = row * outWidth * dpuResChannelNum + col * dpuResChannelNum;
                auto max_ind = max_element(dpuResultBuffer + i, dpuResultBuffer + i + dpuResChannelNum);
                int posit = distance(dpuResultBuffer + i, max_ind);
                segMat.at<cv::Vec3b>(row, col) =
                    cv::Vec3b(colorB[posit], colorG[posit], colorR[posit]);                
            }
        }        
        
        cv::resize(segMat, showMat,  cv::Size(img.cols, img.rows), 0,0, cv::INTER_NEAREST); // expand overlay image to full image format
        
        for (int i = 0; i < showMat.rows * showMat.cols * 3; i++) { // generate semi transparent overlay on top of image
            if (showMat.data[i]>0) {
                img.data[i] = img.data[i] * 0.4 + showMat.data[i] * 0.6;
            }
        }
    }

    // --- generate class index map --- 
    if (paramEnableIdxMap) {

        if (paramEnableMapWithDpuResolution) {
            idxMatFull=cv::Mat::zeros(img.rows, img.cols, CV_8UC1);

            for (int row = 0; row < outHeight; row++) { // read dpu output buffer
                for (int col = 0; col < outWidth; col++) {
                    int i = row * outWidth * dpuResChannelNum + col * dpuResChannelNum;
                    auto max_ind = max_element(dpuResultBuffer + i, dpuResultBuffer + i + dpuResChannelNum);
                    int posit = distance(dpuResultBuffer + i, max_ind);
                    idxMatFull.at<unsigned char>(row, col) = posit; // store index in index mat                 
                }
            }
        } else {
            idxMat=cv::Mat(outHeight, outWidth, CV_8UC1);
            
            for (int row = 0; row < outHeight; row++) { // read dpu output buffer
                for (int col = 0; col < outWidth; col++) {
                    int i = row * outWidth * dpuResChannelNum + col * dpuResChannelNum;
                    auto max_ind = max_element(dpuResultBuffer + i, dpuResultBuffer + i + dpuResChannelNum);
                    int posit = distance(dpuResultBuffer + i, max_ind);
                    idxMat.at<unsigned char>(row, col) = posit; // store index in index mat                 
                }
            }

            cv::resize(idxMat, idxMatFull, cv::Size(img.cols, img.rows), 0, 0, cv::INTER_NEAREST); // expand map size to full image format
        }
    }    
          
    //CPUCalcSoftmax(&dpuResultBuffer[0], netOutDim, softmax);
    
    inputs.clear();    
    outputs.clear();    

	return 1;
}


