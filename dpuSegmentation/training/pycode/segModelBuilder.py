#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Code developed by Harshall Lamba in his post  https://towardsdatascience.com/understanding-semantic-segmentation-with-unet-6be4f42d4b47
under the MIT license https://github.com/hlamba28/UNET-TGS/blob/master/LICENSE
shown again in the following.
'''


'''
MIT License

Copyright (c) 2019 Harshall Lamba

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
/**

* © Copyright (C) 2016-2020 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/
'''

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.




import os
import tensorflow as tf
#from keras.backend.tensorflow_backend import set_session
import sys, time, warnings
keras = tf.keras
#from keras.models import *
#from keras.layers import *
#from keras.utils import plot_model #DB

warnings.filterwarnings("ignore")

IMAGE_ORDERING =  "channels_last"



def buildModel(cfg):
    #tf.reset_default_graph() # clear the current tf graph
    if cfg.MODEL_TYPE=='FCN8':
        return FCN8(nClasses     = cfg.NUM_CLASSES,
                    input_width  = cfg.WIDTH,
                    input_height = cfg.HEIGHT,
                    upscale      = False,
                    ignorePool3  = cfg.FCN8_IGNORE_POOL3)
    if cfg.MODEL_TYPE=='FCN8ups':
        return FCN8(nClasses     = cfg.NUM_CLASSES,
                    input_width  = cfg.WIDTH,
                    input_height = cfg.HEIGHT,
                    upscale      = True,
                    ignorePool3  = cfg.FCN8_IGNORE_POOL3)
    if cfg.MODEL_TYPE=='UNETv1':
        return UNETv1(cfg.NUM_CLASSES, cfg.WIDTH, cfg.HEIGHT, 
                      n_filters=cfg.UNET_NUM_FILTERS, dropout=cfg.UNET_DROPOUT,
                      batchnorm=cfg.UNET_BATCHNORM, activation=cfg.UNET_ACTIVATION)
    if cfg.MODEL_TYPE=='UNETv2':
        return UNETv2(cfg.NUM_CLASSES, cfg.WIDTH, cfg.HEIGHT, 
                      n_filters=cfg.UNET_NUM_FILTERS, dropout=cfg.UNET_DROPOUT,
                      batchnorm=cfg.UNET_BATCHNORM, activation=cfg.UNET_ACTIVATION)
    if cfg.MODEL_TYPE=='UNETv3':
        return UNETv3(cfg.NUM_CLASSES, cfg.WIDTH, cfg.HEIGHT, 
                      n_filters=cfg.UNET_NUM_FILTERS, dropout=cfg.UNET_DROPOUT,
                      batchnorm=cfg.UNET_BATCHNORM, activation=cfg.UNET_ACTIVATION)
    print('ERROR: Unknown MODEL_TYPE')
    
def printModelSummary(cfg):    
    print('MODEL_TYPE :',cfg.MODEL_TYPE)
    print('NUM_CLASSES:',cfg.NUM_CLASSES)
    print('WIDTH      :',cfg.WIDTH)
    print('HEIGHT     :',cfg.HEIGHT)
    if (cfg.MODEL_TYPE=='FCN8' or cfg.MODEL_TYPE=='FCN8ups'):
        print('FCN8_IGNORE_POOL3:',cfg.FCN8_IGNORE_POOL3)
    else:    
        print('UNET_NUM_FILTERS :',cfg.UNET_NUM_FILTERS)
        print('UNET_DROPOUT     :',cfg.UNET_DROPOUT)
        print('UNET_BATCHNORM   :',cfg.UNET_BATCHNORM)
        print('UNET_ACTIVATION  :',cfg.UNET_ACTIVATION)
        

########################################################################################
# FCN8 CNN based on VGG
########################################################################################
#VGG_Weights_path = "../keras_model/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5"
#VGG_Weights_path = "C:/pro/ML_Code/VitisSegmentation/keras_model/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5"
#VGG_Weights_path = "gg16_weights_tf_dim_ordering_tf_kernels_notop.h5"

VGG_Weights_path = os.path.join(os.getcwd(),"pycode/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5")

def FCN8( nClasses , input_width, input_height, upscale=False, ignorePool3=False):
    ## input_height and width must be devisible by 32 because maxpooling with filter size = (2,2) is operated 5 times,
    ## which makes the input_height and width 2^5 = 32 times smaller
    assert input_height%32 == 0
    assert input_width%32 == 0
    IMAGE_ORDERING =  "channels_last"

    img_input = keras.layers.Input(shape=(input_height,input_width, 3)) ## Assume 224,224,3

    ## Block 1: 64, 64
    x = keras.layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv1', data_format=IMAGE_ORDERING )(img_input)
    x = keras.layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv2', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool', data_format=IMAGE_ORDERING )(x)
    f1 = x

    # Block 2: 128, 128
    x = keras.layers.Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv1', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool', data_format=IMAGE_ORDERING )(x)
    f2 = x

    # Block 3: 256, 256, 256
    x = keras.layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv1', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv2', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool', data_format=IMAGE_ORDERING )(x)
    pool3 = x

    # Block 4: 512, 512, 512
    x = keras.layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv1', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv2', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv3', data_format=IMAGE_ORDERING )(x)
    pool4 = keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool', data_format=IMAGE_ORDERING )(x)## (None, 14, 14, 512)

    # Block 5: 512, 512, 512
    x = keras.layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv1', data_format=IMAGE_ORDERING )(pool4)
    x = keras.layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv2', data_format=IMAGE_ORDERING )(x)
    x = keras.layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv3', data_format=IMAGE_ORDERING )(x)
    pool5 = keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool', data_format=IMAGE_ORDERING )(x)## (None, 7, 7, 512)

    vgg  = keras.models.Model(  img_input , pool5  )
    vgg.load_weights(VGG_Weights_path) ## loading VGG weights for the encoder parts of FCN8

    ## 4 times upsamping for pool4 layer
    if upscale:
        conv7_4a=( keras.layers.Conv2D( nClasses , ( 7 , 7 ) ,  activation='relu', padding='same', name="conv7_4a", data_format=IMAGE_ORDERING))(pool5)
        conv7_4b=( keras.layers.Conv2D( nClasses , ( 1 , 1 ) ,  activation='relu', padding='same', name="conv7_4b", data_format=IMAGE_ORDERING))(conv7_4a)
        #conv7_4 = keras.layers.UpSampling2D(size=(4,4), data_format=IMAGE_ORDERING, interpolation="bilinear")(conv7_4b)
        conv7_4 = keras.layers.UpSampling2D(size=(4,4), data_format=IMAGE_ORDERING, interpolation="nearest")(conv7_4b)
    else:
        n = 512 #4096
        o =     ( keras.layers.Conv2D( n , ( 7 , 7 ) , activation='relu' , padding='same', name="conv6", data_format=IMAGE_ORDERING))(pool5)
        conv7 = ( keras.layers.Conv2D( n , ( 1 , 1 ) , activation='relu' , padding='same', name="conv7", data_format=IMAGE_ORDERING))(o)
        conv7_4 = keras.layers.Conv2DTranspose( nClasses , kernel_size=(4,4) ,  strides=(4,4) , use_bias=False, data_format=IMAGE_ORDERING )(conv7)


    ## 2 times upsampling for pool411
    pool411 = ( keras.layers.Conv2D( nClasses , ( 1 , 1 ) , activation='relu' , padding='same', name="pool4_11", data_format=IMAGE_ORDERING))(pool4)
    if upscale == "True" :
        pool411_b = ( keras.layers.Conv2D( nClasses , ( 1 , 1 ) ,  activation='relu', padding='same', name="pool411_b", data_format=IMAGE_ORDERING))(pool411)
        #pool411_2 = (UpSampling2D(size=(2,2), data_format=IMAGE_ORDERING, interpolation="bilinear"))(pool411_b)
        pool411_2 = (keras.layers.UpSampling2D(size=(2,2), data_format=IMAGE_ORDERING, interpolation="nearest"))(pool411_b)

    else:
        pool411_2 = (keras.layers.Conv2DTranspose( nClasses , kernel_size=(2,2) ,  strides=(2,2) , use_bias=False, data_format=IMAGE_ORDERING ))(pool411)

    if ignorePool3:
        add_layer = keras.layers.Add(name="add_layer")([pool411_2,  conv7_4 ])
    else:
        pool311 = ( keras.layers.Conv2D( nClasses , ( 1 , 1 ) , activation='relu' , padding='same', name="pool3_11", data_format=IMAGE_ORDERING))(pool3)
        add_layer = keras.layers.Add(name="add_layer")([pool411_2, pool311, conv7_4 ])
    
    o = add_layer

    # you cannot replace this last Conv2DTranspose layer with an UpSampling2D layer: the CNN will not converge!
    o = keras.layers.Conv2DTranspose( nClasses , kernel_size=(8,8) ,  strides=(8,8) , use_bias=False, data_format=IMAGE_ORDERING , name="conv2DTransFinal")(o)

    o = (keras.layers.Activation("softmax"))(o)

    model = keras.models.Model(img_input, o)

    return model










#Function to add 2 convolutional layers with the parameters passed to it
def conv2d_block(input_tensor, n_filters, kernel_size = 3, batchnorm = True, activation=True):
    #firt layer
    x = keras.layers.Conv2D(filters = n_filters, kernel_size = (kernel_size, kernel_size),
               kernel_initializer = 'he_normal', padding = 'same', data_format=IMAGE_ORDERING)(input_tensor)
    if batchnorm:
        x = keras.layers.BatchNormalization()(x)
    if activation:
        x = keras.layers.Activation('relu')(x)
    # second layer
    x = keras.layers.Conv2D(filters = n_filters, kernel_size = (kernel_size, kernel_size),
               kernel_initializer = 'he_normal', padding = 'same', data_format=IMAGE_ORDERING)(x)
    if batchnorm:
        x = keras.layers.BatchNormalization()(x)
    if activation:
        x = keras.layers.Activation('relu')(x)

    return x


def UNETv1( nClasses, input_width, input_height, n_filters=16*4, dropout=0.1,
          batchnorm=True, activation=True):

    ## input_height and width must be devisible by 16 because maxpooling with filter size = (2,2) is operated 4 times,
    ## which makes the input_height and width 2^4 = 16 times smaller
    assert input_height%16 == 0
    assert input_width%16 == 0

    img_input = keras.layers.Input(shape=(input_height,input_width, 3)) ## Assume 224,224,3

    ## Downscaling

    ## Block 1: 64, 64
    c1 = conv2d_block(img_input, n_filters * 1, kernel_size = 3, batchnorm = batchnorm, activation=activation)
    p1 = keras.layers.MaxPooling2D((2, 2))(c1)
    p1 = keras.layers.Dropout(dropout)(p1) # 112x112x64

    ## Block 2: 128, 128
    c2 = conv2d_block(p1, n_filters * 2, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p2 = keras.layers.MaxPooling2D((2, 2))(c2)
    p2 = keras.layers.Dropout(dropout)(p2) # 56x56x128

    ## Block 3: 256, 256
    c3 = conv2d_block(p2, n_filters * 4, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p3 = keras.layers.MaxPooling2D((2, 2))(c3)
    p3 = keras.layers.Dropout(dropout)(p3) # 28x28x256

    ## Block 4: 512, 512
    c4 = conv2d_block(p3, n_filters * 8, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p4 = keras.layers.MaxPooling2D((2, 2))(c4)
    p4 = keras.layers.Dropout(dropout)(p4) # 14x14x512

    ## Block5: 1024, 1024
    c5 = conv2d_block(p4, n_filters = n_filters * 16, kernel_size = 3, batchnorm = batchnorm, activation=activation)
    p5 = keras.layers.Dropout(dropout)(c5) # 14x14x1024

    ## Upscaling

    ## Block6: 512, 512
    up6 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(p5) # 28x28x1024
    u6 = (up6)
    m6 = keras.layers.Concatenate(axis=3)([u6, c4])
    m6 = keras.layers.Dropout(dropout)(m6)
    c6 = conv2d_block(m6, n_filters * 8, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block7: 256, 256
    up7 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c6) #56x56x256
    u7 = (up7)
    m7 = keras.layers.Concatenate(axis=3)([u7, c3])
    m7 = keras.layers.Dropout(dropout)(m7)
    c7 = conv2d_block(m7, n_filters * 4, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block8: 128, 128
    up8 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c7) # 112x112x128
    u8  = (up8)
    m8  = keras.layers.Concatenate(axis=3)([u8, c2])
    m8  = keras.layers.Dropout(dropout)(m8)
    c8 = conv2d_block(m8, n_filters * 2, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block9: 64, 64
    up9 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c8) # 224x224x64
    u9  = (up9)
    m9 = keras.layers.Concatenate(axis=3)([u9, c1])
    m9 = keras.layers.Dropout(dropout)(m9)
    c9 = conv2d_block(m9, n_filters * 1, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Last layers
    c10 = keras.layers.Conv2D(filters=nClasses, kernel_size=3, data_format=IMAGE_ORDERING,
                 activation="relu", padding="same", kernel_initializer="he_normal")(c9)
    c11 = keras.layers.Conv2D(filters=nClasses, kernel_size=1, data_format=IMAGE_ORDERING, activation="sigmoid")(c10)

    model = keras.models.Model(inputs=img_input, outputs=c10)

    return model




# like UNET_v1 but with a Conv2D layer betweeb UpSampling2D aand Concatenate layers
def UNETv2( nClasses, input_width, input_height, n_filters=16*4, dropout=0.1,
          batchnorm=True, activation=True):

    ## input_height and width must be devisible by 16 because maxpooling with filter size = (2,2) is operated 4 times,
    ## which makes the input_height and width 2^4 = 16 times smaller
    assert input_height%16 == 0
    assert input_width%16 == 0

    img_input = keras.layers.Input(shape=(input_height,input_width, 3)) ## Assume 224,224,3

    ## Downscaling

    ## Block 1: 64, 64
    c1 = conv2d_block(img_input, n_filters * 1, kernel_size = 3, batchnorm = batchnorm, activation=activation)
    p1 = keras.layers.MaxPooling2D((2, 2))(c1)
    p1 = keras.layers.Dropout(dropout)(p1) # 112x112x64

    ## Block 2: 128, 128
    c2 = conv2d_block(p1, n_filters * 2, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p2 = keras.layers.MaxPooling2D((2, 2))(c2)
    p2 = keras.layers.Dropout(dropout)(p2) # 56x56x128

    ## Block 3: 256, 256
    c3 = conv2d_block(p2, n_filters * 4, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p3 = keras.layers.MaxPooling2D((2, 2))(c3)
    p3 = keras.layers.Dropout(dropout)(p3) # 28x28x256

    ## Block 4: 512, 512
    c4 = conv2d_block(p3, n_filters * 8, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p4 = keras.layers.MaxPooling2D((2, 2))(c4)
    p4 = keras.layers.Dropout(dropout)(p4) # 14x14x512

    ## Block5: 1024, 1024
    c5 = conv2d_block(p4, n_filters = n_filters * 16, kernel_size = 3, batchnorm = batchnorm, activation=activation)
    p5 = keras.layers.Dropout(dropout)(c5) # 14x14x1024

    ## Upscaling

    ## Block6: 512, 512
    up6 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(p5) # 28x28x1024
    u6 = keras.layers.Conv2D(filters=n_filters*8, kernel_size=2, data_format=IMAGE_ORDERING,
                activation="relu", padding="same", kernel_initializer="he_normal")(up6)
    m6 = keras.layers.Concatenate(axis=3)([u6, c4])
    m6 = keras.layers.Dropout(dropout)(m6)
    c6 = conv2d_block(m6, n_filters * 8, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block7: 256, 256
    up7 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c6) #56x56x256
    u7 = keras.layers.Conv2D(filters=n_filters*4, kernel_size=2, data_format=IMAGE_ORDERING,
                activation="relu", padding="same", kernel_initializer="he_normal")(up7)
    m7 = keras.layers.Concatenate(axis=3)([u7, c3])
    m7 = keras.layers.Dropout(dropout)(m7)
    c7 = conv2d_block(m7, n_filters * 4, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block8: 128, 128
    up8 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c7) # 112x112x128
    u8  = keras.layers.Conv2D(filters=n_filters*2, kernel_size=2, data_format=IMAGE_ORDERING,
                activation="relu", padding="same", kernel_initializer="he_normal")(up8)
    m8  = keras.layers.Concatenate(axis=3)([u8, c2])
    m8  = keras.layers.Dropout(dropout)(m8)
    c8 = conv2d_block(m8, n_filters * 2, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block9: 64, 64
    up9 = keras.layers.UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c8) # 224x224x64
    u9  = keras.layers.Conv2D(filters=n_filters*1, kernel_size=2, data_format=IMAGE_ORDERING,
                activation="relu", padding="same", kernel_initializer="he_normal")(up9)
    m9 = keras.layers.Concatenate(axis=3)([u9, c1])
    m9 = keras.layers.Dropout(dropout)(m9)
    c9 = conv2d_block(m9, n_filters * 1, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Last layers
    c10 = keras.layers.Conv2D(filters=nClasses, kernel_size=3, data_format=IMAGE_ORDERING,
                 activation="relu", padding="same", kernel_initializer="he_normal")(c9)
    c11 = keras.layers.Conv2D(filters=nClasses, kernel_size=1, data_format=IMAGE_ORDERING, activation="sigmoid")(c10)

    model = keras.models.Model(inputs=img_input, outputs=c10)

    return model


# like UNET_v2 but with a UpSampling2D replaced by Conv2D_transpose layer
def UNETv3( nClasses, input_width, input_height, n_filters=16*4, dropout=0.1,
          batchnorm=True, activation=True):

    ## input_height and width must be devisible by 16 because maxpooling with filter size = (2,2) is operated 4 times,
    ## which makes the input_height and width 2^4 = 16 times smaller
    assert input_height%16 == 0
    assert input_width%16 == 0

    img_input = keras.layers.Input(shape=(input_height,input_width, 3)) ## Assume 224,224,3

    ## Downscaling

    ## Block 1: 64, 64
    c1 = conv2d_block(img_input, n_filters * 1, kernel_size = 3, batchnorm = batchnorm, activation=activation)
    p1 = keras.layers.MaxPooling2D((2, 2))(c1)
    p1 = keras.layers.Dropout(dropout)(p1) # 112x112x64

    ## Block 2: 128, 128
    c2 = conv2d_block(p1, n_filters * 2, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p2 = keras.layers.MaxPooling2D((2, 2))(c2)
    p2 = keras.layers.Dropout(dropout)(p2) # 56x56x128

    ## Block 3: 256, 256
    c3 = conv2d_block(p2, n_filters * 4, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p3 = keras.layers.MaxPooling2D((2, 2))(c3)
    p3 = keras.layers.Dropout(dropout)(p3) # 28x28x256

    ## Block 4: 512, 512
    c4 = conv2d_block(p3, n_filters * 8, kernel_size = 3, batchnorm = batchnorm,  activation=activation)
    p4 = keras.layers.MaxPooling2D((2, 2))(c4)
    p4 = keras.layers.Dropout(dropout)(p4) # 14x14x512

    ## Block5: 1024, 1024
    c5 = conv2d_block(p4, n_filters = n_filters * 16, kernel_size = 3, batchnorm = batchnorm, activation=activation)
    p5 = keras.layers.Dropout(dropout)(c5) # 14x14x1024

    ## Upscaling

    ## Block6: 512, 512
    #up6 = UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(p5) # 28x28x1024
    up6  = keras.layers.Conv2DTranspose( n_filters*8, kernel_size=(3,3), strides=(2,2),padding="same", data_format=IMAGE_ORDERING )(p5)
    #u6 = Conv2D(filters=n_filters*8, kernel_size=2, data_format=IMAGE_ORDERING, activation="relu", padding="same", kernel_initializer="he_normal")(up6)
    u6 = up6
    m6 = keras.layers.Concatenate(axis=3)([u6, c4])
    m6 = keras.layers.Dropout(dropout)(m6)
    c6 = conv2d_block(m6, n_filters * 8, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block7: 256, 256
    #up7 = UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c6) #56x56x256
    up7 = keras.layers.Conv2DTranspose( n_filters*4, kernel_size=(3,3), strides=(2,2),padding="same", data_format=IMAGE_ORDERING )(c6)
    #u7 = Conv2D(filters=n_filters*4, kernel_size=2, data_format=IMAGE_ORDERING, activation="relu", padding="same", kernel_initializer="he_normal")(up7)
    u7 = up7
    m7 = keras.layers.Concatenate(axis=3)([u7, c3])
    m7 = keras.layers.Dropout(dropout)(m7)
    c7 = conv2d_block(m7, n_filters * 4, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block8: 128, 128
    #up8 = UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c7) # 112x112x128
    up8 = keras.layers.Conv2DTranspose( n_filters*2, kernel_size=(3,3), strides=(2,2),padding="same", data_format=IMAGE_ORDERING )(c7)
    #u8  = Conv2D(filters=n_filters*2, kernel_size=2, data_format=IMAGE_ORDERING, activation="relu", padding="same", kernel_initializer="he_normal")(up8)
    u8 = up8
    m8  = keras.layers.Concatenate(axis=3)([u8, c2])
    m8  = keras.layers.Dropout(dropout)(m8)
    c8 = conv2d_block(m8, n_filters * 2, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Block9: 64, 64
    #up9 = UpSampling2D(size=(2, 2),data_format=IMAGE_ORDERING, interpolation="bilinear")(c8) # 224x224x64
    up9 = keras.layers.Conv2DTranspose( n_filters*1, kernel_size=(3,3), strides=(2,2),padding="same", data_format=IMAGE_ORDERING )(c8)
    #u9  = Conv2D(filters=n_filters*1, kernel_size=2, data_format=IMAGE_ORDERING, activation="relu", padding="same", kernel_initializer="he_normal")(up9)
    u9 = up9
    m9 = keras.layers.Concatenate(axis=3)([u9, c1])
    m9 = keras.layers.Dropout(dropout)(m9)
    c9 = conv2d_block(m9, n_filters * 1, kernel_size = 3, batchnorm = batchnorm, activation=activation)

    ## Last layers
    c10 = keras.layers.Conv2D(filters=nClasses, kernel_size=3, data_format=IMAGE_ORDERING,
                 activation="relu", padding="same", kernel_initializer="he_normal")(c9)
    c11 = keras.layers.Conv2D(filters=nClasses, kernel_size=1, data_format=IMAGE_ORDERING, activation="sigmoid")(c10)

    model = keras.models.Model(inputs=img_input, outputs=c10)

    return model
    
    
    
    

