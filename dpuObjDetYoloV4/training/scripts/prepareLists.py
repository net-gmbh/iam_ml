#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


# -----------------------------------------------------------
#           prepare list files for training YoloV4
# -----------------------------------------------------------


import os
import shutil

import random
import argparse
import sys
from pathlib import Path



def createDir(dirName):
    if not os.path.exists(dirName):  
        #Path(dirName).mkdir( 0o777, parents=True, exist_ok=True) # recursive
        Path(dirName).mkdir( parents=True, exist_ok=True) # recursive        
        if not os.path.exists(dirName):  
            print('ERROR: Could not create directory: ',dirName)
            return False
    return True


def main(argv):
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-projectDir', default = 'default', type = str, 
                        help='Path to project directory\n' )  
                        
    parser.add_argument('-workDir', default = 'default', type = str, 
                        help='Path to work directory\n' ) 
                        
    args = parser.parse_args()
    
    
    currentDir = os.getcwd()
    sys.path.insert(1, os.path.join(currentDir,args.projectDir))    # for import    
    import config as cfg
    
    
    ratioTest                   = cfg.TEST_SAMPLES_RATIO
    dataPath                    = os.path.join(args.workDir, "data")
    listsDir                    = os.path.join(args.workDir, "lists")
    testListFileName            = os.path.join(listsDir,"valid.txt")
    trainListFileName           = os.path.join(listsDir,"train.txt")
    calibListFileName           = os.path.join(listsDir,"calib.txt")
    anchorGeneratorListFileName = os.path.join(listsDir,"anchorGen.txt")
    trainConfigFileName         = os.path.join(listsDir,"trainConfig.data")
    nameListFileName           = os.path.join(listsDir,"labelList.txt")
    #samplePrefix                = args.projectDir+"/data/"
    samplePrefix                = args.workDir+"/data/"
    
    createDir(listsDir)
    

    fileNameList = os.listdir(dataPath)
    valid_extensions = ['jpg','jpeg', 'bmp', 'png']
    sampleNameList = [fn for fn in fileNameList if any(fn.endswith(ext) for ext in valid_extensions)]
    numSamples = len(sampleNameList)


    

    numTest = int(numSamples * ratioTest)
    numTest = max(numTest,1)
    numTrain = numSamples-numTest

    random.shuffle(sampleNameList)

    print("Generating verification list file")
    testFile  = open(testListFileName,  'w')
    for sampleId in range(numTest):
        testFile.write("%s%s\n" %(samplePrefix, sampleNameList[sampleId][:]))
    testFile.close()

    print("Generating training list file")
    trainFile = open(trainListFileName, 'w')
    for i in range(numTrain):
        sampleId = i + numTest
        if sampleId>=numSamples:
            sampleId -= numSamples
        trainFile.write("%s%s\n" %(samplePrefix, sampleNameList[sampleId][:]))
    trainFile.close()

    print("Generating calibration list file")
    calibFile  = open(calibListFileName,  'w')
    for sampleId in range(numSamples):
        calibFile.write("%s 0\n" %(sampleNameList[sampleId][:]))
    calibFile.close()
    

    print("Generating anchor generator list file")
    anchorFile  = open(anchorGeneratorListFileName,  'w')
    for sampleId in range(numSamples):
        anchorFile.write("%s%s.txt\n" %(samplePrefix, sampleNameList[sampleId][:-4]))
    anchorFile.close()


    print("Generating trainConfig file")
    configFile  = open(trainConfigFileName,  'w')
    configFile.write("classes= %s\n" %(cfg.NUMBER_OF_CLASSES))
    configFile.write("train= %s\n" %(trainListFileName))
    configFile.write("valid= %s\n" %(testListFileName))
    configFile.write("names= %s\n" %(nameListFileName))
    configFile.write("backup= ./outputTraining")
    configFile.close()



    print("Done")

if __name__=="__main__":
    main(sys.argv)




