/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

// ----------------------------------------------
//       Reference Application for iam ML 
//     DPU accelerated image classification
// ---------------------------------------------- 


#include "main.h"
#include "app.h"
#include "bufferManager.h"

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

appClass    app;

camDeviceClass camDevice;
bufferManagerClass bufferManager;

// --- synview ---
LvSystem    *pSystem    = NULL;
LvInterface *pInterface = NULL;
LvDevice    *pDevice    = NULL;

std::atomic<int>  sensorWidth;
std::atomic<int>  sensorHeight;
std::atomic<int>  imgWidthOut;
std::atomic<int>  imgHeightOut;

std::atomic<bool> enableStreaming;
std::atomic<int>  outputPixelFormat;
std::atomic<int>  outputImageMode;
std::atomic<int>  sensorPixelFormat;   
std::atomic<int>  sensorDecimation;
std::atomic<bool> enableSegmentationFlag;
std::atomic<bool> enableOsdFlag;
std::atomic<int>  outputImagePattern;
std::atomic<bool> modelOkFlag;
std::atomic<bool> enablePrintfDpuFlag;
std::atomic<bool> enablePrintfBufferManager;
std::atomic<bool> enablePrintfBufferFps;
std::atomic<bool> enableTemperatureOsd;

std::atomic<int>  autoExposureMode;
std::atomic<float> exposureTime;   

std::atomic<bool> autorun;
std::atomic<bool> bIsConnected;
std::atomic<bool> exitAppFlag;   

std::atomic<int>  enableTestPicFlag;
std::atomic<int>  testPicIdx;
std::atomic<bool> testImageLoaded;
std::atomic<int>  loadTestImage;   
std::atomic<int>  loadTestImageFlag;
std::vector<std::string> testImageFileNames;
std::mutex mtx_testImageFileNames;
cv::Mat testPicRgb;
std::mutex mtx_testPicRgb;

std::string        modelDirString;
std::atomic<bool>  doModelLoadFlag;
std::atomic<bool>  saveFrontendSettingsRequestFlag;
std::vector<std::string> modelDirectoryNames;
std::atomic<int>   currentModelIdx;
std::vector<std::string> classNames;
std::vector<int> redMarker;
std::vector<int> greenMarker;
std::vector<int> blueMarker;
std::mutex mtx_modelConfig;

std::atomic<bool> enableGevStreaming;

// dpu
std::unique_ptr<xir::Graph> graph;	
std::vector<const xir::Subgraph*> subgraph;	


std::atomic<float> devTempMain;
std::atomic<float> devTempSensor;


bool main_thread_is_running;
bool send_thread_is_running;

array<bool, NUMBER_OF_PROCESSING_THREADS> process_thread_do_modelConfig;
array<bool, NUMBER_OF_PROCESSING_THREADS> process_thread_is_running;

bool ErrorOccurred(LvStatus ErrorStatus)
{
    if (ErrorStatus == LVSTATUS_OK) return false;
    printf(COLOR_RED "synview error: \"%s\"\n" COLOR_RESET, LvLibrary::GetLastErrorMessage().c_str() );
    return true;
}

int getTickCount(void) {
    struct timespec tval;
    if (0==clock_gettime(CLOCK_MONOTONIC, &tval)) {
        return tval.tv_sec*1000 + tval.tv_nsec/1000000;
    }
    else { 
        return 0; 
    }
}



int keyPressed ( char &c ) {
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF) {
        c = ch;
        return 1;
    }

    c = 0;
    return 0;
}



void masterControl(bool& isRunning) {
    static int secCnt=0;
    char c=0;
    

    while(isRunning)
    {
        secCnt++;
        if (secCnt>9) secCnt=0;
        if (secCnt==0) { 
            keyPressed ( c );   // get key
            
                    
            if ((c=='q')||(c=='Q')||(c==27)) {            
                printf("terminating app ...\n");
                send_thread_is_running=false;
                process_thread_is_running.fill(false);
                break;   	
            }	

            if (FILE *file = fopen("/opt/synview/bin/sv.iAMGigEServer.stop", "r")) {
                fclose(file);
                printf("terminating app ...\n");
                send_thread_is_running=false;
                process_thread_is_running.fill(false);
                break;
            }

            if (exitAppFlag) {
                printf("terminating app ...\n");
                send_thread_is_running=false;
                process_thread_is_running.fill(false);
                break;            
            }
            c = 0;    // reset key
        }

        app.processPendingTasks(); 

        std::this_thread::sleep_for(100ms);
    }    
}

int main( int argc, char** argv )
{
    printf("\n---------------------------------------------------\n");
    printf("         %s\n", APPLICATION_NAME  );
    printf("---------------------------------------------------\n");

    thread mainThread;    
    thread sendThread;    
    vector<thread> processThread(NUMBER_OF_PROCESSING_THREADS);    
    int ok;
    
    
    remove("/opt/synview/bin/sv.iAMGigEServer.stop");

    // **********************************************
	//                 Arg Parser 
	// **********************************************
    int autoFlag=0;

    if (argc > 1) {
		for (int i = 1; i < argc; i++) {
			std::string argStr(argv[i]);
			printf("arg %d: %s: ", i, argStr.c_str());

			// --- commands ---	
			if (argStr.compare("-auto") == 0) {
				autoFlag = 1;
				printf("arg:-auto --> autostart frontend\n");
			}
            else {
				printf("Unknown argument\n");
			}
		}
	}
	   

    // **********************************************
	//               Init 
	// **********************************************
    ok=app.init(autoFlag);
	if (!ok) {
        printf(COLOR_RED "ERROR --> Terminate application\n" COLOR_RESET);
		goto _cleanup_;
	}


    // **********************************************
    //               start threads
    // **********************************************

    

    printf(COLOR_YEL "Starting transmitter thread...\n" COLOR_RESET);
    send_thread_is_running = true;  // false-->general disable send
    sendThread=thread(sendFrameThreadFunction, ref(send_thread_is_running));

    printf(COLOR_YEL "Starting processing threads ...\n" COLOR_RESET);
    process_thread_is_running.fill(true);
    if (modelOkFlag) {
        process_thread_do_modelConfig.fill(true);   
    } else {
        process_thread_do_modelConfig.fill(false); 
    }    
    for (int i=0; i<NUMBER_OF_PROCESSING_THREADS; i++) {
        processThread[i]=thread(processFrameThreadFunction, i, ref(process_thread_do_modelConfig[i]), ref(process_thread_is_running[i]));
    }    

    printf(COLOR_YEL "Starting masterControl thread...\n" COLOR_RESET);
    main_thread_is_running = true;
    mainThread = thread(masterControl, ref(main_thread_is_running));
       
    mainThread.join();
    for (int i=0; i<NUMBER_OF_PROCESSING_THREADS; i++) {
        processThread[i].join();
    }
    sendThread.join();

    printf(COLOR_YEL "Running...\n" COLOR_RESET);
    printf("press 'q' or 'ESC' to end program\n");

_cleanup_:

	printf("\n");
    printf("closing application...\n");    

    app.close();

    camDevice.CloseCamera();

    printf("closing system...\n");
    LvSystem::Close ( pSystem );       

    printf("closing library...\n");
    LvLibrary::CloseLibrary ();         

    printf("waiting");
    std::this_thread::sleep_for(200ms);
    printf(".");
    std::this_thread::sleep_for(200ms);
    printf(".");
    std::this_thread::sleep_for(200ms);
    printf(".");
    std::this_thread::sleep_for(200ms);
    printf("done\n");

    return ( 0 );
}
