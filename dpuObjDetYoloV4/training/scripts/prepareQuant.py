#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


# -----------------------------------------------------------
#     prepare prototxt file for quantising training YoloV4
# -----------------------------------------------------------

import os
import shutil
import argparse
import sys


def createDir(dirName):
    if not os.path.exists(dirName):   
        os.mkdir(dirName)
    if not os.path.exists(dirName):  
        print('ERROR: Could not create directory: ',dirName)
        return False
    return True



def main(argv):
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-projectDir', default = 'default', type = str, 
                        help='Path to project directory\n' )  
    
    parser.add_argument('-workDir', default = 'default', type = str, 
                        help='Path to work directory\n' )    
   
    args = parser.parse_args()
    
    currentDir = os.getcwd()
    sys.path.insert(1, os.path.join(currentDir,args.projectDir))    # for import    
    import config as cfg


    inputFileName  = os.path.join(args.workDir, "vaiModels", "yolov4_dpu.prototxt")
    outputFileName = os.path.join(args.workDir, "vaiModels", "yolov4_dpu_quant.prototxt")  


    calibListFile = args.workDir + "/lists/calib.txt"
    dataDir       = args.workDir + "/data"

    print("Input file : ",inputFileName)
    print("Output file: ",outputFileName)
    
    inputFile  = open(inputFileName,   'r')
    outputFile = open(outputFileName,'w')

    cnt=0

    for line in inputFile:
        #destinationFile.write(line)

        if line.find("input:")>=0:
            lineOut = "#" + line
            outputFile.write(lineOut)
        else:
            if line.find("input_dim:")>=0:
                cnt=cnt+1
                lineOut = "#" + line
                outputFile.write(lineOut)
                if cnt==4:
                    # --- add input layer ---
                    outputFile.write("\r\n")
                    outputFile.write("### input data layer for calibration ###\r\n")
                    outputFile.write("\r\n")
                    outputFile.write("layer {\r\n")
                    outputFile.write("  name: \"data\"\r\n")
                    outputFile.write("  type: \"ImageData\"\r\n")
                    outputFile.write("  top: \"data\"\r\n")
                    outputFile.write("  top: \"label\"\r\n")
                    outputFile.write("  include {\r\n")
                    outputFile.write("    phase: TRAIN\r\n")
                    outputFile.write("  }\r\n")
                    outputFile.write("  transform_param {\r\n")
                    outputFile.write("    mirror: false\r\n")
                    newOutputLine  = "    yolo_height: "+str(cfg.MODEL_IMAGE_HEIGHT) + "\r\n"
                    outputFile.write(newOutputLine)
                    newOutputLine  = "    yolo_width: "+str(cfg.MODEL_IMAGE_WIDTH) + "\r\n"
                    outputFile.write(newOutputLine)
                    outputFile.write("  }\r\n")
                    outputFile.write("  image_data_param {\r\n")
                    newOutputLine  = "    source: \"" + calibListFile + "\"\r\n"
                    outputFile.write(newOutputLine)
                    outputFile.write("    root_folder: \""+dataDir+"/\"\r\n")
                    outputFile.write("\r\n")
                    outputFile.write("    batch_size: 1\r\n")
                    outputFile.write("    shuffle: false\r\n")
                    outputFile.write("  }\r\n")
                    outputFile.write("}\r\n")
                    outputFile.write("\r\n")
            else:
                 outputFile.write(line)
        

    outputFile.close()

    print("Done")


if __name__=="__main__":
    main(sys.argv)




