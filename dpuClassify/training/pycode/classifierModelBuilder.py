#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


# -------------------------------------
#         Classifier Model Builder
# -------------------------------------

# build models for classifier transfer learning with tensorflow/keras 2.3

import os
from pathlib import Path

import tensorflow as tf
keras = tf.keras

__version__='3.0.0'




# -----------------------------------------------
# ---               Model Builder             ---
# -----------------------------------------------


def buildModel(cfg, preWeightsCacheDir=None):
    if (tf.__version__[0]== '1'): 
        tf.reset_default_graph() # clear the current tf graph
    if cfg.MODEL_TYPE=='vgg16':
        return buildModelVgg16(cfg, preWeightsCacheDir=preWeightsCacheDir)
    if cfg.MODEL_TYPE=='incV3':
        return buildModelIncV3(cfg, preWeightsCacheDir=preWeightsCacheDir)
    if cfg.MODEL_TYPE=='dense121':
        return buildModelDenseNet121(cfg, preWeightsCacheDir=preWeightsCacheDir)
    if cfg.MODEL_TYPE=='res50V2':
        return buildModelResNet50V2(cfg, preWeightsCacheDir=preWeightsCacheDir)

    print('ERROR: Unknown MODEL_TYPE')
    
    
# get the type of preprocessing for the image data for different net types
# the preprocessing during training and inference has to fit
# the preprocessing used for the pretrained weights    
def getInputScalingType(cfg):    
    if cfg.MODEL_TYPE=='vgg16':
        return 'caffe'
    if cfg.MODEL_TYPE=='incV3':
        return 'tf'
    if cfg.MODEL_TYPE=='dense121':
        return 'torch'
    if cfg.MODEL_TYPE=='res50V2':
        return 'tf'

        

# -----------------------------------------------
# ---                  VGG16                  ---
# -----------------------------------------------

def buildModelVgg16(cfg, preWeightsCacheDir=None):

    # --- caching pretrained weights ---
    weightsParam=cfg.INIT_WEIGHTS
    if (cfg.INIT_WEIGHTS=='imagenet') and (preWeightsCacheDir!=None):
        fileName=os.path.join(preWeightsCacheDir,'vgg16_imagenet.h5')
        if os.path.isfile(fileName):
            weightsParam = fileName
        else:
            mp = keras.applications.VGG16(weights='imagenet', include_top=False)
            mp.save_weights(fileName)
            if os.path.isfile(fileName):
                weightsParam = fileName
                
    # using keras functional api
    vgg_conv = keras.applications.VGG16(weights=weightsParam, include_top=False, input_shape=(cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH, 3))
    
    x = vgg_conv.output    
    x = keras.layers.GlobalAveragePooling2D(name='GlobalAveragePooling')(x)   
    #x = keras.layers.Dropout(0.5,name='Dropout1')(x)   
    x = keras.layers.Dense(cfg.DENSE1_NUM, activation='relu',name='Dense1')(x)
    x = keras.layers.Dropout(0.5, name='Dropout1')(x)    

    if cfg.NUMBER_OF_CLASSES == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = cfg.NUMBER_OF_CLASSES

    preds = keras.layers.Dense(units, activation=activation, name='Dense2')(x)
    model = keras.Model(inputs=vgg_conv.input, outputs=preds)    
    model._name='vgg16class'
    return model


# -----------------------------------------------
# ---             InceptionV3                 ---
# -----------------------------------------------
def buildModelIncV3(cfg, preWeightsCacheDir=None):
            
    # --- caching pretrained weights ---
    weightsParam=cfg.INIT_WEIGHTS
    if (cfg.INIT_WEIGHTS=='imagenet') and (preWeightsCacheDir!=None):
        fileName=os.path.join(preWeightsCacheDir,'incV3_imagenet.h5')
        if os.path.isfile(fileName):
            weightsParam = fileName
        else:
            mp = keras.applications.InceptionV3(weights='imagenet', include_top=False)
            mp.save_weights(fileName)
            if os.path.isfile(fileName):
                weightsParam = fileName
            
    baseModel=keras.applications.InceptionV3(weights=weightsParam, include_top=False, input_shape=(cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH,3))
    
    x = baseModel.output    
    x = keras.layers.GlobalAveragePooling2D(name='GlobalAveragePooling')(x)   
    #x = keras.layers.Dropout(0.5,name='Dropout1')(x)   
    x = keras.layers.Dense(cfg.DENSE1_NUM, activation='relu',name='Dense1')(x)
    x = keras.layers.Dropout(0.5, name='Dropout1')(x)    

    if cfg.NUMBER_OF_CLASSES == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = cfg.NUMBER_OF_CLASSES

    preds = keras.layers.Dense(units, activation=activation, name='Dense2')(x)
    model = keras.Model(inputs=baseModel.input, outputs=preds)
    model._name='incV3class'
    return model
    
    
# -----------------------------------------------
# ---              DenseNet121                ---
# -----------------------------------------------
def buildModelDenseNet121(cfg, preWeightsCacheDir=None):
                    
    # --- caching pretrained weights ---
    weightsParam=cfg.INIT_WEIGHTS
    if (cfg.INIT_WEIGHTS=='imagenet') and (preWeightsCacheDir!=None):
        fileName=os.path.join(preWeightsCacheDir,'denseNet121_imagenet.h5')
        if os.path.isfile(fileName):
            weightsParam = fileName
        else:
            mp = keras.applications.DenseNet121( include_top=False, weights='imagenet')
            mp.save_weights(fileName)
            if os.path.isfile(fileName):
                weightsParam = fileName
    
    baseModel = keras.applications.DenseNet121(
        include_top=False, weights=weightsParam, 
        input_shape=(cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH,3))    
    
    x = baseModel.output    
    x = keras.layers.GlobalAveragePooling2D(name='GlobalAveragePooling')(x)   
    #x = keras.layers.Dropout(0.5,name='Dropout1')(x)   
    x = keras.layers.Dense(cfg.DENSE1_NUM, activation='relu',name='Dense1')(x)
    x = keras.layers.Dropout(0.5, name='Dropout1')(x)    

    if cfg.NUMBER_OF_CLASSES == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = cfg.NUMBER_OF_CLASSES

    preds = keras.layers.Dense(units, activation=activation, name='Dense2')(x)
    model = keras.Model(inputs=baseModel.input, outputs=preds)
    model._name='denseNet121class'
    return model


# -----------------------------------------------
# ---              ResNet50V2                 ---
# -----------------------------------------------

def buildModelResNet50V2(cfg, preWeightsCacheDir=None):
                    
     # --- caching pretrained weights ---
    weightsParam=cfg.INIT_WEIGHTS
    if (cfg.INIT_WEIGHTS=='imagenet') and (preWeightsCacheDir!=None):
        fileName=os.path.join(preWeightsCacheDir,'resNet50V2_imagenet.h5')
        if os.path.isfile(fileName):
            weightsParam = fileName
        else:
            mp = keras.applications.ResNet50V2( include_top=False, weights='imagenet')   
            mp.save_weights(fileName)
            if os.path.isfile(fileName):
                weightsParam = fileName
    
    baseModel = keras.applications.ResNet50V2(
            include_top=False, weights=weightsParam, 
            input_shape=(cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH,3))    
    baseModel.trainable=False

    x = baseModel.output    
    x = keras.layers.GlobalAveragePooling2D(name='GlobalAveragePooling')(x)   
    #x = keras.layers.Dropout(0.5,name='Dropout1')(x)   
    x = keras.layers.Dense(cfg.DENSE1_NUM, activation='relu',name='Dense1')(x)
    x = keras.layers.Dropout(0.5, name='Dropout1')(x)    

    if cfg.NUMBER_OF_CLASSES == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = cfg.NUMBER_OF_CLASSES

    preds = keras.layers.Dense(units, activation=activation, name='Dense2')(x)
    model = keras.Model(inputs=baseModel.input, outputs=preds)    
    model._name='resnet50V2class'
    return model


 

# -----------------------------------------------
# ---         Preprocessing Layers            ---
# -----------------------------------------------

# generate the corresponding keras preprocessing layers for training
# see: https://towardsdatascience.com/preprocessing-layer-in-cnn-models-for-tf2-d471e61ddc2e

def preprocess_input_none(x):
    return x

def preprocess_input_rgb1s(x):
    x /= 127.5
    x -= 1.0
    return x

def preprocess_input_rgb01(x):
    return x/255.

def preprocess_input_tf(x):
    x /= 127.5
    x -= 1.0
    return x

def preprocess_input_torch(x):
    x /= 255.
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    x -= [0.485, 0.456, 0.406]
    x /= [0.229, 0.224, 0.225]
    return x

def preprocess_input_caffe(x):
    # 'RGB'->'BGR'
    x = x[..., ::-1]
    x -= [103.939, 116.779, 123.68]
    return x



@tf.function
def pix_preprocess_input_none(x):
    """    
    :param x: np.asarray([image, image, ...], dtype="float32") in RGB
    :return: normalized image RGB
    """
    batch, height, width, channels = x.shape
    x = tf.cast(x, tf.float32)
    x = tf.keras.backend.reshape(x, (-1, 3))
    result = preprocess_input_none(x)
    return tf.keras.backend.reshape(result, (-1, height, width, channels))

@tf.function
def pix_preprocess_input_rgb1s(x):
    """    
    :param x: np.asarray([image, image, ...], dtype="float32") in RGB
    :return: normalized image RGB
    """
    batch, height, width, channels = x.shape
    x = tf.keras.backend.reshape(x, (-1, 3))
    result = preprocess_input_rgb1s(x)
    return tf.keras.backend.reshape(result, (-1, height, width, channels))

@tf.function
def pix_preprocess_input_rgb01(x):
    """    
    :param x: np.asarray([image, image, ...], dtype="float32") in RGB
    :return: normalized image RGB
    """
    batch, height, width, channels = x.shape
    x = tf.keras.backend.reshape(x, (-1, 3))
    result = preprocess_input_rgb01(x)
    return tf.keras.backend.reshape(result, (-1, height, width, channels))

@tf.function
def pix_preprocess_input_tf(x):
    """    
    :param x: np.asarray([image, image, ...], dtype="float32") in RGB
    :return: normalized image RGB
    """
    batch, height, width, channels = x.shape
    x = tf.keras.backend.reshape(x, (-1, 3))
    result = preprocess_input_tf(x)
    return tf.keras.backend.reshape(result, (-1, height, width, channels))

@tf.function
def pix_preprocess_input_torch(x):
    """    
    :param x: np.asarray([image, image, ...], dtype="float32") in RGB
    :return: normalized image RGB
    """
    batch, height, width, channels = x.shape
    x = tf.keras.backend.reshape(x, (-1, 3))
    result = preprocess_input_torch(x)
    return tf.keras.backend.reshape(result, (-1, height, width, channels))

@tf.function
def pix_preprocess_input_caffe(x):
    """    
    :param x: np.asarray([image, image, ...], dtype="float32") in RGB
    :return: normalized image RGB
    """
    batch, height, width, channels = x.shape
    x = tf.keras.backend.reshape(x, (-1, 3))
    result = preprocess_input_caffe(x)
    return tf.keras.backend.reshape(result, (-1, height, width, channels))

class PixelPreprocessLayer(tf.keras.layers.Layer):
    def __init__(self, name="pixel_preprocess", inputScalingType='none', **kwargs):
        super(PixelPreprocessLayer, self).__init__(name=name, **kwargs)
                
        self.preprocess = pix_preprocess_input_none
        if inputScalingType=='rgb1s':
            self.preprocess = pix_preprocess_input_rgb1s
        if inputScalingType=='rgb01':
            self.preprocess = pix_preprocess_input_rgb01
        if inputScalingType=='tf':
            self.preprocess = pix_preprocess_input_tf
        if inputScalingType=='torch':
            self.preprocess = pix_preprocess_input_torch
        if inputScalingType=='caffe':
            self.preprocess = pix_preprocess_input_caffe
        
    def call(self, input):
        return self.preprocess(input)

    def get_config(self):
        config = super(PixelPreprocessLayer, self).get_config()
        return config
    
    
    

