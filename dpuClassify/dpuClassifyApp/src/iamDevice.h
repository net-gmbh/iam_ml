/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */



//=============================================================================
//      iamDeviceClass implements access to the synview iam camera device
//=============================================================================

#ifndef IAMDEVICE_H
#define IAMDEVICE_H

#define NUMBER_OF_BUFFERS 3     // number of synview frame buffers

#include "sv.synview.class.h"


class iamDeviceClass
{
public:
    iamDeviceClass ();
    ~iamDeviceClass ();

    LvStatus OpenCamera ( LvSystem* pSystem );
    LvStatus InitCamera ( );
    LvStatus OpenBuffers ( );
    LvStatus CloseBuffers ( );    

    LvStatus GetInt32CameraParam (LvFeature Feature, int32_t* piValue);
    LvStatus SetInt32CameraParam (LvFeature Feature, int32_t iValue);

    LvStatus GetFloatCameraParam (LvFeature Feature, float* pValue);
    LvStatus SetFloatCameraParam (LvFeature Feature, float Value);

    void CloseCamera ( );
    int  StartAcquisition ( );
    int  StopAcquisition ( );
    bool IsAcquiring ( );
    void SetCallback ( LvEventCallbackNewBufferFunct pFunction, void* pUserParam );
    void CallbackNewBuffer ( LvBuffer* pBuffer );
    
    bool ErrorOccurred ( LvStatus ErrorStatus );

    LvDevice*    m_pDevice;
    LvEvent*     m_pEvent;
    LvStream*    m_pStream;
    LvEvent*     m_pEventCam;

private:
    LvSystem*    m_pSystem;
    LvInterface* m_pInterface;

    LvEventCallbackNewBufferFunct m_pNewBufferFunction;
    void*                         m_pClass;

    LvBuffer*   m_Buffers[NUMBER_OF_BUFFERS];
    
    int         nWidth;
    int         nHeight;
    int         nOffsetX;
    int         nOffsetY;    

public:
	std::string  sDeviceId;

    int         maxAquiredBuffers;
    int         nInputFrames;
    int         nOutputFrames;
    int			NumUnderrunLast;
};
#endif //IAMDEVICE_H
