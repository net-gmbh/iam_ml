#!/bin/bash

# -------------------------------------------------
#            copy model folder to iam
# -------------------------------------------------


# --- read iam target settings ---
source iam_settings.mk


# --- prepare ssh access ---
if [[ $strict_host_key_checking -eq 0 ]] 
then
	echo no
	ssh-copy-id -o "StrictHostKeyChecking no" -i $key $user@$ip
else
	echo yes
	ssh-copy-id -i $key $user@$ip
fi


# --- create target folders ---
ssh -i $key $user@$ip "mkdir -p $folder"
ssh -i $key $user@$ip "mkdir -p $prog_folder"


# --- copy model folder ---
ssh -i $key $user@$ip "mkdir -p $prog_folder/model"
scp -i $key -r model $user@$ip:$prog_folder





