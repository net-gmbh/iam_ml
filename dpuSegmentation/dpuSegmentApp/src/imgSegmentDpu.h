/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#ifndef IMG_SEGMENT_H
#define IMG_SEGMENT_H

#include <vector>

#include "common.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
//#include "OsDep.h"

class imgSegmentClass 
{   
public:
	imgSegmentClass();
    ~imgSegmentClass();

	int configDpu(const std::string filename); 
	int procImage(cv::Mat img);
	
	int cleanup();

	int paramType;
	int paramEnableOverlay; // generate segmentation overlay on top of camera image
	int paramEnableIdxMap;
	int paramEnableMapWithDpuResolution; 
		
	GraphInfo shapes;
	
	int gapRequired;
	int numGapElements;
	
	int dpuResSize;
	int dpuResChannelNum;
	int dpuInputSize;
		
	float* dpuResultBuffer;
	float* softmax;
	float* dpuInputBuffer;

	int dpuResultBufferBytesAllocated;
	int dpuInputBufferBytesAllocated;

	cv::Mat image2;

	cv::Mat idxMat;
	cv::Mat idxMatFull;
	
    // see: /usr/incluse/xir
	
	std::unique_ptr<xir::Graph> graph;	
	std::vector<const xir::Subgraph*> subgraph;			
	std::unique_ptr<vart::Runner> runner;
	
	std::vector<const xir::Tensor*> inputTensors;	
	std::vector<const xir::Tensor*> outputTensors;
	
	std::vector<std::int32_t> out_dims;	
	std::vector<std::int32_t> in_dims;

	std::vector<std::unique_ptr<vart::TensorBuffer>> inputs, outputs;
	std::vector<vart::TensorBuffer*> inputsPtr, outputsPtr;
  	std::vector<std::shared_ptr<xir::Tensor>> batchTensors;

private:
	int configOkFlag;

};

#endif // IMG_SEGMENT_H
