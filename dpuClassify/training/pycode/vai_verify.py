#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.

# ----------------------------------
#  Classifier evaluation script
#    float vs quantized model   
# ----------------------------------

import sys
import os
import importlib
import argparse
#import numpy as np
#import cv2

import tensorflow as tf
from tensorflow_model_optimization.quantization.keras import vitis_quantize


import classifierModelBuilder as cmb



def main(argv):

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )                              
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )    
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' )                   
                      
    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import            
    cfg = importlib.import_module(cf)


    # --- settings ---
    modelFileNameFloat = os.path.join(args.workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME)
    modelFileNameQuant = os.path.join(args.workDir, 'modelVai','model_quantized.h5')
    datasetDir         = args.datasetDir #"/dataset"
    useDatasetFraction = cfg.VAI_VERIFY_DATASET_FRACTION
    batch_size = cfg.VAI_VERIFY_BATCH_SIZE
    

    # --- data source ---
    if cfg.NUMBER_OF_CLASSES==2:
        label_mode="int"
    else:
        label_mode="categorical"

    if useDatasetFraction<1.0:
        train_in_ds = tf.keras.preprocessing.image_dataset_from_directory(
            datasetDir,
            validation_split = useDatasetFraction,
            subset="validation",
            seed = 1333,
            label_mode = label_mode,
            image_size = (cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH),
            batch_size = batch_size
        )
    else:
        train_in_ds = tf.keras.preprocessing.image_dataset_from_directory(
            datasetDir,        
            label_mode = label_mode,
            image_size = (cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH),
            batch_size = batch_size
        )

    pixprocLayer=cmb.PixelPreprocessLayer(inputScalingType=cmb.getInputScalingType(cfg))
    pixel_scaling = tf.keras.Sequential(pixprocLayer)

    train_ds = train_in_ds.map(lambda x,y: (pixel_scaling(x),y))

    autotune = tf.data.experimental.AUTOTUNE
    train_ds = train_ds.cache()
    train_ds = train_ds.prefetch(buffer_size=autotune)

    if cfg.NUMBER_OF_CLASSES==2:
        loss      = "binary_crossentropy"
        metrics   =["accuracy"]    
    else:    
        loss      = tf.keras.losses.CategoricalCrossentropy()    
        metrics   = [tf.keras.metrics.CategoricalAccuracy()]  

    print('inputScalingType:', cmb.getInputScalingType(cfg))
    print('IMAGE_WIDTH:', cfg.IMAGE_WIDTH)
    print('IMAGE_HEIGHT:', cfg.IMAGE_HEIGHT)

    # --- evaluate float model ---
    if cfg.VAI_VERIFY_FLOAT_MODEL:
        print('Reading float model ... ')
        model = tf.keras.models.load_model(modelFileNameFloat)
        model.compile(loss=loss, metrics= metrics)
        resFloat=model.evaluate(train_ds)


    # --- evaluate quantized model ---
    if cfg.VAI_VERIFY_QUANT_MODEL:
        print('Reading quantized model ... ')
        model = tf.keras.models.load_model(modelFileNameQuant)
        model.compile(loss=loss, metrics= metrics)
        resQuant=model.evaluate(train_ds)


    # --- report result ---
    print('')
    print('-----------------------------------')
    print('--- Classifier Model Evaluation ---')
    print('---     float vs quantized      ---')
    print('-----------------------------------')
    print('')
    print("Dataset: ",datasetDir)
    if useDatasetFraction<1.0:
        print("(Using ",useDatasetFraction*100,"% of dataset samples only)")
    print('')
    if cfg.VAI_VERIFY_FLOAT_MODEL:
        print('--- Float Model ---')
        print("Model: ",modelFileNameFloat)
        print(model.metrics_names[0]+':  '+str(resFloat[0]))
        print(model.metrics_names[1]+':  '+str(resFloat[1]))
        print('')

    if cfg.VAI_VERIFY_QUANT_MODEL:
        print('--- Quantized Model ---')
        print("Model: ",modelFileNameQuant)
        print(model.metrics_names[0]+':  '+str(resQuant[0]))
        print(model.metrics_names[1]+':  '+str(resQuant[1]))
        print('')
    print('-----------------------------------')
    print('')



if __name__=="__main__":
    main(sys.argv)