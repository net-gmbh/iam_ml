#!/bin/bash

source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"	

# --- use fixed local copy of gen_anchors.py ---
#python3 scripts/gen_anchors.py \
#	-filelist $WORK_DIR/lists/anchorGen.txt \
#	-output_dir $WORK_DIR/lists \
#	-num_clusters 9

# --- fix numpy version problem ---
cwd=$(pwd)
cd /darknet/scripts
sed -i 's/np.zeros((k,dim),np.float)/np.zeros((k,dim),float)/g' gen_anchors.py
cd $cwd

# --- execute fixed script ---
python3 /darknet/scripts/gen_anchors.py \
	-filelist $WORK_DIR/lists/anchorGen.txt \
	-output_dir $WORK_DIR/lists \
	-num_clusters 9