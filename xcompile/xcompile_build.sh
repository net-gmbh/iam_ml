#!/bin/bash

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
#
#

helpFunction()
{
   echo "Usage:"
   echo "        xcompile_build  [OPTION] "
   echo -e "\t-h         :: show help "
   echo -e "\t-no option :: build xcompile environment"
   exit 1
}

while getopts "h" opt
do
   case "$opt" in
      h )  helpFunction;;
    \? ) helpFunction;;
   esac
done

##---------------------------
## colors
##---------------------------
mColorState='\033[1;36m' # bold cyan
mColorSubState='\033[0;36m' # cyan
mColorError='\033[1;41m' # red background
mColorInfo='\033[0;32m' # green
mColorFinish='\033[1;97;42m' # green background
mColorDebug='\033[1;35m' # bold magenta
mColorDefault='\033[0m'


##-----------------------------------------------------------------------------
displayState() {
    echo -e "${mColorState}-------------------------------------------------------------"
    echo -e "$1"
    echo -e "-------------------------------------------------------------${mColorDefault}"
}

##-----------------------------------------------------------------------------
displayError() {
    echo -e "${mColorError}$1${mColorDefault}"
}

##-----------------------------------------------------------------------------
displaySubState() {
    echo -e "${mColorSubState}-> $1 ${mColorDefault}"
}

##-----------------------------------------------------------------------------
displayFinish() {
    echo -e "${mColorFinish}$1${mColorDefault}"
}

##---------------------------
## settings
##---------------------------

XCOMPILE_NAME=xcompile_3_12



displayState "clean up"
rm -rf ${XCOMPILE_NAME}
rm -rf sysroots
rm environment-setup-cortexa72-cortexa53-xilinx-linux
rm site-config-cortexa72-cortexa53-xilinx-linux
rm version-cortexa72-cortexa53-xilinx-linux
echo "... done"

displayState "extract xcompile_x_xx.tar.gz"
tar xz -f ${XCOMPILE_NAME}.tar.gz
echo "... done"

displayState "build sdk"
./${XCOMPILE_NAME}/sdk.sh -d . -y
echo "... done"

displayState "install synview to sysroot"
SYNVIEW_IMAGE=$(find ${XCOMPILE_NAME}/synview*.tar.gz)
tar xz -f ${SYNVIEW_IMAGE} -C sysroots/cortexa72-cortexa53-xilinx-linux/
echo "... done"

displayState "install VitisAiRuntime14 to sysroot"
VITIS_AI_RT_EXT=${XCOMPILE_NAME}/VitisAiRuntime14_extract
cp -r ${VITIS_AI_RT_EXT}/vart/usr/include/vart sysroots/cortexa72-cortexa53-xilinx-linux/usr/include
cp    ${VITIS_AI_RT_EXT}/vart/libvart* sysroots/cortexa72-cortexa53-xilinx-linux/usr/lib
cp -r ${VITIS_AI_RT_EXT}/xir/include/xir sysroots/cortexa72-cortexa53-xilinx-linux/usr/include
cp    ${VITIS_AI_RT_EXT}/xir/libxir* sysroots/cortexa72-cortexa53-xilinx-linux/usr/lib
cp -r ${VITIS_AI_RT_EXT}/unilog/include/UniLog sysroots/cortexa72-cortexa53-xilinx-linux/usr/include
cp    ${VITIS_AI_RT_EXT}/unilog/libunilog* sysroots/cortexa72-cortexa53-xilinx-linux/usr/lib
cp    ${VITIS_AI_RT_EXT}/json/libjson* sysroots/cortexa72-cortexa53-xilinx-linux/usr/lib
echo "... done"

displayState "update axis2ddr lib and include files in apps/misc directory"
if [ -e "../apps/misc/axis2ddr" ]
then
  rm -rf ../apps/misc/axis2ddr/*
else
  mkdir ../apps/misc/axis2ddr
fi
cp -r ${XCOMPILE_NAME}/axis2ddr ../apps/misc/
echo "... done"

displayFinish "                                                             "
displayFinish "              XCOMPILE SUCCESSFULLY CREATED                  "
displayFinish "                                                             "
displaySubState "you can start building your apps now"
