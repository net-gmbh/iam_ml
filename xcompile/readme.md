# xcompile

Create docker image for compiling iam applications and enabeling SSH connection to iam cameras.   
The image contains an iam sysroot copy and the compile tools for cross compiling.   
An openssh-client and a generated SSH keypair is available for SSH connections.    


## Requirements 

1. Checkout this repository 

		

2. Start your camera and establish an openSSH connection to get the LNX_VERSION.

		// LNX_VERSION:
		cat /etc/petalinux/version

3. Download the **Cross Compile Archive File** 
from [xcompile_3_10.tar.gz (1.08 GB)](https://net-gmbh.com/wp-content/plugins/wordpress-nextcloud-downloads/download.php?file=files/downloads/data/iam/software/Cross%20Compile%20Environment/xcompile_3_10.tar.gz)
to this directory

		The nomenclature of the xcompile archive file is:
 			xcompile_[LNX_VERSION].tar.gz


4. Execute 

		win_docker_build_image.bat

Optionally "win_docker_run_bash.bat" starts a bash shell on the docker image with the current folder as workspace for test reasons. 
 


