# dpuSegmentation #
---

This application performs DPU accelerated image segmentation on the **iam ML ready** camera.
Image segmentation maps each pixel of the input image to one specific class. 
Numerous types of convolutional models with different complexity are available for segmentation tasks.
The example application corresponds to the Xilinx tutorial [05-Keras_FCN8_UNET_segmentation](https://github.com/Xilinx/Vitis-Tutorials/tree/master/Machine_Learning/Design_Tutorials/05-Keras_FCN8_UNET_segmentation).
  

![segmentation](/dpuSegmentation/misc/images/segmentation.png)
DPU interference time for different models on iam Zu2 and Zu5 cameras:

 Model        | resolution    |  DPU MACs  |   Zu2      |   Zu5
 -------------|---------------|------------|------------|---------
 FCN8         | 224x224       |  32.0 GOPS |      -     |  56.8 ms
 FCN8ups      | 224x224       |  30.7 GOPS |      -     |  53.7 ms
 FCN8_448     | 448x448       | 127.9 GOPS |      -     | 167.2 ms
 UNETv1       | 224x224       |  85.4 GOPS |  289.6 ms  | 104.3 ms
 UNETv2       | 224x224       |  83.7 GOPS |  284.6 ms  | 102.7 ms
 UNETv3       | 224x224       |  78.0 GOPS |  262.4 ms  |  94.6 ms


## Training ##

Training scripts for FCN8 and UNET models are available in folder `/training`.  
Models training is done with Tensorflow 2 and Keras.  


## iam Application ##
The folder /dpuSegmentAppMt contains the application source files and the make file for generating the executable on the iam. The reposity does not contain model and executable files. 
The complete application package with models and executable included, can be downloaded from [dpuSegmentAppMt.tar.gz](https://bitbucket.org/dcarpentier123/iam_ml_3xx/downloads/dpuSegmentAppMt.tar.gz)   
For installation on iam copy the .tar.gz file to `/home/root/dpuSegmentAppMt/` and extract files with...   
 `tar xz -f dpuSegmentAppMt.tar.gz`   

Start application...   
 `./dpuSegmentAppMt`

Connect iam via GigE Vision with Synview. 
The application provides GigE streaming with segmentation result overlay. Several XML features are available for camera control and debug.  

![app](/dpuSegmentation/misc/images/screenShotSegApp.png)

The folder `/dpuSegmentApp` contains a simplified version of the application without multithreading.
