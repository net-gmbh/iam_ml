
rem --------------------------------------------------
rem ---          run script via docker             ---
rem --------------------------------------------------

call configWin.bat

docker run^
  --gpus all^
  -it^
  --rm^
  -v %WORK_DIR_MAPPING%:/work^
  -v %DATASET_DIR_MAPPING%:/datasets^
  -v %cd%:/workspace^
  -w /workspace^
  %TRAIN_IMAGE_NAME%^
  bash -c "./s4_prepareModel.sh"
  


pause