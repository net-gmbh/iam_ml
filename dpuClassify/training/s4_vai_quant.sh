#!/bin/bash

source config.sh

sudo chmod 777 $WORK_DIR_IN_DOCKER/modelVai

python pycode/vai_quant.py \
          --configPath $(pwd)/projectConfig \
		  --configFile $PROJECT_CONFIG \
		  --workDir $WORK_DIR_IN_DOCKER \
		  --datasetDir $DATASET_DIR_IN_DOCKER \