#!/usr/bin/env python
# -*- coding: utf-8 -*-


#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


#import sys
import os
from pathlib import Path
#import string
import cv2
import numpy as np


'''
def mkdir_recursive( path):
    sub_path = os.path.dirname(path)
    if not os.path.exists(sub_path):
        mkdir_recursive(sub_path)
    if not os.path.exists(path):
        try:
            os.mkdir(path)
        except OSError as err:
            print(err)
'''
   


def get_current_directory():
    path = os.getcwd()
    return path
    
def createDir(dirName):
    if not os.path.exists(dirName):  
        #Path(dirName).mkdir( 0o777, parents=True, exist_ok=True) # recursive
        Path(dirName).mkdir( parents=True, exist_ok=True) # recursive
        #mkdir_recursive(dirName)
        if not os.path.exists(dirName):  
            print('ERROR: Could not create directory: ',dirName)
            return False
    return True

def removeFilesInDirectory(dirName):
    if os.path.exists(dirName):
        if (len(os.listdir(dirName))!=0):  # emty directory?
            for filename in os.listdir(dirName):
                file_path = os.path.join(dirName, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                        print('Failed to delete %s. Reason: %s' % (file_path, e))


def deleteImageFilesInDir(dirName):
    if len(os.listdir(dirName))!=0:  # emty directory?             
        print("Removing image files in directory ",dirName)
        for filename in os.listdir(dirName):
            file_path = os.path.join(dirName, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))







def loadImageArrNormalized( path, width, height, valueNormalize=True):
    img1 = cv2.imread(path, 1)
    img  = cv2.resize(img1, (width, height))
    img  = img.astype(np.float32)
    if valueNormalize:
        img  = img/127.5 - 1.0
    return img
    

def loadSegmentationArr( path , width , height, nClasses):
    seg_labels = np.zeros((  height , width  , nClasses ))
    img1 = cv2.imread(path, 1)
    img  = cv2.resize(img1, (width, height))
    img = img[:, : , 0]
    for c in range(nClasses):
        seg_labels[: , : , c ] = (img == c ).astype(int)
    return seg_labels


def loadSegmentationInOutDataset( pathIn, pathOut, width, height, numClasses, valueNormalize=True): 
    train_images = os.listdir(pathIn)
    train_images.sort()
    train_segmentations  = os.listdir(pathOut)
    train_segmentations.sort()
    X_train = []
    Y_train = []

    for im , seg in zip(train_images,train_segmentations) :        
        X_train.append( loadImageArrNormalized(os.path.join(pathIn,im),    width, height , valueNormalize=valueNormalize))
        Y_train.append( loadSegmentationArr   (os.path.join(pathOut,seg) , width, height, numClasses)  )

    X_train, Y_train = np.array(X_train), np.array(Y_train)
    return (X_train, Y_train)



    
def calculate_IoU(truthMat,predictedMat,numClasses):
    ## mean Intersection over Union
    ## Mean IoU = TP/(FN + TP + FP)
    IoUs = []    
    for c in range(numClasses):
        TP = np.sum( (truthMat == c)&(predictedMat == c))
        FP = np.sum( (truthMat != c)&(predictedMat == c))
        FN = np.sum( (truthMat == c)&(predictedMat != c))
        IoU = TP/float(TP + FP + FN)               
        IoUs.append(IoU)    
    return IoUs
    






    