/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef DPUOBJDETYOLOV4_H
#define DPUOBJDETYOLOV4_H


#include "common.h"


typedef struct {
  int w;
  int h;
  int c;
  float* data;
} image;



class dpuObjDetYoloV4Class
{   

public:
	dpuObjDetYoloV4Class();
    ~dpuObjDetYoloV4Class();

	int configDpu();
	int procImg(cv::Mat img);	
	int cleanup();		

    
    void get_output(int8_t* dpuOut, int sizeOut, int oc, int oh, int ow,
                float output_scale, vector<float>& result);

    vector<vector<float>> applyNMS(vector<vector<float>>& boxes, int classes,
                               const float thres);

    void applyNMS_vialib(const vector<vector<float>>& boxes, const vector<float>& scores,
              const float nms,  vector<size_t>& res,
              bool stable);
    

    void detect(vector<vector<float>>& boxes, int channel,
            int gridHeight, int gridWidth, int gridIdx,  float scale);

    void correct_region_boxes(vector<vector<float>>& boxes, int n, int w, int h,
                          int netw, int neth, int relative);


    void postProcess(cv::Mat& frame, vector<int8_t*> results,
                 const float* output_scale);

    
	double tickCountStart;
	double tickCountStop;

	GraphInfo shapes;
    TensorShape inshapes[1];  // 1 input node
    TensorShape outshapes[4]; // up to 4 output nodes
    int inputCnt;
    int outputCnt;
    
    float NMS_THRESHOLD;
   
    bool nmsVialib=true;
    int  numBoxesLimit=6000;
    float confClassMin = 0.5;
    float confObjMin   = 0.5;  
     
	
private:
	// --- VART ---
   	std::unique_ptr<vart::Runner> runner;	
    std::vector<std::unique_ptr<xir::Tensor> > inputTensors;
    std::vector<std::unique_ptr<xir::Tensor> > outputTensors;

    int widthNet;
    int heightNet;

    

    float input_scale;
    vector<float> output_scale;

    int8_t* data    = 0;
    int8_t* result0 = 0;
    int8_t* result1 = 0;
    int8_t* result2 = 0;
    
    vector<int8_t*> results;
    
    int classificationCnt;    
    
    std::vector<std::unique_ptr<vart::TensorBuffer>> inputs, outputs;
    std::vector<vart::TensorBuffer*> inputsPtr, outputsPtr;	

	int configOkFlag;
	cv::Mat image2;	
	
};

#endif // DPUOBJDETYOLOV4_H
