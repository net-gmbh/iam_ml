/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "main.h"
#include "app.h"
#include "bufferManager.h"
#include "gigeServerEvents.h"

extern bufferManagerClass bufferManager;
extern camDeviceClass camDevice;



appClass::appClass () {    
    autorun = false;
}

appClass::~appClass() {
}


// --------------------------------------

int appClass::init( int autorunMode )
{			
    int val;
    int ok;
    LvStatus SynviewStatus;
    
    exitAppFlag            = false;
    modelOkFlag            = false;
    autorun                = autorunMode;
    enableSegmentationFlag   = true;
    enableOsdFlag          = true;
    outputImagePattern     = 0;
    enablePrintfDpuFlag    = false;   
    enablePrintfBufferManager   = false;   
    enablePrintfDpuFlag    = false;   
    enableTestPicFlag      = false;
    loadTestImageFlag      = 0;
    enableStreaming        = false;
    saveFrontendSettingsRequestFlag=0;
    doModelLoadFlag        = false;
    modelDirString         = "empty";
    trainLabel             = "none";
    sensorPixelFormat      = LvPixelFormat_BayerGR8;
    outputImageMode        = 0;    // 0=RGB, 1=Bayer
    outputPixelFormat      = LvPixelFormat_RGB8;
    testImageLoaded        = false;
    enableGevStreaming     = true;
    enableTemperatureOsd   = false;

    

    // --- check iam platform version ---
    printf(COLOR_YEL "scanning iam platform version...\n" COLOR_RESET);
    int imageVersionMain;
    int imageVersionSub;
    getImageVersion(&imageVersionMain, &imageVersionSub);
    if (imageVersionMain==3) {
        printf(COLOR_GRN "   Image version: 3 --> ok\n" COLOR_RESET);
    } else {
        printf(COLOR_RED "   False image version --> error\n" COLOR_RESET);
        return 0;
    }


    // --- check dpu version ---
    printf(COLOR_YEL "scanning dpu version...\n" COLOR_RESET);
    dpuVersion = getDpuVersion();
    if (dpuVersion==2) {
        printf(COLOR_GRN "   DPU zu2 found\n" COLOR_RESET);
    } else if (dpuVersion==5) {
        printf(COLOR_GRN "   DPU zu5 found\n" COLOR_RESET);
    } else {
        printf(COLOR_RED "   ERROR: No DPU found\n" COLOR_RESET);
        return 0;
    }

    // --- open synview ---
    printf(COLOR_YEL "open synview library...\n" COLOR_RESET);

    char Msg[1024];
    string sPathCti;

    string sCtid = "cti\\sv.gentl.cti";    
    
    if (LvLibrary::OpenLibrary() != LVSTATUS_OK)
    {
        printf(COLOR_RED "   Open Synview library failed\n" COLOR_RESET);
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf(COLOR_RED "   Synview: Error open Library: %s\n" COLOR_RESET,   Msg);
        return 0;
    }

    printf("   Open system...\n" );
    if (LvSystem::Open("", pSystem) != LVSTATUS_OK)
    {
        printf("   Open first chance system failed\n" );
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("   Error open system: %s\n",  Msg);

        // 2nd chance
        string sPath;
        LvLibrary::GetLibInfoStr(LvInfo_BinPath, sPath);
        sPath = sPath+sCtid;
        printf("   Open system: \"%s\"\n",   sPath.c_str());
        if (LvSystem::Open(sPath.c_str(), pSystem) != LVSTATUS_OK)
        {
            printf(COLOR_RED "   Open system failed\n" COLOR_RESET);
            LvGetLastErrorMessage(Msg, sizeof(Msg));
            printf(COLOR_RED "   Error open system: %s\n" COLOR_RESET,   Msg);
            LvLibrary::CloseLibrary();
            return 0;
        }
    }
    pSystem->GetString(LvSystem_TLPath, sPathCti);
    printf("   used cti file: \"%s\"\n",  sPathCti.c_str());
       

    // open camera frontend
    printf("   Open camera...\n" );
    if (camDevice.OpenCamera ( ) != LVSTATUS_OK) 
    {
        printf(COLOR_RED "   Synview: Open camera failed\n" COLOR_RESET  );
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf(COLOR_RED "   Error open camera: %s\n" COLOR_RESET, Msg);
        return 0;
    }


    // --- scan model directory ---      
    printf(COLOR_YEL "scanning model directories...\n" COLOR_RESET);

    std::string modelPath=MODEL_FOLDER_NAME;
    modelPath.append("/");

    
    if (!std::filesystem::is_directory(modelPath)) {
        printf(COLOR_RED "   Model directory not found!\n" COLOR_RESET);           
        return 0;       
    }
    
    modelDirectoryNames.clear();    
    for (auto &p : std::filesystem::recursive_directory_iterator(modelPath)) {
        if (p.is_directory()) {
            printf("   %s\n",p.path().string().c_str());
            modelDirectoryNames.push_back(p.path().string());
        }
    }    

    

    // --- select default model --- 
     currentModelIdx = -1;

    if (modelDirectoryNames.size()==0) {
        printf(COLOR_RED "   No model directories found!\n" COLOR_RESET);           
    } else {  
    
        std::string default_model_path = modelPath;  
        std::string defModeTxtFile     = modelPath;  

        default_model_path.append("default_");     
        defModeTxtFile.append("default_"); 
        if (dpuVersion==2) {
            default_model_path.append("zu2");
            defModeTxtFile.append("zu2.txt");
        }
        if (dpuVersion==5) {
            default_model_path.append("zu5");
            defModeTxtFile.append("zu5.txt");
        }
        
        std::string defModelSubDirName;
        printf("   reading default model txt file: %s\n" , defModeTxtFile.c_str()); 
        ok = readDefaultModelFile(defModeTxtFile, defModelSubDirName);
        if (ok) {
            printf("   default model from txt: %s\n" , defModelSubDirName.c_str()); 
            default_model_path=modelPath.append(defModelSubDirName);
        }
        
        for (auto i=0u; i<modelDirectoryNames.size(); i++) {
            if (modelDirectoryNames[i].compare(default_model_path) == 0) {
                currentModelIdx=i;
                break;
            }
        }

        if (currentModelIdx<0) {
            printf("   default model directory %s not found!\n" , default_model_path.c_str());    
            currentModelIdx=0;        // take first entry for startup
            printf("   starting up with model %s\n" , modelDirectoryNames[currentModelIdx].c_str());             
        }
    }

    // --- load dpu model ---         
    if (currentModelIdx>=0) {
        //printf(COLOR_YEL "loading dpu model...\n" COLOR_RESET);  
        printf(COLOR_YEL "   loading dpu model file: %s\n" COLOR_RESET, modelDirectoryNames[currentModelIdx].c_str());  
        modelOkFlag = loadModel(modelDirectoryNames[currentModelIdx]);
    } else {
        modelOkFlag=0;
    }


    // --- read test images directory ---
    printf(COLOR_YEL "scanning test images...\n" COLOR_RESET);
    testImageFileNames.clear();
    std::string test_img_path=TEST_IMG_FOLDER_NAME;//"imgTest";

    if (!std::filesystem::is_directory(test_img_path.c_str())) {
        printf(COLOR_RED "   Test image directory not found!\n" COLOR_RESET);           
    } else {        
        for (auto &p : std::filesystem::recursive_directory_iterator(test_img_path)) {
            if ((p.path().extension() == ".png")||
                (p.path().extension() == ".jpg")||
                (p.path().extension() == ".JPG")||
                (p.path().extension() == ".bmp")) {
                testImageFileNames.push_back(p.path().string());                
            }
        }
                
        if (testImageFileNames.size()>0) {
            for (int i=0; i<(int)testImageFileNames.size(); i++) {
                printf("   #%d: %s\n",i,testImageFileNames[i].c_str());
            }
        }
    }


    // --- xml boosting ---
    printf(COLOR_YEL "xml boosting...\n" COLOR_RESET);
    ok = xmlFileBooster(XML_FILE, "./final.xml");
    bool useBoostedXml = true;
    if (!ok) {
        printf("   xmlFileBooster error\n");
        useBoostedXml =false;
    }
    

    // --- init frontend ---
    printf(COLOR_YEL "init camera frontend...\n" COLOR_RESET);

    SynviewStatus = camDevice.InitCamera ();
    if (ErrorOccurred(SynviewStatus)) return 0;	
		
	setSensorPixFormat();    

    // startup image size ==> full sensor size    
    SynviewStatus=camDevice.GetInt32CameraParam( LvDevice_SensorWidth,  &val );
    sensorWidth = (int)val;
    ErrorOccurred(SynviewStatus);
    SynviewStatus=camDevice.GetInt32CameraParam( LvDevice_SensorHeight, &val);
    sensorHeight = (int)val;
    ErrorOccurred(SynviewStatus);

    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_Width, sensorWidth);
    ErrorOccurred(SynviewStatus);
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_Height, sensorHeight);
    ErrorOccurred(SynviewStatus);
                    
    // setup default input image decimation factor
    if (sensorHeight>4*480)       sensorDecimation=4;
    else if  (sensorHeight>2*480) sensorDecimation=2;
    else                          sensorDecimation=1;

    calculateOutputImgSize();

    // use frontend auto exposure
    autoExposureMode=1;
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Continuous);
    ErrorOccurred(SynviewStatus);

    // get the current exposure value from frontend
    float extime;
    SynviewStatus = camDevice.GetFloatCameraParam(LvDevice_ExposureTime, &extime);
    exposureTime=extime;
    ErrorOccurred(SynviewStatus);

    SynviewStatus = camDevice.SetFloatCameraParam ( LvDevice_Gamma, 1.0);
    ErrorOccurred(SynviewStatus);

    // --- init gige server ---
    printf(COLOR_YEL "setup gige server...\n" COLOR_RESET);
    std::string xmlFileName;
    if (useBoostedXml) {
        xmlFileName =  "./final.xml";
    } else {
        xmlFileName = XML_FILE;
    }
    ok = camDevice.setupGigeServer(xmlFileName);
    if (!ok) return 0;
    
    
         
    // start frontend in case of autostart mode ---
    if (autorun) {
        
        printf(COLOR_YEL "Running in auto mode --> Starting frontend...\n" COLOR_RESET);
        int error = camDevice.StartStream ();
        if ( error ) {
            printf(COLOR_RED "   ERROR: StartStream error\n" COLOR_RESET);
        }

        printf("   bufferManager init\n" );
        bufferManager.initBuffer1(NUMBER_OF_AQUISITION_BUFFERS);
        bufferManager.initBuffer2(NUMBER_OF_SEND_BUFFERS, NUMBER_OF_PROCESSING_THREADS);
        
        error = camDevice.StartAcquisition ();
        if ( error ) {
            printf(COLOR_RED "   ERROR: StartAcquisition error\n" COLOR_RESET);
        }
    }
   
    return 1;
}


void appClass::close() {
   
}



 void appClass::processPendingTasks() {

    bufferManager.enableAnalyser = enablePrintfBufferFps; 

    if (enableTemperatureOsd) {
        double temp;

        pDevice->SetEnumStr(LvDevice_DeviceTemperatureSelector, "Mainboard_FPD");        
        pDevice->GetFloat(LvDevice_DeviceTemperature, &temp);
        devTempMain=temp;
        //PRINTF (( "MainFPD Temperature: %f", devTemp1 ));    

        pDevice->SetEnumStr(LvDevice_DeviceTemperatureSelector, "Sensor");       
        pDevice->GetFloat(LvDevice_DeviceTemperature, &temp);
        devTempSensor = temp;
        //PRINTF (( "Sensor Temperature: %f", devTemp2 ));   
    }

    if (doModelLoadFlag) {        
        
        // --- load new model  ---     
        if (currentModelIdx<(int)modelDirectoryNames.size()) {                   
            modelOkFlag = loadModel(modelDirectoryNames[currentModelIdx]);
            process_thread_do_modelConfig.fill(true); // trigger processing thread reconfiguration
        }
        doModelLoadFlag=0;
    }

    // --- load test image ---
    if (loadTestImageFlag>0) {  
        if (mtx_testImageFileNames.try_lock()) {         
            if (loadTestImage-1<(int)testImageFileNames.size()) {   
                if ( (int)testImageFileNames.size()>0 ) {           
                    cv::Mat testPic=cv::imread(testImageFileNames[loadTestImage-1]);
                    mtx_testPicRgb.lock();
                    cvtColor( testPic, testPicRgb, cv::COLOR_BGR2RGB );
                    mtx_testPicRgb.unlock();
                    testImageLoaded=true;
                }
            }
            mtx_testImageFileNames.unlock();
            loadTestImageFlag=0; 
        }
    }

    if (saveFrontendSettingsRequestFlag) {
        LvFeature ftr;  
        LvStatus SynviewStatus;   

        //SynviewStatus = camDevice.m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "WhiteBalanceAuto", &ftr);
        //SynviewStatus = camDevice.m_pDevice->SetEnumStr(ftr,"Once");
        //ErrorOccurred(SynviewStatus);    
		
		int restartAcquisition=0;
        int error=0;
		
		if ( camDevice.IsAcquiring () ) {
            restartAcquisition=1;
            error = camDevice.StopAcquisition ();
            if ( error) {
                printf(("ERROR: StopAcquisition error\n"));
            }
        }
        error = camDevice.StartAcquisition ();
        if ( error ) {
            printf(("ERROR: StopAcquisition error\n"));
        }
        error = camDevice.StopAcquisition ();
        if ( error) {
            printf(("ERROR: StopAcquisition error\n"));
        }     
                       
        SynviewStatus = pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "UserSetSelector", &ftr);
        ErrorOccurred(SynviewStatus);  
        SynviewStatus = pDevice->SetEnumStr(ftr,"UserSet1");
        ErrorOccurred(SynviewStatus); 
        SynviewStatus = pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "UserSetSave", &ftr);
        ErrorOccurred(SynviewStatus);  
        SynviewStatus = pDevice->CmdExecute(ftr); 
        ErrorOccurred(SynviewStatus);                     
        SynviewStatus = pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "UserSetDefault", &ftr);
        ErrorOccurred(SynviewStatus);  
        SynviewStatus = pDevice->SetEnumStr(ftr, "UserSet1");
        ErrorOccurred(SynviewStatus);  
		
		 if (restartAcquisition) {
             error = camDevice.StartAcquisition ();
            if ( error ) {
                printf(("ERROR: StopAcquisition error\n"));
            }
        }

        saveFrontendSettingsRequestFlag=0;
    }
 }







void appClass::setSensorPixFormat() {
    LvStatus SynviewStatus;
        
    // color sensor --> choose any available bayer format
    sensorPixelFormat = LvPixelFormat_BayerGR8;
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);    
    if (SynviewStatus==LVSTATUS_OK) {
        printf("   Sensor pixel format: BayerGR8\n");
        return;
    }  
    
    sensorPixelFormat = LvPixelFormat_BayerRG8;
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("   Sensor pixel format: BayerRG8\n");
        return;
    }
       
    sensorPixelFormat = LvPixelFormat_BayerGB8;
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("   Sensor pixel format: BayerGB8\n");
        return;
    }

    sensorPixelFormat = LvPixelFormat_BayerBG8;
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("   Sensor pixel format: BayerBG8\n");
        return;
    }

    sensorPixelFormat = LvPixelFormat_Mono8;
    SynviewStatus = camDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
    if (SynviewStatus==LVSTATUS_OK) {
        printf("   Sensor pixel format: Mono8\n");
        return;
    }

    printf(COLOR_RED "   ERROR: No suitable pixel format found\n" COLOR_RESET);                            
}


void appClass::calculateOutputImgSize() {
        
    if (sensorDecimation==1) {
        imgWidthOut =int( sensorWidth /1);
        imgHeightOut=int( sensorHeight/1);
    } 
    if (sensorDecimation==2) {
        imgWidthOut =int( sensorWidth /2);
        imgHeightOut=int( sensorHeight/2);
    }
    if (sensorDecimation==4) {
        imgWidthOut =int( sensorWidth /4);
        imgHeightOut=int( sensorHeight/4);
    }    
}


int appClass::loadModel(std::string path) {

    std::lock_guard<std::mutex> guardFor(mtx_modelConfig);

    std::string fileName;
    //int ok;

    printf(COLOR_CYN ">----   Loading model   ----<\n" COLOR_RESET);

    if (!std::filesystem::is_directory(path.c_str())) {
        printf(COLOR_RED "   Model directory not found!\n" COLOR_RESET);   
        return 0;       
    }

    
        
    // --- load dpu config file ---
    // pick first *.xmodel file in default dir
    std::string ext(".xmodel");
    std::string elfFileName;
    for (auto &p : std::filesystem::recursive_directory_iterator(path)) {
        if (p.path().extension() == ext)
        elfFileName=p.path().stem().string();
    }
    
    fileName=path;
    fileName.append("/");
    fileName.append(elfFileName);
    fileName.append(".xmodel");
                 
    if (FILE *file = fopen(fileName.c_str(), "r")) {
        fclose(file);
        printf ("   loading graph config from file %s\n" ,fileName.c_str());
        int ok=loadGraph(fileName);
        if (!ok) {       
            printf (COLOR_RED "   ERROR: graph config file not found\n" COLOR_RESET);     
            return 0;
        }
        //imgClassifier.configDpu();
        process_thread_do_modelConfig.fill(true); // trigger threads to load new model
    } else {
        printf(COLOR_RED "   graph config file not found!\n" COLOR_RESET);            
        return 0;            
    }  



    // --- load class names ---
    std::string pathFnClass=path;
    pathFnClass.append("/classList.txt");        
    readClassListFile(pathFnClass);

    printf("--- classList ---\n");
    for (int i=0; i<(int)classNames.size(); i++) {
        printf("%s (%d,%d,%d)\n",classNames.at(i).c_str(),redMarker[i],greenMarker[i],blueMarker[i]);
    }



    // --- xml string ---
    std::string pathStr = MODEL_FOLDER_NAME;
    int pathStrLength=pathStr.length()+1; // +1 for the "/"
    std::string displayName=path;
    displayName.erase(0,pathStrLength);
    //strcpy ( modelDirString, displayName.c_str());
    modelDirString = displayName;
    
    
    printf(COLOR_CYN ">---- Loading model done ----<\n" COLOR_RESET);
    return 1;
}






int appClass::loadGraph(const std::string filename) {
    printf("Loading DPU model file %s\n",filename.c_str());
    //printf("Filename: %s\n",filename.c_str());

    if (FILE *file = fopen(filename.c_str(), "r")) {
        fclose(file);        
    } else {
        printf("ERROR: DPU model file not found\n");
        //configOkFlag = 0;
        
        return 0;
    }
    
    graph = xir::Graph::deserialize(filename);
    subgraph = get_dpu_subgraph(graph.get());
    return 1;
}



void appClass::readClassListFile(const std::string filename)
{	
    char cn[200];

    classNames.clear();
    redMarker.clear();
    greenMarker.clear();
    blueMarker.clear();
    

	FILE* fp;
    fp = fopen(filename.c_str(), "r");
	if (fp == NULL) {
        printf("classList.txt file not found\n");	
        fclose(fp);
        return;
    }

	char line[200];
	while (!feof(fp)) {
	
        if (fgets(line, 100, fp) != NULL) {		    
            //if (line.back()=='\r') line.erase(line.length()-1,1); // remove cr

            int c1,c2,c3;
            int sres = sscanf(line, "%s %d %d %d", cn,&c1,&c2,&c3);
            if (sres==4) {
                //printf("classList entry: %s %d %d %d\n",cn,c1,c2,c3);
		    
			    classNames.push_back(cn);

                redMarker.push_back(c1);
                greenMarker.push_back(c2);
                blueMarker.push_back(c3);

               
            }
        }
	}

	fclose(fp);
}





int appClass::xmlFileBooster(const std::string filenameIn, const std::string filenameOut) {
    std::ifstream fp(filenameIn);
	if (!fp.is_open()) {				
        return 0;
	}

    ofstream outFile;
    outFile.open (filenameOut);

    if (!outFile.is_open()) {	
        printf("can not open xml output file\n");
        fp.close();			
        return 0;
	}

    std::string line;    
    int lineCnt=0;

    bool marker_testImageFileNames_found = false;
    bool marker_modelDirNames_found = false;

    while(getline(fp, line)) {
        lineCnt++;
       
        if (line.compare("<!--TEST_IMAGE_ENUM_ENTRY_LIST-->") == 0) {
            printf("   add available test image list at line %d \n", lineCnt);
            marker_testImageFileNames_found = true;
            std::string path=TEST_IMG_FOLDER_NAME;
            int pathLength=path.length()+1; // +1 for the "/"
            // add enum entry for each test image
            for(auto i=0u; i<testImageFileNames.size(); i++) {
                std::string displayName=testImageFileNames[i];
                displayName.erase(0,pathLength);
                outFile<< "\t\t<EnumEntry Name=\"pic"<<i<<"\" NameSpace=\"Standard\">"<<endl;
                outFile<< "\t\t\t<DisplayName>"<<displayName<<"</DisplayName>"<<endl;
                outFile<< "\t\t\t<Value>"<<i+1<<"</Value>"<<endl;
                outFile<< "\t\t</EnumEntry>"<<endl;
            }            
        } else if (line.compare("<!--MODEL_ENUM_ENTRY_LIST-->") == 0) {
            printf("   add available model list at line %d \n", lineCnt);
            marker_modelDirNames_found = true;
            std::string path=MODEL_FOLDER_NAME;
            int pathLength=path.length()+1; // +1 for the "/"
            if (modelDirectoryNames.size()==0) {
                // generate dummy entry, otherwise xml error
                outFile<< "\t\t<EnumEntry Name=\"empy"<<"\" NameSpace=\"Standard\">"<<endl;
                outFile<< "\t\t\t<DisplayName>"<<"empy"<<"</DisplayName>"<<endl;
                outFile<< "\t\t\t<Value>"<<0<<"</Value>"<<endl;
                outFile<< "\t\t</EnumEntry>"<<endl;
            } 
            // add enum entry for each model subdir
            for(auto i=0u; i<modelDirectoryNames.size(); i++) {
                std::string displayName=modelDirectoryNames[i];
                displayName.erase(0,pathLength);
                outFile<< "\t\t<EnumEntry Name=\"dir"<<i<<"\" NameSpace=\"Standard\">"<<endl;
                outFile<< "\t\t\t<DisplayName>"<<displayName<<"</DisplayName>"<<endl;
                outFile<< "\t\t\t<Value>"<<i<<"</Value>"<<endl;
                outFile<< "\t\t</EnumEntry>"<<endl;
            }
        } else {
            outFile<<line<<endl;
        }
        
	}
    printf("   xml lines: %d\n",lineCnt);
    

    fp.close();	
    outFile.close();

    if (!marker_testImageFileNames_found) {
         printf("   TEST_IMAGE_ENUM_ENTRY_LIST marker not found\n");
         return 0;
    }

    if (!marker_modelDirNames_found) {
         printf("   MODEL_ENUM_ENTRY_LIST marker not found\n");
         return 0;
    }

    return 1;    
}






int appClass::getDpuVersion() {
    FILE* fp;
    char line[200];
    int res=0;

    printf("---- Reading Dpu Config ----\n");
    fp = fopen("/etc/petalinux/product", "r");
	if (fp == NULL) {
        printf("ERROR: Can not open /etc/petalinux/product\n");
        fclose(fp);
        return 0;
    }

    if (fgets(line, 100, fp) != NULL) {           
        std::string lineStr=std::string(line);
        
        if (lineStr.compare(0,16,"iam_zu5_mipi_dpu")==0) {
            res=5;
            printf("zu5 id found\n");
        } else if (lineStr.compare(0,16,"iam_zu5_lvds_dpu")==0) {
            res=5;
            printf("zu5 id found\n");
        } else if (lineStr.compare(0,16,"iam_zu2_mipi_dpu")==0) {
            res=2;
            printf("zu2 id found\n");
        } else if (lineStr.compare(0,16,"iam_zu2_lvds_dpu")==0) {
            res=2;
            printf("zu2 id found\n");
        } else if (lineStr.compare(0,12,"iam_lvds_dpu")==0) {
            res=2;
            printf("zu2 id found\n");
        } else {
            printf("ERROR: unknown dpu id\n");
        }        
    } else {
        printf("gets error\n");        
    }

    fclose(fp);
    return res;
}

int appClass::getImageVersion(int *mainVersion, int *subVersion) {
    FILE* fp;
    char f1[200];
    char line[200];
    int res=0;

    //printf("---- Reading  Image Version ----\n");
    fp = fopen("/etc/petalinux/version", "r");
	if (fp == NULL) {
        printf("ERROR: Can not open /etc/petalinux/version\n");
        fclose(fp);
        return 0;
    }

    if (fgets(line, 100, fp) != NULL) {
        int sres = sscanf(line, "%s", f1);        
        if (sres==1) {
            //printf("Image version: %s\n",f1);
            
            int vres = sscanf(f1, "%i.%i",mainVersion, subVersion );     
            if (vres==2) {
                //printf("Main version: %i\n",*mainVersion);
                //printf("Sub  version: %i\n",*subVersion);
            } else {
                printf("Version format error\n");   
                *mainVersion = 0;
                *subVersion  = 0;  
            }
            
        } else {
            printf("sscanf error\n");            
        }
    } else {
        printf("gets error\n");        
    }

    fclose(fp);
    return res;
}



int appClass::readDefaultModelFile(const std::string filename, std::string &name) {
    FILE* fp;    
    char line[200];
    
    
    printf("---- Parsing default model file ----\n");
    fp = fopen(filename.c_str(), "r");
	if (fp == NULL) {
        printf("ERROR: Can not open default model file\n");
        return 0;
    }

    

	if (fgets(line, 200, fp) != NULL) {
        name=std::string(line);            
        fclose(fp);
        return 1;
	} else {
        fclose(fp);
        return 0;
    }
		
	
}


