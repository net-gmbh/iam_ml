/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */



#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <string>
#include <time.h>

#include <cstdint>
#include <atomic>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <vector>
#include <array>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include "sv.synview.class.h"
#include "common.h"
#include "app.h"


typedef uint64_t U64BIT;
typedef uint32_t U32BIT;

#define COLOR_RED   "\x1B[31m"
#define COLOR_GRN   "\x1B[32m"
#define COLOR_YEL   "\x1B[33m"
#define COLOR_BLU   "\x1B[34m"
#define COLOR_MAG   "\x1B[35m"
#define COLOR_CYN   "\x1B[36m"
#define COLOR_WHT   "\x1B[7m"
#define COLOR_RESET "\x1B[0m"

#define APPLICATION_NAME  "dpuObjDetYoloV4"
#define XML_FILE          "./dpuObjDetYoloV4.xml"

#define MODEL_FOLDER_NAME    "model"
#define TEST_IMG_FOLDER_NAME "imgTest"

#define NUMBER_OF_PROCESSING_THREADS 2   // 2 is optimum
#define NUMBER_OF_AQUISITION_BUFFERS NUMBER_OF_PROCESSING_THREADS+4     // number of synview frame buffers
#define NUMBER_OF_SEND_BUFFERS       2*NUMBER_OF_PROCESSING_THREADS+4

// --- synview ---
extern LvSystem*    pSystem;
extern LvInterface* pInterface;
extern LvDevice*    pDevice;

extern void LV_STDC CallbackNewBufferFunction(LvHBuffer buffer, void* pUserPointer, void* pUserParam);


extern LvBuffer*    m_Buffers[NUMBER_OF_AQUISITION_BUFFERS];


extern void sendFrameThreadFunction(bool& is_running);

extern void processFrameThreadFunction(int id, bool& do_modelConfig, bool& is_running);
extern std::array<bool, NUMBER_OF_PROCESSING_THREADS> process_thread_do_modelConfig;
extern std::array<bool, NUMBER_OF_PROCESSING_THREADS> process_thread_is_running;


extern std::atomic<int>  sensorWidth;
extern std::atomic<int>  sensorHeight;
extern std::atomic<int>  imgWidthOut;
extern std::atomic<int>  imgHeightOut;

extern std::atomic<bool> enableStreaming;
extern std::atomic<int>  outputPixelFormat;
extern std::atomic<int>  outputImageMode;
extern std::atomic<int>  sensorPixelFormat;   
extern std::atomic<int>  sensorDecimation;
extern std::atomic<bool> enableClassifierFlag;
extern std::atomic<bool> enableOsdFlag;
extern std::atomic<bool> modelOkFlag;
extern std::atomic<bool> enablePrintfDpuFlag;
extern std::atomic<bool> enablePrintfBufferManager;
extern std::atomic<bool> enablePrintfBufferFps;
extern std::atomic<bool> enableTemperatureOsd;

extern std::atomic<int>   autoExposureMode;
extern std::atomic<float> exposureTime;   

extern std::atomic<int>  enableTestPicFlag;
extern std::atomic<int>  testPicIdx;
extern std::atomic<bool> testImageLoaded;
extern std::atomic<int>  loadTestImage;   
extern std::atomic<int>  loadTestImageFlag;
extern std::vector<std::string> testImageFileNames;
extern std::mutex mtx_testImageFileNames;
extern cv::Mat testPicRgb;
extern std::mutex mtx_testPicRgb;

extern std::string modelDirString;
extern std::atomic<bool>  doModelLoadFlag;
extern std::atomic<bool>  saveFrontendSettingsRequestFlag;
extern std::vector<std::string> modelDirectoryNames;
extern std::atomic<int>   currentModelIdx;
extern std::vector<std::string> classNames;
extern std::mutex mtx_modelConfig;

extern std::atomic<bool> enableGevStreaming;

extern std::atomic<bool> autorun;
extern std::atomic<bool> bIsConnected;
extern std::atomic<bool> exitAppFlag;   

extern std::unique_ptr<xir::Graph> graph;	
extern std::vector<const xir::Subgraph*> subgraph;	
extern std::vector<float> anchorBiases;
extern std::atomic<float> confClassThres;
extern std::atomic<float> confObjThres;
extern std::atomic<float> nmsThres;

extern std::atomic<float> devTempMain;
extern std::atomic<float> devTempSensor;

extern appClass    app;

// functions in main.cpp
bool   ErrorOccurred ( LvStatus ErrorStatus);
int    getTickCount(void);

#define PRINTF(a) { printf("[%05d]:", getTickCount()%100000); printf a; printf("\n"); }
#define NOW (getTickCount()%100000)

#endif // MAIN_H
