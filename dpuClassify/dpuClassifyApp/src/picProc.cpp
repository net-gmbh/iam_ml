/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


int imagaDataEmbedding(cv::Mat img, unsigned char *data, int numBytes)   {    
    // MSBit first
    if (numBytes*8 > img.rows*img.cols*img.channels()) return 0;
  
    unsigned char *pByte=data;
    int bitCnt=7;

    unsigned char* pImg;	
    pImg = (unsigned char*) img.ptr();
   
    for (int i=0; i<numBytes*8; i++) {
        int bitval = (*pByte)&(1<<bitCnt);
        
        if (bitval>0) {
            *pImg++ |= 0x01;
        } else {
            *pImg++ &= 0xfe;
        }
        bitCnt--;
        if (bitCnt<0) {
            pByte++;
            bitCnt=7;
        }
    }    
    return 1;
}


cv::Mat ConvertRGB2BayerGR(cv::Mat inImg)   {
  
    cv::Mat outImg(inImg.rows, inImg.cols, CV_8UC1);
    int channel;
    for (int row = 0; row < outImg.rows; row++) {
        for (int col = 0; col < outImg.cols; col++) {
            if (row % 2 == 0) {
                //channel = (col % 2 == 0) ? 0 : 1;
                //channel = (col % 2 == 0) ? 1 : 2;
                channel = (col % 2 == 0) ? 1 : 0;
            } else {
                //channel = (col % 2 == 0) ? 1 : 2;
                //channel = (col % 2 == 0) ? 0 : 1;
                channel = (col % 2 == 0) ? 2 : 1;
            }
            outImg.at<uchar>(row, col) = inImg.at<cv::Vec3b>(row, col).val[channel];
        }
    }
    return outImg;
}


void bayerFade2GrayDecimate4 ( uint8_t *imgIn, uint8_t *imgOut , int widthIn, int heightIn)
{
    int w = widthIn/4;
    int h = heightIn/4;
	int lineStepIn = widthIn;
	int lineStepOut = w;
	int am;

    for ( int y=0; y<h; y++ ) {
        uint8_t* pSrc0 = (uint8_t *) ( imgIn  + ( y*4     ) * lineStepIn );
		uint8_t* pSrc1 = pSrc0 + lineStepIn;
		uint8_t* pSrc2 = pSrc1 + lineStepIn;
		uint8_t* pSrc3 = pSrc2 + lineStepIn;
        
        uint8_t* pDst  = (uint8_t *) ( imgOut + ( y ) * lineStepOut );

        for ( int x=0; x<w; x++ ) {
                am  = (int)*pSrc0++;
				am += (int)*pSrc0++;
				am += (int)*pSrc0++;
				am += (int)*pSrc0++;
								
				am += (int)*pSrc1++;
				am += (int)*pSrc1++;
				am += (int)*pSrc1++;
				am += (int)*pSrc1++;
								
				am += (int)*pSrc2++;
				am += (int)*pSrc2++;
				am += (int)*pSrc2++;
				am += (int)*pSrc2++;
								
				am += (int)*pSrc3++;
				am += (int)*pSrc3++;
				am += (int)*pSrc3++;
				am += (int)*pSrc3++;				
								
				//am += 8;
				*pDst++ =(uint8_t)(am>>4);
        }
    }
}


void bayerFade2GrayDecimate2 ( uint8_t *imgIn, uint8_t *imgOut , int widthIn, int heightIn)
{
    int w = widthIn/2;
    int h = heightIn/2;
	int lineStepIn = widthIn;
	int lineStepOut = w;
	int am;

    for ( int y=0; y<h; y++ ) {
        uint8_t* pSrc0 = (uint8_t *) ( imgIn  + ( y*2     ) * lineStepIn );
		uint8_t* pSrc1 = pSrc0 + lineStepIn;
		        
        uint8_t* pDst  = (uint8_t *) ( imgOut + ( y ) * lineStepOut );

        for ( int x=0; x<w; x++ ) {
                am  = (int)*pSrc0++;
				am += (int)*pSrc0++;
												
				am += (int)*pSrc1++;
				am += (int)*pSrc1++;
																
				//am += 1;
				*pDst++ =(uint8_t)(am>>2);
        }
    }
}



