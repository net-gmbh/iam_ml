#!/bin/bash

source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"
echo "vai_q_caffe -calib_iter: $QUANTIZATION_NUM_ITER"

sudo chmod 777 $WORK_DIR/vaiModels

#QUANTIZATION_NUM_ITER=6 # test

vai_q_caffe quantize \
-model $WORK_DIR/vaiModels/yolov4_dpu_quant.prototxt \
-keep_fixed_neuron -calib_iter $QUANTIZATION_NUM_ITER \
-weights $WORK_DIR/vaiModels/yolov4_dpu.caffemodel \
-sigmoided_layers layer133-conv,layer144-conv,layer155-conv \
-output_dir $WORK_DIR/vaiModels/ -method 1


