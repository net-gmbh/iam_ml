/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include <string.h>
#include <stdio.h>

#include "iamDevice.h"
#include "OsDep.h"


// -----------------------------------------------------------------------------
//  Callback function for delivered frontent image. Cannot be a method of a class,
//  thus we use the pUserParam to set the pointer to the iamDeviceClass class instance
//  when registering the callback, so that we can then pass the callback to the
//  CallbackNewBuffer() method of iamDeviceClass class.
// -----------------------------------------------------------------------------

void LV_STDC CallbackNewBufferFunction(LvHBuffer hBuffer,
    void* pUserPointer, void* pUserParam) {
    iamDeviceClass* pCamera = (iamDeviceClass*) pUserParam;
    // in UserPointer we hold pointer to buffer
    pCamera->CallbackNewBuffer((LvBuffer*) pUserPointer);
}


// -----------------------------------------------------------------------------
//   Constructor
// -----------------------------------------------------------------------------

iamDeviceClass::iamDeviceClass ()
{    
    m_pSystem     = NULL;
    m_pInterface  = NULL;
    m_pDevice     = NULL;
    m_pStream     = NULL;
    m_pEvent      = NULL;

    NumUnderrunLast = 0;
    maxAquiredBuffers = 0;
    nInputFrames  = 0;
    nOutputFrames = 0;

    memset(m_Buffers, 0, sizeof(m_Buffers));
}

// -----------------------------------------------------------------------------
//      Destructor
// -----------------------------------------------------------------------------

iamDeviceClass::~iamDeviceClass() {
    if (m_pDevice != NULL) CloseCamera();
}

// -----------------------------------------------------------------------------
//      OpenCamera
// -----------------------------------------------------------------------------

LvStatus iamDeviceClass::OpenCamera ( LvSystem* pSystem)
{
    printf ( "[%05d]:: iamDeviceClass::OpenCamera begin\n", NOW );

    if ( m_pDevice != NULL ) CloseCamera();

    m_pSystem = pSystem;

    LvStatus SynviewStatus;
    LvInterface* pInterface = NULL;
    LvDevice* pDevice = NULL;
    pSystem->UpdateInterfaceList ();

    // scan interfaces
    uint32_t pNumberOfInterfaces;
    pSystem->GetNumberOfInterfaces ( &pNumberOfInterfaces );
    printf("[%05d]:: iamDeviceClass::OpenCamera: NumberOfInterfaces:%d\n", NOW,  pNumberOfInterfaces);

    std::string pInterfaceId;
    for ( int k = pNumberOfInterfaces-1; k >= 0; k-- )
    {
        pSystem->GetInterfaceId ( k, pInterfaceId );

        if ( pInterfaceId == "iAM Interface" ) {
            printf("[%05d]:: iamDeviceClass::OpenCamera: Open the Interface nr:%d = >%s<\n", NOW,  k, pInterfaceId.c_str());
        }

        else {
            printf("[%05d]:: iamDeviceClass::OpenCamera: Skip Interface nr:%d = >%s<\n", NOW,  k, pInterfaceId.c_str());
            continue;
        }
        SynviewStatus = pSystem->OpenInterface(pInterfaceId.c_str(), pInterface);

        // scan devices
        uint32_t pNumberOfDevices;
        pInterface->UpdateDeviceList();
        pInterface->GetNumberOfDevices(&pNumberOfDevices);

        printf("[%05d]:: iamDeviceClass::OpenCamera: NumberOfDevices on %s = %d\n", NOW,  pInterfaceId.c_str(), pNumberOfDevices);
        for ( uint32_t l = 0; l < pNumberOfDevices; l++ )
        {
            pInterface->GetDeviceId ( l, sDeviceId );

            printf("[%05d]:: iamDeviceClass::OpenCamera: trying Device nr:%d on:%s Device Id String: >%s<\n", NOW, l, pInterfaceId.c_str(), sDeviceId.c_str());
            SynviewStatus = pInterface->OpenDevice(sDeviceId.c_str(), pDevice, LvDeviceAccess_Exclusive);
            if (ErrorOccurred(SynviewStatus)) continue;

            SynviewStatus = pDevice->OpenStream("", m_pStream);
            if (ErrorOccurred(SynviewStatus)) {
                //SynviewStatus = m_pInterface->CloseDevice(pDevice);
                continue;
            }

            goto _camFound_;
        }
    }
    printf ( "[%05d]:: iamDeviceClass::OpenCamera: No Camera found\n", NOW );
    return (-1);

_camFound_:
    printf ( "[%05d]:: iamDeviceClass::OpenCamera: Opened Cam >%s< on Device:%s (pInterface:%p pDevice:%p)\n", NOW,  sDeviceId.c_str(), pInterfaceId.c_str(), pInterface, pDevice );

    m_pInterface = pInterface;
    m_pDevice    = pDevice;

    printf ( "[%05d]:: iamDeviceClass::OpenCamera end\n", NOW );
    return 0;
}

// -----------------------------------------------------------------------------
//   Set the new callback buffer pointer
// -----------------------------------------------------------------------------

void iamDeviceClass::SetCallback ( LvEventCallbackNewBufferFunct pFunction, void* pUserParam ) {
    m_pNewBufferFunction = pFunction;
    m_pClass             = pUserParam;
}

// -----------------------------------------------------------------------------
//   InitCamera
// -----------------------------------------------------------------------------

LvStatus iamDeviceClass::InitCamera()
{
    LvStatus SynviewStatus;
    LvDevice* pDevice = m_pDevice;

    // chunk off
    if ( pDevice->IsAvailable ( LvDevice_ChunkModeActive ) ) {
        pDevice->SetBool ( LvDevice_ChunkModeActive, 0 );
    }

    // UniProcessMode: off
    SynviewStatus = pDevice->SetEnum ( LvDevice_LvUniProcessMode, LvUniProcessMode_Off );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    if ( pDevice->IsAvailable ( LvDevice_RegionSelector ) ) {
        pDevice->SetEnum ( LvDevice_RegionSelector, LvRegionSelector_Region0 );
    }
    pDevice->GetInt32 ( LvDevice_Width, &nWidth );
    pDevice->GetInt32 ( LvDevice_Height, &nHeight );
    printf ( "[%05d]:: iamDeviceClass::InitCamera: Image format is: %dx%d\n", NOW,  nWidth, nHeight );

    // set new buffer callback pointer
    SetCallback ( CallbackNewBufferFunction, this );

    printf ( "[%05d]:: iamDeviceClass::InitCamera end\n", NOW );
    return 0;
}


// -----------------------------------------------------------------------------
//   Start acquisition
// -----------------------------------------------------------------------------
int iamDeviceClass::StartAcquisition()
{
    //printf ( "[%05d]:: iamDeviceClass::StartAcquisition()\n", NOW );
    if ( m_pDevice == NULL ) return 1;

    LvStatus SynviewStatus;

    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pDevice->AcquisitionStart ();
    ErrorOccurred(SynviewStatus);
    return SynviewStatus;
}


// -----------------------------------------------------------------------------
//   Stop acquisition
// -----------------------------------------------------------------------------
int iamDeviceClass::StopAcquisition()
{
    //printf("[%05d]:: iamDeviceClass::StopAcquisition()\n", NOW);
    if ( ! IsAcquiring () ) return 1;

    LvStatus SynviewStatus;
    SynviewStatus = m_pDevice->AcquisitionStop ();
    ErrorOccurred(SynviewStatus);
    return SynviewStatus;
}


// -----------------------------------------------------------------------------
//   Close camera
// -----------------------------------------------------------------------------
void iamDeviceClass::CloseCamera()
{
    if ( m_pDevice == NULL ) return;
    if ( IsAcquiring() ) StopAcquisition();
    if ( m_pEvent == NULL ) return;

    LvStatus SynviewStatus;
    
    SynviewStatus = CloseBuffers();
    if (ErrorOccurred(SynviewStatus)) return;
    
    SynviewStatus = m_pDevice->CloseStream ( m_pStream );
    if (ErrorOccurred(SynviewStatus)) return;

    SynviewStatus = m_pInterface->CloseDevice ( m_pDevice );
    if (ErrorOccurred(SynviewStatus)) return;

    SynviewStatus = m_pSystem->CloseInterface ( m_pInterface );
    if (ErrorOccurred(SynviewStatus)) return;    
}


// -----------------------------------------------------------------------------
//   OpenBuffers
// -----------------------------------------------------------------------------
LvStatus iamDeviceClass::OpenBuffers()
{    
    int SynviewStatus;

    // open an event for the stream
    //printf("[%05d]:: iamDeviceClass::OpenBuffers OpenEvent\n", NOW);
    SynviewStatus = m_pStream->OpenEvent(LvEventType_NewBuffer, m_pEvent);
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // buffer info
    int PayloadSize=0, width, height, CalcPayloadSize;
    m_pDevice->GetInt32 ( LvDevice_Width, &width );
    m_pDevice->GetInt32 ( LvDevice_Height, &height );
    m_pStream->GetInt32 ( LvStream_LvCalcPayloadSize, &CalcPayloadSize );

    if ( m_pDevice->IsAvailable ( LvDevice_PayloadSize ) )
    {
        m_pDevice->GetInt32 ( LvDevice_PayloadSize, &PayloadSize );
        //printf("[%05d]:: iamDeviceClass::OpenBuffers wxh=%dx%d size=%d size(stream):%d\n", NOW, width, height, PayloadSize, CalcPayloadSize);
    }
    else {
        //printf("[%05d]:: iamDeviceClass::OpenBuffers wxh=%dx%d size:%d\n", NOW, width, height, CalcPayloadSize);
    }

    // setting up buffer descriptors for the stream and queue them
    //printf("[%05d]:: iamDeviceClass::OpenBuffers open %d buffers\n", NOW, NUMBER_OF_BUFFERS);
    for ( int i = 0; i < NUMBER_OF_BUFFERS; i++ )
    {
        //printf("[%05d]:: iamDeviceClass::OpenStream() m_Buffers[%d]\n", NOW, i);
        SynviewStatus = m_pStream->OpenBuffer ( NULL, 0, NULL, 0, m_Buffers[i] );
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
        SynviewStatus = m_Buffers[i]->Queue();
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    }
    //int size;
    m_pStream->GetInt32 ( LvStream_LvCalcPayloadSize, &CalcPayloadSize);
    //printf("[%05d]:: iamDeviceClass::OpenBuffers() opened %d buffer(s) with size: %d bytes\n", NOW, NUMBER_OF_BUFFERS, (int)CalcPayloadSize);

    // enable image callback (new buffer)
    //printf("[%05d]:: iamDeviceClass::OpenBuffers() enable callback\n", NOW);
    SynviewStatus = m_pEvent->SetCallbackNewBuffer ( m_pNewBufferFunction, m_pClass );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pEvent->StartThread ();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    //printf("[%05d]:: iamDeviceClass::OpenBuffers() [end]\n", NOW);
    return 0;
}

// -----------------------------------------------------------------------------
//   CloseBuffers
// -----------------------------------------------------------------------------
LvStatus iamDeviceClass::CloseBuffers()
{
    //printf("[%05d]:: iamDeviceClass::CloseBuffers() [begin]\n", NOW );

    int SynviewStatus;
    if ( m_pDevice == NULL ) return -1;
    if ( IsAcquiring() ) StopAcquisition();
    if ( m_pEvent == NULL ) return -2;

    //printf("[%05d]:: iamDeviceClass::CloseBuffers::FlushQueue() queue all buffers\n", NOW );
    
    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );                              // remove all remaining events in order to prevent further callbacks
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    //printf("[%05d]:: iamDeviceClass::CloseBuffers::StopThread()\n", NOW);
    
    SynviewStatus = m_pEvent->StopThread ();                                                            // wait for running callback to finish, stop thread
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    if (m_pStream == NULL) return -3;
    //printf("[%05d]:: iamDeviceClass::CloseBuffers::CloseEvent()\n", NOW);
    
    SynviewStatus = m_pStream->CloseEvent(m_pEvent);                                                    // close the event loop
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    //printf("[%05d]:: iamDeviceClass::CloseBuffers::FlushQueue() discard all buffers\n", NOW);
    
    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllDiscard );                              // discard all buffers
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    
    for (int i=0; i<NUMBER_OF_BUFFERS; i++)
    {
        if (m_Buffers[i] != NULL) SynviewStatus = m_pStream->CloseBuffer(m_Buffers[i]);                 // close buffers
    }    
    //printf("[%05d]:: iamDeviceClass::CloseBuffers() closed %d buffer(s)n", NOW,  NUMBER_OF_BUFFERS);

    //printf("[%05d]:: iamDeviceClass::CloseBuffers::CloseBuffer() [end]\n", NOW );
    return 0;
}


// -----------------------------------------------------------------------------
//  IsAcquiring ()
// -----------------------------------------------------------------------------
bool iamDeviceClass::IsAcquiring()
{
    if (m_pDevice == NULL) return false;
    int32_t iIsAcquiring;
    m_pDevice->GetInt32(LvDevice_LvDeviceIsAcquiring, &iIsAcquiring);
    return iIsAcquiring != 0;
}


// -----------------------------------------------------------------------------
//   parameter interface functions
// -----------------------------------------------------------------------------

LvStatus iamDeviceClass::GetInt32CameraParam ( LvFeature Feature, int32_t* piValue )
{
    if ( m_pDevice == NULL ) return -1;
    return m_pDevice->GetInt32 ( Feature, piValue );
}

LvStatus iamDeviceClass::SetInt32CameraParam (LvFeature Feature, int32_t iValue)
{
    if ( m_pDevice == NULL ) return -1;
    return m_pDevice->SetInt32 ( Feature, iValue );
}

LvStatus iamDeviceClass::GetFloatCameraParam (LvFeature Feature, float* pValue)
{
    if ( m_pDevice == NULL ) return -1;
    int SynviewStatus;
    double val;
    SynviewStatus = m_pDevice->GetFloat ( Feature, &val );
    if (SynviewStatus!=LVSTATUS_OK) return SynviewStatus;
    *pValue=float(val);
    return LVSTATUS_OK;    
}

LvStatus iamDeviceClass::SetFloatCameraParam (LvFeature Feature, float Value)
{
    if ( m_pDevice == NULL ) return -1;
    return m_pDevice->SetFloat ( Feature, Value );
}


//-----------------------------------------------------------------------------
// Callback function for frontend images
//-----------------------------------------------------------------------------

void iamDeviceClass::CallbackNewBuffer ( LvBuffer* pBuffer )
{
    printf("[%05d]:: iamDeviceClass::CallbackNewBuffer [begin]\n", NOW );
    
    int AwaitDelivery;
    int NumUnderrunAct;

    // buffer undefined
    if ( pBuffer == 0 )
    {        
        printf   ( "[%05d]:: iamDeviceClass::CallbackNewBuffer: buffer pointer undefined\n", NOW );
        return;
    }
    // event no longer defined
    if ( m_pEvent == NULL )
    {        
        printf   ( "[%05d]:: iamDeviceClass::CallbackNewBuffer: event not defined!\n", NOW  );
        pBuffer->Queue();
        return; 
    }

    // close event requested?
    if ( m_pEvent->CallbackMustExit () )
    {        
        //printf   ( "[%05d]:: iamDeviceClass::CallbackNewBuffer: exit requested!\n", NOW  );
        pBuffer->Queue();
        return;
    }

    // get buffer pool info
    if (m_pStream->GetInt32 ( LvStream_LvNumAwaitDelivery, &AwaitDelivery) != LVSTATUS_OK ) {
        printf("[%05d]:: iamDeviceClass::ivQueryFrameGenTL: error get AwaitDelivery\n", NOW );
    }
    if (m_pStream->GetInt32 ( LvStream_LvNumUnderrun, &NumUnderrunAct) != LVSTATUS_OK ) {
        printf("[%05d]:: iamDeviceClass::ivQueryFrameGenTL: error get NumUnderrun\n", NOW );
    }
    //printf("[%05d]:: iamDeviceClass::ivQueryFrameGenTL: buffers in aquisition:%d underrun frames:%d aquiring flag:%d\n", NOW,  AwaitDelivery, NumUnderrunAct-NumUnderrunLast, IsAcquiring());
    NumUnderrunLast = NumUnderrunAct;
    maxAquiredBuffers = std::max ( maxAquiredBuffers, AwaitDelivery );

    // get the pointer to the image data
    void* pImageData = 0;
    int32_t iImageOffset = 0;
    pBuffer->GetPtr ( LvBuffer_Base, &pImageData );
    pBuffer->GetInt32 ( LvBuffer_ImageOffset, &iImageOffset );
    pImageData = (uint8_t*)pImageData + iImageOffset;
    
    // get image information
    LvipImgInfo ImgInfo;
    int err = pBuffer->GetImgInfo ( ImgInfo );  // depends on Uni Processing mode: if "Off" or "HwOnly", this returns the unprocessed buffer info
    if (err) {
        printf("iamDeviceClass::ivQueryFrameGenTL: Cannot retrieve image info");
        pBuffer->Queue();
        return;
    }   

    // put buffer back into pool
    pBuffer->Queue();

    //printf   ( "[%05d]:: iamDeviceClass::CallbackNewBuffer [end]\n", NOW );
    
    return;
}


// -----------------------------------------------------------------------------
//   Synview Error check 
// -----------------------------------------------------------------------------
bool iamDeviceClass::ErrorOccurred(LvStatus ErrorStatus)
{
    if (ErrorStatus == LVSTATUS_OK) return false;
    //MessageBox(GetActiveWindow(), LvLibrary::GetLastErrorMessage().c_str(), "Error", MB_OK | MB_ICONEXCLAMATION);
    LvLibrary::Logf ( "Error:%s\n", LvLibrary::GetLastErrorMessage().c_str() );
    return true;
}

