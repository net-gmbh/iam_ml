#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.



# ----------------------------------
#  Segmentation evaluation script
#    float vs quantized model   
# ----------------------------------

import sys
import os
import importlib
import argparse
import numpy as np
import cv2

import tensorflow as tf
from tensorflow_model_optimization.quantization.keras import vitis_quantize


import segTools as segTools






def main(argv):

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )      
                        
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )    
                        
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    #parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' ) 

    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)
    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import    
        
    cfg = importlib.import_module(cf)
    
    workDir=args.workDir
    print ('workDir:', workDir)

    # --- settings ---
    
    modelFileNameFloat = os.path.join(workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME)
    modelFileNameQuant = os.path.join(workDir, 'modelVai','model_quantized.h5')
    useDatasetFraction = 0.2
    batch_size = 32
    evaluateFloat = True
    evaluateQuantized = True
    
    if cfg.VAI_QUANT_VERIFY_DATASET=='valid':
        imgDir = os.path.join(workDir, 'data', 'valid_img')
        segDir = os.path.join(workDir, 'data', 'valid_seg')
    else:
        imgDir = os.path.join(workDir, 'data', 'train_img')
        segDir = os.path.join(workDir, 'data', 'train_seg')

    X, Y = segTools.loadSegmentationInOutDataset(imgDir, segDir, 
                                        cfg.WIDTH, cfg.HEIGHT, cfg.NUM_CLASSES, valueNormalize=True)
    


    # --- evaluate float model ---
    if cfg.VAI_QUANT_VERIFY_FLOAT_MODEL:
        print('Loading float model ... ')
        model = tf.keras.models.load_model(modelFileNameFloat)
        model.compile(loss='categorical_crossentropy')
        print('Calculating predictions... ')
        y_vec   = model.predict(X, batch_size=batch_size)
        predictMat  = np.argmax(y_vec, axis=3)
        truthMat = np.argmax(Y, axis=3)        
        IoUListFloat = segTools.calculate_IoU(truthMat,predictMat, cfg.NUM_CLASSES)

        

    # --- evaluate quantized model ---
    if cfg.VAI_QUANT_VERIFY_QUANT_MODEL:
        print('Loading quantized model ... ')
        model = tf.keras.models.load_model(modelFileNameQuant)
        model.compile(loss='categorical_crossentropy')
        print('Calculating predictions... ')
        y_vec   = model.predict(X, batch_size=batch_size)
        predictMat  = np.argmax(y_vec, axis=3)
        truthMat = np.argmax(Y, axis=3)        
        IoUListQuant = segTools.calculate_IoU(truthMat,predictMat, cfg.NUM_CLASSES)

    print('')
    print('')
    print('config file: ',cf)
    print('Using dataset (',cfg.VAI_QUANT_VERIFY_DATASET,'): ',imgDir)
    print(X.shape[0],' samples analysed')
    print('')

    if cfg.VAI_QUANT_VERIFY_FLOAT_MODEL:
        print('Float model')
        print("_________________")
        for c in range(cfg.NUM_CLASSES):
            print("%12.12s (%2d):  IoU=%4.3f" % (cfg.CLASS_NAMES[c],c,IoUListFloat[c]))
        mIoU = np.mean(IoUListFloat)
        print("_________________")
        print("Mean IoU: {:4.3f}".format(mIoU))
        
    if cfg.VAI_QUANT_VERIFY_FLOAT_MODEL and cfg.VAI_QUANT_VERIFY_QUANT_MODEL:
        print('')

    if cfg.VAI_QUANT_VERIFY_QUANT_MODEL:
        print('Quantized model')
        print("_________________")
        for c in range(cfg.NUM_CLASSES):
            print("%12.12s (%2d):  IoU=%4.3f" % (cfg.CLASS_NAMES[c],c,IoUListQuant[c]))
        mIoU = np.mean(IoUListQuant)
        print("_________________")
        print("Mean IoU: {:4.3f}".format(mIoU))


if __name__=="__main__":
    main(sys.argv)
    
    
