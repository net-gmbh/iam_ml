
rem -------------------------------------------
rem ---      start application on iam       ---
rem -------------------------------------------


docker run^
  -it^
  --rm^
  -v %cd%:/workspace^
  -w /workspace^
  iam_xcomp2021^
  bash -c "./scripts/sh_start_on_iam_nohup.sh"


rem pause