# dpuClassityAppMt

Application for DPU accelerated image classification on the **iam ML** camera.   
The application can be build on the iam camera or via cross compiling, using the docker image build in the [xcompile](iam_ml/xcompile) section.


## A. Build application on iam

1. Copy this folder to **iam ML** camera

2. Build application with remote shell on iam by executing...    

		make		

3. Start application...

		./dpuClassifyAppMt

4. Open iam camera via GigE from host with Synview-Explorer


## B. Cross-Compile with Docker

1. Build docker image in [xcompile](./../../xcompile) section.   
(Needs to be done only once)

2. Build application by executing...    

		win_make.bat


3. Copy application to iam...  

		win_copy_app_to_iam.bat
		win_copy_models_to_iam.bat

4. Start application on iam....

		win_start_on_iam.bat
 
	(Optionally use win_stop_on_iam.bat to terminate application)


## Adding Trained  Models
	New models have to be added to the /model folder.   
	Use GigE XML-features to load different models. 