
rem -----------------------------------------------------------
rem --- run sh_train_finalize.sh in vai_141_train container ---
rem -----------------------------------------------------------

call configWin.bat


docker run^
  --gpus all^
  -it^
  --rm^
  -v %WORK_DIR_MAPPING%:/work^
  -v %cd%:/workspace^
  -w /workspace^
  %TRAIN_IMAGE_NAME%^
  bash -c "./s3_train_finalize.sh"
  


pause