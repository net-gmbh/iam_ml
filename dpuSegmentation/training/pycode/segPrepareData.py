#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.




# -----------------------------------------------------------
#           prepare dataset for segmentation
# -----------------------------------------------------------

import os
from pathlib import Path
import json
import numpy as np
import random
import cv2


import argparse
import sys

import importlib

import segTools as segTools




def main(argv):
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )      
                        
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )  

    #parser.add_argument('--dockerMode', type = bool, default=False)

    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' )        
           
    args = parser.parse_args()
    
    cp = args.configPath
    print('config path: ',cp)
    
    cf = args.configFile
    print('config file: ',cf)
    
    # --- import congig ---    
    sys.path.insert(1, cp) # for import            
    cfg = importlib.import_module(cf)
    

    #if args.dockerMode:    
    #    workDir = cfg.WORK_DIR_DOCKER
    #    origDir = cfg.ORIG_DATASET_DIR_DOCKER
    #else:
    #    workDir = cfg.WORK_DIR
    #    origDir = cfg.ORIG_DATASET_DIR
    #print ('workDir:', workDir)
    
    workDir    = args.workDir
    datasetDir = args.datasetDir
    
    print ('workDir:', workDir)
    print ('datasetDir:', datasetDir)
    
    origImageDir  = os.path.join(datasetDir, cfg.ORIG_IMAGE_DIR)
    origLabelFile = os.path.join(datasetDir, cfg.ORIG_LABEL_FILE)
    
    res=segTools.createDir(os.path.join(workDir, 'data'))
    
    genMaskDir = os.path.join(workDir, 'data','masks') 
        
    
    # --- generating mask images from VAI label file ---    
    if cfg.GENERATE_MASKS_FROM_LABEL_FILE:
        
        if cfg.NUM_CLASSES!=2:
            print('The VIA label file reader only supports 1 class+background mode')
        
           
        res=segTools.createDir(genMaskDir)#cfg.GENERATED_MASK_DIR))        
        segTools.deleteImageFilesInDir(genMaskDir)  
                       
        annotations = json.load(open(origLabelFile))
        annotations = list(annotations.values())  # don't need the dict keys

        # The VIA tool saves images in the JSON even if they don't have any
        # annotations. Skip unannotated images.
        annotations = [a for a in annotations if a['regions']]
        print("Generating mask images from json label file...")

        for a in annotations:
            # Get the x, y coordinaets of points of the polygons that make up
            # the outline of each object instance. These are stores in the
            # shape_attributes (see json format above)
            # The if condition is needed to support VIA versions 1.x and 2.x.
            if type(a['regions']) is dict:
                polygons = [r['shape_attributes'] for r in a['regions'].values()]
            else:
                polygons = [r['shape_attributes'] for r in a['regions']] 
                
            imageFileName=os.path.join(origImageDir,a['filename'])
        
            #print(imageFileName)
            #print(polygons)
            #print(a)
            
            #image = skimage.io.imread(imageFileName)
            #height, width = image.shape[:2]
            image = cv2.imread(imageFileName,cv2.IMREAD_COLOR)
            height, width, nchan = image.shape
            #print(height, width)
        
            # Convert polygons to a single channel bitmap mask image 
            #mask = np.zeros([height, width], dtype=np.uint8)
            #for i, p in enumerate(polygons):
            #    # Get indexes of pixels inside the polygon and set them to 1
            #    rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            #    mask[rr, cc] = 1     
            
            mask = np.zeros([height, width], dtype=np.uint8)
            for i, p in enumerate(polygons):
                points=np.zeros((len(p['all_points_x']),2), dtype=int)
                for n in range(0, len(p['all_points_x'])):
                    points[n,0]=p['all_points_x'][n]
                    points[n,1]=p['all_points_y'][n]
                cv2.fillPoly(mask, pts=[points], color=(1))
            
            maskFileName=os.path.join(genMaskDir,a['filename'].replace(".jpg",".png"))
            #skimage.io.imsave(maskFileName,mask)
            retval = cv2.imwrite(maskFileName, mask)
            
            
    # --- generating mask images from VAI label file ---   
    if cfg.GENERATE_MASKS_FROM_LABEL_FILE and cfg.GENERATE_VIEWABLE_MASKS:
    
        if cfg.NUM_CLASSES!=2:
            print('The VIA label file reader only supports 1 class+background mode')
        
        viewableMaskDir=os.path.join(workDir,'data', 'masks_view')
        res=segTools.createDir(viewableMaskDir)        
        segTools.deleteImageFilesInDir(viewableMaskDir)
               
        annotations = json.load(open(origLabelFile))
        annotations = list(annotations.values())  # don't need the dict keys

        # The VIA tool saves images in the JSON even if they don't have any
        # annotations. Skip unannotated images.
        annotations = [a for a in annotations if a['regions']]
        print("Generating viewable mask images from json label file...")

        for a in annotations:
            # Get the x, y coordinaets of points of the polygons that make up
            # the outline of each object instance. These are stores in the
            # shape_attributes (see json format above)
            # The if condition is needed to support VIA versions 1.x and 2.x.
            if type(a['regions']) is dict:
                polygons = [r['shape_attributes'] for r in a['regions'].values()]
            else:
                polygons = [r['shape_attributes'] for r in a['regions']] 
                
            imageFileName=os.path.join(origImageDir,a['filename'])
        
            #print(imageFileName)
            #print(polygons)
            #print(a)
                        
            img = cv2.imread(imageFileName,cv2.IMREAD_COLOR)
            height, width, nchan = img.shape
            #print(height, width)
            
            # 100% overlay
            #for i, p in enumerate(polygons):
            #    points=np.zeros((len(p['all_points_x']),2), dtype=int)
            #    for n in range(0, len(p['all_points_x'])):
            #        points[n,0]=p['all_points_x'][n]
            #        points[n,1]=p['all_points_y'][n]
            #    cv2.fillPoly(img, pts=[points], color=(255, 0, 0))
            
            # 
            mask = np.zeros([height, width,3], dtype=np.uint8)
            for i, p in enumerate(polygons):
                points=np.zeros((len(p['all_points_x']),2), dtype=int)
                for n in range(0, len(p['all_points_x'])):
                    points[n,0]=p['all_points_x'][n]
                    points[n,1]=p['all_points_y'][n]
                cv2.fillPoly(mask, pts=[points], color=( 0, 0,255))
            dst = cv2.addWeighted(img, 1, mask, 1, 0)
                     
            maskFileName=os.path.join(viewableMaskDir,a['filename'].replace(".jpg",".png"))
            retval = cv2.imwrite(maskFileName, dst)
    
      
    # --- split dataset ---

    train_images_list = os.listdir(origImageDir)

    if cfg.GENERATE_MASKS_FROM_LABEL_FILE:
        maskDir  = genMaskDir
    else:
        maskDir  = os.path.join(datasetDir,cfg.MASK_DIR)
        
    train_segmentations_list  = os.listdir(maskDir)

    # exclude non image files
    valid_extensions = ['jpg','jpeg', 'bmp', 'png']
    train_images = [fn for fn in train_images_list if any(fn.endswith(ext) for ext in valid_extensions)]
    train_segmentations = [fn for fn in train_segmentations_list if any(fn.endswith(ext) for ext in valid_extensions)]


    # using zip() to map training and segmentation images
    zip_mapped = zip(train_images,train_segmentations)
    list_mapped=list(set(zip_mapped))

    # --- shuffle  dataset ---
    # seed random number generator
    random.seed(1)
    # randomly shuffle the list of images
    random.shuffle(list_mapped)

    numValidationSamples = int(len(list_mapped)*cfg.VALIDATION_SAMPLE_RATIO)
    numTrainSamples = len(list_mapped) - numValidationSamples
    numCalibrationSamples = int(len(list_mapped)*cfg.CALIB_SAMPLE_RATIO)

    print('Samples            : ',len(list_mapped))
    print('Train samples      : ',numTrainSamples)
    print('Validation samples : ',numValidationSamples)
    print('Calibration samples: ',numCalibrationSamples)
    
    
    trainImgDir = os.path.join(workDir, 'data', 'train_img')
    trainSegDir = os.path.join(workDir, 'data', 'train_seg')
    validImgDir = os.path.join(workDir, 'data', 'valid_img')
    validSegDir = os.path.join(workDir, 'data', 'valid_seg')
    calibImgDir = os.path.join(workDir, 'data', 'calib_img')
    calibSegDir = os.path.join(workDir, 'data', 'calib_seg')


    res=segTools.createDir(trainImgDir)
    res=segTools.createDir(trainSegDir)

    res=segTools.createDir(validImgDir)
    res=segTools.createDir(validSegDir)

    res=segTools.createDir(calibImgDir)
    res=segTools.createDir(calibSegDir)

    segTools.deleteImageFilesInDir(trainImgDir)
    segTools.deleteImageFilesInDir(trainSegDir)

    segTools.deleteImageFilesInDir(validImgDir)
    segTools.deleteImageFilesInDir(validSegDir)

    segTools.deleteImageFilesInDir(calibImgDir)
    segTools.deleteImageFilesInDir(calibSegDir)

    print('Writing dataset...')


    for i in range(len(list_mapped)):
        imgFileName=os.path.join(origImageDir, list_mapped[i][0])
        maskFileName=os.path.join(maskDir, list_mapped[i][1])
        
        #print('img:  ',list_mapped[i][0],'  mask: ',list_mapped[i][1])
                
        img = cv2.imread(imgFileName, cv2.IMREAD_COLOR)
        img = cv2.resize(img, ( cfg.WIDTH, cfg.HEIGHT ))
        height, width, numChan = img.shape
            
        if cfg.GENERATE_MASKS_FROM_LABEL_FILE:
            # --- read single channel mask file from VAI converter ---
            maskImp = cv2.imread(maskFileName, cv2.IMREAD_UNCHANGED)
            maskOut = cv2.resize(maskImp, ( width, height ), interpolation = cv2.INTER_NEAREST)
        else:
            # --- read RGB segmentation image with colors according to config file coding ---
            maskImp = cv2.imread(maskFileName, cv2.IMREAD_UNCHANGED)
            maskImp = cv2.resize(maskImp, ( width, height ), interpolation = cv2.INTER_NEAREST)
            maskOut = np.zeros((img.shape[0],img.shape[1]),dtype=np.uint8)
            #colors = np.array(cfg.CLASS_COLORS, dtype="int")
            colors = cfg.CLASS_COLORS
            for y in range(img.shape[0]):
                for x in range(img.shape[1]):                   
                    for c in range(cfg.NUM_CLASSES):
                        if maskImp[y,x,0]==colors[c][2] and maskImp[y,x,1]==colors[c][1] and maskImp[y,x,2]==colors[c][0]:  # img:BGR (opencv!!!), colors:RGB
                            maskOut[y,x]=c  
                                
        if i<numTrainSamples:
            cv2.imwrite(trainImgDir+"/img_" +str(i)+".png", img)
            cv2.imwrite(trainSegDir+"/seg_" +str(i)+".png", maskOut)  
        else:
            cv2.imwrite(validImgDir+"/img_" +str(i-numTrainSamples)+".png", img)
            cv2.imwrite(validSegDir+"/seg_" +str(i-numTrainSamples)+".png", maskOut)  
        
        
        if i<numCalibrationSamples:
            cv2.imwrite(calibImgDir+"/img_" +str(i)+".png", img)
            cv2.imwrite(calibSegDir+"/seg_" +str(i)+".png", maskOut)  
            
    print('Done!')

if __name__=="__main__":
    main(sys.argv)




