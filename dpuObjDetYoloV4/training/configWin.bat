

rem ----------------------------------------
rem ---   directories to map to docker   ---
rem ----------------------------------------

set WORK_DIR_MAPPING=D:/ml2_work
set DATASET_DIR_MAPPING=D:/ml2_datasets


rem ----------------------------------------
rem ---      train docker image        ---
rem ----------------------------------------

set TRAIN_IMAGE_NAME=vai_141_yolo_devel


rem ----------------------------------------
rem ---      vitisAi docker image        ---
rem ----------------------------------------

rem use gpu when enough cuda memory available...
rem set VAI_IMAGE_NAME=xilinx/vitis-ai-gpu:1.4.1.978

rem otherwise...
set VAI_IMAGE_NAME=xilinx/vitis-ai-cpu:1.4.1.978
