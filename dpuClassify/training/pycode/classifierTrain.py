#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


import sys
import os
import argparse
from pathlib import Path
import numpy as np
import csv
import shutil
import platform
import string
import importlib
import warnings
from time import time
from copy import copy
import matplotlib.pyplot as plt

import cv2 as cv
import tensorflow as tf
keras = tf.keras

import classifierModelBuilder as cmb




__version__='3.0.0'
epochCounter=0
historyLog=[]
hllDll=0
interruptibleMem = False
priorDiagramFullFile = ''
priorDiagramZoomFile = ''
workDir=''
datasetDir=''
kerasCallbackList=[]
interruptedFlag=False


# -------------------------------------------------------------------
#                            File Utilities                      
# -------------------------------------------------------------------

# recursive directory create
def createDir(dirName):
    if not os.path.exists(dirName):  
        #Path(dirName).mkdir( 0o777, parents=True, exist_ok=True) # recursive
        Path(dirName).mkdir( parents=True, exist_ok=True) # recursive        
        if not os.path.exists(dirName):  
            print('ERROR: Could not create directory: ',dirName)
            return False
    return True
    

def removeFilesInDirectory(dirName):
    if os.path.exists(dirName):
        if (len(os.listdir(dirName))!=0):  # emty directory?
            for filename in os.listdir(dirName):
                file_path = os.path.join(dirName, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                        print('Failed to delete %s. Reason: %s' % (file_path, e))


def countNumberOfSubDirs(dirName):
    entryNames = os.listdir(dirName)
    numberOfSubDirs=0
    for entry in entryNames:
        if os.path.isdir(os.path.join(dirName,entry)):
            numberOfSubDirs += 1
    return numberOfSubDirs





# -------------------------------------------------------------------
#                            File Writer                      
# -------------------------------------------------------------------

#def printCategoryDict(categoryDict):
#    for key, value in categoryDict.items():
#        print("%s %s" %(str(key), str(value)))

#def readCsvLogFieldnames(fileName):
#    dr = csv.DictReader(open(fileName))
#    return dr.fieldnames

#def writeCategoryDictToFile(categoryDict, fileName, fileType='txt'):

#    # txt, one label text per line
#    if fileType=='txt':
#        txtFile = open(fileName, 'w')
#        for key, value in categoryDict.items():
#            txtFile.write("%s\n" % key)
#        txtFile.close()
#        return
#    # txt, value+label text per line
#    if fileType=='txt2':
#        txtFile = open(fileName, 'w')
#        for key, value in categoryDict.items():
#            txtFile.write("%s %s\n" %(str(key), str(value)))
#        txtFile.close()
#        return

#def writeGeneratorCategoryDictToFile(dataSource, fileName, fileType='txt'):
#
#    # txt, one label text per line
#    if fileType=='txt':
#        txtFile = open(fileName, 'w')
#        for key in dataSource.class_indices:
#            txtFile.write("%s\n" % key)
#        txtFile.close()
#        return
#    # txt, value+label text per line
#    if fileType=='txt2':
#        txtFile = open(fileName, 'w')
#        for key in dataSource.class_indices:
#            txtFile.write("%s %s\n" %(str(dataSource.class_indices[key]), key))
#        txtFile.close()
#        return


#def writeClassNamesToFile(classNames, fileName):
#    txtFile = open(fileName, 'w')    
#    for i in range(len(classNames)):
#        #txtFile.write("classLabel_%s = \'%s\'\n" %(str(i), classNames[i]))
#        txtFile.write("\'%s\'\n" %(str(i), classNames[i])) # label text only
#    txtFile.close()
#    return

# --- save model parameters ---
#def saveModelConfFile(fileName, modelName, imageWidth, imageHeight, inputScalingType, numberOfClasses):
#    with open(fileName, 'w', newline='') as csvfile:
#        fieldnames = ['name', 'value']
#        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
#        #writer.writeheader()
#        writer.writerow({'name': 'name',             'value': modelName})
#        writer.writerow({'name': 'imageWidth',       'value': imageWidth})
#        writer.writerow({'name': 'imageHeight',      'value': imageHeight})
#        writer.writerow({'name': 'inputScalingType', 'value': inputScalingType})
#        writer.writerow({'name': 'numberOfClasses',  'value': numberOfClasses})


def writeModelCfgFilePy(model, classNames, cfg, fileName ):
    
    txtFile = open(fileName, 'w')
    txtFile.write("modelName        = \'%s\'\n" %str(model.name))
    
    txtFile.write("imageWidth       = %s\n" %str(cfg.IMAGE_WIDTH))
    txtFile.write("imageHeight      = %s\n" %str(cfg.IMAGE_HEIGHT))
    txtFile.write("inputScalingType = \'%s\'\n" %str(cmb.getInputScalingType(cfg)))
    txtFile.write("numberOfClasses  = %s\n" %str(cfg.NUMBER_OF_CLASSES))

   
    for i in range(len(classNames)):
            txtFile.write("classLabel_%s = \'%s\'\n" %(str(i), classNames[i]))


    txtFile.close()
    return



#def writeModelCfgFileSh(model, fileName, cfg):
#    txtFile = open(fileName, 'w',newline='')
#    txtFile.write("#!/bin/bash\n")
#    txtFile.write("export modelName=\"%s\"\n" %str(model.name))
#    txtFile.write("export trainLabel=\"%s\"\n" %str(cfg.trainLabel))
#    if cfg.inputNodeName  == 'None':
#        txtFile.write("export inputNodeName=\"%s\"\n" %str(model.input.name[:-2]))
#    else:
#        txtFile.write("export inputNodeName=\"%s\"\n" %str(cfg.inputNodeName))
#    if cfg.outputNodeName  == 'None':
#        txtFile.write("export outputNodeNameDpu=\"%s\"\n" %str(model.output.name[:-2]))
#    else:
#        txtFile.write("export outputNodeNameDpu=\"%s\"\n" %str(cfg.outputNodeName))
#    txtFile.write("export outputNodeName=\'%s\'\n" %str(model.output.name[:-2]))
#    txtFile.write("export imageWidth=%s\n" %str(cfg.IMAGE_WIDTH))
#    txtFile.write("export imageHeight=%s\n" %str(cfg.IMAGE_HEIGHT))
#    txtFile.write("export inputScalingType=\"%s\"\n" %str(cfg.inputScalingType))
#    txtFile.write("export numberOfClasses=%s\n" %str(cfg.NUMBER_OF_CLASSES))
#    txtFile.close()
#    return


# ---------------------------------------------------
#                   Model Utilities                    
# ---------------------------------------------------

# unfreeze num top layers of the model
# if num<0: unfreeze all model mayers
def makeTopLayersTrainable(model, num):
    for layer in model.layers[:]:
        layer.trainable = False
    if num<0: # unfreeze all layers
        for layer in model.layers[:]:
            if not isinstance(layer, keras.layers.BatchNormalization): # never unfreeze batchnorm layers
                layer.trainable = True
    else:
        for layer in model.layers[-num:]:
            if not isinstance(layer, keras.layers.BatchNormalization): # never unfreeze batchnorm layers
                layer.trainable = True
        
# set dense 1 layer dropout rate
def setDense1DropoutRate(model, value):
    for layer in model.layers:
        if layer.name=='Dropout1':
            layer.rate = value


# print model layer names and trainable status
def printLayerSummary(model):
    print('Model Layers:')
    for idx, layer in enumerate(model.layers):        
        print('  Layer #'+str(idx)+' Name: '+layer.name+' Trainable: '+str(layer.trainable), end='')
        if hasattr(layer,'rate'):
            print(', Rate: '+str(layer.rate), end='')
        print('')

# print keras model summary        
def printKerasSummary(model):
    model.summary()
    
 
#def saveTrainedWeights(model, modelDir):
#    fn=os.path.join(modelDir,'weightsTrained_'+str(epochCounter-1)+'.h5')
#    model.save_weights(fn)
#    print('Saved weights to: ',fn)



# ---------------------------------------------------
#                 Data Generators                  
# ---------------------------------------------------

# build keras data generators for training and evaluation
def buildGenerators(cfg,  datasetDir):
    
    if cfg.NUMBER_OF_CLASSES==2:
        label_mode="int"        
    else:
        label_mode="categorical" 

    inputScalingType = cmb.getInputScalingType(cfg)        
        
    # --- build data sources ---    
    train_in_ds = keras.preprocessing.image_dataset_from_directory(
        datasetDir,
        validation_split=cfg.SPLIT_VALIDATION_RATIO,
        subset="training",
        seed = cfg.SPLIT_SEED,
        label_mode = label_mode,
        image_size = (cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH),
        batch_size = cfg.BATCH_SIZE
    )
    
    val_in_ds = keras.preprocessing.image_dataset_from_directory(
        datasetDir,
        validation_split=cfg.SPLIT_VALIDATION_RATIO,
        subset="validation",
        seed = cfg.SPLIT_SEED,
        label_mode=label_mode,    
        image_size = (cfg.IMAGE_HEIGHT,cfg.IMAGE_WIDTH),
        batch_size = cfg.BATCH_SIZE
    )
    
    if cfg.USE_AUGMENTATION:
        augmentationSeq = keras.Sequential(name="augmentationSeq")
        if cfg.RANDOM_FLIP_HORIZONTAL:
            augmentationSeq.add(keras.layers.experimental.preprocessing.RandomFlip("horizontal"))
        if cfg.RANDOM_FLIP_VERTICAL:
            augmentationSeq.add(keras.layers.experimental.preprocessing.RandomFlip("vertical"))
        if (abs(cfg.RANDOM_CONTRAST[0])>0) or (abs(cfg.RANDOM_CONTRAST[1])>0):
            augmentationSeq.add(keras.layers.experimental.preprocessing.RandomContrast(cfg.RANDOM_CONTRAST))
        if (abs(cfg.RANDOM_ZOOM[0])>0) or (abs(cfg.RANDOM_ZOOM[1])>0):
            augmentationSeq.add(keras.layers.experimental.preprocessing.RandomZoom( cfg.RANDOM_ZOOM, fill_mode="constant", interpolation="bilinear"))
        if (abs(cfg.RANDOM_ROTATION[0])>0) or (abs(cfg.RANDOM_ROTATION[1])>0):
            augmentationSeq.add(keras.layers.experimental.preprocessing.RandomRotation(cfg.RANDOM_ROTATION, fill_mode="constant", interpolation="bilinear"))

    pixprocLayer=cmb.PixelPreprocessLayer(inputScalingType=inputScalingType)
    pixel_scaling = keras.Sequential(pixprocLayer)

    if cfg.USE_AUGMENTATION:
        train_aug_ds = train_in_ds.map(
          lambda x, y: (augmentationSeq(x, training=True),y))
        train_ds = train_aug_ds.map(
          lambda x,y: (pixel_scaling(x),y))
    else:
        train_ds = train_in_ds.map(
          lambda x,y: (pixel_scaling(x),y))

    val_ds = val_in_ds.map(
      lambda x,y: (pixel_scaling(x),y))

    autotune = tf.data.experimental.AUTOTUNE

    if cfg.CACHE_DATASET:
        train_ds = train_ds.cache()
        val_ds = val_ds.cache()

    train_ds = train_ds.prefetch(buffer_size=autotune)
    val_ds = val_ds.prefetch(buffer_size=autotune)
    
    return train_ds, val_ds, train_in_ds.class_names
    
    
    
        
    
# ---------------------------------------------------
# ---             Training Tools                  --- 
# ---------------------------------------------------


def append_history(hist, histNew):
    [hist.epoch.append(v) for v in histNew.epoch]
    for key in hist.history.keys():
        [hist.history[key].append(v) for v in histNew.history[key]]
        
def get_capslock_state():
    VK_CAPITAL = 0x14
    if ((hllDll.GetKeyState(VK_CAPITAL)) & 0xffff) != 0:
        return True
    else:
        return False

def exit_capslock_state():
    hllDll.keybd_event(0x14, 69,1,0)
    hllDll.keybd_event(0x14, 69,3,0)

    

    

# ---------------------------------------------------
#               Training Progress Plots                  
# ---------------------------------------------------    
    
def historyPlt(historyLog, cfg, scalingRange=0):

    if ('categorical_accuracy' in historyLog.history.keys()):
        accKey = 'categorical_accuracy'
    else:
        accKey = 'accuracy' # when numberOfClasses==2
        
    if ('val_categorical_accuracy' in historyLog.history.keys()):
        accValKey = 'val_categorical_accuracy'
    else:
        accValKey = 'val_accuracy' # when numberOfClasses==2
    
    
    fig, ax = plt.subplots(1, 2, figsize=(12,6))
    ax[0].set_title('loss')
    ax[0].plot(historyLog.epoch, historyLog.history['loss'], label="Train")
    ax[0].plot(historyLog.epoch, historyLog.history['val_loss'], label="Validation")
    ax[1].set_title('acc')
    ax[1].plot(historyLog.epoch, historyLog.history[accKey], label="Train")
    ax[1].plot(historyLog.epoch, historyLog.history[accValKey], label="Validation")
    ax[0].legend()
    ax[1].legend()
    
    xmax=historyLog.epoch[-1]
    
    if scalingRange==0:    
        ax[0].set_xlim(left=0.0)
        ax[0].set_ylim(bottom=0.0)
        ax[1].set_xlim(left=0.0)
        ax[1].set_ylim(top=1.0)
    else:
        xleft  = max(xmax-scalingRange, 0)
        xright = xmax
        loss_max = max(historyLog.history['loss'][-scalingRange:])
        loss_min = min(historyLog.history['loss'][-scalingRange:])
        val_loss_max = max(historyLog.history['val_loss'][-scalingRange:])
        val_loss_min = min(historyLog.history['val_loss'][-scalingRange:])
        ax[0].set_ylim([min(loss_min,val_loss_min),max(loss_max,val_loss_max)])
        ax[0].set_xlim([xleft, xright])
            
        
        acc_max = max(historyLog.history[accKey][-scalingRange:])
        acc_min = min(historyLog.history[accKey][-scalingRange:])
        val_acc_max = max(historyLog.history[accValKey][-scalingRange:])
        val_acc_min = min(historyLog.history[accValKey][-scalingRange:])
            
        ax[1].set_ylim([min(acc_min,val_acc_min),max(acc_max,val_acc_max)])
        ax[1].set_xlim([xleft, xright])

    if len(cfg.EPOCHS)>1:
        xp=0.0        
        for i in range(len(cfg.EPOCHS)):
            xp += cfg.EPOCHS[i]
            if (xp>xmax): break
            ax[0].plot([xp+0.5, xp+0.5], [0,10000], color='green', linestyle='dashed')
            ax[1].plot([xp+0.5, xp+0.5], [0,1], color='green', linestyle='dashed')
       
    
    ax[0].grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
    ax[1].grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
    
    

def readCsvLog(fileName):
    dr = csv.DictReader(open(fileName))
    log=dict()
    for n in dr.fieldnames:
        log[n]=list()
    
    for line in dr:
        for k in dr.fieldnames:
            log[k].append(float(line[k]))
            
    return log


def csvLogPlt(fileName):
    history=readCsvLog(fileName)
    fig, ax = plt.subplots(1, 2, figsize=(12,6))
    
    ax[0].set_title('loss')
    ax[0].plot(history['epoch'], history['loss'], label="Train")
    ax[0].plot(history['epoch'], history['val_loss'], label="Val")
    ax[1].set_title('accuracy')

    # --- numClasses>2 ---
    if 'categorical_accuracy' in history.keys():
        ax[1].plot(history['epoch'], history['categorical_accuracy'], label="Train")
    if 'val_categorical_accuracy' in history.keys():
        ax[1].plot(history['epoch'], history['val_categorical_accuracy'], label="Val")
    
    # --- numClasses==2 ---
    if 'accuracy' in history.keys():
        ax[1].plot(history['epoch'], history['accuracy'], label="Train")
    if 'val_accuracy' in history.keys():
        ax[1].plot(history['epoch'], history['val_accuracy'], label="Val")
    ax[0].legend()
    ax[1].legend()
    
    ax[0].set_xlim(left=0.0)
    ax[0].set_ylim(bottom=0.0)
    ax[1].set_xlim(left=0.0)
    ax[1].set_ylim(top=1.0)
    
    ax[0].grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
    ax[1].grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
    
    


# ---------------------------------------------------
#                     Training                 
# ---------------------------------------------------    

# initialize the training, build and clear directories, build keras callbacks
def initTraining(cfg, 
                restartTraining=False,
                useTensorBoard=False,
                interruptible=False,
                epochStart=0):
    
    global epochCounter
    global historyLog        
    global kerasCallbackList
    global workDir  
    global interruptibleOnWindows
    global interruptibleOnLinux 
    
    print('initTraining...')        
    
    if restartTraining:
        epochCounter=epochStart
    else:
        epochCounter=0
        
    historyLog=[]    
    
    modelDir = os.path.join(workDir, 'modelCkpts')
    logDir   = os.path.join(workDir, 'log')
    tbLogDir = os.path.join(workDir, 'tbLogs')
    finalDir = os.path.join(workDir, 'modelFinal')
    diagramFullDir = os.path.join(workDir, 'diagramFull')
    diagramZoomDir = os.path.join(workDir, 'diagramZoom')

    if not restartTraining:
        #print('Removing ckpts files in ',modelDir)
        removeFilesInDirectory(modelDir)        
        removeFilesInDirectory(logDir)
        removeFilesInDirectory(finalDir)
        #print('Removing diagram files...')
        removeFilesInDirectory(diagramFullDir)
        removeFilesInDirectory(diagramZoomDir)
        
    removeFilesInDirectory(tbLogDir)    
         
    res = createDir(modelDir)
    res = createDir(finalDir)
    res = createDir(diagramFullDir)
    res = createDir(diagramZoomDir)    
    res = createDir(logDir)   

    res = createDir(os.path.join(workDir, 'modelVai'))    
        
    if useTensorBoard:
        res=createDir(tbLogDir)        
        if not os.path.exists(tbLogDir):  
            print('ERROR: Could not create tbLog directory')
            return                
        if len(os.listdir(tbLogDir))!=0:
            print("WARNING: The tbLog directory allready contains files.")
                   
    # --- create keras train callbacks ---
    modelFileName = os.path.join(modelDir, 'ckpt_e{epoch:05d}_loss{loss:.5f}_lossVal{val_loss:.5f}'+'.h5')
    callbackModelCheckpointer = keras.callbacks.ModelCheckpoint(
        modelFileName, 
        monitor='loss', # monitor='loss or 'val_loss'
        verbose=0, save_best_only=cfg.SAVE_BEST_MODEL_ONLY,
        save_weights_only=True) 

    kerasCallbackList = [callbackModelCheckpointer]    
    if useTensorBoard:
        callbackTensorBoard = keras.callbacks.TensorBoard(
                    log_dir=tbLogDir, histogram_freq=0,  write_graph=True,  write_images=False  )
        kerasCallbackList.append(callbackTensorBoard)
    
    csvLogFileName = os.path.join(logDir, 'csvLog.csv')
    callbackCsvLogger   = keras.callbacks.CSVLogger(csvLogFileName, separator=',', append=True)
    kerasCallbackList.append(callbackCsvLogger)
    
    interruptibleOnWindows = False
    interruptibleOnLinux   = False      
    if interruptible:        
        if (platform.system()=='Windows'):
            # load dll if keyboard interruptibility is required for windows   
            import ctypes
            global hllDll
            hllDll = ctypes.WinDLL ("User32.dll")
            interruptibleOnWindows=True
        else:
            import signal
            signal.signal(signal.SIGINT, signal_handler)
            interruptibleOnLinux   = True  
    


# restart training with the latest saved model checkpoint file 
def restartTraining(cfg, model):
    global workDir    
    global epochCounter

    # --- load latest model checkpoint ---
    fileList = os.listdir(os.path.join(workDir, 'modelCkpts'))
    if len(fileList)==0:
        print('No model checkpoint found for restart')
        return -1
    fileList.sort()
    print('Reloading model file: '+fileList[-1])
    model.load_weights(os.path.join(workDir, 'modelCkpts', fileList[-1]))
        
    no_digits = string.printable[10:]
    trans = str.maketrans(no_digits, " "*len(no_digits))
    epochStart=int(fileList[-1].translate(trans).split()[0])
    print('Restart epoch counter: '+str(epochStart))
    epochCounter=epochStart    
    
    return epochStart


# ctrl-C handling on linux
def signal_handler(sig, frame):
    global interruptedFlag
    print('You pressed Ctrl+C!')
    print('Closing training...')
    interruptedFlag = True
    #sys.exit(0)
    
    
# train the model with data generators for several epochs    
def trainModel(cfg, model,  trainDs, valDs, epochs=100,  verbose=1):
    global epochCounter
    global historyLog
    global interruptibleOnWindows
    global interruptibleOnLinux 
    global kerasCallbackList     
    global priorDiagramFullFile
    global priorDiagramZoomFile
    global workDir
    global interruptedFlag
    
    interruptedFlag=False
    print('Starting train epochs '+str(epochCounter)+'...'+str(epochCounter+epochs-1),end='')
        
    if interruptibleOnWindows:
        print(' (interruptible with capslock)')
    elif interruptibleOnLinux:
        print(' (interruptible with ctrl+c)')
    else:
        print('')
        
    for i in range(epochs):
        if interruptibleOnWindows:
            if get_capslock_state(): 
                print('Training interrupted with capslock after epoch ', epochCounter)
                exit_capslock_state()
                interruptedFlag=True
                break #exit epoch loop
                
        if interruptibleOnLinux:
            if interruptedFlag: # set by signal handler
                print('Training interrupted with ctrl+c after epoch ', epochCounter)
                break #exit epoch loop
            
        t1=time()        
        history=model.fit(
                trainDs,                
                epochs=1+epochCounter, # thats how keras works
                verbose=verbose,                
                validation_data=valDs,                
                initial_epoch=epochCounter,
                callbacks=kerasCallbackList                
        )
        t2=time()
                
        if epochCounter==0:
            historyLog=copy(history)
        else:
            if historyLog==[]:
                historyLog=copy(history) # restart after crash
            else:
                append_history(historyLog,history)        
                
        print('Epoch #'+str(epochCounter)+': '+str(t2-t1)+'s'+'  loss: '+str(history.history["loss"]))     
        
        # --- generate progress diagrams ---       
        if epochCounter%cfg.DIAGRAM_UPDATE_RATE==0 and epochCounter>0:

            fn = os.path.join(workDir, 'diagramFull', 'trainLossCurve_'+str(epochCounter)+'.'+cfg.DIAGRAM_FORMAT)            
            historyPlt(historyLog, cfg, scalingRange=0)                       
            plt.savefig(fn, format=cfg.DIAGRAM_FORMAT, dpi=1000)
            plt.close() #important, otherwise plt is rendered on screen
            
            if not priorDiagramFullFile=='':
                if os.path.isfile(priorDiagramFullFile):
                    os.remove(priorDiagramFullFile)                    
            priorDiagramFullFile = fn     
                        
            fn = os.path.join(workDir, 'diagramZoom', 'trainLossCurve_'+str(epochCounter)+'.'+cfg.DIAGRAM_FORMAT)            
            historyPlt(historyLog, cfg, scalingRange=cfg.DIAGRAM_ZOOM_EPOCHS)                       
            plt.savefig(fn, format=cfg.DIAGRAM_FORMAT, dpi=1000)
            plt.close() #important, otherwise plt is rendered on screen            #

            if not priorDiagramZoomFile=='':
                if os.path.isfile(priorDiagramZoomFile):
                    os.remove(priorDiagramZoomFile)                    
            priorDiagramZoomFile = fn     
                
        epochCounter += 1
           
    return historyLog
    
    
# finalize training: load last checkpoint and save trained model
def finalize_training(cfg, model):
    global workDir
        
    print('Finalize trained model')
    modelDir = os.path.join(workDir, 'modelCkpts')
    fileList = os.listdir(modelDir)
    fileList.sort()
    print('Reloading model checkpoint file: '+fileList[-1])
    model.load_weights(os.path.join(modelDir, fileList[-1]))
    print('Writing trained model file')
    #writeClassListFile(cfg)
    
    model.save(os.path.join(workDir, 'modelFinal',cfg.MODEL_TRAINED_FILE_NAME))
    return
    
    
    
    
# ----------------------------------------------------------------
#                              MAIN
# ----------------------------------------------------------------

def main(argv):
    global workDir
    global datasetDir
    global interruptedFlag
    
    # --- arguments ---
    parser = argparse.ArgumentParser()        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )                              
    parser.add_argument('--configFile', default = 'default', type = str,                         
                        help='.py configFile name\n' )          
    parser.add_argument('--restart', type = bool, default=False)    
    parser.add_argument('--finalize', type = bool, default=False)    
    parser.add_argument('--verify', type = bool, default=False)    
    
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' )    
    args = parser.parse_args()

    # --- check environment ---
    print('GPU test: '+tf.test.gpu_device_name())
    print('Tensorflow version:',tf.__version__)
    
    warnings.filterwarnings("ignore")   

    if (tf.__version__[0]== '1'): 
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.85
        config.gpu_options.allow_growth = True # dc in
        config.gpu_options.visible_device_list = "0"
        tf.keras.backend.set_session(tf.Session(config=config))
    
    # --- read config file ---    
    cp = args.configPath
    cf = args.configFile
    print('config file: ',cf)
    cfp=os.path.join(cp, str(cf+str('.py')))
         
    if os.path.isfile(cfp):
        print('reading config file...')
    else:
        print('ERROR: config file not found')
        return
       
    sys.path.insert(1, cp) # needed for import            
    cfg = importlib.import_module(cf)
    
    if args.finalize:
        print('    Finalize training')
    elif args.verify:  
        print('    Verify model performance')
    elif args.restart:
        print('    Restarting training')
    else:
        print('    Starting training')
            
    
    
    
    workDir    = args.workDir
    datasetDir = args.datasetDir
    
    print ('workDir:', workDir)
    print ('datasetDir:', datasetDir)
    
    modelDir = os.path.join(workDir, 'modelCkpts')    
    finalDir = os.path.join(workDir, 'modelFinal')
       
    


    
    # --- build the model ---
    print('--- Model Configuration ---')    
    
    #preWeightsCacheDir=None        
    #if hasattr(cfg,'PRETRAINED_WEIGHTS_CACHE_DIRECTORY'):
    #    if cfg.PRETRAINED_WEIGHTS_CACHE_DIRECTORY!=None:
    preWeightsCacheDir=os.path.join(workDir, 'classifierPretrainedWeights')
    createDir(preWeightsCacheDir)
    print('Pretrained weigths cache directory: ', preWeightsCacheDir)
    
    numberOfClassesInDataset = countNumberOfSubDirs(datasetDir)    
    print('Number of classes in dataset: ', numberOfClassesInDataset)
    if not(numberOfClassesInDataset == cfg.NUMBER_OF_CLASSES):
        print('ERROR: Number of classes in dataset do not fit to model')
        return    
    
    if args.verify:            
        fm=os.path.join(workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME)
        if os.path.isfile(fm):
            print('Loading finalized model: '+fm)
            model = tf.keras.models.load_model(fm)
        else:            
            model=cmb.buildModel(cfg, preWeightsCacheDir)
    else:  model=cmb.buildModel(cfg, preWeightsCacheDir)
    
    inputNodeName=model.input.op.name
    outputNodeName=model.output.op.name
    print('Keras Model Input Node:  '+inputNodeName)
    print('Keras Model Output Node: '+outputNodeName)
    printLayerSummary(model)
    
    # see https://keras.io/guides/transfer_learning/
    if cfg.NUMBER_OF_CLASSES==2:
        loss      = "binary_crossentropy"
        metrics   =["accuracy"]
    else:
        loss      = tf.keras.losses.CategoricalCrossentropy()    
        metrics   = [tf.keras.metrics.CategoricalAccuracy()]        
    
    # ----------------------------
    # --- do finalize training ---
    # ----------------------------
    if args.finalize:
        finalize_training(cfg, model)        
        return    
   
    # --- build data generators ---
    train_ds, val_ds, classNames = buildGenerators(cfg,  datasetDir)
    print('Class names: ',classNames)    
    
    # ----------------------------    
    # --- do model validation  ---
    # ----------------------------
    if args.verify:            
        fm=os.path.join(workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME)
        if os.path.isfile(fm):
            print('Loading finalized model: '+fm)
            model = tf.keras.models.load_model(fm)
        else:            
            fileList = os.listdir(modelDir)
            fileList.sort()
            model=cmb.buildModel(cfg, None) # weight init useless here
            fm=os.path.join(modelDir, fileList[-1])
            print('Loading checkpoint model: ', fm)
            model.load_weights(fm)
        
        model.compile(optimizer = keras.optimizers.RMSprop(lr=0),
            loss      = loss,
            metrics   = metrics    
        )   
        
        resTrain=model.evaluate(train_ds)
        resVal=model.evaluate(val_ds)
        
        print('--- Train Data Set ---')
        print(model.metrics_names[0]+':  '+str(resTrain[0]))
        print(model.metrics_names[1]+':  '+str(resTrain[1]))
        print('--- Validation Data Set ---')
        print(model.metrics_names[0]+':  '+str(resVal[0]))
        print(model.metrics_names[1]+':  '+str(resVal[1]))
                
        return
    
    # ----------------------------
    # ---  do model training   ---
    # ----------------------------
    numTrainSteps = len(cfg.EPOCHS)
    stepEpochEnd=[]
    see=0
    for i in range(numTrainSteps):
        see += cfg.EPOCHS[i]
        stepEpochEnd.append(see-1)        
    
    if args.restart:
        epochStart = restartTraining(cfg, model)
        if epochStart<0:
            print('No checkpoint for restart found')
            return
        print('Restart training at epoch counter value: '+str(epochStart))
        # --- calculate the train step to restart with ---
        startupTrainStep=0
        for i in range(numTrainSteps):
            if epochStart>stepEpochEnd[i]:
                startupTrainStep += 1
        print('Restart at training step # '+str(startupTrainStep))
        # calculate number of missing epochs in first learning step
        numEpochsRestartFirst = stepEpochEnd[startupTrainStep] - epochStart
        print('Missing epochs in this training step: '+str(numEpochsRestartFirst))
    else:
        epochStart=0
        startupTrainStep=0
        
    if hasattr(cfg,'USE_TENSORBOARD'): use_tb=cfg.USE_TENSORBOARD
    else: use_tb = False
    if hasattr(cfg,'CAPSLOCK_INTERRUPTABLE'): capint=cfg.CAPSLOCK_INTERRUPTABLE
    else: capint = True
           
    initTraining( cfg, 
                restartTraining = args.restart, 
                useTensorBoard = use_tb, 
                interruptible = capint,
                epochStart = epochStart)
                
    writeModelCfgFilePy(model, classNames,cfg, os.path.join(workDir,'modelFinal','modelCfg.py'))
                  
    # --- train step loop ---
    for trainStep in range(startupTrainStep,numTrainSteps):
        makeTopLayersTrainable(model, cfg.TRAINABLE_LAYERS[trainStep])
        setDense1DropoutRate(model, cfg.DENSE1_DROPOUT_RATE[trainStep])
    
        model.compile(
            #optimizer=keras.optimizers.Adam(cfg.LEARNING_RATE[trainStep]),
            optimizer = keras.optimizers.RMSprop(lr=cfg.LEARNING_RATE[trainStep]),
            loss      = loss,
            metrics   = metrics    
        )            
        
        if args.restart and trainStep==startupTrainStep:            
            numEpochs = numEpochsRestartFirst            
        else:
            numEpochs = cfg.EPOCHS[trainStep]        
        
        print('---------------------------------------------------------------')        
        print('Starting train step #',trainStep,', epochs: ',numEpochs)
        print('   trainable layers: ',cfg.TRAINABLE_LAYERS[trainStep])
        print('   learning rate: ',cfg.LEARNING_RATE[trainStep])
        print('   dense1 dropout rate: ',cfg.DENSE1_DROPOUT_RATE[trainStep])
        print('---------------------------------------------------------------')
    
        trainModel( cfg, model,  train_ds, val_ds, 
                    epochs=numEpochs, verbose=0   )
                   
        if interruptedFlag: break # keyboard interrupted                      
    
    finalize_training(cfg, model) # convert weights only checkpoint to full model .h5
    
    
    
if __name__=="__main__":
    main(sys.argv)

    
    
