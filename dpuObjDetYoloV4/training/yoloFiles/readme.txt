This directory has to contain the pretrained yoloV4 weight file, e.g. "yolov4.conv.137", which can be downloaded from

https://github.com/AlexeyAB/darknet/releases/download/yolov4/yolov4.conv.137