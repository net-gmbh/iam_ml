#!/bin/bash

source config.sh

echo "Config: $PROJECT_CONFIG"

python pycode/vai_verify.py \
            --configPath $(pwd)/projectConfig \
		    --configFile $PROJECT_CONFIG \
		    --workDir $WORK_DIR_IN_DOCKER \
		    --datasetDir $DATASET_DIR_IN_DOCKER
