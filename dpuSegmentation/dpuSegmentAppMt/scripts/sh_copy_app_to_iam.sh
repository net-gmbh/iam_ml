#!/bin/bash

# -------------------------------------------------
#            copy application to iam
# -------------------------------------------------


# --- read iam target settings ---
source iam_settings.mk


# --- prepare ssh access ---
if [[ $strict_host_key_checking -eq 0 ]] 
then
	#echo no
	ssh-copy-id -o "StrictHostKeyChecking no" -i $key $user@$ip
else
	#echo yes
	ssh-copy-id -i $key $user@$ip
fi


# --- create target folders ---
ssh -i $key $user@$ip "mkdir -p $folder"
ssh -i $key $user@$ip "mkdir -p $prog_folder"


# --- copy elf ---
scp -i $key $elf_name $user@$ip:$prog_folder

# --- copy xml ---
scp -i $key $xml_name $user@$ip:$prog_folder

# --- copy imgTest folder ---
ssh -i $key $user@$ip "mkdir -p $prog_folder/imgTest"
scp -i $key -r imgTest $user@$ip:$prog_folder

# --- copy icon ---
scp -i $key icon.png $user@$ip:$prog_folder



