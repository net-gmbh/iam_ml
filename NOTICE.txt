iam ML
Copyright 2021 Net GmbH

This product includes software developed at
Net GmbH (https://net-gmbh.com/).

This product contains code derived Xilinx/Vitis-AI, which is available under a "Apache License, Version 2.0" license. 
For details, see https://github.com/Xilinx/Vitis-AI/tree/master/examples/DPUCADF8H/tf_resnet/src


