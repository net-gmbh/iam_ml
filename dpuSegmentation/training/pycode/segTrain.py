#!/usr/bin/env python
# -*- coding: utf-8 -*-


#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


import sys
import os
from pathlib import Path
import string
import cv2
import numpy as np
import time, warnings
import argparse
import importlib
import shutil
import platform
import csv
import random
from copy import copy
import matplotlib.pyplot as plt
 
import tensorflow as tf
keras = tf.keras


from tensorflow.keras.models import load_model

import segModelBuilder as mb

import segTools as segTools




    
epochCounter=0
historyLog=[]
hllDll=0
tbLogDirMem ='None'
interruptibleMem = False
modelDirMem ='None'
modelFileName = ''
priorDiagramFullFile = ''
priorDiagramZoomFile = ''

__version__='0.0.1'

workDir=''    
    
    
    
    


def preprocess_input_pm1(x):
    x /= 127.5
    x -= 1.0
    return x

def preprocess_input_01(x):
    return x/255.

def preprocess_none(x):
    return x

# keras sequence generator
# len() can be detected, better than in case of python Generator

class segmentationSampleSeqGen(keras.utils.Sequence):
    
    def __init__(self, img_folder, mask_folder, batch_size, 
                 img_width , img_height, nClasses, useKerasAug=False, kerasImageGen=None):
        self.batch_size = batch_size
        self.img_width  = img_width
        self.img_height = img_height
        self.nClasses = nClasses
        self.img_folder = img_folder
        self.mask_folder = mask_folder
        self.imageNames = os.listdir(img_folder) 
        self.maskNames = os.listdir(mask_folder) 
        self.idx=list(range(len(self.imageNames)))
        self.useKerasAug=useKerasAug
        self.kerasImageGen=kerasImageGen
        random.shuffle(self.idx)
        
            

    def __len__(self):
        return len(self.imageNames) // self.batch_size

    def __getitem__(self, idx2):
        # Returns tuple (image, mask) for batch #idx.
        
        #if idx2==0: # added
        #    random.shuffle(self.idx)
        c = idx2 * self.batch_size
        
        imgBatch  = np.zeros((self.batch_size, self.img_height, self.img_width, 3)).astype('float')
        maskBatch = np.zeros((self.batch_size, self.img_height, self.img_width, self.nClasses)).astype('float')

        for i in range(self.batch_size):
            
            image = cv2.imread(self.img_folder+'/'+self.imageNames[self.idx[c]]) 
            if (self.useKerasAug):
                imageAug=next(self.kerasImageGen.flow(np.expand_dims(image, axis=0), batch_size=1))
                #imageAugInt=imageAug[0].astype(np.uint8)
                image=imageAug[0]
            else:
                image  = image.astype(np.float32)
            image /= 127.5 
            image -= 1.0   
            image =  cv2.resize(image, (self.img_height, self.img_width))
            imgBatch[i] = image                                                   

            mask = cv2.imread(self.mask_folder+'/'+self.maskNames[self.idx[c]], cv2.IMREAD_GRAYSCALE)
            mask = cv2.resize(mask, (self.img_height, self.img_width))
            seg_labels = np.zeros((  self.img_height , self.img_width  , self.nClasses ))
            for cc in range(self.nClasses):
                seg_labels[: , : , cc ] = (mask == cc ).astype(int)

            maskBatch[i] = seg_labels
            c += 1
            if c>=len(self.imageNames):
                c=0
                         
        return imgBatch, maskBatch
    
    def on_epoch_end(self):
        random.shuffle(self.idx)
        #print("shuffle")
    
    
# python train generator
# also ok for train, keras sequence seems to be better
def segmentationSamplePythonGenerator(img_folder, mask_folder, batch_size, width , height, nClasses):
    c = 0
    imageNames = os.listdir(img_folder) 
    maskNames = os.listdir(mask_folder) 
    idx=list(range(len(imageNames)))
    random.shuffle(idx)
      
    while (True):
        imgBatch  = np.zeros((batch_size, height, width, 3)).astype('float')
        maskBatch = np.zeros((batch_size, height, width, nClasses)).astype('float')

        for i in range(batch_size): #initially from 0 to 16, c = 0. 

            image = cv2.imread(img_folder+'/'+imageNames[idx[c]])
            image  = image.astype(np.float32)
            image /= 127.5
            image -= 1.0
            image =  cv2.resize(image, (height, width))# Read an image from folder and resize
      
            imgBatch[i] = image #add to array - img[0], img[1], and so on.
                                                   

            mask = cv2.imread(mask_folder+'/'+maskNames[idx[c]], cv2.IMREAD_GRAYSCALE)
            mask = cv2.resize(mask, (height, width))
            seg_labels = np.zeros((  height , width  , nClasses ))
            for c in range(nClasses):
                seg_labels[: , : , c ] = (mask == c ).astype(int)

            maskBatch[i] = seg_labels
            c += 1
            if c>=len(imageNames):
                c=0
                #random.shuffle(idx) # shuffle again for next epoch 

                     
        yield imgBatch, maskBatch


# -----------------------------------------------------------
#     Build data generators for training
# -----------------------------------------------------------
def buildGenerators(cfg):
    global workDir
    
    trainImgDir = os.path.join(workDir, 'data', 'train_img')
    trainSegDir = os.path.join(workDir, 'data', 'train_seg')
    validImgDir = os.path.join(workDir, 'data', 'valid_img')
    validSegDir = os.path.join(workDir, 'data', 'valid_seg')

    if (cfg.TRAIN_FROM_DISK):
        if cfg.USE_KERAS_AUG:
            imageGeneratorTrain = keras.preprocessing.image.ImageDataGenerator(
                brightness_range = cfg.AUG_BRIGHTNESS_RANGE,
                channel_shift_range = cfg.AUG_CHANNEL_SHIFT_RANGE )
        else:
            imageGeneratorTrain=None

        dataGeneratorTrain = segmentationSampleSeqGen(
                     trainImgDir, trainSegDir, 
                     batch_size =cfg.BATCH_SIZE, 
                     img_width=cfg.WIDTH, img_height=cfg.HEIGHT, nClasses=cfg.NUM_CLASSES,
                     useKerasAug=cfg.USE_KERAS_AUG, kerasImageGen=imageGeneratorTrain)
        dataGeneratorEval   = segmentationSampleSeqGen(
                     validImgDir, validSegDir, 
                     batch_size = cfg.BATCH_SIZE, 
                     img_width=cfg.WIDTH, img_height=cfg.HEIGHT, nClasses=cfg.NUM_CLASSES,
                     useKerasAug=False)
        numTrainSamples=len(os.listdir(trainImgDir))
        numEvalSamples =len(os.listdir(validImgDir))
        print('Train samples found on disk: ',numTrainSamples) 
        print('Eval  samples found on disk: ',numEvalSamples) 

    else:
        # load training images
        X_train, Y_train = segTools.loadSegmentationInOutDataset(trainImgDir, trainSegDir, 
                                                       cfg.WIDTH, cfg.HEIGHT, cfg.NUM_CLASSES, valueNormalize=False)
        #print(X_train.shape,Y_train.shape)

        # load validation images
        X_valid, Y_valid = segTools.loadSegmentationInOutDataset(validImgDir, validSegDir, 
                                                       cfg.WIDTH, cfg.HEIGHT, cfg.NUM_CLASSES, valueNormalize=False)
        #print(X_valid.shape,Y_valid.shape)

        #X_train, Y_train = random.shuffle(X_train, Y_train)
        #X_valid, Y_valid = random.shuffle(X_valid, Y_valid)

        print(X_train.shape[0],'train samples loaded to memory')
        print(X_valid.shape[0],'eval  samples loaded to memory')

        if cfg.USE_KERAS_AUG:
            imageGeneratorTrain = keras.preprocessing.image.ImageDataGenerator(
                brightness_range = cfg.AUG_BRIGHTNESS_RANGE,
                channel_shift_range = cfg.AUG_CHANNEL_SHIFT_RANGE,
                preprocessing_function=preprocess_input_pm1 )
        else:
            imageGeneratorTrain = keras.preprocessing.image.ImageDataGenerator(
                preprocessing_function=preprocess_input_pm1 )

        dataGeneratorTrain = imageGeneratorTrain.flow(
                X_train, Y_train,
                #target_size=(imageWidth, imageHeight),
                batch_size=cfg.BATCH_SIZE,
                #class_mode='categorical',
                shuffle=True)

        imageGeneratorEval = keras.preprocessing.image.ImageDataGenerator(
                    preprocessing_function=preprocess_input_pm1    
            )

        dataGeneratorEval = imageGeneratorEval.flow(
                X_valid, Y_valid,
                #target_size=(imageWidth, imageHeight),
                batch_size=cfg.BATCH_SIZE,
                #class_mode='categorical',
                shuffle=False)
        
    return dataGeneratorTrain, dataGeneratorEval

    








#------------------------------------------------------------------

# --- write python model property file ---
# (used for VitisAi 1.2)
def writeModelCfgFile(cfg):    
    global workDir
    fileName=os.path.join(workDir, 'modelFinal', 'config.py')
    txtFile = open(fileName, 'w')
    
    txtFile.write("MODEL_TYPE             = \'%s\'\n" %str(cfg.MODEL_TYPE))
    txtFile.write("WIDTH                  = %s\n" %str(cfg.WIDTH))
    txtFile.write("HEIGHT                 = %s\n" %str(cfg.HEIGHT))
    txtFile.write("NUM_CLASSES            = %s\n" %str(cfg.NUM_CLASSES))
        
    if (cfg.MODEL_TYPE=='FCN8' or cfg.MODEL_TYPE=='FCN8ups'):
        txtFile.write("INPUT_NODE         = \'%s\'\n" %str('input_1'))
        txtFile.write("OUTPUT_NODE        = \'%s\'\n" %str('activation_1/truediv'))
        txtFile.write("OUTPUT_NODE_DPU    = \'%s\'\n" %str('conv2DTransFinal/conv2d_transpose'))
    else:
        txtFile.write("INPUT_NODE         = \'%s\'\n" %str('input_1'))
        if (cfg.MODEL_TYPE=='UNETv2'):
            txtFile.write("OUTPUT_NODE        = \'%s\'\n" %str('conv2d_22/Relu'))
            txtFile.write("OUTPUT_NODE_DPU    = \'%s\'\n" %str('conv2d_22/Relu'))
        else:
            txtFile.write("OUTPUT_NODE        = \'%s\'\n" %str('conv2d_18/Relu'))
            txtFile.write("OUTPUT_NODE_DPU    = \'%s\'\n" %str('conv2d_18/Relu'))
    txtFile.close()   
    return
    
    
def writeClassListFile(cfg):
    global workDir
    fileName=os.path.join(workDir, 'modelFinal', 'classList.txt') 
    txtFile = open(fileName, 'w')
    for i in range(cfg.NUM_CLASSES):
        txtFile.write("%s %s %s %s\n" %(cfg.CLASS_NAMES[i], str(int(cfg.CLASS_COLORS[i][0])), str(int(cfg.CLASS_COLORS[i][1])), str(int(cfg.CLASS_COLORS[i][2]))))
    txtFile.close()


def readCsvLog(fileName):
    dr = csv.DictReader(open(fileName))
    log=dict()
    for n in dr.fieldnames:
        log[n]=list()
    
    for line in dr:
        for k in dr.fieldnames:
            log[k].append(float(line[k]))
            
    return log


def readCsvLogFieldnames(fileName):
    dr = csv.DictReader(open(fileName))
    return dr.fieldnames


def csvLogPlt(fileName):
    history=readCsvLog(fileName)
    fig, ax = plt.subplots(1, 2, figsize=(12,6))
    ax[0].set_title('loss')
    ax[0].plot(history['epoch'], history['loss'], label="Train loss")
    ax[0].plot(history['epoch'], history['val_loss'], label="Validation loss")
    ax[1].set_title('acc')
    if ('acc' in history.keys()): # older keras version
        ax[1].plot(history['epoch'], history['acc'], label="Train acc")
        ax[1].plot(history['epoch'], history['val_acc'], label="Validation acc")
    else:
        ax[1].plot(history['epoch'], history['accuracy'], label="Train acc")
        ax[1].plot(history['epoch'], history['val_accuracy'], label="Validation acc")
    ax[0].legend()
    ax[1].legend()
    
def csvLogPltTrace(csvLogFileName, trace):
    history=readCsvLog(csvLogFileName)
    if not trace in history:
        print('ERROR: ',trace,' not found in csv log file traces: ', readCsvLogFieldnames(csvLogFileName))
        return
    fig = plt.figure(figsize=(12,6))
    #plt.set_title('lr')
    plt.plot(history['epoch'], history['lr'], label="lr")
    plt.legend()



def append_history(hist, histNew):
    [hist.epoch.append(v) for v in histNew.epoch]
    for key in hist.history.keys():
        [hist.history[key].append(v) for v in histNew.history[key]]
        
def get_capslock_state():
    VK_CAPITAL = 0x14
    if ((hllDll.GetKeyState(VK_CAPITAL)) & 0xffff) != 0:
        return True
    else:
        return False

def exit_capslock_state():
    hllDll.keybd_event(0x14, 69,1,0)
    hllDll.keybd_event(0x14, 69,3,0)



def historyPlt(historyLog, scalingRange=0):
    #history=readCsvLog(fileName)
    fig, ax = plt.subplots(1, 2, figsize=(12,6))
    ax[0].set_title('loss')
    ax[0].plot(historyLog.epoch, historyLog.history['loss'], label="Train")
    ax[0].plot(historyLog.epoch, historyLog.history['val_loss'], label="Validation")
    ax[1].set_title('acc')
    if ('acc' in historyLog.history.keys()): # older keras versions
        ax[1].plot(historyLog.epoch, historyLog.history['acc'], label="Train")
        ax[1].plot(historyLog.epoch, historyLog.history['val_acc'], label="Validation")
    else:
        ax[1].plot(historyLog.epoch, historyLog.history['accuracy'], label="Train")
        ax[1].plot(historyLog.epoch, historyLog.history['val_accuracy'], label="Validation")
    ax[0].legend()
    ax[1].legend()
    if scalingRange==0:        
        ax[0].set_ylim(bottom=0.0)
        ax[1].set_ylim(top=1.0)
    else:
        #xmax=len(historyLog.epoch)-1
        xmax=historyLog.epoch[-1]
        xleft  = max(xmax-scalingRange, 0)
        xright = xmax
        loss_max = max(historyLog.history['loss'][-scalingRange:])
        loss_min = min(historyLog.history['loss'][-scalingRange:])
        val_loss_max = max(historyLog.history['val_loss'][-scalingRange:])
        val_loss_min = min(historyLog.history['val_loss'][-scalingRange:])
        ax[0].set_ylim([min(loss_min,val_loss_min),max(loss_max,val_loss_max)])
        ax[0].set_xlim([xleft, xright])
            
        if ('acc' in historyLog.history.keys()): # older keras versions
            acc_max = max(historyLog.history['acc'][-scalingRange:])
            acc_min = min(historyLog.history['acc'][-scalingRange:])
            val_acc_max = max(historyLog.history['val_acc'][-scalingRange:])
            val_acc_min = min(historyLog.history['val_acc'][-scalingRange:])
        else:
            acc_max = max(historyLog.history['accuracy'][-scalingRange:])
            acc_min = min(historyLog.history['accuracy'][-scalingRange:])
            val_acc_max = max(historyLog.history['val_accuracy'][-scalingRange:])
            val_acc_min = min(historyLog.history['val_accuracy'][-scalingRange:])
        ax[1].set_ylim([min(acc_min,val_acc_min),max(acc_max,val_acc_max)])
        ax[1].set_xlim([xleft, xright])



def initTraining( cfg, 
                  restartTraining=False,
                  useCsvLogger=True, 
                  useTensorBoard=False,
                  interruptible=False,
                  epochStart=0):
    
    global epochCounter
    global historyLog
    global interruptibleMem
    global kerasCallbackList
    global workDir        
    
    epochCounter=0
    historyLog=[]    

    modelDir = os.path.join(workDir, 'modelCkpts')
    logDir   = os.path.join(workDir, 'log')
    tbLogDir = os.path.join(workDir, 'tbLogs')
    finalDir = os.path.join(workDir, 'modelFinal')
    diagramFullDir = os.path.join(workDir, 'diagramFull')
    diagramZoomDir = os.path.join(workDir, 'diagramZoom')
    
    if restartTraining:
        epochCounter=epochStart
    
    if not restartTraining:
        segTools.removeFilesInDirectory(modelDir)
        segTools.removeFilesInDirectory(logDir)
        segTools.removeFilesInDirectory(finalDir)
        segTools.removeFilesInDirectory(diagramFullDir)
        segTools.removeFilesInDirectory(diagramZoomDir)
    
    segTools.removeFilesInDirectory(tbLogDir)    
    
    
    res=segTools.createDir(modelDir)    
    res=segTools.createDir(finalDir)
    res=segTools.createDir(diagramFullDir)
    res=segTools.createDir(diagramZoomDir)
    
    if useCsvLogger:
        res=segTools.createDir(logDir)
                   
    
    if useTensorBoard:        
        res=segTools.createDir(tbLogDir)
        
        segTools.createDir(tbLogDir)
        if not os.path.exists(tbLogDir):  
            print('ERROR: Could not create tbLog directory')            
                
        if len(os.listdir(tbLogDir))!=0:
            print("WARNING: The tbLog directory allready contains files.")
    
    modelFileName = os.path.join(modelDir, 'ckpt_e{epoch:05d}_loss{loss:.5f}_lossVal{val_loss:.5f}'+'.h5')

    #callbackearlyStopper = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=20, verbose=1)
    callbackModelCheckpointer = keras.callbacks.ModelCheckpoint(modelFileName, 
                               monitor='loss', verbose=0, save_best_only=cfg.SAVE_BEST_MODEL_ONLY,
                               save_weights_only=True) # or monitor='val_loss'

    callbackTensorBoard = keras.callbacks.TensorBoard(log_dir=tbLogDir, histogram_freq=0,  write_graph=True,  write_images=False  )
    #callbackReduceLR    = keras.callbacks.ReduceLROnPlateau(monitor='loss', mode='min', factor=0.5, min_lr=0.000001, patience=5,verbose=1)
    
    callbackCsvLogger   = keras.callbacks.CSVLogger(os.path.join(logDir,"csvLog.csv"), separator=',', append=True)
 
    kerasCallbackList = [callbackModelCheckpointer]
    #kerasCallbackList = [callbackModelCheckpointer, callbackReduceLR]

    if useTensorBoard:
        kerasCallbackList.append(callbackTensorBoard)
                
    if useCsvLogger:
        kerasCallbackList.append(callbackCsvLogger)
        
    # --- create model directory ---    
    segTools.createDir(modelDir)    
    if len(os.listdir(modelDir))!=0:
        print("WARNING: The model directory allready contains files.")
        
        
    if (interruptible and (platform.system()=='Windows')):
        import ctypes
        global hllDll
        hllDll = ctypes.WinDLL ("User32.dll")
        interruptibleMem = True
    else:
        interruptibleMem = False
        
    



    # eval step does not work for numpy based gerneratos....
#def trainModelGen(model, dataGeneratorTrain, dataGeneratorEval, epochs=100,
def trainModel( cfg, model,  dataGeneratorTrain, dataGeneratorEval, 
                   epochs=100,                   
                   datasetFractionTrain=1.0, datasetFractionEval=1.0):
    global epochCounter
    global historyLog
    global interruptibleMem
    global kerasCallbackList    
    global priorDiagramFullFile
    global priorDiagramZoomFile
    global workDir
    
    if hasattr(dataGeneratorTrain, 'samples'):
        numTrainSamples=dataGeneratorTrain.samples # csv filename generator
        stepsPerEpoch=max(1,int(numTrainSamples*datasetFractionTrain) // dataGeneratorTrain.batch_size)
    else:
        if hasattr(dataGeneratorTrain, 'x'):
            numTrainSamples=dataGeneratorTrain.x.shape[0] # np array gererator
            stepsPerEpoch=max(1,int(numTrainSamples*datasetFractionTrain) // dataGeneratorTrain.batch_size)
        else:
            stepsPerEpoch=max( 1,int(len(dataGeneratorTrain)*datasetFractionTrain) )
        
                
    if interruptibleMem:
        print('Running training epoch '+str(epochCounter)+'...'+str(epochCounter+epochs-1)
          +' (stop with capslock)')
    else:
         print('Running training epoch '+str(epochCounter)+'...'+str(epochCounter+epochs-1))
         
         
            
    for i in range(epochs):
        
        if interruptibleMem:
            if get_capslock_state(): 
                print('Training interrupted by capslock after epoch '+str(i-1))
                exit_capslock_state()
                break #exit epoch loop
            
        t1=time.time()
        
        history=model.fit(
                dataGeneratorTrain,                
                epochs=1+epochCounter, # thats how keras works
                verbose=cfg.TRAINING_VERBOSE,                
                validation_data=dataGeneratorEval,                
                initial_epoch=epochCounter,
                callbacks=kerasCallbackList                
        )

        t2=time.time()
        
        if epochCounter==0:
            historyLog=copy(history)
        else:
            if historyLog==[]:
                historyLog=copy(history) # restart after crash
            else:
                append_history(historyLog,history)        
        
        #print('1Epoch #'+str(epochCounter)+': '+str(t2-t1)+'s'
        #    +'  loss: '+str(history.history["loss"][0])
        #    +'  acc: '+str(history.history["accuracy"][0])
        #    +'  val_loss: '+str(history.history["val_loss"][0])
        #    +'  val_acc: '+str(history.history["val_accuracy"][0]) )  
        if ('acc' in history.history.keys()): # older keras version
             print('Epoch #%d: (%.2fs)  loss: %.5f  acc: %.5f   val_loss: %.5f  val_acc:  %.5f' %(epochCounter,(t2-t1),history.history["loss"][0],history.history["acc"][0],history.history["val_loss"][0], history.history["val_acc"][0]))  
        else:
            print('Epoch #%d: (%.2fs)  loss: %.5f  acc: %.5f   val_loss: %.5f  val_acc:  %.5f' %(epochCounter,(t2-t1),history.history["loss"][0],history.history["accuracy"][0],history.history["val_loss"][0], history.history["val_accuracy"][0]))  
            
            
        if epochCounter%cfg.DIAGRAM_UPDATE_RATE==0 and epochCounter>0:

            fn = os.path.join(workDir, 'diagramFull', 'trainLossCurve_'+str(epochCounter)+'.'+cfg.DIAGRAM_FORMAT)            
            historyPlt(historyLog)                       
            plt.savefig(fn, format=cfg.DIAGRAM_FORMAT, dpi=1000)
            plt.close() #important, otherwise plt is rendered on screen
            
            if not priorDiagramFullFile=='':
                if os.path.isfile(priorDiagramFullFile):
                    os.remove(priorDiagramFullFile)                    
            priorDiagramFullFile = fn     
                        
            fn = os.path.join(workDir, 'diagramZoom', 'trainLossCurve_'+str(epochCounter)+'.'+cfg.DIAGRAM_FORMAT)            
            historyPlt(historyLog, scalingRange=cfg.DIAGRAM_ZOOM_EPOCHS)                       
            plt.savefig(fn, format=cfg.DIAGRAM_FORMAT, dpi=1000)
            plt.close() #important, otherwise plt is rendered on screen            

            if not priorDiagramZoomFile=='':
                if os.path.isfile(priorDiagramZoomFile):
                    os.remove(priorDiagramZoomFile)                    
            priorDiagramZoomFile = fn     

        epochCounter += 1
           
    return historyLog
    


def finalize_training(cfg, model):
    global workDir
    
    vaiDir = os.path.join(workDir, 'modelVai')
    segTools.createDir(vaiDir)
    
    print('Finalize trained model')
    modelDir = os.path.join(workDir, 'modelCkpts')
    fileList = os.listdir(modelDir)
    fileList.sort()
    print('Reloading model checkpoint file: '+fileList[-1])
    model.load_weights(os.path.join(modelDir, fileList[-1]))
    print('Writing trained model file')
    writeClassListFile(cfg)
    model.save(os.path.join(workDir, 'modelFinal',cfg.MODEL_TRAINED_FILE_NAME))
    return
    
    
def main(argv):
    global workDir

    print('GPU test: '+tf.test.gpu_device_name())

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )      
                        
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )      
    
    parser.add_argument('--restart', type = bool, default=False)
    
    parser.add_argument('--finalize', type = bool, default=False)
    
    parser.add_argument('--verify', type = bool, default=False)
    
    #parser.add_argument('--dockerMode', type = bool, default=False)
    
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )
    #parser.add_argument('--datasetDir', default = 'default', type = str, help='Path to dataset directory\n' )     
               
    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)
    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import    
        
    cfg = importlib.import_module(cf)
    
    if args.restart:
        print('Restarting training')
    else:
        print('Starting training')

    #if args.dockerMode:    
    #    workDir = cfg.WORK_DIR_DOCKER
    #else:
    #    workDir = cfg.WORK_DIR
        
    workDir=args.workDir
    print ('workDir:', workDir)
    
    
    warnings.filterwarnings("ignore")
    
    


    if (tf.__version__[0]== '1'): 
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.85
        config.gpu_options.allow_growth = True # dc in
        config.gpu_options.visible_device_list = "0"
        tf.keras.backend.set_session(tf.Session(config=config))


    

    print('Tensorflow version:',tf.__version__)

    print('--- Model Configuration ---')
    mb.printModelSummary(cfg)
    
    
    
    # --- do model validation ---
    if args.verify:            
        fm=os.path.join(workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME)
        if os.path.isfile(fm):
            print('Loading finalized model: '+fm)
            model = load_model(fm)
        else:
            # load last checkpoint model
            modelDir = os.path.join(workDir, 'modelCkpts')
            fileList = os.listdir(modelDir)
            fileList.sort()
            model=mb.buildModel(cfg)
            fm=os.path.join(modelDir, fileList[-1])
            print('Loading checkpoint model: ', fm)
            model.load_weights(fm)
        if cfg.TRAINING_VERIFY_DATASET=='valid':
            imgDir=os.path.join(workDir, 'data', 'valid_img')
            segDir=os.path.join(workDir, 'data', 'valid_seg')
            print("Calculating IoU for validation data set...")
           
        else:
            imgDir=os.path.join(workDir, 'data', 'train_img')
            segDir=os.path.join(workDir, 'data', 'train_seg')
            print("Calculating IoU for training data set...")            
                                                   
        X, Y = segTools.loadSegmentationInOutDataset(imgDir, segDir, 
                                   cfg.WIDTH, cfg.HEIGHT, cfg.NUM_CLASSES, 
                                   valueNormalize=True)                                         
        print(X.shape[0],' samples loaded')
        print()
        predVec   = model.predict(X, batch_size=1)
        pred  = np.argmax(predVec, axis=3)
        truth = np.argmax(Y, axis=3)
        IoUList = segTools.calculate_IoU(truth,pred, cfg.NUM_CLASSES)
        
        print('')
        print('')
        print('Model file: ',fm)
        print('Image dir: ',imgDir)
        print('Number of samples: ',X.shape[0])
        print('')

        for c in range(cfg.NUM_CLASSES):
            print("%12.12s (%2d):  IoU=%4.3f" % (cfg.CLASS_NAMES[c],c,IoUList[c]))
        mIoU = np.mean(IoUList)        
        print("_________________")
        print("Mean IoU: {:4.3f}".format(mIoU))
        return
    
    # --- build the model ---
    model=mb.buildModel(cfg)
    
    if args.finalize:
        finalize_training(cfg, model)        
        return
    
    
    if args.restart:
        fileList = os.listdir(os.path.join(workDir, 'modelCkpts'))
        if len(fileList)==0:
            print('No model checkpoint found for restart')
            exit(1)
        fileList.sort()
        print('Reloading model file: '+fileList[-1])
        model.load_weights(os.path.join(workDir, 'modelCkpts', fileList[-1]))
        
        no_digits = string.printable[10:]
        trans = str.maketrans(no_digits, " "*len(no_digits))
        epochStart=int(fileList[-1].translate(trans).split()[0])
        print('Restart epoch counter: '+str(epochStart))
    else:
        epochStart=0
        
        

    # --- data generators for training ---
    dataGeneratorTrain, dataGeneratorEval = buildGenerators(cfg)
    
    
    initTraining( cfg, restartTraining=args.restart,
                  useCsvLogger=cfg.USE_CSV_LOGGER, 
                  useTensorBoard=cfg.USE_TENSORBOARD, 
                  interruptible=cfg.CAPSLOCK_INTERRUPTABLE,
                  epochStart=epochStart)

    
    
    
        

    sgd = keras.optimizers.SGD(lr=1E-2, decay=5**(-4), momentum=0.9, nesterov=True)

    model.compile(loss='categorical_crossentropy',
                  optimizer=sgd,
                  metrics=['accuracy'])

    inputNodeName=model.input.op.name
    outputNodeName=model.output.op.name
    print('Keras Model Input Node:  '+inputNodeName)
    print('Keras Model Output Node: '+outputNodeName)
    
    
    trainModel( cfg, model,  dataGeneratorTrain, dataGeneratorEval, 
                   epochs=cfg.EPOCHS  )
    
    finalize_training(cfg, model) # convert weights only checkpoint to full model .h5
    
    
    
if __name__=="__main__":
    main(sys.argv)

