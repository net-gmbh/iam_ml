# --- value pre scaling ---

import numpy as np
        
def preprocess_input_none(x):
    return x

def preprocess_input_rgb1s(x):
    x /= 127.5
    x -= 1.0
    return x

def preprocess_input_rgb01(x):
    return x/255.

def preprocess_input_tf(x):
    x /= 127.5
    x -= 1.0
    return x

def preprocess_input_torch(x):
    x /= 255.
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    x[..., 0] -= mean[0]
    x[..., 1] -= mean[1]
    x[..., 2] -= mean[2]       
    x[..., 0] /= std[0]
    x[..., 1] /= std[1]
    x[..., 2] /= std[2]
    return x

def preprocess_input_caffe(x):
    # 'RGB'->'BGR'
    x = x[..., ::-1]
    mean = [103.939, 116.779, 123.68]
    x[..., 0] -= mean[0]
    x[..., 1] -= mean[1]
    x[..., 2] -= mean[2]
    return x  

def preprocess(img, inputScalingType):
    if inputScalingType=='rgb1s':
       return preprocess_input_rgb1s(img)
    if inputScalingType=='rgb01':
        return preprocess_input_rgb01(img)
    if inputScalingType=='tf':
        return preprocess_input_tf(img)
    if inputScalingType=='torch':
        return preprocess_input_torch(img)
    if inputScalingType=='caffe':
        return preprocess_input_caffe(img)
    return preprocess_input_none(img)





