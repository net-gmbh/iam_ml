#!/bin/bash

source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"



sudo chmod 777 $WORK_DIR/vaiModels

vai_c_caffe \
	--prototxt $WORK_DIR/vaiModels/deploy.prototxt \
	--caffemodel $WORK_DIR/vaiModels/deploy.caffemodel \
	--arch arch/$TARGET_ARCH.json \
	--output_dir $WORK_DIR/vaiModels/  \
	--net_name $FINAL_FILE_NAME \
	--options "{'mode':'normal','save_kernel':''}";
	
	

