/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "main.h"

#include "frameProcessing.h"
#include "bufferManager.h"
#include "camDevice.h"
#include "common.h"


extern bufferManagerClass bufferManager;
extern camDeviceClass camDevice;


int processFrame(cv::Mat img, dpuObjDetYoloV4Class *dpuObjDetYoloV4) {

 
    if (enableClassifierFlag && modelOkFlag) {        

        double tickCountClass1;
        double tickCountClass2;

        tickCountClass1 = (double) cv::getTickCount();
        int ok = dpuObjDetYoloV4->procImg(img);
        tickCountClass2 = (double) cv::getTickCount();

        if (!ok) return 0;

        if (enablePrintfDpuFlag) {
            printf("dpu time %.2f ms (classifier %.2f ms) \n",1000.0*(dpuObjDetYoloV4->tickCountStop-dpuObjDetYoloV4->tickCountStart)/cv::getTickFrequency(),1000.0*(tickCountClass2-tickCountClass1)/cv::getTickFrequency());
        }
    
    }   

    return 1;
}








void processFrameThreadFunction(int id,  bool& do_modelConfig, bool& is_running) {

    int inBufferIdx=-1;
    long long unsigned int fcnt=0;
    int err;
    int ImgWidth,ImgHeight;
    int ImgPixelFormat;
    

    dpuObjDetYoloV4Class dpuObjDetYoloV4;
        
    while (is_running) {     

        if (do_modelConfig) {
            printf(">>>-- Procc-Thread %d: Config model      --<<<\n" , id);
            dpuObjDetYoloV4.configDpu();
            do_modelConfig=false;
            printf(">>>-- Procc-Thread %d: Config model done --<<<\n" , id);
        }   
        
        int newBufferAvailable = bufferManager.pullBuffer1(&inBufferIdx, &fcnt);        
        
        if (newBufferAvailable) {
            if (enablePrintfBufferManager) {
                printf(COLOR_MAG "Procc-Thread #%d: Processing frame %d (fcnt %llu)\n" COLOR_RESET, id, inBufferIdx, fcnt);
            }

            LvBuffer* pBuffer = m_Buffers[inBufferIdx];

            cv::Mat inPicConv;  // debayered sensor image
            cv::Mat inPicMat;
            uint8_t* pImageData;

            // -----------------------------
            // ---   Get image info      ---
            // -----------------------------

            LvipImgInfo ImgInfo;
            err = pBuffer->GetImgInfo ( ImgInfo );       // depends on Uni Processing mode: if "Off" or "HwOnly", this returns the unprocessed buffer info
            if (err) {
                PRINTF (( "ERROR: CallbackNewBuffer::ivQueryFrameGenTL: Cannot retrieve image info" ));
                pBuffer->Queue();
                
            } else {            
                pImageData = (uint8_t*)ImgInfo.pData; // get image data pointer
                ImgWidth         = ImgInfo.Width;
                ImgHeight        = ImgInfo.Height;
                //int ImgLinePitch     = ImgInfo.LinePitch;
                //int ImgBytesPerPixel = ImgInfo.BytesPerPixel;
                ImgPixelFormat   = ImgInfo.PixelFormat;
                
                //PRINTF (( "CallbackNewBuffer:  Input: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgWidth, ImgHeight, ImgBytesPerPixel, ImgPixelFormat, pImageData ));
                
                // frontend is providing bayer data
                inPicMat = cv::Mat(cv::Size(ImgWidth, ImgHeight), CV_8U,(unsigned char*) pImageData); 
                
                // -----------------------------
                // --- Preprocessing         ---
                // -----------------------------                
                
                if (ImgPixelFormat==LvPixelFormat_Mono8) { // mono sensor       
                    cvtColor( inPicMat, inPicConv, cv::COLOR_GRAY2RGB );   
                } else if (sensorPixelFormat == LvPixelFormat_BayerGR8) {
                    cvtColor( inPicMat, inPicConv, cv::COLOR_BayerGR2BGR );             
                } else if (sensorPixelFormat == LvPixelFormat_BayerRG8) {
                    cvtColor( inPicMat, inPicConv, cv::COLOR_BayerRG2BGR ); 
                } else if (sensorPixelFormat == LvPixelFormat_BayerGB8) {
                    cvtColor( inPicMat, inPicConv, cv::COLOR_BayerGB2BGR ); 
                } else {
                    cvtColor( inPicMat, inPicConv, cv::COLOR_BayerBG2BGR );         
                }
                // this produces RGB image,  opencv/synview bayer naming mismatch

                bufferManager.pullDoneBuffer1(inBufferIdx);  // put sv buffer back into pool

                int outBufferIdx;
                int ok = bufferManager.pushBuffer2(fcnt, &outBufferIdx);

                if (ok) {
                    
                    if (sensorDecimation==1) {         
                        bufferManager.imagesBuffer2[outBufferIdx]=inPicConv;
                    }
                    if (sensorDecimation==2) {         
                        cv::resize(inPicConv,bufferManager.imagesBuffer2[outBufferIdx],cv::Size (ImgWidth/2,ImgHeight/2),0,0,cv::INTER_NEAREST );
                    }
                    if (sensorDecimation==4) {         
                        cv::resize(inPicConv,bufferManager.imagesBuffer2[outBufferIdx],cv::Size (ImgWidth/4,ImgHeight/4),0,0,cv::INTER_NEAREST );
                    }
                    
                    //printf("pii width: %d height %d\n",bufferManager.imagesBuffer2[outBufferIdx].cols, bufferManager.imagesBuffer2[outBufferIdx].rows);
                        
                    // -----------------------------
                    // --- Auto Exposure Control ---
                    // -----------------------------

                    if (autoExposureMode==2) {
                        double ev;
                        float newExpValue;

                        cv::Scalar mv = cv::mean(bufferManager.imagesBuffer2[outBufferIdx]);        
                        ev=0.114*mv.val[0]+0.587*mv.val[1]+0.299*mv.val[2];  

                        camDevice.GetFloatCameraParam(LvDevice_ExposureTime, &newExpValue);                                  
                        if ( ev>128) { // too bright
                            if      ( ev>220) newExpValue/=1.3;
                            else if ( ev>180) newExpValue/=1.15;
                            else if ( ev>150) newExpValue/=1.06;            
                        } else {        // too dark
                            if      ( ev< 30) newExpValue*=1.3;
                            else if ( ev< 50) newExpValue*=1.15;
                            else if ( ev<100) newExpValue*=1.06;
                        }
                        if (newExpValue<   100) newExpValue=100;    // 100 us
                        if (newExpValue>250000) newExpValue=250000; // 0.25 sec
                        
                        exposureTime=newExpValue;
                        //camDevice.SetFloatCameraParam(LvDevice_ExposureTime,  newExpValue);
                        // --> done in sv callback now
                        printf ("AE: EV: %f --> Exposure: %f\n",ev,newExpValue );     
                    }

                    // --- overlay test pic --- 
                    if ((testPicIdx>0)&&(testImageLoaded)) {                        
                        int xs=bufferManager.imagesBuffer2[outBufferIdx].cols;
                        int ys=bufferManager.imagesBuffer2[outBufferIdx].rows;
                        mtx_testPicRgb.lock();
                        cv::resize(testPicRgb, bufferManager.imagesBuffer2[outBufferIdx], cv::Size(xs, ys), 0,0, cv::INTER_NEAREST);
                        mtx_testPicRgb.unlock();
                    }
                    
                    processFrame(bufferManager.imagesBuffer2[outBufferIdx], &dpuObjDetYoloV4);

                    if (enableTemperatureOsd) {        
                            char charBuffer [30];
                            float val;
                            float fontScale = 3.0;
                            int textThickness = 2;
                            if (bufferManager.imagesBuffer2[outBufferIdx].size().width < 1000) {
                            textThickness = 1;
                            fontScale = 2.0;
                        }
                            cv::String cto;

                            val = devTempMain;
                            sprintf (charBuffer, "MainFPD Temp:  %3.1f", val);   
                            cto=cv::String(charBuffer);
                            cv::putText(bufferManager.imagesBuffer2[outBufferIdx], cto, cv::Point(20,80), 
                                        cv::FONT_HERSHEY_PLAIN, fontScale, cv::Scalar(0, 0, 255), textThickness,  cv::LINE_AA, false);

                            val = devTempSensor;
                            sprintf (charBuffer, "Sensor Temp:  %3.1f", val);   
                            cto=cv::String(charBuffer);
                            cv::putText(bufferManager.imagesBuffer2[outBufferIdx], cto, cv::Point(20,120), 
                                        cv::FONT_HERSHEY_PLAIN, fontScale, cv::Scalar(0, 0, 255), textThickness,  cv::LINE_AA, false);
                    }
                    
                    bufferManager.pushDoneBuffer2(outBufferIdx, fcnt);
                    
                }
            }            

        }
    } 

    dpuObjDetYoloV4.cleanup();
    printf("closing processing thread #%d\n",id);
    
}







// -----------------------------------------------------------------------------
//                Callback function for each frontend frame
// -----------------------------------------------------------------------------

void LV_STDC CallbackNewBufferFunction(LvHBuffer buffer,
    void* pUserPointer, void* pUserParam) {
    
    LvBuffer* pBuffer = (LvBuffer*) pUserPointer;

    //PRINTF (( "appClass::CallbackNewBuffer [begin] (Buffer:%p)", pBuffer ));
    int AwaitDelivery;
    int NumUnderrunAct;
   

    if (autorun) printf("process image...\n");

    // buffer undefined
    if ( pBuffer == 0 )
    {
        PRINTF (( "CallbackNewBuffer: buffer pointer undefined [end]" ));
        return;
    }
    // event no longer defined
    if ( camDevice.m_pEvent == NULL )
    {
        PRINTF (( "CallbackNewBuffer: event not defined! [end]" ));
        pBuffer->Queue();
        return;
    }

    // close event requested?
    if ( camDevice.m_pEvent->CallbackMustExit () )
    {
        PRINTF (( "CallbackNewBuffer: exit requested! [end]" ));
        pBuffer->Queue();
        return;
    }

    // get buffer pool info
    if (camDevice.m_pStream->GetInt32 ( LvStream_LvNumAwaitDelivery, &AwaitDelivery) != LVSTATUS_OK ) {
        PRINTF (( "appClass::ivQueryFrameGenTL: error get AwaitDelivery" ));
    }
    if (camDevice.m_pStream->GetInt32 ( LvStream_LvNumUnderrun, &NumUnderrunAct) != LVSTATUS_OK ) {
        PRINTF (( "appClass::ivQueryFrameGenTL: error get NumUnderrun" ));
    }
        

    int bufferIdx;
    long long unsigned int fcnt;
    //int ok = bufferManager.pushBuffer1(pBuffer, &bufferIdx, &fcnt);
    int ok = bufferManager.pushBuffer1(buffer, &bufferIdx, &fcnt);

    if (!ok) {
        printf(COLOR_RED "Camera Callback: push buffer1 failed (buffer to small ?)\n" COLOR_RESET);
    } else {
        if (enablePrintfBufferManager) {
            printf(COLOR_YEL "Camera Callback: push buffer1 %d (fcnt %llu)\n" COLOR_RESET, bufferIdx, fcnt);    
        }
    }

    // --- set exposure value for ae ----
    if (autoExposureMode==2) {
        camDevice.SetFloatCameraParam(LvDevice_ExposureTime,  exposureTime);
    }
 
    return;
}





cv::Mat ConvertRGB2BayerGR(cv::Mat inImg)   {
  
    cv::Mat outImg(inImg.rows, inImg.cols, CV_8UC1);
    int channel;
    for (int row = 0; row < outImg.rows; row++) {
        for (int col = 0; col < outImg.cols; col++) {
            if (row % 2 == 0) {
                //channel = (col % 2 == 0) ? 0 : 1;
                //channel = (col % 2 == 0) ? 1 : 2;
                channel = (col % 2 == 0) ? 1 : 0;
            } else {
                //channel = (col % 2 == 0) ? 1 : 2;
                //channel = (col % 2 == 0) ? 0 : 1;
                channel = (col % 2 == 0) ? 2 : 1;
            }
            outImg.at<uchar>(row, col) = inImg.at<cv::Vec3b>(row, col).val[channel];
        }
    }
    return outImg;
}






void sendFrameThreadFunction(bool& is_running) {
    while (is_running) {
        if (enableStreaming & enableGevStreaming) {
            int bufferReadIdx;
            long long unsigned int fcnt;
            int ok=bufferManager.pullBuffer2(&bufferReadIdx, &fcnt);
            if (ok) {
                if (enablePrintfBufferManager) {
                    printf(COLOR_CYN "GigE Transmitter: Sending frame %d (fcnt %llu)\n" COLOR_RESET, bufferReadIdx, fcnt); 
                }

                LvipImgInfo ImgInfo;
                
                // ------------------------------
                // --- Generate Output Image  ---
                // ------------------------------
                
                    if (outputImageMode==0) {   // RGB image output mode             
                        ImgInfo.Width=bufferManager.imagesBuffer2[bufferReadIdx].cols;
                        ImgInfo.Height=bufferManager.imagesBuffer2[bufferReadIdx].rows;
                        ImgInfo.LinePitch=bufferManager.imagesBuffer2[bufferReadIdx].cols*3;
                        ImgInfo.PixelFormat=outputPixelFormat;
                        ImgInfo.BytesPerPixel=3;
                        ImgInfo.pData=bufferManager.imagesBuffer2[bufferReadIdx].ptr();    
                        //PRINTF (( "Sending GigE img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgInfo.Width, ImgInfo.Height, ImgInfo.BytesPerPixel, ImgInfo.PixelFormat, ImgInfo.pData ));	    
                        //SendImage ( &ImgInfo ); // send image via gige  

                    } else {                    // Bayer image output mode        
                        cv::Mat bayerImg=ConvertRGB2BayerGR(bufferManager.imagesBuffer2[bufferReadIdx]);
                        ImgInfo.Width=bufferManager.imagesBuffer2[bufferReadIdx].cols;
                        ImgInfo.Height=bufferManager.imagesBuffer2[bufferReadIdx].rows;
                        ImgInfo.LinePitch=bufferManager.imagesBuffer2[bufferReadIdx].cols;
                        ImgInfo.PixelFormat=outputPixelFormat;
                        ImgInfo.BytesPerPixel=1;
                        ImgInfo.pData=bayerImg.ptr();   
                        //PRINTF (( "Sending GigE img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgInfo.Width, ImgInfo.Height, ImgInfo.BytesPerPixel, ImgInfo.PixelFormat, ImgInfo.pData ));	    
                        //SendImage ( &ImgInfo ); // send image via gige               
                    }    

                    
                    //PRINTF (( "iAMServerClass::SendImage: LvDevice_LvGigEServerImage: pImgInfo:%p pImg:%p PixFmt:%x Width:%d Height:%d LinePitch:%d", (int64_t)ImgInfo, (int64_t)ImgInfo->pData, ImgInfo->PixelFormat, ImgInfo->Width, ImgInfo->Height, ImgInfo->LinePitch ));
                    pDevice->SetInt ( LvDevice_LvGigEServerImage, (int64_t)&ImgInfo );
                
                bufferManager.pullDoneBuffer2(bufferReadIdx);

            } else {
                //printf("------> sendFrame: nothing to send \n");   
            }


        } else {
            //printf("------> sendFrame: enableStreaming false \n");            
            std::this_thread::sleep_for(20ms);
        }
    }
    printf("------> closing  sendFrame thread\n");
}
