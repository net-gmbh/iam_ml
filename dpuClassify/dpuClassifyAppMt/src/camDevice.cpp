/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#include "camDevice.h"
#include "main.h"
#include "gigeServerEvents.h"

typedef struct _EventData
{
    void*  pCBEvent;
    void*  pCBContext;
} EventData;


LvBuffer*   m_Buffers[NUMBER_OF_AQUISITION_BUFFERS];



// -----------------------------------------------------------------------------
//   Constructor
// -----------------------------------------------------------------------------

camDeviceClass::camDeviceClass ()
{    
    m_pStream     = NULL;
    m_pEvent      = NULL;

    memset(m_Buffers, 0, sizeof(m_Buffers));
}

// -----------------------------------------------------------------------------
//      Destructor
// -----------------------------------------------------------------------------

camDeviceClass::~camDeviceClass() {  
}

// -----------------------------------------------------------------------------
//      OpenCamera
// -----------------------------------------------------------------------------

LvStatus camDeviceClass::OpenCamera ( )
{
    if ( pDevice != NULL ) CloseCamera();

    LvStatus SynviewStatus;
    
    pSystem->UpdateInterfaceList ();

    // scan interfaces
    uint32_t pNumberOfInterfaces;
    pSystem->GetNumberOfInterfaces ( &pNumberOfInterfaces );
    printf("   NumberOfInterfaces:%d\n",   pNumberOfInterfaces);

    std::string pInterfaceId;
    for ( int k = pNumberOfInterfaces-1; k >= 0; k-- )
    {
        pSystem->GetInterfaceId ( k, pInterfaceId );

        if ( pInterfaceId == "iAM Interface" ) {
            printf("   camDeviceClass::OpenCamera: Open the Interface nr:%d = >%s<\n",   k, pInterfaceId.c_str());
        }

        else {
            printf("   camDeviceClass::OpenCamera: Skip Interface nr:%d = >%s<\n",   k, pInterfaceId.c_str());
            continue;
        }
        SynviewStatus = pSystem->OpenInterface(pInterfaceId.c_str(), pInterface);

        // scan devices
        uint32_t pNumberOfDevices;
        pInterface->UpdateDeviceList();
        pInterface->GetNumberOfDevices(&pNumberOfDevices);

        printf("   camDeviceClass::OpenCamera: NumberOfDevices on %s = %d\n",   pInterfaceId.c_str(), pNumberOfDevices);
        for ( uint32_t l = 0; l < pNumberOfDevices; l++ )
        {
            pInterface->GetDeviceId ( l, sDeviceId );

            printf("   camDeviceClass::OpenCamera: trying Device nr:%d on:%s Device Id String: >%s<\n",  l, pInterfaceId.c_str(), sDeviceId.c_str());
            SynviewStatus = pInterface->OpenDevice(sDeviceId.c_str(), pDevice, LvDeviceAccess_Exclusive);
            if (ErrorOccurred(SynviewStatus)) continue;

            SynviewStatus = pDevice->OpenStream("", m_pStream);
            if (ErrorOccurred(SynviewStatus)) {
                //SynviewStatus = m_pInterface->CloseDevice(pDevice);
                continue;
            }

            goto _camFound_;
        }
    }
    printf (COLOR_RED "   ERROR: No Camera found\n" COLOR_RESET);
    return (-1);
    

_camFound_:
    printf ( "   Opened Cam >%s< on Device:%s (pInterface:%p pDevice:%p)\n",   sDeviceId.c_str(), pInterfaceId.c_str(), pInterface, pDevice );
   
    OpenFrameBuffers();
    return 0;
}


int camDeviceClass::setupGigeServer(std::string xmlFileName) {    

    int val;
    LvStatus SynviewStatus;   

    // --- init  GigE Server ---
    // enable Smart functions     
    // GlobalSmartEnable + GlobalFeatureLock + GlobalXMLEnable
    SetInt32CameraParam  ( LvDevice_LvSmartGeneralInq,     7 );      // do this first!

    char fn[1024];
    strncpy(fn, xmlFileName.c_str(), sizeof(fn));
    fn[sizeof(fn) - 1] = 0;
    pDevice->SetString ( LvDevice_LvSmartAppXMLPath,fn);
    
    std::string path;
    pDevice->GetString ( LvDevice_LvSmartAppXMLPath, path);
    printf("   Applied XML File: %s\n", path.c_str());

    // enable gige vision server
    GetInt32CameraParam( LvDevice_LvGigEServerCapability, &val );
    if ( (val & 0x01) != 0 )
    {
        // enable gige server
        pDevice->SetInt ( LvDevice_LvGigEServerEnable, 0x01 );

        // check
        GetInt32CameraParam( LvDevice_LvGigEServerEnable, &val );
        printf ( "   Enable GEV server. Result: %s\n", (val & 0x01) == 1?"OK":"NOT OK!" );
        if ( (val & 0x01) == 1  )
        {
            // register the xml event callback
            EventData Buffer;
            Buffer.pCBEvent   = (void *)gigeEventCallback;
            Buffer.pCBContext = (void *)this;

            printf ( "   InitServer: Register GigE Vision Server Event %p with context: %p\n", Buffer.pCBEvent, Buffer.pCBContext );
            SynviewStatus = pDevice->SetBuffer ( LvDevice_LvGigEServerEvent, (void *)&Buffer, sizeof(Buffer) );
            if (ErrorOccurred(SynviewStatus)) return 0;	
        }
    } 
    return 1;
}




// -----------------------------------------------------------------------------
//   InitCamera
// -----------------------------------------------------------------------------

LvStatus camDeviceClass::InitCamera()
{
    LvStatus SynviewStatus;
    
    // chunk off
    if ( pDevice->IsAvailable ( LvDevice_ChunkModeActive ) ) {
        pDevice->SetBool ( LvDevice_ChunkModeActive, 0 );
    }

    // UniProcessMode: off
    SynviewStatus = pDevice->SetEnum ( LvDevice_LvUniProcessMode, LvUniProcessMode_Off );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    if ( pDevice->IsAvailable ( LvDevice_RegionSelector ) ) {
        pDevice->SetEnum ( LvDevice_RegionSelector, LvRegionSelector_Region0 );
    }
        
    return 0;
}


// -----------------------------------------------------------------------------
//   Start acquisition
// -----------------------------------------------------------------------------
int camDeviceClass::StartAcquisition()
{
    if ( pDevice == NULL ) return 1;

    LvStatus SynviewStatus;

    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = pDevice->AcquisitionStart ();
    ErrorOccurred(SynviewStatus);
    return SynviewStatus;
}


// -----------------------------------------------------------------------------
//   Stop acquisition
// -----------------------------------------------------------------------------
int camDeviceClass::StopAcquisition()
{
    if ( ! IsAcquiring () ) return 1;

    LvStatus SynviewStatus;
    SynviewStatus = pDevice->AcquisitionStop ();
    ErrorOccurred(SynviewStatus);
    return SynviewStatus;
}


// -----------------------------------------------------------------------------
//   Close camera
// -----------------------------------------------------------------------------
LvStatus camDeviceClass::CloseCamera()
{
    if (pDevice != NULL) {        
        if ( IsAcquiring() ) StopAcquisition();
		
		LvStatus svStatus;
		
		 // de-register the event callback
        EventData Buffer;
        Buffer.pCBEvent   = 0;
        Buffer.pCBContext = 0;        
        svStatus = pDevice->SetBuffer ( LvDevice_LvGigEServerEvent, (void *)&Buffer, sizeof(Buffer) );
        if (ErrorOccurred(svStatus)) return svStatus;
		
        if ( m_pEvent == NULL ) return 1;
        
        svStatus = StopStream();        
        if (ErrorOccurred(svStatus)) return svStatus;

        CloseFrameBuffers();
        
        svStatus = pDevice->CloseStream ( m_pStream );
        if (ErrorOccurred(svStatus)) return svStatus;

        svStatus = pInterface->CloseDevice ( pDevice );
        if (ErrorOccurred(svStatus)) return svStatus;

        svStatus = pSystem->CloseInterface ( pInterface );
        if (ErrorOccurred(svStatus)) return svStatus;    
    }
    return LVSTATUS_OK;
}


// -----------------------------------------------------------------------------
//   OpenBuffers
// -----------------------------------------------------------------------------
LvStatus camDeviceClass::StartStream()
{    
    int SynviewStatus;

    // open an event for the stream
    //printf("[%05d]:: camDeviceClass::OpenBuffers OpenEvent\n", NOW);
    SynviewStatus = m_pStream->OpenEvent(LvEventType_NewBuffer, m_pEvent);
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    
    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );                              // remove all remaining events in order to prevent further callbacks
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllDiscard );                              // discard all buffers
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;


    // enable image callback (new buffer)
    //printf("[%05d]:: camDeviceClass::OpenBuffers() enable callback\n", NOW);
    SynviewStatus = m_pEvent->SetCallbackNewBuffer ( CallbackNewBufferFunction/*m_pNewBufferFunction*/, 0/*m_pClass*/ );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pEvent->StartThread ();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    
    return 0;
}

// -----------------------------------------------------------------------------
//   CloseBuffers
// -----------------------------------------------------------------------------
LvStatus camDeviceClass::StopStream()
{
    
    int SynviewStatus;
    if ( pDevice == NULL ) return -1;
    if ( IsAcquiring() ) StopAcquisition();
    if ( m_pEvent == NULL ) return -2;    
    
    //SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );  // remove all remaining events in order to prevent further callbacks
    //if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    
    SynviewStatus = m_pEvent->StopThread ();                                  // wait for running callback to finish, stop thread
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    if (m_pStream == NULL) return -3;
        
    SynviewStatus = m_pStream->CloseEvent(m_pEvent);                          // close the event loop
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    
    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllDiscard );    // discard all buffers
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

      
    return 0;
}

LvStatus camDeviceClass::OpenFrameBuffers() {
    int SynviewStatus;
    

    for ( int i = 0; i < NUMBER_OF_AQUISITION_BUFFERS; i++ )
    {
        //printf("[%05d]:: camDeviceClass::OpenStream() m_Buffers[%d]\n", NOW, i);
        SynviewStatus = m_pStream->OpenBuffer ( NULL, 0, NULL, 0, m_Buffers[i] );
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
        SynviewStatus = m_Buffers[i]->Queue();
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    }
    return SynviewStatus;
}

LvStatus camDeviceClass::CloseFrameBuffers() {
    int SynviewStatus=0;

    //SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );  // remove all remaining events in order to prevent further callbacks
    //if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllDiscard );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    for (int i=0; i<NUMBER_OF_AQUISITION_BUFFERS; i++) {
        if (m_Buffers[i] != NULL) {
            SynviewStatus = m_pStream->CloseBuffer(m_Buffers[i]);
        }
    }
    return SynviewStatus;
}


// -----------------------------------------------------------------------------
//  IsAcquiring ()
// -----------------------------------------------------------------------------
bool camDeviceClass::IsAcquiring()
{
    if (pDevice == NULL) return false;
    int32_t iIsAcquiring;
    pDevice->GetInt32(LvDevice_LvDeviceIsAcquiring, &iIsAcquiring);
    return iIsAcquiring != 0;
}


// -----------------------------------------------------------------------------
//   parameter interface functions
// -----------------------------------------------------------------------------

LvStatus camDeviceClass::GetInt32CameraParam ( LvFeature Feature, int32_t* piValue )
{
    if ( pDevice == NULL ) return -1;
    return pDevice->GetInt32 ( Feature, piValue );
}

LvStatus camDeviceClass::SetInt32CameraParam (LvFeature Feature, int32_t iValue)
{
    if ( pDevice == NULL ) return -1;
    return pDevice->SetInt32 ( Feature, iValue );
}

LvStatus camDeviceClass::GetFloatCameraParam (LvFeature Feature, float* pValue)
{
    if ( pDevice == NULL ) return -1;
    int SynviewStatus;
    double val;
    SynviewStatus = pDevice->GetFloat ( Feature, &val );
    if (SynviewStatus!=LVSTATUS_OK) return SynviewStatus;
    *pValue=float(val);
    return LVSTATUS_OK;    
}

LvStatus camDeviceClass::SetFloatCameraParam (LvFeature Feature, float Value)
{
    if ( pDevice == NULL ) return -1;
    return pDevice->SetFloat ( Feature, Value );
}

