# dpuYoloV4 #

YoloV4 is a popular model for realtime object detection, which is available on github [AlexeyAB/darknet](https://github.com/AlexeyAB/darknet). It  detects instances of semantic objects defined by rectangular bounding boxes. Model training is done with with the included darknet framework. After model post processing with the Vitis-AI tools, the compiled model can be executed on the **iam ML ready** camera.


![yolo](/dpuObjDetYoloV4/misc/images/yoloDetect.png)

Framerate on iam Zu2 and Zu5 cameras:

 Model        | resolution    |     Zu2    |   Zu5
 -------------|---------------|------------|------------
 YoloV4       | 416x416       |     tbd    |  14.6  fps
 

## Model Training ##
---
Folder `/training` contains scripts for retraining the YoloV4 model for a custom datasets with darknet and the VitisAi tool flow for generating the DPU model file.



## Running YoloV4 Application on iam ##
---


An application package with models and executable included, can be downloaded from [dpuObjDetYoloV4AppMt.tar.gz](https://bitbucket.org/dcarpentier123/iam_ml_3xx/downloads/dpuObjDetYoloV4AppMt.tar.gz)   
 For installation on iam copy the .tar.gz file to `/home/root/dpuObjDetYoloV4AppMt/` and extract files with...
> `tar xz -f dpuObjDetYoloV4AppMt.tar.gz`   

 Start application...
> `./dpuObjDetYoloV4AppMt`

> Connect iam via GigE Vision with Synview. 


![YoloApp](/dpuObjDetYoloV4/misc/images/screenshotNetLogo.png)

The application starts with its default model, which is trained on detection NET Logos. With the camera GUI features `TestImages->TestPicList`, test images from the training dataset can be inserted. 
  

