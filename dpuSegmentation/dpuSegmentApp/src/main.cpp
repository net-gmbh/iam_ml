/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */
 

#include "main.h"

LvSystem    *System = 0;
appClass    app;


int main( int argc, char** argv )
{
    printf("-----------------------------------------------------------------\n");
    printf("       %s Version: %s\n", APPLICATION_NAME,  APPLICATION_VERSION);
    printf("-----------------------------------------------------------------\n");

    char c=0;
    int  ok;
    int  secCnt=0;

    remove("/opt/synview/bin/sv.iAMGigEServer.stop");

    // **********************************************
	//                 Arg Parser 
	// **********************************************
    int autoFlag=0;

    if (argc > 1) {
		for (int i = 1; i < argc; i++) {
			std::string argStr(argv[i]);
			printf("arg %d: %s: ", i, argStr.c_str());

			// --- commands ---	
			if (argStr.compare("-auto") == 0) {
				autoFlag = 1;
				printf("arg:-auto --> autostart frontend\n");
			}
            else {
				printf("Unknown argument\n");
			}
		}
	}

	// **********************************************
	//                 Open Synview 
	// **********************************************
       
    char Msg[1024];
    string sPathCti;

    printf("[%05d]: Opening synview library...\n", NOW );
    if (LvLibrary::OpenLibrary() != LVSTATUS_OK)
    {
        printf("[%05d]: Opening synview library failed\n", NOW );
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("[%05d]: Error: %s\n", NOW, Msg);
        goto _cleanup_;
    }

    printf("[%05d]: Synview: Opening the system...\n", NOW);
    if (LvSystem::Open("", System) != LVSTATUS_OK)
    {
        printf("[%05d]: Synview: Opening the system failed\n", NOW);
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("[%05d]: Synview: Error opening the system:%s\n", NOW, Msg);
        goto _cleanup_;       
    }
    
    System->GetString(LvSystem_TLPath, sPathCti);
    printf("[%05d]: cti file=\"%s\"\n", NOW,  sPathCti.c_str());


	// **********************************************
	//            open frontend 
	// **********************************************   

    //app = new appClass();
    
    printf("Synview: Opening camera frontend ...\n");
    if (app.iamDevice.OpenCamera ( System ) != LVSTATUS_OK)  //stay in main :-)
    {
        printf("Open camera frontend failed\n" );
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf("Error: %s\n",  Msg);
        goto _cleanup_;
    }
    
    
    ok=app.init(autoFlag);
	if (!ok) {
		goto _cleanup_;
	}
    
    
    // **********************************************
    //       endless loop, react to user inputs
    // **********************************************

    printf("[%05d]: press 'q' or 'ESC' to end program\n", NOW);    

    while(true)
    {
        secCnt++;
        if (secCnt>999) secCnt=0;
        if (secCnt==0) { 
            // user keys
            OsKeyPressed ( c );   // get key
                    
            if ((c=='q')||(c=='Q')||(c==27)) break;   		

            if (FILE *file = fopen("/opt/synview/bin/sv.iAMGigEServer.stop", "r")) {
                fclose(file);
                break;
            }

            if (app.exitAppFlag) break;            
            c = 0;    // reset key
        }

        app.loop1ms();

        OsSleep(1); // 1ms
    }

_cleanup_:

	printf("\n");
    printf("[%05d]: cleanup\n", NOW);    

    		
    printf("[%05d]: StopAcquisition camera", NOW);
    app.iamDevice.StopAcquisition ();        

    printf("[%05d]: close system", NOW);
    LvSystem::Close ( System );
        
    printf("[%05d]: close library", NOW);
    LvLibrary::CloseLibrary ();        
    

    printf("[%05d]: waiting to close..\n", NOW);
    OsSleep ( 1000 );
    //getchar();

    printf("[%05d]: exiting\n", NOW);

    return ( 0 );
}
