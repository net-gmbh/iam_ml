#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


import os


# ---------------------------------------------------------------
#     ORIGINAL DATASET
# ---------------------------------------------------------------

ORIG_IMAGE_DIR  = 'images/'
ORIG_LABEL_FILE = 'labels/via_region_data_646.json'
MASK_DIR  = ''

NUM_CLASSES = 2  #number of classes (including background)
CLASS_NAMES = ("None", "NetLogo")
CLASS_COLORS =  [[0,0,0],[128,0,0],[0,128,0],[0,0,128],[128,128,0],[0,128,128],[128,0,128]] # RGB


# ---------------------------------------------------------------
#     WORKING DATASET
# ---------------------------------------------------------------
GENERATE_MASKS_FROM_LABEL_FILE = True
GENERATE_VIEWABLE_MASKS = True

VALIDATION_SAMPLE_RATIO = 0.15
CALIB_SAMPLE_RATIO = 0.2




#---------------------------------------------------------------------
#      MODEL CONFIGURATION
#---------------------------------------------------------------------
MODEL_TYPE = 'FCN8'

FCN8_IGNORE_POOL3 = True
UNET_NUM_FILTERS  = 16*4
UNET_DROPOUT      = 0.1
UNET_BATCHNORM    = True
UNET_ACTIVATION   = True

#Size of images at network input
WIDTH  = 224
HEIGHT = 224

#WIDTH  = 448
#HEIGHT = 448

MODEL_TRAINED_FILE_NAME  = 'model_trained.h5'

TRAINING_VERBOSE = 0
USE_TENSORBOARD = False
USE_CSV_LOGGER  = True
SAVE_BEST_MODEL_ONLY = True

DIAGRAM_UPDATE_RATE = 5
DIAGRAM_FORMAT  = 'png'
DIAGRAM_ZOOM_EPOCHS = 10

TRAIN_FROM_DISK = False

BATCH_SIZE = 32 # for 224x224 on rtx2060
#BATCH_SIZE = 6 # for 448x448 on rtx2060
EPOCHS = 200

USE_KERAS_AUG = True
#AUG_BRIGHTNESS_RANGE =None
AUG_BRIGHTNESS_RANGE = [0.5,1.6]
AUG_CHANNEL_SHIFT_RANGE = 0.25

CAPSLOCK_INTERRUPTABLE = True

TRAINING_VERIFY_DATASET = 'valid' # valid or train


#---------------------------------------------------------------------
#      VITIS AI MODEL CONVERSION
#---------------------------------------------------------------------
VAI_CALIB_BATCH_SIZE = 4
VAI_QUANT_MODE = 'normal' # 'normal' or 'fine'

VAI_QUANT_VERIFY_FLOAT_MODEL = True
VAI_QUANT_VERIFY_QUANT_MODEL = True
VAI_QUANT_VERIFY_DATASET = 'valid' # valid or train

VAI_TARGET_DEVICE = 'zu5'
VAI_FINAL_MODEL_FILE_NAME = 'model_seg_netLogo_fcn8_zu5'


