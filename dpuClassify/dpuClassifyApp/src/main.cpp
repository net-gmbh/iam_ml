/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

// ----------------------------------------------
//       Reference Application for iam ML 
//     DPU accelerated image classification
// ---------------------------------------------- 


#include "main.h"

LvSystem    *System       = 0;
appClass    app;

int main( int argc, char** argv )
{
    printf("\n---------------------------------------------------\n");
    printf("         %s\n", APPLICATION_NAME  );
    printf("---------------------------------------------------\n");

    char c=0;
    int ok;
    
	//LvStatus SynviewStatus;

    int secCnt=0;

    remove("/opt/synview/bin/sv.iAMGigEServer.stop");

    // **********************************************
	//                 Arg Parser 
	// **********************************************
    int autoFlag=0;

    if (argc > 1) {
		for (int i = 1; i < argc; i++) {
			std::string argStr(argv[i]);
			printf("arg %d: %s: ", i, argStr.c_str());

			// --- commands ---	
			if (argStr.compare("-auto") == 0) {
				autoFlag = 1;
				printf("arg:-auto --> autostart frontend\n");
			}
            else {
				printf("Unknown argument\n");
			}
		}
	}

	// **********************************************
	//                 Open Synview 
	// **********************************************
       
    char Msg[1024];
    string sPathCti;

    string sCtid = "cti\\sv.gentl.cti";

    printf("[%05d]: Synview: Open library...\n", NOW );
    if (LvLibrary::OpenLibrary() != LVSTATUS_OK)
    {
        printf("[%05d]: Synview: Open library failed\n", NOW );
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("[%05d]: Synview: Error open Library:%s\n", NOW,  Msg);
        goto _cleanup_;
    }

    printf("[%05d]: Synview: Open system...\n", NOW );
    if (LvSystem::Open("", System) != LVSTATUS_OK)
    {
        printf("[%05d]: Synview: Open system failed\n", NOW );
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("[%05d]: Synview: Error open system:%s\n", NOW,  Msg);

        // 2nd chance
        string sPath;
        LvLibrary::GetLibInfoStr(LvInfo_BinPath, sPath);
        sPath = sPath+sCtid;
        printf("[%05d]: Synview: Open system: \"%s\"\n", NOW,  sPath.c_str());
        if (LvSystem::Open(sPath.c_str(), System) != LVSTATUS_OK)
        {
            printf("[%05d]: Synview: Open system failed\n", NOW );
            LvGetLastErrorMessage(Msg, sizeof(Msg));
            printf("[%05d]: Synview: Error open system:%s\n", NOW,  Msg);
            LvLibrary::CloseLibrary();
            goto _cleanup_;
        }
    }
    System->GetString(LvSystem_TLPath, sPathCti);
    printf("[%05d]: cti file=\"%s\"\n", NOW,  sPathCti.c_str());
       

    // open camera frontend
    printf("[%05d]: Synview: Open camera...\n", NOW );
    if (app.iamDevice.OpenCamera ( System ) != LVSTATUS_OK)  //stay in main :-)
    {
        printf("[%05d]: Synview: Open camera failed\n", NOW );
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf("[%05d]: Synview: Error open camera:%s\n", NOW,  Msg);
        goto _cleanup_;
    }

    // **********************************************
	//               Init Application 
	// **********************************************
    ok=app.init(autoFlag);
	if (!ok) {
        printf(COLOR_RED "ERROR --> Terminate application\n" COLOR_RESET);
		goto _cleanup_;
	}
    
    
    // **********************************************
    //       endless loop
    // **********************************************

    printf("[%05d]: press 'q' or 'ESC' to end program\n", NOW );    

    while(true)
    {
        secCnt++;
        if (secCnt>999) secCnt=0;
        if (secCnt==0) { 
            OsKeyPressed ( c );   // get key
                    
            if ((c=='q')||(c=='Q')||(c==27)) break;   		

            if (FILE *file = fopen("/opt/synview/bin/sv.iAMGigEServer.stop", "r")) {
                fclose(file);
                break;
            }

            if (app.exitAppFlag) break;            
            c = 0;    // reset key
        }

        app.loop1ms();
        OsSleep(1); // 1ms
    }

_cleanup_:

	printf("\n");
    printf("[%05d]: cleanup\n", NOW);    

    	
    printf("[%05d]: StopAcquisition camera", NOW );
    app.iamDevice.StopAcquisition ();        

    printf("[%05d]: close system", NOW );
    LvSystem::Close ( System );       

    printf("[%05d]: close library", NOW );
    LvLibrary::CloseLibrary ();         

    printf("[%05d]: waiting to close..\n", NOW);
    OsSleep ( 1000 );
    //getchar();

    printf("[%05d]: exiting\n", NOW);

    return ( 0 );
}
