#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


# -------------------------------------------------------
#       inference single image with quantized model
# -------------------------------------------------------

import sys
import os
import importlib
import argparse
import tensorflow as tf
import numpy as np
import cv2


from tensorflow_model_optimization.quantization.keras import vitis_quantize


import classifierModelBuilder as cmb
import pre_proc as pp
  
  
  
def loadModel(fileName, numClasses):
    model = tf.keras.models.load_model(fileName)


    if numClasses==2:
        loss      = "binary_crossentropy"
        metrics   =["accuracy"]    
    else:    
        loss      = tf.keras.losses.CategoricalCrossentropy()    
        metrics   = [tf.keras.metrics.CategoricalAccuracy()]

    model.compile(loss=loss, metrics= metrics)
    
    return model
    

  
def main(argv):

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )                              
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )    
    parser.add_argument('--imageFile', default = '', type = str, 
                        help='image file name\n' )
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )                        
    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import            
    cfg = importlib.import_module(cf)
    
    # --- load image file ---
    if not os.path.isfile(args.imageFile):
        print('ERROR: Image file not found')
        return
        
    image = cv2.imread(args.imageFile)

    image = cv2.resize(image,(cfg.IMAGE_WIDTH,cfg.IMAGE_HEIGHT), interpolation = cv2.INTER_LINEAR)
    image=cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    imgNorm=(image.astype('float32'))
    imgNorm=pp.preprocess(imgNorm, cmb.getInputScalingType(cfg))

    print('input image shape: ',imgNorm.shape)

    if imgNorm.shape[0]!=cfg.IMAGE_WIDTH:
        print('Width error !')
        return
        
    if imgNorm.shape[1]!=cfg.IMAGE_HEIGHT:
        print('Height error !')
        return
        
    if imgNorm.shape[2]!=3:
        print('Channel error !')
        return
    
    print('image file: ',args.imageFile)
    print('inputScalingType:', cmb.getInputScalingType(cfg))
    print('IMAGE_WIDTH:', cfg.IMAGE_WIDTH)
    print('IMAGE_HEIGHT:', cfg.IMAGE_HEIGHT)  

    # --- inference float model ---
    #print('reading float model ... ')
    modelFileNameFloat = os.path.join(args.workDir, 'modelFinal', cfg.MODEL_TRAINED_FILE_NAME)
    model = loadModel(modelFileNameFloat, cfg.NUMBER_OF_CLASSES)

    resFloat = model.predict(np.expand_dims(imgNorm, axis=0), verbose=0)
    
    
        
    
    # --- inference quant model ---
    #print('reading float model ... ')
    
    modelFileNameQuant = os.path.join(args.workDir, 'modelVai','model_quantized.h5')
    model = loadModel(modelFileNameQuant, cfg.NUMBER_OF_CLASSES)   

    resQuant = model.predict(np.expand_dims(imgNorm, axis=0), verbose=0)
    
    
    # --- output ---
    print()
    print()
    print("Float model output:")
    print(resFloat[0])
    print("max class index: ",np.argmax(resFloat[0]))
    
    print()
    
    print("Quantized model output:")
    print(resQuant[0])
    print("max class index: ",np.argmax(resQuant[0]))


if __name__=="__main__":
    main(sys.argv)