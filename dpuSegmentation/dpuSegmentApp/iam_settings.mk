# --------------------------------------

ip=192.168.1.10                # iam ip
user=root                      # iam user

elf_name=dpuSegmentApp         # application elf file name
xml_name=dpuSegmentApp.xml     # xml file name
folder=/home/root/appSpace     # application folder on iam
prog_folder=$folder/$elf_name  # folder for this application

strict_host_key_checking=0     # ssh fingerprint check 0=off 1=on
key=/root/.ssh/id_rsa_xcd      # ssh key on docker image

# --------------------------------------