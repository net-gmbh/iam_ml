#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.




# -----------------------------------------------------------
#           prepare dataset for training YoloV4
# -----------------------------------------------------------

import os
import shutil
import xml.etree.ElementTree as ET
import cv2
import random
import argparse
import sys
from pathlib import Path

    
# recursive directory create
def createDir(dirName):
    if not os.path.exists(dirName):  
        Path(dirName).mkdir( 0o777, parents=True, exist_ok=True) # recursive
        #Path(dirName).mkdir( parents=True, exist_ok=True) # recursive        
        if not os.path.exists(dirName):  
            print('ERROR: Could not create directory: ',dirName)
            return False
    return True

def removeFilesInDirectory(dirName):
    if os.path.exists(dirName):
        if (len(os.listdir(dirName))!=0):  # emty directory?
            for filename in os.listdir(dirName):
                file_path = os.path.join(dirName, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                        print('Failed to delete %s. Reason: %s' % (file_path, e))




def main(argv):
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-projectDir', default = 'default', type = str, 
                        help='Path to project directory\n' )      
                        
    parser.add_argument('-visualCheck', default = True, type = bool, 
                        help='Generate visual check images\n' )  
                        
    parser.add_argument('-workDir', default = 'default', type = str, 
                        help='Path to work directory\n' )    
                        
    parser.add_argument('-datasetDir', default = 'default', type = str, 
                        help='Path to dataset\n' )    
       
    args = parser.parse_args()
    
    currentDir = os.getcwd()
    sys.path.insert(1, os.path.join(currentDir,args.projectDir))    # for import    
    import config as cfg
        

    imgHeightMax  = cfg.TRAIN_IMAGE_MAX_HEIGHT
    imgWidthMax   = cfg.TRAIN_IMAGE_MAX_WIDTH
    origDirPath   = args.datasetDir
    destPath      = os.path.join(args.workDir,  "data")
    visualDirPath = os.path.join(args.workDir,  "visualCheckImages")
    listDirPath   = os.path.join(args.workDir,  "lists")
    
    labelListFileName =  os.path.join(listDirPath,"labelList.txt")
    
    
    # --- create directories ---
    createDir(args.workDir)
    
    if args.visualCheck:
        createDir(visualDirPath)
        removeFilesInDirectory(visualDirPath)

    createDir(destPath)
    removeFilesInDirectory(destPath)
    
    createDir(listDirPath)
    #removeFilesInDirectory(listDirPath)
    
    

    fileNameList = os.listdir(origDirPath)

    valid_extensions = ['jpg','jpeg', 'bmp', 'png']
    imgFileNameList = [fn for fn in fileNameList if any(fn.endswith(ext) for ext in valid_extensions)]

    valid_extensions = ['xml']
    xmlFileNameList = [fn for fn in fileNameList if any(fn.endswith(ext) for ext in valid_extensions)]

    numSamples = len(imgFileNameList)
    
    print("Dataset: ",origDirPath)
    print("Number of Samples: ",numSamples)

    root = ET.parse(os.path.join(origDirPath, xmlFileNameList[0])).getroot()

    labelList=[]
    num=numSamples
    
    for i in range(num):
    
        img = cv2.imread(os.path.join(origDirPath, imgFileNameList[i]),cv2.IMREAD_COLOR)
        h_img, w_img, c_img = img.shape
        
        # --- generate label file ---
        txtFile = open(os.path.join(destPath,xmlFileNameList[i][:-3])+"txt", 'w')
        root = ET.parse(os.path.join(origDirPath, xmlFileNameList[i])).getroot()
        filename = root.find('filename')
        print("#",i,": ",filename.text)
    
        for obj in root.findall('object'):
            #print(obj)
            objName = obj.find('name')
            #print(objName.text)
            bndbox  = obj.find('bndbox')
            xmin = bndbox.find('xmin')
            xmin_f=float(xmin.text)
            #print(xmin.text)
            ymin = bndbox.find('ymin')
            ymin_f=float(ymin.text)
            #print(ymin.text)
            xmax = bndbox.find('xmax')
            xmax_f=float(xmax.text)
            #print(xmax.text)
            ymax = bndbox.find('ymax')
            ymax_f=float(ymax.text)
            #print(ymax.text)
                                      
            # --- add label to list if it is new ---
            if not (objName.text in labelList):
                labelList.append(objName.text)
                
            # --- get index of the label ---
            objectId = labelList.index(objName.text)
            
            x_center = (xmin_f+xmax_f)/2.0
            x_center_rel = x_center/float(w_img)
            y_center = (ymin_f+ymax_f)/2.0
            y_center_rel = y_center/float(h_img)
            
            x_width = (xmax_f-xmin_f)
            x_width_rel = x_width/float(w_img)
            
            y_width = (ymax_f-ymin_f)
            y_width_rel = y_width/float(h_img)
            
            txtFile.write("%s %s %s %s %s\n" %(str(objectId), 
                    str(x_center_rel), str(y_center_rel), str(x_width_rel), str(y_width_rel)))
            
            col = (255,  255,   0)               # Y
            if objectId==0: col=(255,    0,   0) # R
            if objectId==1: col=(  0,  255,   0) # G
            if objectId==2: col=(  0,    0, 255) # B
            if objectId==3: col=(255,    0, 255) # M
            if objectId==4: col=(  0,  255, 255) # C
            
            cv2.line(img,(int(xmin_f),int(ymin_f)), (int(xmax_f),int(ymin_f)), col,3) 
            cv2.line(img,(int(xmin_f),int(ymax_f)), (int(xmax_f),int(ymax_f)), col,3)
            cv2.line(img,(int(xmin_f),int(ymin_f)), (int(xmin_f),int(ymax_f)), col,3) 
            cv2.line(img,(int(xmax_f),int(ymin_f)), (int(xmax_f),int(ymax_f)), col,3) 

            img = cv2.putText(img, objName.text, (int(xmin_f)+2, int(ymax_f)-3), 
                           cv2.FONT_HERSHEY_DUPLEX, 1.0, col, 1 ) # x,y
            
        txtFile.close()
        
        # --- convert image file ---
        if (h_img>imgHeightMax) or (w_img>imgWidthMax):
            # --- shring the image file ---
            fw=imgWidthMax/w_img
            fh=imgHeightMax/h_img
            fs=min(fw,fh)
            imgOrig = cv2.imread(os.path.join(origDirPath, imgFileNameList[i]),cv2.IMREAD_COLOR)
            imgResized = cv2.resize(imgOrig, (int(w_img*fs+0.5),int(h_img*fs+0.5)), interpolation= cv2.INTER_LINEAR)
            retval = cv2.imwrite(os.path.join(destPath, imgFileNameList[i][:-3]+'jpg'), imgResized)
        else:
            if (imgFileNameList[i][-3:]!='jpg' and imgFileNameList[i][-3:]!='JPG'):
                # --- recode image file ---
                imgOrig = cv2.imread(os.path.join(origDirPath, imgFileNameList[i]),cv2.IMREAD_COLOR)
                retval = cv2.imwrite(os.path.join(destPath, imgFileNameList[i][:-3]+'jpg'), imgOrig)        
            else:
                # --- just copy file ---
                shutil.copy2(os.path.join(origDirPath, imgFileNameList[i]), destPath)
        
        # --- write visual check file ---
        retval = cv2.imwrite(os.path.join(visualDirPath, imgFileNameList[i]), img)
        
    # --- write label list file ---
    file = open(labelListFileName, 'w')
    for i in range(len(labelList)):
        file.write("%s\n" %labelList[i])
    file.close()
        
        
    print("Label list:")
    print(labelList)




if __name__=="__main__":
    main(sys.argv)




