/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#include "gigeServerEvents.h"

#include "main.h"
#include "camDevice.h"
#include "bufferManager.h"

extern camDeviceClass camDevice;
extern bufferManagerClass bufferManager;


// --- GigE Server Event Processing ---
#define GEVSrvEv_Exit                   0x0000
#define GEVSrvEv_Connect                0x0001
#define GEVSrvEv_Disconnect             0x0002
#define GEVSrvEv_StreamStart            0x0003
#define GEVSrvEv_StreamStop             0x0004
#define GEVSrvEv_SmartXMLEvent          0x0010

#define GEVSrvEv_ReadFlag               0x0000
#define GEVSrvEv_WriteFlag              0x0001

#define GEVSrvEv_ImgFmt_DeviceWidth     0x0005
#define GEVSrvEv_ImgFmt_DeviceHeight    0x0006
#define GEVSrvEv_ImgFmt_PixFmt          0x0007

// --- XML Register Adresses ---
#define AdrSmartWidth             0x0100
#define AdrSmartHeight            0x0104
#define AdrSmartPixFmt            0x0200
#define AdrSmartPayloadSize       0x0300

#define AdrOutputImageMode        0x0400
#define AdrSensorDecimation       0x0404
#define AdrExposureTime           0x0408
#define AdrAutoExposureMode       0x040C
#define AdrAwb                    0x0418
#define AdrEnableClassifier       0x0420
#define AdrEnableOsd              0x042C
#define AdrEnablePrintfDpuOut     0x0430
#define AdrEnablePrintfPropOut    0x0434
#define AdrEnablePrintfTime       0x0438
#define AdrModelDirString         0x043C
#define AdrLoadModelCommand       0x0440
#define AdrLoadModelIndex         0x0444

#define AdrEnableTemperatureOsd   0x0448

#define AdrSensorWidth            0x0510
#define AdrSensorHeight           0x0514

#define AdrEnableTestPic          0x0530
#define AdrTestPicIdx             0x0534
#define AdrTestPicNumber          0x0538

#define AdrSaveFrontendSettings   0x0540

#define AdrEnableGevStreaming     0x0550

#define AdrObjConf                0x0600
#define AdrNms                    0x0604
#define AdrClassConf              0x0608



 

void  putCharPtr   ( U32BIT *Param , char *p )   { Param[2] = (U32BIT)((U64BIT)p)&0xffffffff; Param[3] = (U32BIT)(((U64BIT)p)>>32); }
char* getCharPtr   ( U32BIT *Param )             { return (char  *)( Param[2] + (((U64BIT)Param[3])<<32) ); }


// --------------------------------------------
//    GigE Server Event Callback Processing
// --------------------------------------------
U32BIT gigeEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam )
{
    int error=0;
    int Write = int(Param[0]);
    
    // --------------------------------------
    //    GigE Server Event: Connect
    // --------------------------------------
    if ( Event == GEVSrvEv_Connect )
    {
        PRINTF(("GigE Server Event: Connect"));
        if ( bIsConnected ) {
            PRINTF(("ERROR: Already connected!"));
        }
        bIsConnected = true;
        enableStreaming=false;
    }

    // --------------------------------------
    //    GigE Server Event: Disconnect
    // --------------------------------------
    else if ( Event == GEVSrvEv_Disconnect )
    {
        printf(("GigE Server Event: Start Disconnect ...\n"));
        if ( !bIsConnected ) {
            printf(("ERROR: Not connected!\n"));
        }

        if (!autorun) {
            if ( camDevice.IsAcquiring () ) {
                printf(("Stopping acquisition\n"));
                camDevice.StopAcquisition ();
                camDevice.StopStream ();
            }
        }

        bIsConnected = false;
        printf(("Disconnect done\n"));
    }

    // --------------------------------------
    //    GigE Server Event: Exit
    // --------------------------------------
    else if ( Event == GEVSrvEv_Exit ) {
        PRINTF(("GigE Server Event: Exit..."));
        int         *data  = (int        *) &Param[2];

        if ( *data == 0 ) {     // just exit
            exitAppFlag = true;
            bIsConnected = false;
        }
    }

    // -----------------------------------------------------------------
    //    GigE Server Event: Stream Start (=GigE acquisition start)
    // -----------------------------------------------------------------
    else if ( Event == GEVSrvEv_StreamStart )
    {
        int *data = (int *) &Param[2];

        if ( Write )
        {
             if (!autorun) {
                PRINTF(("GigE Server Event: Start Stream Start..."));
                if ( camDevice.IsAcquiring () ) {                    
                    camDevice.StopAcquisition ();                    
                    camDevice.StopStream();
                }

                error = camDevice.StartStream ();
                if ( error ) {
                    PRINTF(("ERROR: StartStream error"));
                }

                printf(COLOR_YEL "bufferManager: init()\n" COLOR_RESET);
                bufferManager.initBuffer1(NUMBER_OF_AQUISITION_BUFFERS);
                bufferManager.initBuffer2(NUMBER_OF_SEND_BUFFERS, NUMBER_OF_PROCESSING_THREADS);

                //send_thread_is_running=true;
                //sendThread=thread(sendFrame, ref(send_thread_is_running));
                //sendThread.join();

                // start frontend acquisition                
                error = camDevice.StartAcquisition ();
                if ( error ) {
                    PRINTF(("ERROR: StopAcquisition error"));
                }

                PRINTF(("Start stream done"));
             }
             enableStreaming=true;
        } else {
            if (autorun) {
                *data = 0; // return command is done immediately 
            } else {
                *data = camDevice.IsAcquiring () ? 0 : 1;     // reading the register returns the command value (1) until the acquisition is started (0) 
            }
            PRINTF(("GigE Server Event: GEVSrvEv_StreamStart read value:%d", *data ));
        }
    }

    // ----------------------------------------------------------------
    //    GigE Server Event: Stream Stop (=GigE acquisition stop)
    // ----------------------------------------------------------------
    else if ( Event == GEVSrvEv_StreamStop )
    {
        int *data = (int *) &Param[2];

        if ( Write )
        {
            if (!autorun) {
                PRINTF(("GigE Server Event: Start Stream Stop..."));
                error = camDevice.StopAcquisition ();
                if ( error) {
                    PRINTF(("ERROR: StopAcquisition error"));
                }

                //PRINTF(("Closing buffers"));
                camDevice.StopStream ();        

                //send_thread_is_running=false;        
            }
            enableStreaming=false;
        } else {
            if (autorun) {
                *data = 0;   // return command is done immediately 
            } else {
                *data = camDevice.IsAcquiring () ? 1 : 0;    // reading the register returns the command value (1) until the acquisition is stopped (0)
            }
            //PRINTF(("GigE Server Event: GEVSrvEv_StreamStop read value:%d", *data ));
        }
    }

    
    // --------------------------------------------------
    //    GigE Server Event: XML Parameter Read/Write
    // --------------------------------------------------
    else if (Event == GEVSrvEv_SmartXMLEvent )
    {
        unsigned int adr   = (unsigned int)  Param[1];
        int         *data  = (int        *) &Param[2];
        float       *fdata = (float*)       &Param[2];

        if ( adr == AdrSmartWidth ) {  // width of GigE-Server output image
            if ( Write ) {
                // read only in XML
            }
            else {
                *data=imgWidthOut;
                PRINTF (( "XmlEvent: Read(AdrSmartWidth) -->: %d", *data ));                
            }
        }

        if ( adr == AdrSmartHeight ) // height of GigE-Server output image
        {
            if ( Write ) {
                // read only in XML
            }
            else {                
                *data=imgHeightOut;
                PRINTF (( "XmlEvent: Read(AdrSmartHeight) --> %d", *data ));
            }
        }

        if ( adr == AdrSmartPixFmt ) // pixel format of GigE-Server output image
        {
            if ( Write ) {                            
                // read only in XML       
            }
            else {                            
                *data=outputPixelFormat; 
                PRINTF (( "XmlEvent: Read(AdrSmartPixFmt) --> 0x%8.8x", *data ));
            }
        }

        if ( adr == AdrSmartPayloadSize ) {
            if ( Write ) {
                // read only in XML                
            }
            else {
                
                int SmartPayloadSize = imgWidthOut * imgHeightOut;
                if (outputPixelFormat==LvPixelFormat_RGB8) SmartPayloadSize *=3;                
                // XML invalitated from width, height and pixel format
                *data = SmartPayloadSize;
                PRINTF (( "XmlEvent: Read(AdrSmartPayloadSize) --> PayloadSize: %d Byte", *data ));
            }
        }

        if ( adr == AdrOutputImageMode ) {
            if ( Write ) {
                outputImageMode = *data;
                PRINTF (( "XmlEvent: Write(AdrOutputFormat) --> %d", *data ));
                // 0=RGB 1=Bayer
                if (outputImageMode==0 )outputPixelFormat=LvPixelFormat_RGB8;
                if (outputImageMode==1 )outputPixelFormat=LvPixelFormat_BayerGR8;
                // --> xml invalidates pixel format
            }
            else {
                *data=outputImageMode;
                PRINTF (( "XmlEvent: Read(AdrOutputFormat) -->: %d", *data ));
            }
        }

        if ( adr == AdrSensorDecimation ) {
            if ( Write ) {
                PRINTF (( "XmlEvent: Write(AdrSensorDecimation) --> %d", *data ));
                // xml enum: 1=div1, 2=div2, 4=div4
                sensorDecimation=*data;
                app.calculateOutputImgSize();   
                // --> xml invalidates width and height     
            }
            else {
                *data=sensorDecimation;
                PRINTF (( "XmlEvent: Read(AdrSensorDecimation) -->: %d", *data ));
            }
        }

        if ( adr == AdrExposureTime )  {
            if ( Write ) {
                exposureTime = *fdata;
                camDevice.SetFloatCameraParam(LvDevice_ExposureTime,  exposureTime);
                float extime;
                camDevice.GetFloatCameraParam(LvDevice_ExposureTime, &extime);
                exposureTime=extime;
                PRINTF (( "XmlEvent: Write(AdrExposureTime) --> %f (%f)", *fdata,extime ));                
            }
            else {
                float extime=exposureTime;
                *fdata = extime;
                PRINTF (( "XmlEvent: Read(AdrExposureTime) -->: %f", extime ));
            }
        }        

        if ( adr == AdrAutoExposureMode ) {
            if ( Write ) {
                LvStatus svStatus;
                autoExposureMode = *data;
                PRINTF (( "XmlEvent: Write(AdrAutoExposureMode) --> %d", *data ));   
                if (autoExposureMode==0) {  //Off
                    svStatus = camDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(svStatus);  
                }           
                if (autoExposureMode==1) {  // Fronten AE
                    svStatus = camDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Continuous);
                    ErrorOccurred(svStatus);  
                }    
                if (autoExposureMode==2) { // App AE
                    svStatus = camDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(svStatus);  
                }      
                // get current exposure value from frontend
                float extime;
                svStatus = camDevice.GetFloatCameraParam(LvDevice_ExposureTime, &extime);
                exposureTime = extime;
                ErrorOccurred(svStatus);     
            }
            else {
                *data = autoExposureMode;
                PRINTF (( "XmlEvent: Read(AdrAutoExposureMode) -->: %d", *data ));
            }
        }

        if ( adr == AdrAwb ) {  //WhiteBalanceAuto
            if ( Write ) {
                if (*data==1) {                    
                    LvFeature ftr;   
                    LvStatus svStatus;                 
                    svStatus = pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "WhiteBalanceAuto", &ftr);
                    ErrorOccurred(svStatus);  
                    svStatus =pDevice->SetEnumStr(ftr,"Once");
                    ErrorOccurred(svStatus);  
                    //camDevice.m_pDevice->SetEnumStr(ftr,"Off");
                    //ErrorOccurred(svStatus);  
                }               
            }
            else {
                *data=0; // done
            }
        }
        
        if ( adr == AdrEnableClassifier ) {
            if ( Write ) {
                enableClassifierFlag = *data;
                PRINTF (( "XmlEvent: Write(AdrEnableClassifier) --> %d", *data ));                  
            }
            else {
                *data=enableClassifierFlag;
                PRINTF (( "XmlEvent: Read(AdrEnableClassifier) -->: %d", *data ));
            }
        }

        if ( adr == AdrEnableOsd ) {
            if ( Write ) {
                enableOsdFlag = *data;                                
            }
            else {
                *data=enableOsdFlag;                
            }
        }

        if ( adr == AdrEnableTestPic ) {
            if ( Write ) {
                enableTestPicFlag = *data;                                
            }
            else {
                *data=enableTestPicFlag;                
            }
        }
        
        if ( adr == AdrTestPicIdx ) {
            if ( Write ) {
                testPicIdx= *data;
                if (testPicIdx>0) {
                    loadTestImage = *data;   
                    if (loadTestImage<1) loadTestImage=1;
                    mtx_testImageFileNames.lock();
                    if (loadTestImage>(int)testImageFileNames.size()) {
                        loadTestImage = (int)testImageFileNames.size();
                    }
                    mtx_testImageFileNames.unlock();
                    loadTestImageFlag=1;  
                }                           
            }
            else {
                if (testPicIdx<0) *data=0; 
                else *data=testPicIdx;            
            }
        }
                        
        if ( adr == AdrEnablePrintfDpuOut ) {
            if ( Write ) {
                enablePrintfDpuFlag = *data;                                
            }
            else {
                *data=enablePrintfDpuFlag;                
            }
        }

        if ( adr == AdrEnablePrintfPropOut ) {
            if ( Write ) {
                enablePrintfBufferManager = *data;                                
            }
            else {
                *data=enablePrintfBufferManager;                
            }
        }

        if ( adr == AdrEnableTemperatureOsd ) {
            if ( Write ) {
                enableTemperatureOsd = *data;                                
            }
            else {
                *data=enableTemperatureOsd;                
            }
        }  
        

        if ( adr == AdrEnablePrintfTime ) {
            if ( Write ) {
                enablePrintfBufferFps = *data;                                
            }
            else {
                *data=enablePrintfBufferFps;                
            }
        }  

        if ( adr == AdrSensorWidth ) // width of GigE-Server output image
        {
            if ( Write ) {                
                // read only
            }
            else {
                *data=sensorWidth;
                PRINTF (( "XmlEvent: Read(AdrSensorWidth) -->: %d", *data ));                
            }
        }

        if ( adr == AdrSensorHeight ) // width of GigE-Server output image
        {
            if ( Write ) {
                // read only 
            }
            else {
                *data=sensorHeight;
                PRINTF (( "XmlEvent: Read(AdrSensorHeight) -->: %d", *data ));                
            }
        }

        if ( adr == AdrModelDirString )
        {
            if ( Write ) {
                // read only                
            }
            else {                
                putCharPtr ( Param, (char*) modelDirString.c_str());
            }
        }

        if ( adr == AdrLoadModelCommand ) {  //load model command
            if ( Write ) {
                if (*data==1) {                    
                    if ((currentModelIdx>=0)&&(currentModelIdx<(int)modelDirectoryNames.size())) {             
                        doModelLoadFlag = true;
                    }
                }               
            }
            else {
                *data=doModelLoadFlag; // done
            }
        }

        
        if ( adr == AdrLoadModelIndex ) {  //load model via list index
            if ( Write ) {
                if (*data!=currentModelIdx) {       
                    currentModelIdx=*data;  
                }               
            }
            else {
                if (currentModelIdx<0) *data=0;
                else *data=currentModelIdx;
            }
        }

        if ( adr == AdrSaveFrontendSettings ) {
            if ( Write ) {
                if (*data==1) {
                    printf ( "XmlEvent:Save frontend settings\n");                      
                    saveFrontendSettingsRequestFlag=1;                    
                }               
            }
            else {
                *data=saveFrontendSettingsRequestFlag;
            }
        }


        
        if ( adr == AdrEnableGevStreaming ) {
            if ( Write ) {
                enableGevStreaming = *data;                                
            }
            else {
                *data=enableGevStreaming;                
            }
        }         

        if ( adr == AdrObjConf )  {
            if ( Write ) {
                confObjThres = *fdata;
                               
            }
            else {
                float val=confObjThres;
                *fdata = val;                
            }
        }        

        if ( adr == AdrNms )  {
            if ( Write ) {
                nmsThres = *fdata;
                               
            }
            else {
                float val=nmsThres;
                *fdata = val;                
            }
        }        

        if ( adr == AdrClassConf )  {
            if ( Write ) {
                confClassThres = *fdata;
                               
            }
            else {
                float val=confClassThres;
                *fdata = val;                
            }
        }       

    }

    return 0;
}