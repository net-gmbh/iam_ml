# dpuClassify #
---

This application executes several Tensorflow2/Keras image classification models
on the **iam ML ready** camera. These models can easily be retrained for custom image datasets via transfer learning.

 
![classifier](/dpuClassify/misc/images/classifier.png)
DPU interference time for different models on iam Zu2 and Zu5 cameras:

 Model        | resolution    |  DPU MACs  |   Zu2      |   Zu5
 -------------|---------------|------------|------------|---------
 VGG16        | 224x224       | 30.69 GOPS |  148.7 ms  |  29.0 ms
 DenseNet121  | 224x224       |  5.69 GOPS |   37.3 ms  |  11.6 ms
 ResNet50V2   | 224x224       |  6.97 GOPS |   43.2 ms  |  13.2 ms
 InceptionV3  | 299x299       | 11.43 GOPS |   68.4 ms  |  17.9 ms

## Model Trainig ##
Training scripts for VGG16, DenseNet121, ResNet50V2 and InceptionV3 models are available in folder `/training`.  
Models are generated with the Keras API with pretrained weights and transfer training is done with Tensorflow 2.  

## iam Application ##
The folder `/dpuClassifyAppMt` contains the application source files, the make file and build scripts for compiling the application on the iam or using the xcompile docker image.

The complete application package with models and executable included, can be downloaded from [dpuClassifyAppMt.tar.gz](https://bitbucket.org/dcarpentier123/iam_ml_3xx/downloads/dpuClassifyAppMt)   
Copy the .tar.gz file to `/home/root/dpuClassifyAppMt/` and extract files with...   
 `tar xz -f dpuClassifyAppMt.tar.gz`   

Start application with...   
`./dpuClassifyAppMt`

The application provides GigE output streaming, so the camera can be connected from the host with Synview explorer. 
The application generates an overlay with the classification result.
Several XML features are available for application control, e.g. test images and debug output modes.
Models models can be loaded dynamically during stopped acquisition .


![app](/dpuClassify/misc/images/screenShotApp.png)

The folder /dpuClassifyApp contains a simplified version of the application without multithreading.
