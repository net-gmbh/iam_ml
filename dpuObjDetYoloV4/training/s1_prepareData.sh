#!/bin/bash


source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"	
echo "Dataset dir: $DATASET_DIR"	
	
python3 scripts/prepareData.py -projectDir $PROJECT_CONFIG -workDir $WORK_DIR -datasetDir $DATASET_DIR