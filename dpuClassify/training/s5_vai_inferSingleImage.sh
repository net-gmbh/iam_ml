#!/bin/bash

source config.sh


python pycode/vai_inferenceSingleImage.py \
              --configPath $(pwd)/projectConfig \
			  --configFile $PROJECT_CONFIG \
			  --workDir $WORK_DIR_IN_DOCKER \
			  --imageFile misc/paprika_green.jpg
