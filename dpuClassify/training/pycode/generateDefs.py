#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


# ----------------------------------
#    Generate sh defines from  
#       python config file 
# ----------------------------------


import os
import importlib
import argparse
import sys


# --- shell script file ---
def writeDefFile(cfg, fileName, vaiDir):
    
    
    txtFile = open(fileName, 'w',newline='')
    txtFile.write("#!/bin/bash\n")
    
    txtFile.write("export VAI_DIR=\"%s\"\n" %str(vaiDir))
    
    
    txtFile.write("export VAI_TARGET_DEVICE=\"%s\"\n" %str(cfg.VAI_TARGET_DEVICE))
    txtFile.write("export VAI_FINAL_MODEL_FILE_NAME=\"%s\"\n" %str(cfg.VAI_FINAL_MODEL_FILE_NAME))
    
    
    txtFile.write("export MODEL_TYPE=\"%s\"\n" %str(cfg.MODEL_TYPE))
    txtFile.write("export IMAGE_WIDTH=%s\n" %str(cfg.IMAGE_WIDTH))
    txtFile.write("export IMAGE_HEIGHT=%s\n" %str(cfg.IMAGE_HEIGHT))
    txtFile.write("export NUMBER_OF_CLASSES=%s\n" %str(cfg.NUMBER_OF_CLASSES))
    
    
   
    txtFile.close()   
    
    return





def main(argv):

    parser = argparse.ArgumentParser()
        
    parser.add_argument('--configPath', default = 'default', type = str, 
                        help='configFile path\n' )      
                        
    parser.add_argument('--configFile', default = 'default', type = str, 
                        help='.py configFile name\n' )   
                        
    #parser.add_argument('--outputPath', default = '/', type = str, 
    #                    help='output file path\n' )   

    #parser.add_argument('--outputFile', default = 'config.sh', type = str, 
    #                    help='output file name\n' )    
                        
    parser.add_argument('--workDir', default = 'default', type = str, help='Path to work directory\n' )

    args = parser.parse_args()
        
    cp = args.configPath
    print('config path: ',cp)
    
    cf = args.configFile
    print('config file: ',cf)
       
    sys.path.insert(1, cp) # for import    
        
    cfg = importlib.import_module(cf)
    
    
    
    vaiDir = os.path.join(args.workDir, 'modelVai')

    writeDefFile(cfg, os.path.join(args.workDir,'modelVai','vai_config.sh'), vaiDir)



if __name__=="__main__":
    main(sys.argv)
    
    
