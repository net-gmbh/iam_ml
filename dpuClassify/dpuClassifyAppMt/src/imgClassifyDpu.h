/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef IMG_CLASSIFY_H
#define IMG_CLASSIFY_H

//#include "main.h"

#include "common.h"



class imgClassifyClass 
{   

public:
	imgClassifyClass();
    ~imgClassifyClass();

	int configDpu();
	int procImg(cv::Mat img);	
	int cleanup();	

	//int topKSearch(int k, std::vector<int> &idxVector, std::vector<float> &propVector);
	int topKSearch(int k);
	void calcSoftmax();
	void calcSigmoid();
	
	void TopK(const float* d, int size, int k);

	double tickCountStart;
	double tickCountStop;

	float* activationsOutput;

	float* dpuResultBuffer;	
	float* dpuInputBuffer;

	int dpuResultBufferBytesAllocated;
	int dpuInputBufferBytesAllocated;

	int netOutDim; 
	//int dpuResSize;	

	int inSize;
	int inWidth;
	int inHeight;
	
	//int inputValueScalingMode;
	int inputMode;

	std::vector<int> topIdxVector;
    std::vector<float> topPropVector;

private:
	// --- VART interface ---
    // types --> see: /usr/incluse/xir	
	
	//std::unique_ptr<xir::Graph> graph;	
	//std::vector<const xir::Subgraph*> subgraph;	
	std::unique_ptr<vart::Runner> runner;	

	//std::vector<const xir::Tensor*> inputTensors;	
	//std::vector<const xir::Tensor*> outputTensors;	
	std::vector<std::unique_ptr<xir::Tensor> > inputTensors;	
	std::vector<std::unique_ptr<xir::Tensor> > outputTensors;
	std::vector<std::int32_t> out_dims;
	std::vector<std::int32_t> in_dims;
	std::vector<std::unique_ptr<vart::TensorBuffer>> inputs, outputs;
	std::vector<vart::TensorBuffer*> inputsPtr, outputsPtr;
  	std::vector<std::shared_ptr<xir::Tensor>> batchTensors;

	int configOkFlag;
	//cv::Mat image2;
		
	
	
	
};

#endif // IMG_CLASSIFY_H
