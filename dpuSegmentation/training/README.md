
## Workflow ##

![flow](/dpuSegmentation/misc/images/flow.png)

For iam LNX version 3.xx the Vitis Ai version 1.4.1 has to be used. This toolset uses Tensorflow 2.3. In order to prevent any model file compatibility issues, Tensorflow 2.3 should also be used for training the model. GPU support via Cuda is highly recommended for training. The Dockerfile for building the Docker ontainer for training is provided. On a Windows system, Docker-Desktop with WSL-Backend and the Cuda-Driver for Docker/WSL should be installed.  
The the Vitis Ai 1.4 toolset for model quantization and compilation also provides a Docker Container for execution.



## Dataset ##
>The dataset for this training example can be downloaded from [NetLogoDataset.zip](https://nc.net-gmbh.com/s/wKwcyWf2EsEpyNw).
The dataset contains 646 images with NET logo patters, seperated in train and eval folders, with given segmentation masks. 
The zip folder must be extracted to any location, available in the training environment. The file netLogo.pdf contains a printable version of this pattern.



## Training ##
Bash scripts for training with the training docker container are provided. For training on Windows systems batch files for using Docker-Desktop are provided. Alternatively a Jupyter-Notebook for interactive training is available.

### Preparation ###
**1. Build Training Container**
Execute...   
`win_docker_build_image.bat`    
   
**2. Edit configWin.bat**   
Set `PROJECT_CONFIG` to python project config file name in subdirectory /projectConfig.   
Set `DATASET_DIR_MAPPING` to dataset directory location, which will be mapped to docker images as `dataset`.   

Set `WORK_DIR_MAPPING` to working directory location, which will be mapped to docker images as `work`.
    
**3. Edit config.sh**   
Set `PROJECT_CONFIG` to python project config file name in directory /projectConfig.  



**4. Download pretrained weights**
FCN models are initialized with VGG16 weights, pretrained on imagenet. 
Therfore, when using FCN models, the file [vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5](https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5) 
has to be copied to the subdirectory /training/pycode.    


### Step 1: Prepare Data ###
Start data preprocessing with...   
`win_s1_prepareData.bat`   
This script converts mask data and copies training data to `WORK_DIR/data`

### Step 2: Training ###
Start training with...   
`win_s2_train.bat`   
Weight files are written to `WORK_DIR/modelCkpts`.   
During training the the chart files in directory `WORK_DIR/diagramFull` and `WORK_DIR/diagramZoom` are periodically updated, in order to observe the training progress. 
Training can be stopped with Cntr+C.   
Training can be restarted by executing...    
`win_s2a_train_continue.bat`   
Restarted training starts with the last weights in `WORK_DIR/modelCkpts`.

### Step 3: Finalize Training ###
When the model training is done, the last model checkpoint is fozen with...   
`win_s3_train_finalize.bat`   
This script generates the final model file `WORK_DIR/modelFinal/model_trained.h5`

### Step 4: Verify Training Performance ###
Optionally executing the script...   
`win_s4_train_verify.bat`    
analyses the trained model performance on the train and eval dataset.

### Step 5: Model Quantization ###
The following steps (5,6,7) have to be executed in the Vitis-AI docker image. 
1. Start the Vitis-AI docker with...   
`win_vitisAi_start.bat`   
2. Activate the conda environment for Tensorflow 2...   
`vitis-ai-tensorflow2`    
3. Execute VitisAi model quantization...   
`s5_vai_quant.sh`   
This generates the quantized model file `WORK_DIR/vaiModels/model_quantized.h5`.



### Step 6: Verify Quantized Model Performance ###
Optionally executing the script...   
`s6_vai_quant_verify.sh`    
which calculates the quantized model performance on the train and eval dataset.

### Step 7: Compiling the Model ###
Finally executing the script...   
`s7_vai_xcompile.sh`   
which compiles the model with the VitisAi tool `vai_c_tensorflow2` for the target iam device. The compiled model xmodel file is `WORK_DIR/modelVai/FINAL_FILE_NAME.xmodel`. This file has to be copied to the iam camera.


### Running the iam Application ###
The folder  [dpuSegmentAppMt](./../dpuSegmentAppMt) contains the application source files and the scripts for building the application and transfering it to **iam ML**. 



After starting the application the **iam Ml** camera can be connected from host with Synview Explorer. 

New trained models and the corresponding class list files have to be places in subdirectories of the model directory.  

Several XML features are available for camera control and debug. Other models from other /model subfolders can be loaded dynamically when acquisition is stopped. 

The XML section 'TestImages' loads test images which are located in the folder /imgTest.

![app](/dpuSegmentation/misc/images/screenShotSegApp.png)

The folder `/dpuSegmentApp` contains a simplified version of the application without multithreading.
