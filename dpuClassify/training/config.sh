#!/bin/bash

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


INDEX=1

# --- config file name in /projectConfig directory ---
if [[ $INDEX -eq 1 ]] 
then
    PROJECT_CONFIG=config_Vgg16
elif [[ $INDEX -eq 2 ]]
then
    PROJECT_CONFIG=config_Resnet50V2
elif [[ $INDEX -eq 3 ]]
then
    PROJECT_CONFIG=config_Dense121
elif [[ $INDEX -eq 4 ]]
then
    PROJECT_CONFIG=config_IncV3
fi


# --- docker directory mapping ---
WORK_DIR_IN_DOCKER=/work
DATASET_DIR_IN_DOCKER=/dataset

