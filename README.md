# **iam ML ready** Reference Applications #
### for LNX Version 3xx ###
---

This repository contains reference applications for **iam ML ready** cameras.   

**iam ML ready** cameras have the "Xilinx Zync DPU 3.3" block implemented inside the Zyncs FPGA section and the VitisAi libraries installed.   

The "Deep Learning Processing Unit" (DPU) accelerates "Convolutional Neuronal Network" (CNN) calculation on the camera.

Two versions of **"iam ML ready"** cameras with differnt Zync (zu2, zu5) and DPU versions are available. Both versions provide different processing performance and support different network complexity, see [Xilinx Zynq DPU v3.3](https://www.xilinx.com/support/documentation/ip_documentation/dpu/v3_3/pg338-dpu.pdf) for DPU details.  

 Type  | DPU Version  | PeakOps/Clock
 ------|--------------|---------------
 zu2   | B1152        |  1150
 zu5   | B4096        |  4096 




## Workflow ##
![flow](/misc/images/train_vitis_iam.png)





## CNN Reference Application ##


### [dpuClassify](/dpuClassify) ###

* Reference  application for image classification 
* VGG, DenseNet, ResNet and Inception models are available 
* Training code for transfere learning with Keras and Tensorflow
* Vitis Ai tool flow
* iam application
* Download application package: [dpuClassifyAppMt.tar.gz](https://bitbucket.org/dcarpentier123/iam_ml_3xx/downloads/dpuClassifyAppMt.tar.gz)

### [dpuSegmentation](/dpuSegmentation) ###

* Reference application for image segmentation.
* UNET and FCN models are available
* Training code using Keras and Tensorflow
* Vitis Ai tool flow
* iam application
* Download application package: [dpuSegmentAppMt.tar.gz](https://bitbucket.org/dcarpentier123/iam_ml_3xx/downloads/dpuSegmentAppMt.tar.gz)

### [dpuObjDetYoloV4](/dpuObjDetYoloV4) ###

* Reference application for object detection with YoloV4 model. 
* Training code using darknet 
* Vitis Ai tool flow
* iam application for Yolo object detection.
* Download application package: [dpuObjDetYoloV4AppMt.tar.gz](https://bitbucket.org/dcarpentier123/iam_ml_3xx/downloads/dpuObjDetYoloV4AppMt.tar.gz)


### Training ###
>Training code and Docker files are provided for the classifier, segmentation and object detection model training. 
Training for the classifier and segmentation models is done with Tensorflow 2. The Yolo object detection model training is done with darknet. 
For training on Windows system with GPU support, Docker-Desktop with WSL2 backend and CUDA for WSL/Docker is recommended.   


### Vitis Ai ###
>Trained models have to be quantized and compiled for DPU based interference on the iam camera.  
The iam LNX 3xx was build with Vitis 2021.1. Therefore VitisAi v1.4.1 has to be used. For running VitisAi a Docker Container is available   
See [Vitis AI User Guide](https://docs.xilinx.com/r/1.4-English/ug1414-vitis-ai/Revision-History)
for details.   




