
rem ---------------------------------------------
rem --- run bash in yolov4 training container ---
rem ---------------------------------------------

call configWin.bat


docker run^
  --gpus all^
  -it^
  --rm^
  -v %WORK_DIR_MAPPING%:/work^
  -v %DATASET_DIR_MAPPING%:/datasets^
  -v %cd%:/workspace^
  -w /workspace^
  vai_141_yolo_devel^
  bash


rem pause