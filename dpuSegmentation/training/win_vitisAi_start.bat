


call configWin.bat
echo "VAI_IMAGE_NAME: " %VAI_IMAGE_NAME%

docker run^
   --gpus all^
   -e VERSION=latest^
   -v %WORK_DIR_MAPPING%:/work^
   -v %DATASET_DIR_MAPPING%:/dataset^
   -v %cd%:/workspace^
   -w /workspace^
   --rm^
   --network=host^
   -it^
   %VAI_IMAGE_NAME%^
   bash
   
 
   
pause

