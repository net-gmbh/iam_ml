
rem -------------------------------------------
rem ---        open bash on iam             ---
rem -------------------------------------------


docker run^
  -it^
  --rm^
  -v %cd%:/workspace^
  -w /workspace^
  iam_xcomp2021^
  bash -c "./scripts/sh_bash_on_iam.sh"


rem pause