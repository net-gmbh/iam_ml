# Classifier Model Training #
---



## Workflow ##

![flow](/dpuClassify/misc/images/flow.png)

For iam LNX version 3.xx the Vitis Ai version 1.4.1 has to be used. This toolset uses Tensorflow 2.3. In order to prevent any model file compatibility issues, Tensorflow 2.3 should also be used for training the model. GPU support via Cuda is highly recommended for training. A Dockerfile for building the Docker image for training is provided. When using a Windows system, Docker-Desktop with WSL-Backend and the Cuda-Driver for Docker/WSL should be installed.  
The Vitis Ai 1.4 toolset for model quantization and compilation also provides a Docker Image.


## Dataset ##
An example dataset can be downloaded from [fruit26Dataset.zip](https://nc.net-gmbh.com/s/TK544dmoPWLANXC).
The dataset contains about 3000 images of 25 different types of fruits and vegitables 
plus one additional class of image without any objects.
The zip folder must be extracted to any location, available in the training environment. Images for different classes are placed in subfolders. For simplicity the subfolder names directly define the class names.



## Training ##
Bash scripts for training with the training docker container are provided. For training on Windows systems batch files for using Docker-Desktop are provided. Alternatively a Jupyter-Notebook for interactive training is available.   

### Preparation ###
** 1. Build Training Container **
Execute...   
`win_docker_build_image.bat`    
   
** 2. Edit configWin.bat **   
Set `PROJECT_CONFIG` to python project config file name in subdirectory /projectConfig.   
Set `DATASET_DIR_MAPPING` to dataset directory location, which will be mapped to docker images as `dataset`.   

Set `WORK_DIR_MAPPING` to working directory location, which will be mapped to docker images as `work`.
    
**3. Edit config.sh **   
Set `PROJECT_CONFIG` to python project config file name in directory /projectConfig.  
    

### Step 1: Training ###
Start training with...   
`win_s1_train.bat`   
Weight files are written to `WORK_DIR/modelCkpts`.   
During training the the chart files in directory `WORK_DIR/diagramFull` and `WORK_DIR/diagramZoom` are periodically updated, in order to observe the training progress. 
Training can be stopped with Cntr+C.   
Training can be restarted by executing...    
`win_s1a_train_continue.bat`   
Restarted training starts with the last weights in `WORK_DIR/modelCkpts`.

### Step 2: Finalize Training ###
When the model training is done, the last model checkpoint is fozen with...   
`win_s2_train_finalize.bat`   
This script generates the final model file `WORK_DIR/modelFinal/model_trained.h5`

### Step 3: Verify Training Performance ###
Optionally executing the script...
`win_s3_train_verify.bat` 
analyses the trained model performance on the train and eval dataset.

### Step 4: Model Quantization ###
The following steps (4,5,6,7) have to be executed on the Vitis-AI docker image. 
1. Start the Vitis-AI docker with...   
`win_vitisAi_start.bat`   
2. Activate the conda environment for Tensorflow 2...   
`vitis-ai-tensorflow2`    
3. Execute VitisAi model quantization...   
`s4_vai_quant.sh`   
This generates the quantized model file `WORK_DIR/vaiModels/model_quantized.h5`.

### Step 5: Inference with Test Image ###
Optionally executing the script...
`s5_vai_inferSingleImage.sh` 
which calculates the model output for one provided test image.

### Step 6: Verify Quantized Model Performance ###
Optionally executing the script...
`s6_vai_quant_verify.sh` 
which calculates the quantized model performance on the train and eval dataset.

### Step 7: Compiling the Model ###
Finally executing the script...
`s7_vai_xcompile.sh`
which compiles the model with the VitisAi tool `vai_c_tensorflow2` for the target iam device. The compiled model xmodel file is `WORK_DIR/modelVai/FINAL_FILE_NAME.xmodel`. This file has to be copied to the iam camera.




## Running Application on iam ##
The folder [dpuClassifyAppMt](./../dpuClassifyAppMt) contains the application source files and the make file for compiling the application. The trained model and the model configuration have to be copied to the applications `/model` subfolder on the **iam ML** camera.
 

The application provides GigE output streaming, so the camera can be connected from the host with Synview explorer. 
The application generates an overlay with the classification result.
Several XML features are available for application control, e.g. test images and debug output modes.
Models models can be loaded dynamically during stopped acquisition .


![app](/dpuClassify/misc/images/screenShotApp.png)

The folder /dpuClassifyApp contains a simplified version of the application without multithreading.
