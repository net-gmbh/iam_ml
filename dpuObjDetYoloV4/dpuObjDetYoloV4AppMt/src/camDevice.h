/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */



#ifndef CAMDEVICE_H
#define CAMDEVICE_H

#include "sv.synview.class.h"
#include <string>



class camDeviceClass
{
public:
    camDeviceClass ();
    ~camDeviceClass ();

    LvStatus OpenCamera ( );
    LvStatus InitCamera ( );
    LvStatus CloseCamera ( );
    LvStatus StartStream ( );
    LvStatus StopStream ( );    

    LvStatus OpenFrameBuffers(); 
    LvStatus CloseFrameBuffers(); 

    LvStatus GetInt32CameraParam (LvFeature Feature, int32_t* piValue);
    LvStatus SetInt32CameraParam (LvFeature Feature, int32_t iValue);

    LvStatus GetFloatCameraParam (LvFeature Feature, float* pValue);
    LvStatus SetFloatCameraParam (LvFeature Feature, float Value);

    int setupGigeServer(std::string xmlFileName);

    
    int  StartAcquisition ( );
    int  StopAcquisition ( );
    bool IsAcquiring ( );    
    
    LvEvent*     m_pEvent;
    LvStream*    m_pStream;
    LvEvent*     m_pEventCam;
    
	std::string  sDeviceId;

   
};
#endif //CAMDEVICE_H
