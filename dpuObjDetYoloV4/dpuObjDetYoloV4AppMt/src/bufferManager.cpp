/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#include "bufferManager.h"
#include "main.h"


bufferManagerClass::bufferManagerClass() {
    numElementsBuffer1 = 0;
    numElementsBuffer2 = 0;
    numSourcesBuffer2  = 0;

    enableAnalyser    = true;
    numFramesAnalyser = 20;
    
}

bufferManagerClass::~bufferManagerClass() {}


 // ----------------------------------------------------------------
//                      buffer 1
// ----------------------------------------------------------------

void bufferManagerClass::initBuffer1(int numBuffers) {        
    
    std::lock_guard<std::mutex> guardFor(mtx);

    // --- analyser ---
    fpsBuffer1In      = 0.0;
    analyserBuffer1InFrameCounter = -1;
    fpsBuffer1Out      = 0.0;
    analyserBuffer1OutFrameCounter = -1;
    
   
    hndlVectorBuffer1.clear();
    lockedVectorBuffer1.clear();
    filledVectorBuffer1.clear();    
    fcntVectorBuffer1.clear(); 

    hndlVectorBuffer1.reserve(numBuffers);
    lockedVectorBuffer1.reserve(numBuffers);
    filledVectorBuffer1.reserve(numBuffers);
    fcntVectorBuffer1.reserve(numBuffers);

    numElementsBuffer1 = numBuffers;
    fcntInWork.clear();

    for (int i=0; i<numElementsBuffer1; i++) {
        LvBuffer* pBuffer = m_Buffers[i];
        hndlVectorBuffer1.push_back(pBuffer->GetHandle());
        lockedVectorBuffer1.push_back(false);
        filledVectorBuffer1.push_back(false);  
        fcntVectorBuffer1.push_back(0);       
    }

    latestBufferIdxBuffer1=-1;
    fcnt=1; // not 0, because of prevFcntBuffer2 comparison    
}


int bufferManagerClass::pushBuffer1(uint32_t hndlBuffer, int *idx, long long unsigned int *frameCount) {

    std::lock_guard<std::mutex> guardFor(mtx);
    
    if (numElementsBuffer1<=0) { // initialized?
        return 0;
    }    

    // --- identify buffer by its handel ---
    int bufferIdx = -1;
    for (int i=0; i<numElementsBuffer1; i++) {        
        if (hndlBuffer==hndlVectorBuffer1[i]) {
            bufferIdx=i;
            break;
        }        
    }
    
    if (bufferIdx<0) {
        printf(COLOR_RED "bufferManager: Error: pushBuffer1() unknown buffer \n" COLOR_RESET);
        return 0;
    }

    // --- skip/kill any other unlocked buffers ---
    for (int i=0; i<numElementsBuffer1; i++) {     
        if (i!=bufferIdx) {
            if ((!lockedVectorBuffer1[i])&(filledVectorBuffer1[i])) {
                LvBuffer* pBuffer = m_Buffers[i];
                pBuffer->Queue();
                lockedVectorBuffer1[i] = false;
                filledVectorBuffer1[i] = false;
            }
        }
    }

    filledVectorBuffer1[bufferIdx] = true;
    lockedVectorBuffer1[bufferIdx] = false;
    fcntVectorBuffer1[bufferIdx]   = fcnt;
    latestBufferIdxBuffer1         = bufferIdx;  // memorize index for next pull request
    *frameCount = fcnt;
    *idx = bufferIdx;  

    if (enableAnalyser) {
        if (analyserBuffer1InFrameCounter<0) {
            analyserBuffer1InFrameCounter = 0;
            analyserBuffer1InStartTime = std::chrono::high_resolution_clock::now();
        } else {
            analyserBuffer1InFrameCounter++;
            if (analyserBuffer1InFrameCounter>=numFramesAnalyser) {
                // --- finalize measurement ---
                std::chrono::high_resolution_clock::time_point stopTime;
                stopTime = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> span;
                span = std::chrono::duration_cast<std::chrono::duration<double>>(stopTime - analyserBuffer1InStartTime);
                fpsBuffer1In = double(numFramesAnalyser) / double(span.count());
                printf("Buffer 1 input fps: %f\n", float(fpsBuffer1In));
                // --- start nex measurement ---
                analyserBuffer1InFrameCounter = 0;
                analyserBuffer1InStartTime = std::chrono::high_resolution_clock::now();
            }
        }        
    }

    
    fcnt++;
    return 1;
}


int bufferManagerClass::pullBuffer1(int *idx, long long unsigned int *frameCount) {

    std::lock_guard<std::mutex> guardFor(mtx);

    if (numElementsBuffer1<=0) {  // initialized?
        return 0;
    }

    if (latestBufferIdxBuffer1<0) { // none available yet
        return 0;
    }

    *frameCount=fcntVectorBuffer1[latestBufferIdxBuffer1];
    *idx = latestBufferIdxBuffer1;

    fcntInWork.push_back(fcntVectorBuffer1[latestBufferIdxBuffer1]); // add fcnt to "in work" list

    lockedVectorBuffer1[latestBufferIdxBuffer1] = true;
    latestBufferIdxBuffer1 = -1; // now it is not available any more

    if (enableAnalyser) {
        if (analyserBuffer1OutFrameCounter<0) {
            analyserBuffer1OutFrameCounter = 0;
            analyserBuffer1OutStartTime = std::chrono::high_resolution_clock::now();
        } else {
            analyserBuffer1OutFrameCounter++;
            if (analyserBuffer1OutFrameCounter>=numFramesAnalyser) {
                // --- finalize measurement ---
                std::chrono::high_resolution_clock::time_point stopTime;
                stopTime = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> span;
                span = std::chrono::duration_cast<std::chrono::duration<double>>(stopTime - analyserBuffer1OutStartTime);
                fpsBuffer1Out = double(numFramesAnalyser) / double(span.count());
                printf("Buffer 1 output fps: %f\n", float(fpsBuffer1Out));
                // --- start nex measurement ---
                analyserBuffer1OutFrameCounter = 0;
                analyserBuffer1OutStartTime = std::chrono::high_resolution_clock::now();
            }
        }        
    }
    
    return 1;
}


void bufferManagerClass::pullDoneBuffer1(int idx) {

    std::lock_guard<std::mutex> guardFor(mtx);

    if (numElementsBuffer1<=0) {  // initialized?
        return ;
    }
    
    LvBuffer* pBuffer = m_Buffers[idx];
    pBuffer->Queue(); // give back to synview queue
    lockedVectorBuffer1[idx] = false;
    filledVectorBuffer1[idx] = false;
}






// ----------------------------------------------------------------
//                      buffer2
// ----------------------------------------------------------------



void bufferManagerClass::initBuffer2(int numBuffers, int numSources) {
    
    std::lock_guard<std::mutex> guardFor(mtx);

    numElementsBuffer2 = numBuffers;
    numSourcesBuffer2  = numSources;
    prevFcntBuffer2 = 0;

    // --- analyser ---
    fpsBuffer2In      = 0.0;
    analyserBuffer2InFrameCounter = -1;
    fpsBuffer2Out      = 0.0;
    analyserBuffer2OutFrameCounter = -1;

    lockedVectorBuffer2.clear();
    fcntVectorBuffer2.clear();    
    imagesBuffer2.clear();
    fcntInWork.clear();

    lockedVectorBuffer2.reserve(numBuffers);
    fcntVectorBuffer2.reserve(numBuffers);
    imagesBuffer2.reserve(numBuffers);
    fcntInWork.reserve(numSources);

    for (int i=0; i<numBuffers; i++) {
        lockedVectorBuffer2.push_back(false);
        filledVectorBuffer2.push_back(false);
        fcntVectorBuffer2.push_back(0);
        cv::Mat img;
        imagesBuffer2.push_back(img);
    }    
}




int bufferManagerClass::pushBuffer2(long long unsigned int frameCount, int* idx) {

    std::lock_guard<std::mutex> guardFor(mtx);

    if (numElementsBuffer2<=0) { // not initialized?
        return 0;
    }

    // --- count filled buffers ---
    int numFilledBuffers=0;
    for (int i=0; i<numElementsBuffer2; i++) {
        if ((!lockedVectorBuffer2[i])&(filledVectorBuffer2[i])) {
            numFilledBuffers++;
        }
    }

    // ---  skip/kill old buffers ---
    if (numFilledBuffers>numSourcesBuffer2) {
        int numKills = numFilledBuffers-(numSourcesBuffer2);

        for (int k=0; k<numKills; k++) {
            // search oldest
            int iOldest =-1;
            long long unsigned int fcntOldest = 0;
            for (int i=0; i<numElementsBuffer2; i++) {
                if ((!lockedVectorBuffer2[i])&(filledVectorBuffer2[i])) {                    
                    if ((iOldest<0) || (fcntVectorBuffer2[i]<=fcntOldest)) {
                        iOldest = i;
                        fcntOldest = fcntVectorBuffer2[i];
                    }                    
                }
            }
            if (iOldest<0) {
                printf(COLOR_RED "pushBuffer2: ERROR during buffer skip\n" COLOR_RESET);
                return 0;     
            }
            // skip/kill oldest buffer
            lockedVectorBuffer2[iOldest] = false;
            filledVectorBuffer2[iOldest] = false; 
        }
    }
        
    // --- search emty buffer ---
    int idxFound=-1;
    for (int i=0; i<numElementsBuffer2; i++) {
        if ((!lockedVectorBuffer2[i])&(!filledVectorBuffer2[i])) {
            idxFound=i;
            break;
        }
    } 

    if (idxFound<0) {
        int numFilledBuffers=0;
        for (int i=0; i<numElementsBuffer2; i++) {
            if ((!lockedVectorBuffer2[i])&(filledVectorBuffer2[i])) {
               numFilledBuffers++;
            }
        }
        printf(COLOR_RED "pushBuffer2: Error: pushBuffer2 full, filled= %d of %d (buffer to small?)\n" COLOR_RESET, numFilledBuffers, numElementsBuffer2);
        return 0;
    }

    // --reserve buffer
    lockedVectorBuffer2[idxFound] = true;
    fcntVectorBuffer2[idxFound]   = frameCount;
    filledVectorBuffer2[idxFound] = false;    
    *idx = idxFound;

    if (enableAnalyser) {
        if (analyserBuffer2InFrameCounter<0) {
            analyserBuffer2InFrameCounter = 0;
            analyserBuffer2InStartTime = std::chrono::high_resolution_clock::now();
        } else {
            analyserBuffer2InFrameCounter++;
            if (analyserBuffer2InFrameCounter>=numFramesAnalyser) {
                // --- finalize measurement ---
                std::chrono::high_resolution_clock::time_point stopTime;
                stopTime = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> span;
                span = std::chrono::duration_cast<std::chrono::duration<double>>(stopTime - analyserBuffer2InStartTime);
                fpsBuffer2In = double(numFramesAnalyser) / double(span.count());
                printf("Buffer 2 input fps: %f\n", float(fpsBuffer2In));
                // --- start nex measurement ---
                analyserBuffer2InFrameCounter = 0;
                analyserBuffer2InStartTime = std::chrono::high_resolution_clock::now();
            }
        }        
    }
    
    return 1;    
}


void bufferManagerClass::pushDoneBuffer2(int idx, long long unsigned int frameCount) {
    std::lock_guard<std::mutex> guardFor(mtx);    

    if (numElementsBuffer2<=0) { // not initialized?
        return;
    }

    lockedVectorBuffer2[idx] = false;
    filledVectorBuffer2[idx] = true;  // mark filled

    // --- remove fcnt from "in work" list ---
    int iList=-1;
    if ((int)fcntInWork.size()>0) {
        for (int i=0; i<(int)fcntInWork.size(); i++) {
            if (fcntInWork[i]==frameCount) {
                iList=i;
                break;
            }
        }
        if (iList<0) {
            printf(COLOR_RED "bufferManager: Remove fcnt %llu  from fcntInWork --> ERROR\n" COLOR_RESET , frameCount);
            return;
        }
        fcntInWork.erase (fcntInWork.begin()+iList);         
    }
}



int bufferManagerClass::pullBuffer2(int* idx, long long unsigned int *frameCount) {

    std::lock_guard<std::mutex> guardFor(mtx);    

    if (numElementsBuffer2<=0) { // not initialized?
        return 0;
    }
   
    // --- find fcntInWork min value ---
    long long unsigned int fcntInWorkMin = 0;
    if ((int)fcntInWork.size()>0) {
        fcntInWorkMin = fcntInWork[0];
        for (int i=1; i<(int)fcntInWork.size(); i++) {
            if (fcntInWork[i]<fcntInWorkMin) fcntInWorkMin = fcntInWork[i];
        }
    }

    // --- search buffer with newest fcnt which can not be overtaken by any frame that is still in work ---
    int idxFound=-1;
    long long unsigned int fcntFound=0;
    for (int i=0; i<numElementsBuffer2; i++) {
        if ((!lockedVectorBuffer2[i])&(filledVectorBuffer2[i])) {
            if ( (fcntVectorBuffer2[i]<fcntInWorkMin)||((int)fcntInWork.size()==0) )  { //if it can not be overtaken by other frame, which is still in progress...
                if (idxFound<0) { // init
                    idxFound=i;
                    fcntFound=fcntVectorBuffer2[i];
                } else {
                    if (fcntVectorBuffer2[i]>fcntFound) {  // search newest fcnt
                        idxFound=i;
                        fcntFound=fcntVectorBuffer2[i];
                    }
                }                
            }         
        }
    } 

    if (idxFound<0) {    // nothing available        
        return 0;
    }  

    // --- skip/kill older buffers ---
    for (int i=0; i<numElementsBuffer2; i++) {
        if ((!lockedVectorBuffer2[i])&(filledVectorBuffer2[i])) {
            if (fcntVectorBuffer2[i]<fcntFound) {
                lockedVectorBuffer2[i] = false;
                filledVectorBuffer2[i] = false; // kill buffer
            }
        }
    }

    // --- safety function: kill/skip buffer if a newer one is already out ---
    if (fcntVectorBuffer2[idxFound]<prevFcntBuffer2) {        
        printf(COLOR_RED "sendBufferManager: Warning: Skip: %llu because %llu is already out\n" COLOR_RESET, fcntVectorBuffer2[idxFound] , prevFcntBuffer2);
        lockedVectorBuffer2[idxFound] = false;
        filledVectorBuffer2[idxFound] = false; // kill buffer
        mtx.unlock();
        return 0;
    }


    /*if (0) {   
        printf("sending %llu, Available=[", fcntVectorBuffer2[idxFound]);
        for (int i=0; i<numElementsBuffer2; i++) {
            if ((!lockedVectorBuffer2[i])&(filledVectorBuffer2[i])) {
                printf(" %llu",fcntVectorBuffer2[i]);
            }            
        }
        printf("]  fcntInWorkMin=%llu\n", fcntInWorkMin);
    }*/



    // --- output ---
    *idx = idxFound;
    *frameCount = fcntVectorBuffer2[idxFound];
    lockedVectorBuffer2[idxFound] = true;
    filledVectorBuffer2[idxFound] = false;
    prevFcntBuffer2 = fcntVectorBuffer2[idxFound];

    if (enableAnalyser) {
        if (analyserBuffer2OutFrameCounter<0) {
            analyserBuffer2OutFrameCounter = 0;
            analyserBuffer2OutStartTime = std::chrono::high_resolution_clock::now();
        } else {
            analyserBuffer2OutFrameCounter++;
            if (analyserBuffer2OutFrameCounter>=numFramesAnalyser) {
                // --- finalize measurement ---
                std::chrono::high_resolution_clock::time_point stopTime;
                stopTime = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> span;
                span = std::chrono::duration_cast<std::chrono::duration<double>>(stopTime - analyserBuffer2OutStartTime);
                fpsBuffer2Out = double(numFramesAnalyser) / double(span.count());
                printf("Buffer 2 output fps: %f\n\n", float(fpsBuffer2Out));
                // --- start nex measurement ---
                analyserBuffer2OutFrameCounter = 0;
                analyserBuffer2OutStartTime = std::chrono::high_resolution_clock::now();
            }
        }        
    }

    return 1;
}



void bufferManagerClass::pullDoneBuffer2(int idx) {
    std::lock_guard<std::mutex> guardFor(mtx);

    if (numElementsBuffer2<=0) { // not initialized?
        return;
    }

    lockedVectorBuffer2[idx] = false;
    filledVectorBuffer2[idx] = false;
}


// ----------------------------------------------------------------
//                      debug
// ----------------------------------------------------------------

void bufferManagerClass::print_fcntInWork() {
    printf("fcntInWork=[ ");
    for (int i=0; i<(int)fcntInWork.size(); i++) {
        printf("%llu ",fcntInWork[i]);
    }
    printf(" ]\n");
}