#!/bin/bash

# -------------------------------------------------
#            start application on iam
# -------------------------------------------------


# --- read iam target settings ---
source iam_settings.mk


# --- prepare ssh access ---
if [[ $strict_host_key_checking -eq 0 ]] 
then
	echo no
	ssh-copy-id -o "StrictHostKeyChecking no" -i $key $user@$ip
else
	echo yes
	ssh-copy-id -i $key $user@$ip
fi


# --- stop all applications on iam ---
ssh -i $key $user@$ip "touch /opt/synview/bin/sv.iAMGigEServer.stop"
sleep 2s


# --- start app ---
ssh -i $key $user@$ip "cd $prog_folder;./$elf_name"
