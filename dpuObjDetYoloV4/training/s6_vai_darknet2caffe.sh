#!/bin/bash

source config.sh
echo "Config: $PROJECT_CONFIG"	
echo "Work dir: $WORK_DIR"	


sudo chmod 777 $WORK_DIR/models
sudo chmod 777 $WORK_DIR/vaiModels
	
python /opt/vitis_ai/conda/envs/vitis-ai-caffe/bin/convert.py \
	$WORK_DIR/modelCfg/yolov4_dpu.cfg \
	$WORK_DIR/outputTraining/yolov4_dpu_last.weights \
	$WORK_DIR/vaiModels/yolov4_dpu.prototxt \
	$WORK_DIR/vaiModels/yolov4_dpu.caffemodel

